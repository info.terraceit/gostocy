


exports.qrCodeFile = function(size, name, content){
	var result;
	if(cc.sys.os == cc.sys.OS_ANDROID)
	{
	    result = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/SdkCrypto", "qrCodeFile", 
	    	"(FLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;", 
	    	size, name, content);
	}
	else if (cc.sys.os == cc.sys.OS_IOS)
	{
		result = jsb.reflection.callStaticMethod("SdkCrypto", "qrCodeFile:name:content:", size, name, content);
	}

	return result;
};

exports.qrCodePNG = function(size, content){
	var result;
	if(cc.sys.os == cc.sys.OS_ANDROID)
	{
	    result = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/SdkCrypto", "qrCodePNG", 
	    	"(FLjava/lang/String;)Ljava/lang/String;", 
	    	size, content);
	}
	else if (cc.sys.os == cc.sys.OS_IOS)
	{
		result = jsb.reflection.callStaticMethod("SdkCrypto", "qrCodePNG:content:", size, content);
	}

	return result;
};

exports.createSDK = function()
{
	if(cc.sys.os == cc.sys.OS_ANDROID)
	{
	    jsb.reflection.callStaticMethod("org/cocos2dx/javascript/SdkCrypto", "createSDK","()V");
	}
	else if (cc.sys.os == cc.sys.OS_IOS)
	{
		jsb.reflection.callStaticMethod("SdkCrypto", "createSDK");
	}
};

exports.getMnemoric = function()
{
	var result;

	if(cc.sys.os == cc.sys.OS_ANDROID)
	{
	    result = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/SdkCrypto", "getMnemoric",
	     "()Ljava/lang/String;");
	}
	else if (cc.sys.os == cc.sys.OS_IOS)
	{
		result = jsb.reflection.callStaticMethod("SdkCrypto", "getMnemoric");
	}

	return result;
};

exports.getSeed = function()
{
	var result;

	if(cc.sys.os == cc.sys.OS_ANDROID)
	{
	    result = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/SdkCrypto", "getSeed",
	     "()Ljava/lang/String;");
	}
	else if (cc.sys.os == cc.sys.OS_IOS)
	{
		result = jsb.reflection.callStaticMethod("SdkCrypto", "getSeed");
	}

	return result;
};

exports.addWallet = function(network, walletName)
{
	var result;
	if(cc.sys.os == cc.sys.OS_ANDROID)
	{
	    result = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/SdkCrypto", "addWallet", 
	    	"(Ljava/lang/String;Ljava/lang/String;)Z", 
	    	network, walletName);
	}
	else if (cc.sys.os == cc.sys.OS_IOS)
	{
		result = jsb.reflection.callStaticMethod("SdkCrypto", "addWallet:walletName:", network, walletName);
	}

	return result;
};

exports.backup = function()
{
	var result;
	if(cc.sys.os == cc.sys.OS_ANDROID)
	{
	    result = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/SdkCrypto", "backup", 
	    	"()V");
	}
	else if (cc.sys.os == cc.sys.OS_IOS)
	{
		result = jsb.reflection.callStaticMethod("SdkCrypto", "backup");
	}

	return result;
}

exports.restore = function(mnemoric)
{
	if(cc.sys.os == cc.sys.OS_ANDROID)
	{
	    result = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/SdkCrypto", "restore", 
	    	"(Ljava/lang/String;)V", mnemoric);
	}
	else if (cc.sys.os == cc.sys.OS_IOS)
	{
		result = jsb.reflection.callStaticMethod("SdkCrypto", "restore:", mnemoric);
	}
}

exports.encrypted = function()
{
	var result;
	if(cc.sys.os == cc.sys.OS_ANDROID)
	{
	    result = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/SdkCrypto", "encrypted", 
	    	"()Ljava/lang/String;");
	}
	else if (cc.sys.os == cc.sys.OS_IOS)
	{
		result = jsb.reflection.callStaticMethod("SdkCrypto", "encrypted");
	}

	return result;
}

exports.decrypted = function(seedHex, encrypted)
{
	var result;
	if(cc.sys.os == cc.sys.OS_ANDROID)
	{
	    result = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/SdkCrypto", "decrypted", 
	    	"(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;", seedHex, encrypted);
	}
	else if (cc.sys.os == cc.sys.OS_IOS)
	{
		result = jsb.reflection.callStaticMethod("SdkCrypto", "decrypted:encrypted:", seedHex, encrypted);
	}

	return result;
}

exports.saveSDK = function()
{
	var result;
	if(cc.sys.os == cc.sys.OS_ANDROID)
	{
	    result = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/SdkCrypto", "saveSDK", 
	    	"()Ljava/lang/String;");
	}
	else if (cc.sys.os == cc.sys.OS_IOS)
	{
		result = jsb.reflection.callStaticMethod("SdkCrypto", "saveSDK");
	}

	return result;
}

exports.loadSDK = function(str)
{
	var result;
	if(cc.sys.os == cc.sys.OS_ANDROID)
	{
	    result = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/SdkCrypto", "loadSDK", 
	    	"(Ljava/lang/String;)Z", str);
	}
	else if (cc.sys.os == cc.sys.OS_IOS)
	{
		result = jsb.reflection.callStaticMethod("SdkCrypto", "loadSDK:", str);
	}

	return result;	
}

exports.removeAll = function()
{
	var result;
	if(cc.sys.os == cc.sys.OS_ANDROID)
	{
	    result = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/SdkCrypto", "removeAll", 
	    	"()Z");
	}
	else if (cc.sys.os == cc.sys.OS_IOS)
	{
		result = jsb.reflection.callStaticMethod("SdkCrypto", "removeAll");
	}

	return result;	
}

exports.removeWallet = function(walletName)
{
	var result;
	if(cc.sys.os == cc.sys.OS_ANDROID)
	{
	    result = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/SdkCrypto", "removeWallet", 
	    	"(Ljava/lang/String;)Z", walletName);
	}
	else if (cc.sys.os == cc.sys.OS_IOS)
	{
		result = jsb.reflection.callStaticMethod("SdkCrypto", "removeWallet:", walletName);
	}

	return result;	
}

//---------------------------------------------------------------------//

exports.addCoin = function(walletName, coinType)
{
	var result;
	if(cc.sys.os == cc.sys.OS_ANDROID)
	{
	    result = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/SdkCrypto", "addCoin", 
	    	"(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;",
	    	 walletName, coinType);
	}
	else if (cc.sys.os == cc.sys.OS_IOS)
	{
		result = jsb.reflection.callStaticMethod("SdkCrypto", "addCoin:coinType:", walletName, coinType);
	}

	return result;
};

exports.connect_BTC = function (walletName, url, username, pass)
{
	var result;
	if(cc.sys.os == cc.sys.OS_ANDROID)
	{
	   result = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/SdkCrypto", "connect_BTC", 
	    	"(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z", 
	    	walletName, url, username, pass);
	}
	else if (cc.sys.os == cc.sys.OS_IOS)
	{
		result = jsb.reflection.callStaticMethod("SdkCrypto", "connect_BTC:url:username:pass:", walletName, url, username, pass);
	}

	return result;
};

exports.connect_LTC = function (walletName, url, username, pass)
{
	var result;
	if(cc.sys.os == cc.sys.OS_ANDROID)
	{
	    result = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/SdkCrypto", "connect_LTC", 
	    	"(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z", 
	    	walletName, url, username, pass);
	}
	else if (cc.sys.os == cc.sys.OS_IOS)
	{
		result = jsb.reflection.callStaticMethod("SdkCrypto", "connect_LTC:url:username:pass:", walletName, url, username, pass);
	}

	return result;
};

exports.connect_BCH = function (walletName, url, username, pass)
{
	var result;
	if(cc.sys.os == cc.sys.OS_ANDROID)
	{
	    result = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/SdkCrypto", "connect_BCH", 
	    	"(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z", 
	    	walletName, url, username, pass);
	}
	else if (cc.sys.os == cc.sys.OS_IOS)
	{
		result = jsb.reflection.callStaticMethod("SdkCrypto", "connect_BCH:url:username:pass:", walletName, url, username, pass);
	}

	return result;
};

exports.connect_BSV = function (walletName, url, username, pass)
{
	var result;
	if(cc.sys.os == cc.sys.OS_ANDROID)
	{
	    result = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/SdkCrypto", "connect_BSV", 
	    	"(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z", 
	    	walletName, url, username, pass);
	}
	else if (cc.sys.os == cc.sys.OS_IOS)
	{
		result = jsb.reflection.callStaticMethod("SdkCrypto", "connect_BSV:url:username:pass:", walletName, url, username, pass);
	}

	return result;
};

exports.connect_ETH = function (walletName, url)
{
	var result;
	if(cc.sys.os == cc.sys.OS_ANDROID)
	{
	    result = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/SdkCrypto", "connect_ETH",
	     "(Ljava/lang/String;Ljava/lang/String;)Z", walletName, url);
	}
	else if (cc.sys.os == cc.sys.OS_IOS)
	{
		result = jsb.reflection.callStaticMethod("SdkCrypto", "connect_ETH:url:", walletName, url);
	}

	return result;
};

exports.getBlockCount = function(walletName, coinType)
{
	var result;
	if(cc.sys.os == cc.sys.OS_ANDROID)
	{
	    result = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/SdkCrypto", "getBlockCount",
	     "(Ljava/lang/String;Ljava/lang/String;)F", walletName, coinType);
	}
	else if (cc.sys.os == cc.sys.OS_IOS)
	{
		result = jsb.reflection.callStaticMethod("SdkCrypto", "getBlockCount:coinType:", walletName, coinType);
	}

	return result;
}

exports.syncBlock = function(walletName, coinType, fromHeight, toHeight)
{
	if(cc.sys.os == cc.sys.OS_ANDROID)
	{
	    jsb.reflection.callStaticMethod("org/cocos2dx/javascript/SdkCrypto", "syncBlock",
	     "(Ljava/lang/String;Ljava/lang/String;FF)V", 
	     walletName, coinType, fromHeight, toHeight);
	}
	else if (cc.sys.os == cc.sys.OS_IOS)
	{
		jsb.reflection.callStaticMethod("SdkCrypto", "syncBlock:coinType:fromHeight:toHeight:", walletName, coinType, fromHeight, toHeight);
	}
}

exports.getPrivateKey = function(walletName, coinType)
{
	var result;
	if(cc.sys.os == cc.sys.OS_ANDROID)
	{
	    result = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/SdkCrypto", "getPrivateKey",
	     "(Ljava/lang/String;Ljava/lang/String)Ljava/lang/String;", 
	     walletName, coinType);
	}
	else if (cc.sys.os == cc.sys.OS_IOS)
	{
		result = jsb.reflection.callStaticMethod("SdkCrypto", "getPrivateKey:coinType:", walletName, coinType);
	}

	return result;	
}

exports.getPublicAddress = function(walletName, coinType)
{
	var result;
	if(cc.sys.os == cc.sys.OS_ANDROID)
	{
	    result = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/SdkCrypto", "getPublicAddress",
	     "(Ljava/lang/String;Ljava/lang/String)Ljava/lang/String;", 
	     walletName, coinType);
	}
	else if (cc.sys.os == cc.sys.OS_IOS)
	{
		result = jsb.reflection.callStaticMethod("SdkCrypto", "getPublicAddress:coinType:", walletName, coinType);
	}

	return result;	
}

exports.listTransaction = function(walletName, coinType)
{
	var result;
	if(cc.sys.os == cc.sys.OS_ANDROID)
	{
	    result = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/SdkCrypto", "listTransaction",
	     "(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;", 
	     walletName, coinType);
	}
	else if (cc.sys.os == cc.sys.OS_IOS)
	{
		result = jsb.reflection.callStaticMethod("SdkCrypto", "listTransaction:coinType:", walletName, coinType);
	}

	return result;	
}

exports.getBalance = function(walletName, coinType)
{
	var result;
	if(cc.sys.os == cc.sys.OS_ANDROID)
	{
	    result = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/SdkCrypto", "getBalance",
	     "(Ljava/lang/String;Ljava/lang/String;)F",
	      walletName, coinType);
	}
	else if (cc.sys.os == cc.sys.OS_IOS)
	{
		result = jsb.reflection.callStaticMethod("SdkCrypto", "getBalance:coinType:", walletName, coinType);
	}

	return result;
};

exports.getFaucet = function(walletName, coinType, addr)
{
	var result;
	if(cc.sys.os == cc.sys.OS_ANDROID)
	{
	    result = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/SdkCrypto", "getFaucet",
	     "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;", 
	     walletName, coinType, addr);
	}
	else if (cc.sys.os == cc.sys.OS_IOS)
	{
		result = jsb.reflection.callStaticMethod("SdkCrypto", "getFaucet:coinType:addr:", walletName, coinType, addr);
	}

	return result;
};

exports.send_BTC_Simple = function(walletName, from, to, relayFeePerKb, amount)
{
	var result;
	if(cc.sys.os == cc.sys.OS_ANDROID)
	{
	    result = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/SdkCrypto", "send_BTC_Simple", 
	    	"(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;FF)Ljava/lang/String;", 
	    	walletName, from, to, relayFeePerKb, amount);
	}
	else if (cc.sys.os == cc.sys.OS_IOS)
	{
		result = jsb.reflection.callStaticMethod("SdkCrypto", "send_BTC_Simple:from:to:relayFeePerKb:amount:", walletName, from, to, relayFeePerKb, amount);
	}

	return result;
};

exports.send_LTC_Simple = function(walletName, from, to, relayFeePerKb, amount)
{
	var result;
	if(cc.sys.os == cc.sys.OS_ANDROID)
	{
	    result = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/SdkCrypto", "send_LTC_Simple", 
	    	"(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;FF)Ljava/lang/String;", 
	    	walletName, from, to, relayFeePerKb, amount);
	}
	else if (cc.sys.os == cc.sys.OS_IOS)
	{
		result = jsb.reflection.callStaticMethod("SdkCrypto", "send_LTC_Simple:from:to:relayFeePerKb:amount:", walletName, from, to, relayFeePerKb, amount);
	}

	return result;
};

exports.send_BCH_Simple = function(walletName, from, to, relayFeePerKb, amount)
{
	var result;
	if(cc.sys.os == cc.sys.OS_ANDROID)
	{
	    result = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/SdkCrypto", "send_BCH_Simple", 
	    	"(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;FF)Ljava/lang/String;", 
	    	walletName, from, to, relayFeePerKb, amount);
	}
	else if (cc.sys.os == cc.sys.OS_IOS)
	{
		result = jsb.reflection.callStaticMethod("SdkCrypto", "send_BCH_Simple:from:to:relayFeePerKb:amount:", walletName, from, to, relayFeePerKb, amount);
	}

	return result;
};

exports.send_BSV_Simple = function(walletName, from, to, relayFeePerKb, amount)
{
	var result;
	if(cc.sys.os == cc.sys.OS_ANDROID)
	{
	    result = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/SdkCrypto", "send_BSV_Simple", 
	    	"(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;FF)Ljava/lang/String;", 
	    	walletName, from, to, relayFeePerKb, amount);
	}
	else if (cc.sys.os == cc.sys.OS_IOS)
	{
		result = jsb.reflection.callStaticMethod("SdkCrypto", "send_BSV_Simple:from:to:relayFeePerKb:amount:", walletName, from, to, relayFeePerKb, amount);
	}

	return result;
};

exports.send_ETH_Simple = function(walletName, prvKey, to, amount, data)
{
	var result;
	if(cc.sys.os == cc.sys.OS_ANDROID)
	{
	    result = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/SdkCrypto", "send_ETH_Simple", 
	    	"(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;FLjava/lang/String;)Ljava/lang/String;", 
	    	walletName, prvKey, to, amount, data);
	}
	else if (cc.sys.os == cc.sys.OS_IOS)
	{
		result = jsb.reflection.callStaticMethod("SdkCrypto", "send_ETH_Simple:prvKey:to:amount:data:", walletName, prvKey, to, amount, data);
	}

	return result;
};

exports.send_XLM_Simple = function(walletName, prvKey, to, amount)
{
	var result;
	if(cc.sys.os == cc.sys.OS_ANDROID)
	{
	    result = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/SdkCrypto", "send_XLM_Simple",
	     "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;F)Ljava/lang/String;",
	      walletName, prvKey, to, amount);
	}
	else if (cc.sys.os == cc.sys.OS_IOS)
	{
		result = jsb.reflection.callStaticMethod("SdkCrypto", "send_XLM_Simple:prvKey:to:amount:", walletName, prvKey, to, amount);
	}

	return result;
};

exports.send_EOS_Simple = function(walletName, prvKey, from, to, amount)
{
	var result;
	if(cc.sys.os == cc.sys.OS_ANDROID)
	{
	    result = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/SdkCrypto", "send_EOS_Simple",
	     "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;",
	      walletName, prvKey, from, to, amount);
	}
	else if (cc.sys.os == cc.sys.OS_IOS)
	{
		result = jsb.reflection.callStaticMethod("SdkCrypto", "send_EOS_Simple:prvKey:from:to:amount:", walletName, prvKey, from, to, amount);
	}

	return result;
};























