package org.cocos2dx.javascript;


import android.graphics.Bitmap;
import java.nio.ByteBuffer;

import org.cocos2dx.lib.Cocos2dxActivity;
import org.cocos2dx.lib.Cocos2dxHandler;
import org.cocos2dx.lib.Cocos2dxLocalStorage;
import org.cocos2dx.lib.Cocos2dxHelper;

import sdk.Sdk;
import sdk.Wallet;

public class SdkCrypto {

    public static Cocos2dxActivity app;

    public static void init(Cocos2dxActivity context){
        app = context;
    }

    public static String qrCodeFile(float size, String name, String content) {

        String path = Cocos2dxHelper.getCocos2dxWritablePath();// .getWritablePath();

        return Sdk.qrCodeFile((long)size, name, content, path);
    }

    public static String qrCodePNG(float size, String content) throws Exception {

        byte[] png = Sdk.qrCodePNG((long)size, content);

        String s = new String(png);

        return s;
    }

    public static void createSDK(){

        try {
            Sdk.createSDK();
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("......JAVA...... mnemoric : "+ Sdk.getMnemoric());
        System.out.println("......JAVA...... seed :"+ Sdk.seedToHex());
    }

    public static String getMnemoric(){
        return Sdk.getMnemoric();
    }

    public static String getSeed(){
        return Sdk.seedToHex();
    }

    public static boolean addWallet(String network, String walletName){

        try {

            Wallet w = Sdk.addWallet(network, walletName);
            if (w == null){
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    public static String backUp()
    {
        return Sdk.backUp();
    }

    public static void restore(String mnemoric) {
        try {
             Sdk.restore(mnemoric);
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("......JAVA...... mnemoric : "+ Sdk.getMnemoric());
        System.out.println("......JAVA...... seed :"+ Sdk.seedToHex());
    }

    public static String decrypted(String seedHex, String encrypted){
        return Sdk.decrypted(seedHex, encrypted);
    }

    public static String encrypted(){
        return Sdk.encrypted();
    }

    public static String saveSDK(){

        String key = "SdkCrypto";
        String value = Sdk.saveSDK();
        Cocos2dxLocalStorage.setItem(key, value);
        return value;
    }

    public static boolean loadSDK(String str){
        String key = "SdkCrypto";
        String value = Cocos2dxLocalStorage.getItem(key);
        boolean ok = Sdk.loadSDK(value);
        return ok;
    }

    public static boolean removeAll(){
        return Sdk.removeAll();
    }

    public static boolean removeWallet(String name){
        return Sdk.removeWallet(name);
    }

    //---------------------------------------------------------------------//

    public static String addCoin(String walletName, String coinType){

        String addr = "";

        try {

            Wallet w = Sdk.getWallet(walletName);
            if (w == null){
                return "";
            }

            addr = w.addCoin(coinType);


            System.out.println("............JAVA Address :"+ addr);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return addr;
    }

    public static boolean connect_BTC(String walletName, String url, String username, String pass){

        try {

            Wallet w = Sdk.getWallet(walletName);
            if (w == null){
                return false;
            }

            return  w.connect_BTC(url, username, pass);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;

    }

    public static boolean connect_LTC(String walletName, String url, String username, String pass){

        try {

            Wallet w = Sdk.getWallet(walletName);
            if (w == null){
                return false;
            }

            return  w.connect_LTC(url, username, pass);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;

    }

    public static boolean connect_BCH(String walletName, String url, String username, String pass){

        try {

            Wallet w = Sdk.getWallet(walletName);
            if (w == null){
                return false;
            }

            return  w.connect_BCH(url, username, pass);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;

    }

    public static boolean connect_BSV(String walletName, String url, String username, String pass){

        try {

            Wallet w = Sdk.getWallet(walletName);
            if (w == null){
                return false;
            }

            return  w.connect_BSV(url, username, pass);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;

    }

    public static boolean connect_ETH(String walletName, String url){

        try {

            Wallet w = Sdk.getWallet(walletName);
            if (w == null){
                return false;
            }

            return w.connect_ETH(url);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;

    }

    public static float getBlockCount(String walletName, String coinType){

        try {

            Wallet w = Sdk.getWallet(walletName);
            if (w == null){
                return 0;
            }

            return w.getBlockCount(coinType);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    public static void syncBlock(String walletName, String coinType, float fromHeight, float toHeight) {
        try {

            Wallet w = Sdk.getWallet(walletName);
            if (w == null){
                return;
            }

            w.syncBlock(coinType, (long)fromHeight, (long)toHeight);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getPrivateKey(String walletName, String coinType){

        try {

            Wallet w = Sdk.getWallet(walletName);
            if (w == null){
                return "";
            }

            return w.getPrivateKey(coinType);

        } catch (Exception e) {
            e.printStackTrace();
        }


        return "";
    }

    public static String getPublicAddress(String walletName, String coinType){

        try {

            Wallet w = Sdk.getWallet(walletName);
            if (w == null){
                return "";
            }

            return w.getPublicAddress(coinType);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    public static String listTransaction(String walletName, String coinType){

        try {

            Wallet w = Sdk.getWallet(walletName);
            if (w == null){
                return "";
            }

            return w.listTransaction(coinType);

        } catch (Exception e) {
            e.printStackTrace();
        }


        return "";
    }

    public static float getBalance(String walletName, String coinType){

        try {

            Wallet w = Sdk.getWallet(walletName);
            if (w == null){
                return 0;
            }

            double b = w.getBalance(coinType);
            float balance = (float)b;

            System.out.println("............JAVA getBalance :"+coinType+"  :  "+ balance);

            return balance;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    public static String getFaucet(String walletName, String coinType, String addr){

        String tx = "";

        try {

            Wallet w = Sdk.getWallet(walletName);
            if (w == null){
                return "";
            }

            tx = w.faucet(coinType, addr);

            System.out.println("............JAVA getFaucet :"+ tx);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return tx;
    }

    public static String send_BTC_Simple(String walletName, String from, String to, float relayFeePerKb, float amount){

        try {

            Wallet w = Sdk.getWallet(walletName);
            if (w == null){
                return "";
            }


           String tx = w.send_BTC_Simple(from, to, (double) relayFeePerKb, (double) amount);

            System.out.println("............JAVA sendCoin_BTC :"+ tx);

            return tx;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    public static String send_LTC_Simple(String walletName, String from, String to, float relayFeePerKb, float amount){

        try {

            Wallet w = Sdk.getWallet(walletName);
            if (w == null){
                return "";
            }


            String tx = w.send_LTC_Simple(from, to, (double) relayFeePerKb, (double) amount);

            System.out.println("............JAVA sendCoin_LTC :"+ tx);

            return tx;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    public static String send_BCH_Simple(String walletName, String from, String to, float relayFeePerKb, float amount){

        try {

            Wallet w = Sdk.getWallet(walletName);
            if (w == null){
                return "";
            }


            String tx = w.send_BCH_Simple(from, to, (double) relayFeePerKb, (double) amount);

            System.out.println("............JAVA sendCoin_BCH :"+ tx);

            return tx;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    public static String send_BSV_Simple(String walletName, String from, String to, float relayFeePerKb, float amount){

        try {

            Wallet w = Sdk.getWallet(walletName);
            if (w == null){
                return "";
            }


            String tx = w.send_BSV_Simple(from, to, (double) relayFeePerKb, (double) amount);

            System.out.println("............JAVA sendCoin_BSV :"+ tx);

            return tx;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    public static String send_ETH_Simple(String walletName, String prvKey, String to, float amETH, String data){

        try {

            Wallet w = Sdk.getWallet(walletName);
            if (w == null){
                return "";
            }


            String tx = w.send_ETH_Simple(prvKey, to, (double) amETH, data);

            System.out.println("............JAVA sendCoin_ETH :"+ tx);

            return tx;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    public static String send_XLM_Simple(String walletName, String prvKey, String to, float lumens){

        try {

            Wallet w = Sdk.getWallet(walletName);
            if (w == null){
                return "";
            }

            String tx = w.send_XLM_Simple(prvKey, to, (double) lumens);

            System.out.println("............ sendCoin_XLM :"+ tx);

            return tx;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }

    public static String send_EOS_Simple(String walletName, String prvKey, String from, String to, String amount){
        try {

            Wallet w = Sdk.getWallet(walletName);
            if (w == null){
                return "";
            }

            String tx = w.send_EOS_Simple(prvKey, from, to, amount);

            System.out.println("............ sendCoin_XLM :"+ tx);

            return tx;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }
}

