 var JSB = require("JSB");
 var Config = require("Config");


exports.NET = "simnet";
exports.WALLET_NAME = "w";
exports.walletManager = "";
exports.mapCLI = {};
exports.COIN_ID = "";


exports.qrCodeFile = function(size, name, content){
  return JSB.qrCodeFile(size, name, content);
}

exports.qrCodePNG = function(size, content){
    return JSB.qrCodePNG(size, content);
}

exports.saveSDK = function(){
  this.walletManager = JSB.saveSDK();
  cc.sys.localStorage.setItem("SdkCrypto", this.walletManager); 
  console.log("................SdkCrypto saveSDK : ", this.walletManager);
};

exports.loadSDK = function(){
  var ok = false;
  var walletManager = cc.sys.localStorage.getItem("SdkCrypto");
  if(walletManager !== "")
  {
      this.walletManager = walletManager;
      ok = JSB.loadSDK(this.walletManager);
      if(ok)
      {
        console.log("................SdkCrypto loadSDK : ", this.walletManager);        
      }
  }  

  return ok;
};

exports.createSDK = function(net, walletName){

		//var JSB = require("JSB");
        JSB.createSDK();

        var mnemoric =  JSB.getMnemoric();
        var seed = JSB.getSeed();
        console.log(".........SdkCrypto mnemoric........"+mnemoric);
        console.log(".........SdkCrypto seed ........"+seed);

        var ok = JSB.addWallet(net, walletName);
        if (ok)
        {
            var addr_BTC = JSB.addCoin(walletName, "BTC");
            var addr_BCH = JSB.addCoin(walletName, "BCH");
            var addr_BSV = JSB.addCoin(walletName, "BSV");
            var addr_LTC = JSB.addCoin(walletName, "LTC");
            var addr_ETH = JSB.addCoin(walletName, "ETH");
            var addr_XLM = JSB.addCoin(walletName, "XLM");
            var addr_EOS = JSB.addCoin(walletName, "EOS");

            console.log("............SdkCrypto BTC : "+addr_BTC);
            console.log("............SdkCrypto BCH : "+addr_BCH);
            console.log("............SdkCrypto BSV : "+addr_BSV);
            console.log("............SdkCrypto LTC : "+addr_LTC);
            console.log("............SdkCrypto ETH : "+addr_ETH);
            console.log("............SdkCrypto XLM : "+addr_XLM);
            console.log("............SdkCrypto EOS : "+addr_EOS);
            
            this.NET = net;
            this.WALLET_NAME = walletName;
            console.log("............SdkCrypto Wallet : "+this.WALLET_NAME);

            //this.saveSDK();           
        }

        return ok;
};

exports.connect_cli = function(coinType){

    if(coinType === "XLM")
    {
       this.mapCLI[coinType] = true;
      return true;
    }

    var ok = false;

    var net = this.NET;
    var walletName = this.WALLET_NAME;

    var ip = Config.config_cli[net][coinType].ip;
    var port = Config.config_cli[net][coinType].port;
    var user = Config.config_cli[net][coinType].user;
    var pass = Config.config_cli[net][coinType].pass;

    switch(coinType)
    {
      case "BTC":
          ok = JSB.connect_BTC(walletName, ip+":"+port, user, pass);     
      break;

      case "LTC":
        ok = JSB.connect_LTC(walletName, ip+":"+port, user, pass);
      break;

      case "BCH":
        ok = JSB.connect_BCH(walletName, ip+":"+port, user, pass);
      break;

      case "BSV":
        ok = JSB.connect_BSV(walletName, ip+":"+port, user, pass);
      break;     

      case "ETH":
        ok = JSB.connect_ETH(walletName, "http://"+ip+":"+port);
      break;
    }

    this.mapCLI[coinType] = ok;

    console.log("..................SdkCrypto connect : "+ip+":"+port);
    console.log("............SdkCrypto CLI "+coinType+" : "+ok);

    return ok;
};

exports.getPublicAddress = function(coinType){
  var addr = JSB.getPublicAddress(this.WALLET_NAME, coinType);
  console.log(".................SdkCrypto address : "+coinType+" : "+ addr);
  return addr;
};

exports.getPrivateKey = function(coinType){
  var prvKey = JSB.getPrivateKey(this.WALLET_NAME, coinType);
  console.log(".................SdkCrypto prvKey : "+ prvKey);
  return prvKey;
};

exports.getFaucet = function(coinType){

  if(this.NET != "simnet")
  {
    return;
  }

  var addr = JSB.getPublicAddress(this.WALLET_NAME, coinType);

  JSB.getFaucet(this.WALLET_NAME, coinType, addr);
};

exports.getBalance = function(coinType){

  var addr = JSB.getPublicAddress(this.WALLET_NAME, coinType);
  var balance = ""+JSB.getBalance(this.WALLET_NAME, coinType);

  var sub = function(str) {
    //var str = "1000.12";
    var spl = str.split(".");
    if(spl.length < 2)
    {
      return str;
    }    
    var res1 = spl[0];
    var res2 = spl[1].substring(0, 6);
    var res = res1 +"."+ res2;
    
    return res;
  };

  balance = sub(balance);

  console.log(".......SdkCrypto balance : "+coinType+" : "+addr+ " : "+ balance);

  return balance;
};

exports.getBlockCount = function(coinType){

  var blockcount = JSB.getBlockCount(this.WALLET_NAME, coinType)

  return ""+blockcount;
};

exports.listTranscation = function(coinType){

  var addr = JSB.getPublicAddress(this.WALLET_NAME, coinType);

  var list = JSB.listTransaction(this.WALLET_NAME, coinType);

  //console.log(".......SdkCrypto listTransaction : "+coinType+" : "+ addr+ ":"+ list);

  return list;
};

exports.sendTxSimple = function(coinType, to, amount){

    var walletName = this.WALLET_NAME;
    var from = this.getPublicAddress(coinType);

    amount = parseFloat(amount);
    //relayFeePerKb = parseFloat(relayFeePerKb);
    var relayFeePerKb = 0.0;

    var tx = "";
    switch(coinType)
    {
        case "BTC":
            relayFeePerKb = 0.00001;
            tx = JSB.send_BTC_Simple(walletName, from, to, relayFeePerKb, amount);
            break;

        case "LTC":
            relayFeePerKb = 0.0001;
            tx = JSB.send_LTC_Simple(walletName, from, to, relayFeePerKb, amount);
            break;

        case "BCH":
            relayFeePerKb = 0.00001;
            tx = JSB.send_BCH_Simple(walletName, from, to, relayFeePerKb, amount);
            break;

        case "BSV":
            relayFeePerKb = 0.00001;
            tx = JSB.send_BSV_Simple(walletName, from, to, relayFeePerKb, amount);
        break;    

        case "ETH":
            var prvKey = JSB.getPrivateKey(walletName, coinType);
            tx = JSB.send_ETH_Simple(walletName, prvKey, to, amount, "");
            break;

        case "XLM":
            var prvKey = JSB.getPrivateKey(walletName, coinType);
            tx = JSB.send_XLM_Simple(walletName, prvKey, to, amount);
            break;    

         case "EOS":
            var from = JSB.getPublicAddress(walletName, coinType);
            var prvKey = JSB.getPrivateKey(walletName, coinType); 
            tx = JSB.send_EOS_Simple(walletName, prvKey, from,  to, ""+amount);
            break;
    }

    console.log("............SdkCrypto Tx............"+tx);

    return tx;
};

exports.onGetCoinMarketCap = function (coinType, symbol, obj) {

    var self = this;

    var id = "";
    if(coinType === "BTC")
    {
        id = "bitcoin";
    }
    else if(coinType === "BCH")
    {
        id = "bitcoin-cash";
    }
    else if(coinType === "BSV")
    {
        id = "bitcoin-sv";
    }   
    else if(coinType === "LTC")
    {
        id = "litecoin";
    }
    else if(coinType === "ETH")
    {
        id = "ethereum";
    }
    else if(coinType === "XLM")
    {
        id = "stellar";
    }
    else if(coinType === "EOS")
    {
        id = "eos";
    }
    else if(coinType === "DOGE")
    {
        id = "dogecoin";
    }
    else if(coinType === "XRP")
    {
        id = "ripple";
    }

    var url = "https://api.coinmarketcap.com/v1/ticker/"+id+"/?convert="+ symbol;

    //var xhr = cc.loader.getXMLHtt:Request();
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url);
    xhr.setRequestHeader("Content-Type", "text/plain");
    xhr.send();

    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status >= 200 && xhr.status <= 207) {

            if (xhr.responseText == "") {
                return;
            }

            var response = JSON.parse(xhr.responseText);
            var ratio = parseFloat(response[0]["price_"+symbol]);
            var price = parseFloat(self.getBalance(coinType));
            var balance =  parseFloat(price * ratio);

            var data = response[0];
            data["balance"] = ""+balance;

            //obj[coinType] = ""+balance;   
            obj[coinType] = data; 
        }
    };
};