//
//  SdkCryto.m
//  wallet-mobile
//
//  Created by LV on 2/25/19.
//

#import <Foundation/Foundation.h>
#import "Sdk/Sdk.h"
#import "storage/local-storage/LocalStorage.h"
#import "platform/CCFileUtils.h"


@interface SdkCrypto : NSObject

@end

@implementation SdkCrypto

+(void)createSDK{
    SdkCreateSDK(nil);
}

+(NSString*) getMnemoric{
    return Sdk.mnemoric;
}

+(NSString*) getSeed{
    return SdkSeedToHex();
}

+(BOOL)addWallet:(NSString *)network walletName:(NSString *)walletName{
    
    SdkWallet *w = SdkAddWallet(network, walletName, nil);
    if(w == nil){
        return false;
    }
    
    return true;
}

+(NSString*) backUp{
    return SdkBackUp();
}

+(void)restore:(NSString *)mnemoric {
    
    SdkRestore(mnemoric, nil);
}

+(NSString*) decrypted:(NSString *)seedHex encrypted:(NSString *)encrypted{
    
    return SdkDecrypted(seedHex, encrypted);
}

+(NSString*) encrypte{
    return SdkEncrypted();
}

+(NSString*) saveSDK{
 
    std::string const key("SdkCrypto");
    NSString *value = SdkSaveSDK();
    
    localStorageSetItem(key, [value UTF8String]);
    
    return value;
}

+(BOOL)loadSDK:(NSString *)str{
    std::string const key("SdkCrypto");
    std::string value;
    
    localStorageGetItem(key, &value);
    
    NSString *val = [NSString stringWithUTF8String:value.c_str()];
    
    bool ok = SdkLoadSDK(val);
    return ok;
}

+(BOOL)removeAll{
    return SdkRemoveAll();
}

+(BOOL)removeWallet:(NSString *)name{
    return SdkRemoveWallet(name);
}

//---------------------------------------------------------------------//


+(NSString*) addCoin:(NSString *)walletName  coinType:(NSString *)coinType{

    SdkWallet *w = SdkGetWallet(walletName, nil);
    if (w == nil){
        return nil;
    }
    
    NSString *addr = [w addCoin:coinType];
    
    return addr;
}

+(BOOL)connect_BTC:(NSString *)walletName url:(NSString *)url username:(NSString *)username pass:(NSString *)pass{
    
    SdkWallet *w = SdkGetWallet(walletName, nil);
    if(w == nil){
        return false;
    }
    
    return [w connect_BTC:url username:username pass:pass];
}

+(BOOL)connect_LTC:(NSString *)walletName url:(NSString *)url username:(NSString *)username pass:(NSString *)pass{

    SdkWallet *w = SdkGetWallet(walletName, nil);
    if(w == nil){
        return false;
    }
    
    return [w connect_LTC:url username:username pass:pass];
}

+(BOOL)connect_BCH:(NSString *)walletName url:(NSString *)url username:(NSString *)username pass:(NSString *)pass{
    
    SdkWallet *w = SdkGetWallet(walletName, nil);
    if(w == nil){
        return false;
    }
    
    return [w connect_BCH:url username:username pass:pass];
}

+(BOOL)connect_BSV:(NSString *)walletName url:(NSString *)url username:(NSString *)username pass:(NSString *)pass{
    
    SdkWallet *w = SdkGetWallet(walletName, nil);
    if(w == nil){
        return false;
    }
    
    return [w connect_BSV:url username:username pass:pass];
}

+(BOOL)connect_ETH:(NSString *)walletName url:(NSString *)url{
    
    SdkWallet *w = SdkGetWallet(walletName, nil);
    if(w == nil){
        return false;
    }
    
    return  [w connect_ETH:url];
}

+(float)getBlockCount:(NSString *)walletName coinType:(NSString *)coinType{
    
    SdkWallet *w = SdkGetWallet(walletName, nil);
    if(w == nil){
        return 0;
    }
    
    return [w getBlockCount:walletName];
}

+(void)syncBlock:(NSString *)walletName coinType:(NSString *)coinType fromHeight:(NSNumber *)fromHeight toHeight:(NSNumber*)toHeight{

    SdkWallet *w = SdkGetWallet(walletName, nil);
    if(w == nil){
        return ;
    }
    
    [w syncBlock:coinType fromHeight:(int64_t)fromHeight toHeight:(int64_t)toHeight];
}


+(NSString*) getPrivateKey:(NSString *)walletName coinType:(NSString *)coinType{
    
    SdkWallet *w = SdkGetWallet(walletName, nil);
    if(w == nil){
        return nil;
    }
    
    NSString *prvKey = [w getPrivateKey:coinType];
    
    return prvKey;
}

+(NSString*) getPublicAddress:(NSString *)walletName coinType:(NSString *)coinType{
    
    SdkWallet *w = SdkGetWallet(walletName, nil);
    if(w == nil){
        return nil;
    }
    
    NSString *tx = [w getPublicAddress:coinType];
    
    return tx;
}

+(NSString*) listTransaction:(NSString *)walletName coinType:(NSString *)coinType{

    SdkWallet *w = SdkGetWallet(walletName, nil);
    if(w == nil){
        return nil;
    }
    
    NSString *tx = [w listTransaction:coinType];
    
    return tx;
}

+(float)getBalance:(NSString *)walletName coinType:(NSString *)coinType{
    
    SdkWallet *w = SdkGetWallet(walletName, nil);
    if(w == nil){
        return 0;
    }

    return [w getBalance:coinType];
}

+(NSString*) getFaucet:(NSString *)walletName coinType:(NSString *)coinType addr:(NSString *)addr{

    SdkWallet *w = SdkGetWallet(walletName, nil);
    if(w == nil){
        return nil;
    }
    
    NSString *tx = [w faucet:coinType addr:addr];
    
    return tx;
}

+(NSString*) send_BTC_Simple:(NSString *)walletName from:(NSString *)from to:(NSString *)to relayFeePerKb:(NSNumber *)relayFeePerKb amount:(NSNumber *)amount{

    SdkWallet *w = SdkGetWallet(walletName, nil);
    if(w == nil){
        return nil;
    }
    
    NSString *tx = [w send_BTC_Simple:from to:to relayFeePerKb:[relayFeePerKb doubleValue] amount:[amount doubleValue]];
    
    return tx;
}

+(NSString*) send_LTC_Simple:(NSString *)walletName from:(NSString *)from to:(NSString *)to relayFeePerKb:(NSNumber *)relayFeePerKb amount:(NSNumber *)amount{
    
    SdkWallet *w = SdkGetWallet(walletName, nil);
    if(w == nil){
        return nil;
    }
    
    NSString *tx = [w send_LTC_Simple:from to:to relayFeePerKb:[relayFeePerKb doubleValue] amount:[amount doubleValue]];
    
    return tx;
}

+(NSString*) send_BCH_Simple:(NSString *)walletName from:(NSString *)from to:(NSString *)to relayFeePerKb:(NSNumber *)relayFeePerKb amount:(NSNumber *)amount{
    
    SdkWallet *w = SdkGetWallet(walletName, nil);
    if(w == nil){
        return nil;
    }
    
    NSString *tx = [w send_BCH_Simple:from to:to relayFeePerKb:[relayFeePerKb doubleValue] amount:[amount doubleValue]];
    
    return tx;
}

+(NSString*) send_BSV_Simple:(NSString *)walletName from:(NSString *)from to:(NSString *)to relayFeePerKb:(NSNumber *)relayFeePerKb amount:(NSNumber *)amount{
    
    SdkWallet *w = SdkGetWallet(walletName, nil);
    if(w == nil){
        return nil;
    }
    
    NSString *tx = [w send_BSV_Simple:from to:to relayFeePerKb:[relayFeePerKb doubleValue] amount:[amount doubleValue]];
    
    return tx;
}

+(NSString*) send_ETH_Simple:(NSString *)walletName prvKey:(NSString *)prvKey to:(NSString *)to amETH:(NSNumber *)amETH data:(NSString *)data{
    
    SdkWallet *w = SdkGetWallet(walletName, nil);
    if(w == nil){
        return nil;
    }
    
    NSString *tx = [w send_ETH_Simple:prvKey to:to amETH:[amETH doubleValue] data:data];
    
    return tx;
}

+(NSString*) send_XLM_Simple:(NSString *)walletName prvKey:(NSString *) prvKey to:(NSString *)to lumens:(NSNumber *)lumens{
    
    SdkWallet *w = SdkGetWallet(walletName, nil);
    if(w == nil){
        return nil;
    }
    
    NSString *tx = [w send_XLM_Simple:prvKey to:to amLumens:[lumens doubleValue]];
    
    return tx;
}

+(NSString*) send_EOS_Simple:(NSString *)walletName prvKey:(NSString *)prvKey from:(NSString *)from to:(NSString *)to amount:(NSString *)amount{

    SdkWallet *w = SdkGetWallet(walletName, nil);
    if(w == nil){
        return nil;
    }
    
    NSString *tx = [w send_EOS_Simple:prvKey from:from to:to amount:amount];
    
    return tx;
}

//----------------------------------------------------------------//

+(NSString*) qrCodeFile:(NSNumber *)size name:(NSString *)name content:(NSString *)content {

    std::string path = cocos2d::FileUtils::getInstance()->getWritablePath();
    //std::string addr = [FileUtilsApple getWritablePath];
    
    NSString *pathNS = [NSString stringWithUTF8String:path.c_str()];

    NSString *str = SdkQrCodeFile((long)size, name, content,pathNS);
    
    
    return str;
}

+(NSString*) qrCodePNG:(NSNumber *)size content:(NSString *)content{

    NSData *png = SdkQrCodePNG((long)size, content, nil);
    
    NSString* s = [[NSString alloc] initWithData:png encoding:NSUTF8StringEncoding];
    
    return s;
}

@end
