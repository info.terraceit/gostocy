//
//  ViewController.swift
//  credify.swift
//
//  Created by LV on 6/3/19.
//  Copyright © 2019 LV. All rights reserved.
//

import UIKit
import HDWallet

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let pass = "123"
        
        let errorCreate: NSErrorPointer = nil
        HDWalletCreateSDK(pass,errorCreate)
        
        let mnemonic = HDWallet.mnemonic()
        let seed = HDWalletSeedToHex()
        print("............ mnemonic : \(mnemonic)")
        print("............ seed : \(seed))")
        
        let errorAddWallet: NSErrorPointer = nil
        let w = HDWalletAddWallet(HDWalletTEST_NET, "wallet", errorAddWallet)
        
        let masPrivKey = HDWalletGenMasterPrivKey(HDWalletTEST_NET, HDWallet.seed())
        let masPubKey = HDWalletGenMasterPubKey(HDWalletTEST_NET, HDWallet.seed())
        print(masPrivKey)
        print(masPubKey)
        
        let msg = "abcxyz"
        let signature = HDWalletSignMessage(masPrivKey, msg)
        let verified = HDWalletVerifySignature(masPubKey, signature, msg)
        print("signature : \(signature)")
        print("verify : \(verified)")
        
        let errEncrypted : NSErrorPointer = nil
        let encrypted = HDWalletEncryptWithPubKey(masPubKey, msg, errEncrypted)
        let errDecrypted : NSErrorPointer = nil
        let decrypted = HDWalletDecryptWithPKI(masPrivKey, encrypted, errDecrypted)
        print("verify : \(encrypted)")
        print("verify : \(encrypted)")
        
        
        w!.addCoin(HDWalletCOIN_EOS)
        let priv_EOS = w!.getPrivateKey(HDWalletCOIN_EOS)
        let pub_EOS = w!.getPubKey(HDWalletCOIN_EOS)
        let addr_EOS = w!.getPublicAddress(HDWalletCOIN_EOS)
        print(priv_EOS)
        print(pub_EOS)
        print(addr_EOS)
        
        /*
        let owner_account = "twogapabc123"
        let owner_privKey = "5KNaNVobFvHT7zQ4AX3hEQ7wGfxhWQxNguZU5p7brWG6VUR3hSB"
        let owner_pubKey = "EOS6My6TumLH6Q1Vc7DcvYoYbQLtC1GaAVqGmYjxYKTxMZxLc8Ard"
        
        let tx = w!.newAccountEOS(HdwalletSIM_NET, wif: owner_privKey, owner_account: owner_account, owner_pubKey: owner_pubKey, cpuStakeStr: "0.1 EOS", netStakeStr: "0.1 EOS", buyRAMBytes: 8)
        print("..........NewAccountEOS tx : \(tx!)")
        
        print("............ EOS : \(addr_EOS!) : \(pub_EOS!) : \(priv_EOS!)")
        
        print("......................Encrypted..................................")
        let encrypted = HdwalletEncrypted()
        print(".....encrypted :  \(encrypted!)")
        let b2 = HdwalletDecrypted(HdwalletSeedToHex(), encrypted!)
        print("..... Decrypted : \(b2!)")
        
        
        let okEOS = true
       //w!.faucet(WalletCOIN_EOS, addr: addr_EOS!)
        while true {
     
            if okEOS {
                // txs := p.ListTransaction(Wallet.COIN_EOS)
                // fmt.Println("..........txs eos.... : ", txs)
                
                let balance = w!.getBalance(HdwalletCOIN_EOS)
                print("....... EOS : \(addr_EOS!) : \(balance)")
                
                // if balance >= 100 {
                //     tx := p.Send_EOS_Simple(priv_EOS, addr_EOS, "twogapabc123", "100")
                //     fmt.Println("..........txeos :", tx)
                // }
            }
            sleep(5)
        }*/
    }

}
