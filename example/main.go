package main

import (
	"encoding/hex"
	"os"

	//	"encoding/json"
	"fmt"
	"time"

	sdk "gitlab.com/info.terraceit/gostocy"
)

func main() {
	switch os.Args[1] {
	case "1":
		test1()
	case "2":
		test2()
	}
}

func test1() {
	err := sdk.CreateSDK("123")
	if err != nil {
		return
	}

	fmt.Println("..... mnemonic : ", sdk.Mnemonic)
	fmt.Println("..... seed :", hex.EncodeToString(sdk.Seed))
	fmt.Println("..... master Priv : ", sdk.GenMasterPrivKey(sdk.TEST_NET, sdk.Seed))
	fmt.Println("..... master Pub : ", sdk.GenMasterPubKey(sdk.TEST_NET, sdk.Seed))

	w, err := sdk.AddWallet(sdk.MAIN_NET, "wallet")
	if err != nil {
		return
	}

	index := "1835364963" //uint32(1835364963)
	addr := w.AddCoin(sdk.COIN_ETH, index)
	priv := w.GetPrivKey(sdk.COIN_ETH, index)
	fmt.Println("...addr...", addr)
	fmt.Println("...priv...", priv)
}

func test2() {
	err := sdk.CreateSDK("123")
	if err != nil {
		return
	}

	fmt.Println("..... mnemonic : ", sdk.Mnemonic)
	fmt.Println("..... seed :", hex.EncodeToString(sdk.Seed))
	fmt.Println("..... master Priv : ", sdk.GenMasterPrivKey(sdk.TEST_NET, sdk.Seed))
	fmt.Println("..... master Pub : ", sdk.GenMasterPubKey(sdk.TEST_NET, sdk.Seed))

	w, err := sdk.AddWallet(sdk.MAIN_NET, "wallet")
	if err != nil {
		return
	}

	index := "1" //uint32(1)
	addrBTC := w.AddCoin(sdk.COIN_BTC, index)
	privBTC := w.GetPrivKey(sdk.COIN_BTC, index)

	addrBCH := w.AddCoin(sdk.COIN_BCH, index)
	privBCH := w.GetPrivKey(sdk.COIN_BCH, index)

	addrBSV := w.AddCoin(sdk.COIN_BSV, index)
	privBSV := w.GetPrivKey(sdk.COIN_BSV, index)

	addrLTC := w.AddCoin(sdk.COIN_LTC, index)
	privLTC := w.GetPrivKey(sdk.COIN_LTC, index)

	addrETH := w.AddCoin(sdk.COIN_ETH, index)
	privETH := w.GetPrivKey(sdk.COIN_ETH, index)

	addrXLM := w.AddCoin(sdk.COIN_XLM, index)
	privXLM := w.GetPrivKey(sdk.COIN_XLM, index)

	addrXRP := w.AddCoin(sdk.COIN_XRP, index)
	privXRP := w.GetPrivKey(sdk.COIN_XRP, index)

	w.AddCoin(sdk.COIN_EOS, index)
	privEOS := w.GetPrivKey(sdk.COIN_EOS, index)
	pubEOS := w.GetPubKey(sdk.COIN_EOS, index)
	addrEOS := w.GetPublicAddress(sdk.COIN_EOS, index)

	addrTRX := w.AddCoin(sdk.COIN_TRX, index)
	privTRX := w.GetPrivKey(sdk.COIN_TRX, index)

	addrAVAX := w.AddCoin(sdk.COIN_AVAX, index)
	privAVAX := w.GetPrivKey(sdk.COIN_AVAX, index)

	/*
		owner_account := "twogapabc123"
		owner_privKey := "5KNaNVobFvHT7zQ4AX3hEQ7wGfxhWQxNguZU5p7brWG6VUR3hSB"
		owner_pubKey := "EOS6My6TumLH6Q1Vc7DcvYoYbQLtC1GaAVqGmYjxYKTxMZxLc8Ard"

		tx := w.NewAccountEOS(sdk.TEST_NET, owner_privKey, owner_account, owner_pubKey, "0.1 EOS", "0.1 EOS", 8)
		fmt.Println("..........NewAccountEOS tx : ", tx)
	*/

	fmt.Println("......Addcoin...BTC.....", addrBTC, privBTC)
	fmt.Println("......Addcoin...BCH.....", addrBCH, privBCH)
	fmt.Println("......Addcoin...BSV.....", addrBSV, privBSV)
	fmt.Println("......Addcoin...LTC.....", addrLTC, privLTC)
	fmt.Println("......Addcoin...ETH.....", addrETH, privETH)
	fmt.Println("......Addcoin...EOS.....", addrEOS, pubEOS, privEOS)
	fmt.Println("......Addcoin...XLM.....", addrXLM, privXLM)
	fmt.Println("......Addcoin...XRP.....", addrXRP, privXRP)
	fmt.Println("......Addcoin...TRX.....", addrTRX, privTRX)
	fmt.Println("......Addcoin...AVAX.....", addrAVAX, privAVAX)

	return

	okBTC := w.ConnectBTC("54.255.203.6:18443", "123", "123")
	//w.Faucet(sdk.COIN_BTC, addr_BTC)

	okLTC := w.ConnectLTC("54.255.203.6:19443", "123", "123")
	//w.Faucet(sdk.COIN_LTC, addr_LTC)

	okBCH := w.ConnectBCH("54.255.203.6:17443", "123", "123")
	//w.Faucet(sdk.COIN_BCH, addr_BCH)

	okBSV := w.ConnectBSV("54.255.203.6:16443", "123", "123")
	//w.Faucet(sdk.COIN_BSV, addr_BSV)

	okETH := w.ConnectETH("http://54.255.203.6:8545")
	//w.Faucet(sdk.COIN_ETH, addr_ETH)

	okEOS := true
	//w.Faucet(sdk.COIN_EOS, addr_EOS)

	okXLM := true
	//w.Faucet(sdk.COIN_XLM, addr_XLM)

	OkXRP := w.ConnectXRP()
	//w.Faucet(sdk.COIN_XRP, addr_XRP)

	time.Sleep(1 * time.Second)

	fmt.Println("......................SaveSDK..................................")
	w.Stop()
	//sdk.RemoveAll()
	str := sdk.SaveSDK()
	//fmt.Println(str)

	isLoaded := sdk.LoadSDK(str)
	if isLoaded {
		p, err := sdk.GetWallet("wallet")
		if err != nil {
			return
		}
		fmt.Println("......................LoadSDK..................................")
		fmt.Println(p.String())

		okBTC = p.ConnectBTC("54.255.203.6:18443", "123", "123")
		//p.Faucet(sdk.COIN_BTC, addr_BTC)

		okLTC = p.ConnectLTC("54.255.203.6:19443", "123", "123")
		//p.Faucet(sdk.COIN_LTC, addr_LTC)

		okBCH = p.ConnectBCH("54.255.203.6:17443", "123", "123")
		//p.Faucet(sdk.COIN_BCH, addr_BCH)

		okBSV = p.ConnectBSV("54.255.203.6:16443", "123", "123")
		//p.Faucet(sdk.COIN_BSV, addr_BSV)

		okETH = p.ConnectETH("http://54.255.203.6:8545")
		//p.Faucet(sdk.COIN_ETH, addr_ETH)

		okXLM = true
		//p.Faucet(sdk.COIN_XLM, addr_XLM)

		OkXRP = p.ConnectXRP()

		privBTC = p.GetPrivKey(sdk.COIN_BTC, index)
		addrBTC = p.GetPublicAddress(sdk.COIN_BTC, index)

		privBCH = p.GetPrivKey(sdk.COIN_BCH, index)
		addrBCH = p.GetPublicAddress(sdk.COIN_BCH, index)

		privBSV = p.GetPrivKey(sdk.COIN_BSV, index)
		addrBSV = p.GetPublicAddress(sdk.COIN_BSV, index)

		privLTC = p.GetPrivKey(sdk.COIN_LTC, index)
		addrLTC = p.GetPublicAddress(sdk.COIN_LTC, index)

		privETH = p.GetPrivKey(sdk.COIN_ETH, index)
		addrETH = p.GetPublicAddress(sdk.COIN_ETH, index)

		privXLM = p.GetPrivKey(sdk.COIN_XLM, index)
		addrXLM = p.GetPublicAddress(sdk.COIN_XLM, index)

		privEOS = p.GetPrivKey(sdk.COIN_EOS, index)
		addrEOS = p.GetPublicAddress(sdk.COIN_EOS, index)

		addrTRX = w.AddCoin(sdk.COIN_TRX, index)
		privTRX = w.GetPrivKey(sdk.COIN_TRX, index)

		addrAVAX = w.AddCoin(sdk.COIN_AVAX, index)
		privAVAX = w.GetPrivKey(sdk.COIN_AVAX, index)

		fmt.Println("......Addcoin...BTC.....", addrBTC, privBTC)
		fmt.Println("......Addcoin...BCH.....", addrBCH, privBCH)
		fmt.Println("......Addcoin...BSV.....", addrBSV, privBSV)
		fmt.Println("......Addcoin...LTC.....", addrLTC, privLTC)
		fmt.Println("......Addcoin...ETH.....", addrETH, privETH)
		fmt.Println("......Addcoin...EOS.....", addrEOS, pubEOS, privEOS)
		fmt.Println("......Addcoin...XLM.....", addrXLM, privXLM)
		fmt.Println("......Addcoin...XRP.....", addrXRP, privXRP)
		fmt.Println("......Addcoin...TRX.....", addrTRX, privTRX)
		fmt.Println("......Addcoin...AVAX.....", addrAVAX, privAVAX)

		for {

			if okBTC {
				//p.ListTransaction(sdk.COIN_BTC)
				// fmt.Println("..........txs btc.... : ", txs)

				bbtc := p.GetBalance(sdk.COIN_BTC)
				fmt.Println("....btc...", addrBTC, ":", bbtc)

				// if bbtc >= 20 {
				// 	p.Send_BTC_Simple(addr_BTC, "mohWPAZxv5uvjwQKSQsuH21zbgimnWwV3y", float64(0.00001), float64(5))
				// }
			}

			if okLTC {
				// txs := p.ListTransaction(sdk.COIN_LTC)
				// fmt.Println("..........txs ltc.... : ", txs)

				bltc := p.GetBalance(sdk.COIN_LTC)
				fmt.Println("....ltc...", addrLTC, ":", bltc)

				// if bltc >= 2 {
				// 	w.Send_LTC_Simple(addr_LTC, "mh3LtTYP1SqHVodXS9uCbs6LmTBggttj6f", float64(0.0001), float64(0.03))
				// }
			}

			if okBCH {
				// txs := p.ListTransaction(sdk.COIN_BCH)
				// fmt.Println("..........txs bch.... : ", txs)

				bbch := p.GetBalance(sdk.COIN_BCH)
				fmt.Println("....bch...", addrBCH, ":", bbch)

				// if bbch >= 20 {
				// 	p.Send_BCH_Simple(addr_BCH, "bchreg:qq7h5gtuswxu5jkdda5qaq26seh9s9wmwv7fmjwrcl", float64(0.00001), float64(5))
				// }
			}

			if okBSV {
				// txs := p.ListTransaction(sdk.COIN_BSV)
				// fmt.Println("..........txs bsv.... : ", txs)

				bbsv := p.GetBalance(sdk.COIN_BSV)
				fmt.Println("....bsv...", addrBSV, ":", bbsv)

				// if bbsv >= 20 {
				// 	p.Send_BSV_Simple(addr_BSV, "bchreg:qqkqvr9k52sexh474y52g6qgktdkunhe3yyr6age3j", float64(0.00001), float64(5))
				// }
			}

			if okETH {
				//p.ListTransaction(sdk.COIN_ETH)
				// fmt.Println("..........txs eth.... : ", txs)

				beth := p.GetBalance(sdk.COIN_ETH)
				fmt.Println("...eth....", addrETH, ":", beth)

				// if beth > 2 {
				// 	//teth := w.Send_ETH_Simple("47de15108b35169c4aff4826d5c413fe117e361a900325f6d3df1f0e04cbd706", addr_ETH, float64(0.1), "")
				// 	teth := w.Send_ETH_Simple(priv_ETH, "0xba32d2731f50597cc648a030b32799fbce3bcf3b", float64(0.1), "")
				// 	fmt.Println("..........teth :", teth)
				// }

			}

			if okEOS {
				// txs := p.ListTransaction(sdk.COIN_EOS)
				// fmt.Println("..........txs eos.... : ", txs)

				beos := p.GetBalance(sdk.COIN_EOS)
				fmt.Println("...eos....", addrEOS, ":", beos)

				// if beos >= 100 {
				// 	tx := p.Send_EOS_Simple(priv_EOS, addr_EOS, "twogapabc123", "100")
				// 	fmt.Println("..........txeos :", tx)
				// }
			}

			if okXLM {
				//p.ListTransaction(sdk.COIN_XLM)
				// fmt.Println("..........payments xlm.... : ", txs)

				bxlm := p.GetBalance(sdk.COIN_XLM)
				fmt.Println("...xlm....", addrXLM, ":", bxlm)

				// txlm := w.Send_XLM_Simple(w.GetPrivKey(sdk.COIN_XLM, 0), "GCIUNX3NN67HTXCDSNUDF3TED25C2GAGKNQLIWCLE3FIAPTPMVPVN563", 1)
				// fmt.Println("..........txlm :", txlm)
			}

			if OkXRP {
				blockcount := p.GetBlockCount(sdk.COIN_XRP)
				fmt.Println("......blockcount....xrp", blockcount)

				bxrp := p.GetBalance(sdk.COIN_XRP)
				fmt.Println("...xrp....", addrXRP, ":", bxrp)
			}

			time.Sleep(2 * time.Second)

			fmt.Println("........................................................")
		}
	}

	//	fmt.Println("......................Encrypted..................................")
	//	encrypted := sdk.Encrypted()
	//	fmt.Println(".....encrypted : ", encrypted)
	//	b2 := sdk.Decrypted(sdk.SeedToHex(), encrypted)
	//	fmt.Println("..... Decrypted : ", b2)

	//	fmt.Println("......................Storage..................................")
	//	wll, err := sdk.GetWallet("wallet")
	//	if err != nil {
	//		return
	//	}
	//	fmt.Println(wll.String())

	//	fmt.Println("......................BackUp Restore..................................")
	//	mnemoric := sdk.BackUp()
	//	err = sdk.Restore(mnemoric)
	//	if err != nil {
	//		return
	//	}

}
