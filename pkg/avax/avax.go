package avax

import (
	"context"
	"errors"
	"fmt"
	"math/big"
	"strings"

	"gitlab.com/info.terraceit/gostocy/pkg/eth"

	Abi "github.com/ava-labs/coreth/accounts/abi"
	"github.com/ava-labs/coreth/accounts/abi/bind"
	"github.com/ava-labs/coreth/core/types"
	"github.com/ava-labs/coreth/ethclient"
	"github.com/ava-labs/coreth/interfaces"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/crypto"
)

//---------------------------------------------------------------------//

type Client struct {
	ethclient.Client
	URL     string
	ChainId *big.Int
}

func NewClient(url string) (*Client, error) {
	cli, err := ethclient.Dial(url)
	if err != nil {
		fmt.Println("error ethClient ", err)
		return nil, err
	}

	chainId, err := cli.ChainID(context.Background())
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	return &Client{Client: cli, URL: url, ChainId: chainId}, nil
}

func (ec *Client) GetBalanceOf(addr, contract string, decimal int) (*big.Int, string) {
	hex, err := ec.SolidityCallRaw(addr, contract, `balanceOf(address)`, addr)
	if err != nil {
		fmt.Println("ERC20 Token not enough !!!")
		return nil, ""
	}

	// weiBigInt := new(big.Int).SetBytes(hex)
	// weBigIntCommon := common.BytesToHash(hex).Big()
	// fmt.Println("balanceOf BigInt :", weiBigInt, weBigIntCommon)
	// fmt.Println("balanceOf Int64 :", weiBigInt.Int64())
	// fmt.Println("balanceOf Uint64 :", weBigIntCommon.Uint64())

	bigI, fStr := eth.ByteToFloat(hex, decimal)
	//fmt.Println("balanceOf :", bigI, fStr)
	return bigI, fStr
}

func (ec *Client) EsimateGasLimit(fromAddress, contractAddress, abi, method string, params ...interface{}) ([]byte, *big.Int, uint64, uint64) {
	// price
	gasPrice, err := ec.Client.SuggestGasPrice(context.Background())
	if err != nil {
		fmt.Println(err)
		return nil, nil, 0, 0
	}
	// fmt.Println("gasPrice : ", gasPrice)

	// bytecode
	byteCode, err := eth.Pack(abi, method, params...)
	if err != nil {
		fmt.Println(err)
		return nil, nil, 0, 0
	}
	// fmt.Println("byteCode", hex.EncodeToString(byteCode))

	// estimate gas
	var from common.Address = common.HexToAddress(fromAddress)
	var to common.Address = common.HexToAddress(contractAddress)
	msg := interfaces.CallMsg{From: from, To: &to, Data: byteCode}

	// gasUsed
	gasUsed, err := ec.Client.EstimateGas(context.Background(), msg)
	if err != nil {
		fmt.Println(err)
		return nil, nil, 0, 0
	}
	// fmt.Println("gasUsed : ", gasUsed)

	// gas
	gas := gasUsed * gasPrice.Uint64()
	// fmt.Println("gas :", gas)

	return byteCode, gasPrice, gasUsed, gas
}

func (ec *Client) SendTransactionRaw(prvKey, to string, amount *big.Int, gasLimit uint64, gasPrice *big.Int, data []byte) (string, uint64, error) {
	blockNumber, err := ec.Client.BlockNumber(context.Background())
	if err != nil {
		fmt.Println(err)
		return "", 0, err
	}
	//fmt.Println("............blockNumber :", blockNumber, hexutil.EncodeUint64(blockNumber))

	key0, err := crypto.HexToECDSA(prvKey)
	if err != nil {
		fmt.Println("error estimateGas")
		return "", 0, err
	}
	addr0 := crypto.PubkeyToAddress(key0.PublicKey)
	//opts := bind.NewKeyedTransactor(key0)

	//valueWei := ToWei(amETH, "ether")
	//f := strconv.FormatFloat(valueWei, 'f', -1, 64)
	//value := ToBig256(f)

	// gasLimit //
	//var aC common.Address = common.HexToAddress(to)
	//msg := ethereum.CallMsg{From: opts.From, To: &aC, Value: big.NewInt(value), Data: data}
	//gasLimit, erg := ec.client.EstimateGas(context.Background(), msg)
	//if erg != nil {
	//	fmt.Println("Error EstimateGas", erg)
	//	return ""
	//}
	//fmt.Println("msg : ", msg)

	// Nonce //
	number := big.NewInt(int64(blockNumber))
	nonce, err := ec.Client.NonceAt(context.Background(), addr0, number)
	if err != nil {
		n, _ := fmt.Printf("%v", err)
		fmt.Println(n)
		return "", 0, err
	}
	//fmt.Println("nonce : ", nonce)

	// NewTransaction
	tx := types.NewTransaction(uint64(nonce), common.HexToAddress(to), amount, gasLimit, gasPrice, data)
	//fmt.Println("tx un signed : ", tx)

	//signer := types.HomesteadSigner{}
	signer := types.NewEIP155Signer(ec.ChainId)
	signature, err := crypto.Sign(signer.Hash(tx).Bytes(), key0)
	if err != nil {
		fmt.Println("error Sign")
		return "", 0, err
	}
	rawTx, err := tx.WithSignature(signer, signature)
	if err != nil {
		fmt.Println("error withSignature")
		return "", 0, err
	}
	//fmt.Println("rawTx : ", rawTx, rawTx.Hash().Hex())

	err = ec.Client.SendTransaction(context.Background(), rawTx)
	if err != nil {
		fmt.Println("error sendTransaction :", err)
		return "", 0, err
	}

	return rawTx.Hash().Hex(), nonce, nil
}

func (ec *Client) SolidityCall(addr, abi, method string, params ...interface{}) (result []interface{}, err error) {
	parsed, err := Abi.JSON(strings.NewReader(abi))
	if err != nil {
		fmt.Println(err)
		return
	}

	contract := bind.NewBoundContract(common.HexToAddress(addr), parsed, ec.Client, ec.Client, ec.Client)
	err = contract.Call(nil, &result, method, params...)
	if err != nil {
		fmt.Println(err)
		return
	}
	return
}

// method = 'transfer'
func (ec *Client) SolidityTransact(prvKey, addr, abi, method string, value, gasPrice, gasLimit int64, params ...interface{}) (string, uint64, error) {
	blockNumber, err := ec.Client.BlockNumber(context.Background())
	if err != nil {
		fmt.Println(err)
		return "", 0, err
	}
	//fmt.Println("............blockNumber :", blockNumber, hexutil.EncodeUint64(blockNumber))

	parsed, err := Abi.JSON(strings.NewReader(abi))
	if err != nil {
		return "", 0, err
	}

	key0, err := crypto.HexToECDSA(prvKey)
	if err != nil {
		return "", 0, err
	}
	addr0 := crypto.PubkeyToAddress(key0.PublicKey)

	// Nonce //
	number := big.NewInt(int64(blockNumber))
	nonce, err := ec.Client.NonceAt(context.Background(), addr0, number)
	if err != nil {
		fmt.Println("error nonce", err)
		return "", 0, err
	}
	//fmt.Println("nonce : ", nonce)

	opts, err := bind.NewKeyedTransactorWithChainID(key0, ec.ChainId)
	if err != nil {
		fmt.Println(err)
		return "", 0, err
	}
	opts.Nonce = big.NewInt(int64(nonce))
	opts.Value = big.NewInt(value)
	opts.GasPrice = big.NewInt(gasPrice)
	opts.GasLimit = uint64(gasLimit)

	contract := bind.NewBoundContract(common.HexToAddress(addr), parsed, ec.Client, ec.Client, ec.Client)
	tx, err := contract.Transact(opts, method, params...)
	if err != nil {
		return "", 0, err
	}

	return tx.Hash().Hex(), nonce, nil
}

func (ec *Client) SolidityDeploy(prvKey, abi, codeHex string, value, gasPrice *big.Int, gasLimit uint64, params ...interface{}) (string, string, *bind.BoundContract, error) {
	parsed, err := Abi.JSON(strings.NewReader(abi))
	if err != nil {
		fmt.Println(err)
		return "", "", nil, err
	}
	//fmt.Println("abi : ", parsed)

	key, err := crypto.HexToECDSA(prvKey)
	if err != nil {
		fmt.Println(err)
		return "", "", nil, err
	}
	opts, err := bind.NewKeyedTransactorWithChainID(key, ec.ChainId)
	if err != nil {
		fmt.Println(err)
		return "", "", nil, err
	}
	opts.Value = value
	opts.GasPrice = gasPrice
	opts.GasLimit = gasLimit
	//fmt.Println("params........", len(params), params)

	addr, tx, contract, err := bind.DeployContract(opts, parsed, common.FromHex(codeHex), ec.Client, params...)
	if err != nil {
		fmt.Println(err)
		return "", "", nil, err
	}
	// fmt.Println("contract : ", contract)
	// fmt.Println("address : ", addr.Hex())
	// fmt.Println("tx : ", tx)

	return addr.Hex(), tx.Hash().Hex(), contract, nil
}

// SolRaw(prvKey, addressContract, `transfer(address,uint256)`, `0x5e531bb813994f27f65d2d5f7dc7c51dbe5406f7`, big.NewInt(100))
func (ec *Client) SolidityTransactRaw(prvKey, addr, method string, amount *big.Int, gasLimit uint64, gasPrice *big.Int, params ...interface{}) (string, uint64, error) {
	blockNumber, err := ec.Client.BlockNumber(context.Background())
	if err != nil {
		fmt.Println(err)
		return "", 0, err
	}
	//fmt.Println("............blockNumber :", blockNumber, hexutil.EncodeUint64(blockNumber))

	key0, _ := crypto.HexToECDSA(prvKey)
	addr0 := crypto.PubkeyToAddress(key0.PublicKey)
	//opts := bind.NewKeyedTransactor(key0)

	// gasLimit //
	//var aC common.Address = common.HexToAddress(addr)
	//msg := ethereum.CallMsg{From: opts.From, To: &aC, Value: amount, Data: data}
	//gasLimit, erg := ec.client.EstimateGas(context.Background(), msg)
	//if erg != nil {
	//	fmt.Println("Error gasLimit")
	//	return ""
	//}
	//fmt.Println("gasLimit", gasLimit)
	//
	//gasPrice, err := ec.client.SuggestGasPrice(context.Background())
	//if erg != nil {
	//	fmt.Println("Error gasPrice")
	//	return ""
	//}
	//fmt.Println("gasPrice", gasPrice)

	// Nonce //
	number := big.NewInt(int64(blockNumber))
	nonce, err := ec.Client.NonceAt(context.Background(), addr0, number)
	if err != nil {
		fmt.Println("error nonce", err)
		return "", 0, err
	}
	//fmt.Println("nonce : ", nonce)

	// Input Data
	data := eth.PackInputData(method, params...)
	if len(data) <= 0 {
		return "", 0, errors.New("error data length empty")
	}

	// NewTransaction
	tx := types.NewTransaction(nonce, common.HexToAddress(addr), amount, gasLimit, gasPrice, data)
	//fmt.Println("tx un signed : ", tx)

	//signer := types.HomesteadSigner{}
	signer := types.NewEIP155Signer(ec.ChainId)
	signature, err := crypto.Sign(signer.Hash(tx).Bytes(), key0)
	if err != nil {
		fmt.Println("error sign", err)
		return "", 0, err
	}
	rawTx, err := tx.WithSignature(signer, signature)
	if err != nil {
		fmt.Println("error withSignature", err)
		return "", 0, err
	}

	// SendTraction to Contract //
	err = ec.Client.SendTransaction(context.Background(), rawTx)
	if err != nil {
		fmt.Println("error sendTransacction ", err)
		return "", 0, err
	}
	//fmt.Println("rawTx : ", rawTx, rawTx.Hash().Hex())

	return rawTx.Hash().Hex(), nonce, nil
}

// `balanceOf(address)`
func (ec *Client) SolidityCallRaw(_from, _to, method string, params ...interface{}) (hexutil.Bytes, error) {
	// Input Data
	data := eth.PackInputData(method, params...)
	if len(data) <= 0 {
		return nil, errors.New("error data length empty")
	}

	var from common.Address = common.HexToAddress(_from)
	var to common.Address = common.HexToAddress(_to)
	msg := interfaces.CallMsg{From: from, To: &to, Data: data}
	hex, err := ec.Client.CallContract(context.Background(), msg, nil)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	return hex, err
}
