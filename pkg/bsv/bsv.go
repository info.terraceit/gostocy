package bsv

import (
	"encoding/hex"
	"encoding/json"
	"strconv"

	//"crypto/sha256"

	"github.com/bchsuite/bchd/bchec"
	"github.com/bchsuite/bchd/chaincfg"
	"github.com/bchsuite/bchd/chaincfg/chainhash"
	"github.com/bchsuite/bchd/rpcclient"
	"github.com/bchsuite/bchd/txscript"
	"github.com/bchsuite/bchutil"
)

var MainNetParams = "mainnet"
var TestNet3Params = "testnet"
var SimNetParams = "simnet"

type chainParams struct {
	Params *chaincfg.Params
}

func ChainConfig(net string) *chaincfg.Params {
	switch net {
	case MainNetParams:
		return &chaincfg.MainNetParams
	case TestNet3Params:
		return &chaincfg.TestNet3Params
	case SimNetParams:
		return &chaincfg.RegressionNetParams
	}

	return nil
}

func ToBCH(amount string) float64 {
	f, err := strconv.ParseFloat(amount, 64)
	if err != nil {
		return 0
	}

	value, err := bchutil.NewAmount(f)
	if err != nil {
		return 0
	}

	return value.ToBCH()
}

func ToSatoshi(amount string) float64 {
	f, err := strconv.ParseFloat(amount, 64)
	if err != nil {
		return 0
	}

	value, err := bchutil.NewAmount(f)
	if err != nil {
		return 0
	}

	return value.ToUnit(bchutil.AmountSatoshi)
}

func SignMessage(privHex, message string) string {
	privKeyBytes, err := hex.DecodeString(privHex)
	if err != nil {
		return ""
	}

	privKey, _ := bchec.PrivKeyFromBytes(bchec.S256(), privKeyBytes)
	messageHash := chainhash.DoubleHashB([]byte(message))
	signature, err := privKey.Sign(messageHash)
	if err != nil {

		return ""
	}

	return hex.EncodeToString(signature.Serialize())
}

func VerifySignature(pubKeyHex, sigHex, message string) bool {
	pubKeyBytes, err := hex.DecodeString(pubKeyHex)
	if err != nil {
		return false
	}
	pubKey, err := bchec.ParsePubKey(pubKeyBytes, bchec.S256())
	if err != nil {
		return false
	}

	sigBytes, err := hex.DecodeString(sigHex)
	if err != nil {
		return false
	}

	signature, err := bchec.ParseSignature(sigBytes, bchec.S256())
	if err != nil {
		return false
	}
	messageHash := chainhash.DoubleHashB([]byte(message))
	verified := signature.Verify(messageHash, pubKey)

	return verified
}

func EncryptMessage(pubKeyHex, message string) string {
	pubKeyBytes, err := hex.DecodeString(pubKeyHex) // uncompressed pubkey
	if err != nil {
		return ""
	}
	pubKey, err := bchec.ParsePubKey(pubKeyBytes, bchec.S256())
	if err != nil {
		return ""
	}

	ciphertext, err := bchec.Encrypt(pubKey, []byte(message))
	if err != nil {
		return ""
	}

	return string(ciphertext)
}

func DecryptMessage(privHex, ciphertext string) string {
	pkBytes, err := hex.DecodeString(privHex)
	if err != nil {
		return ""
	}

	privKey, _ := bchec.PrivKeyFromBytes(bchec.S256(), pkBytes)
	plaintext, err := bchec.Decrypt(privKey, []byte(ciphertext))
	if err != nil {
		return ""
	}

	return string(plaintext)
}

func PayToAddrScript(addr, net string) ([]byte, error) {
	address, err := DecodeAddress(addr, ChainConfig(net))
	if err != nil {
		return []byte{}, err
	}

	pkScript, err := cashPayToAddrScript(address)
	//pkScript, err := txscript.PayToAddrScript(address)
	if err != nil {
		return []byte{}, err
	}

	return pkScript, nil
}

func ExtractPkScriptAddrs(pkScript []byte, net string) (string, error) {
	conf := ChainConfig(net)
	addr, err := CashExtractPkScriptAddrs(pkScript, conf)
	//_, addrs, _, err := txscript.ExtractPkScriptAddrs(pkScript, conf.Params)
	if err != nil {
		return "", err
	}

	return addr.EncodeAddress(), nil
}

func DisasmString(script []byte) string {
	disasm, err := txscript.DisasmString(script)
	if err != nil {
		return ""
	}

	return disasm
}

//---------------------------------------------------------------------//

type KeyPair struct {
	wif *bchutil.WIF
	//pubKey   *bchec.PublicKey
	chaincfg *chaincfg.Params
}

func NewKeyPairRandom(net string, compress bool) (*KeyPair, error) {
	conf := ChainConfig(net)
	privKey, err := bchec.NewPrivateKey(bchec.S256())
	if err != nil {
		return &KeyPair{}, err
	}

	wif, err := bchutil.NewWIF(privKey, conf, compress)
	if err != nil {
		return &KeyPair{}, err
	}

	kp := &KeyPair{}
	kp.wif = wif
	kp.chaincfg = conf

	return kp, nil
}

func NewKeyPairFromBytes(net, keyHex string, compress bool) (*KeyPair, error) {
	conf := ChainConfig(net)
	privKeyBytes, err := hex.DecodeString(keyHex)
	if err != nil {
		return &KeyPair{}, err
	}

	privKey, _ := bchec.PrivKeyFromBytes(bchec.S256(), privKeyBytes)
	wif, err := bchutil.NewWIF(privKey, conf, compress)
	if err != nil {
		return &KeyPair{}, err
	}

	kp := &KeyPair{}
	kp.wif = wif
	kp.chaincfg = conf

	return kp, nil
}

func NewKeyPairWIF(net, privHex string) (*KeyPair, error) {
	conf := ChainConfig(net)
	wif, err := bchutil.DecodeWIF(privHex)
	if err != nil {
		return &KeyPair{}, err
	}

	kp := &KeyPair{}
	kp.wif = wif
	kp.chaincfg = conf

	return kp, nil
}

func (kp KeyPair) GetPrivKey() string {
	//	wif, err := bchutil.NewWIF(kp.privKey, kp.chaincfg, false)
	//	if err != nil {
	//		return ""
	//	}
	//	return wif.String()
	return kp.wif.String()
}

func (kp KeyPair) GetPubKeyHash() []byte {
	return bchutil.Hash160(kp.GetPubKey())
}

func (kp KeyPair) GetPubKey() []byte {
	//return kp.wif.PrivKey.PubKey().SerializeUncompressed()
	return kp.wif.SerializePubKey()
}

//---------------------------------------------------------------------//

type Client struct {
	client   *rpcclient.Client
	connCfg  *rpcclient.ConnConfig
	chaincfg *chaincfg.Params
	Net      string
	URL      string
}

func NewClient(net, host, username, password string) (*Client, error) {
	connCfg := &rpcclient.ConnConfig{}
	connCfg.Host = host
	connCfg.User = username
	connCfg.Pass = password
	connCfg.HTTPPostMode = true
	connCfg.DisableTLS = true

	client, err := rpcclient.New(connCfg, nil)
	if err != nil {
		return &Client{}, err
	}
	//defer client.Shutdown()

	cli := &Client{}
	cli.client = client
	cli.connCfg = connCfg
	cli.chaincfg = ChainConfig(net)
	cli.Net = net
	cli.URL = host

	return cli, nil
}

func (cli *Client) GetBlockCount() int64 {
	blockCount, err := cli.client.GetBlockCount()
	if err != nil {
		return 0
	}

	return blockCount
}

func (cli *Client) GetBestBlockHash() string {
	hs, err := cli.client.GetBestBlockHash()
	if err != nil {
		return ""
	}

	return hs.String()
}

func (cli *Client) GetBlock(blockHash string) string {
	hx, err := chainhash.NewHashFromStr(blockHash)
	if err != nil {
		return ""
	}

	block, err := cli.client.GetBlock(hx)
	if err != nil {
		return ""
	}

	b, err := json.MarshalIndent(block, "", " ")
	if err != nil {
		return ""
	}

	return string(b)
}

func (cli *Client) GetBlockVerbose(blockHash string) string {
	hx, err := chainhash.NewHashFromStr(blockHash)
	if err != nil {
		return ""
	}

	block, err := cli.client.GetBlockVerbose(hx)
	if err != nil {
		return ""
	}

	b, err := json.MarshalIndent(block, "", " ")
	if err != nil {
		return ""
	}

	return string(b)
}

func (cli *Client) GetBlockHash(blockHeight int64) string {
	hs, err := cli.client.GetBlockHash(blockHeight)
	if err != nil {
		return ""
	}

	return hs.String()
}

func (cli *Client) DecodeAddress(hex string) string {
	address, err := DecodeAddress(hex, cli.chaincfg)
	if err != nil {
		return ""
	}

	return address.String()
}

func (cli *Client) WalletPassphrase(pass string, second int64) (bool, error) {
	//  WalletPassphrase
	err := cli.client.WalletPassphrase(pass, second)
	if err != nil {
		return false, err
	}

	return true, nil
}

// NOTE: This function requires to the wallet to be unlocked
func (cli *Client) DumpPrivKey(addr string) string {
	address, err := DecodeAddress(addr, cli.chaincfg)
	if err != nil {
		return ""
	}

	// DumpPrivKey
	wif, err := cli.client.DumpPrivKey(address)
	if err != nil {
		return ""
	}

	return wif.String()
}

func (cli *Client) ImportPrivKey(prv, label string, rescan bool) error {
	wif, err := bchutil.DecodeWIF(prv)
	if err != nil {
		return err
	}

	return cli.client.ImportPrivKeyRescan(wif, label, rescan)
}

func (cli *Client) ImportAddress(addr string) error {
	return cli.client.ImportAddress(addr)
}

func (cli *Client) GetTransaction(txHex string) string {
	txHash, err := chainhash.NewHashFromStr(txHex)
	if err != nil {
		return ""
	}

	btcj, err := cli.client.GetTransaction(txHash)
	if err != nil {
		return ""
	}

	b, err := json.MarshalIndent(btcj, "", " ")
	if err != nil {
		return ""
	}

	return string(b)
}

func (cli *Client) GetRawTransaction(txHex string) string {
	txHash, err := chainhash.NewHashFromStr(txHex)
	if err != nil {
		return ""
	}

	btcj, err := cli.client.GetRawTransaction(txHash)
	if err != nil {
		return ""
	}

	b, err := json.MarshalIndent(btcj, "", " ")
	if err != nil {
		return ""
	}

	return string(b)
}

func (cli *Client) GetRawTransactionVerbose(txHex string) string {
	txHash, err := chainhash.NewHashFromStr(txHex)
	if err != nil {
		return ""
	}

	btcj, err := cli.client.GetRawTransactionVerbose(txHash)
	if err != nil {
		return ""
	}

	b, err := json.MarshalIndent(btcj, "", " ")
	if err != nil {
		return ""
	}

	return string(b)
}

func (cli *Client) Generate(num int32) string {
	bl, err := cli.client.Generate(uint32(num))
	if err != nil {
		return ""
	}

	b, err := json.MarshalIndent(bl, "", " ")
	if err != nil {
		return ""
	}

	return string(b)
}

func (cli *Client) SendToAddress(addr string, amount float64) string {
	address, err := DecodeAddress(addr, cli.chaincfg)
	if err != nil {
		return ""
	}

	value, err := bchutil.NewAmount(amount)
	if err != nil {
		return ""
	}

	hs, err := cli.client.SendToAddress(address, value)
	if err != nil {
		//fmt.Println("......error not SendToAddress.....")
		return ""
	}

	if cli.Net == "simnet" {
		cli.Generate(1)
	}

	return hs.String()
}
