package btc

import (
	"encoding/hex"
	//"encoding/json"
	"fmt"
	"testing"
)

func Test_Addr(t *testing.T) {
	//keypair, _ := NewKeyPairWIF("simnet", "cQj5Zg2kArrosXr1oshPVg416bFqVnr1fA9taxuRYcZRKpr9EDvx")
	keypair, _ := NewKeyPairRandom("simnet", true)
	fmt.Println("--- PrivKey : ", keypair.GetPrivKey())
	fmt.Println("--- PubKey : ", hex.EncodeToString(keypair.GetPubKey()))
	fmt.Println("--- PubKeyHash : ", hex.EncodeToString(keypair.GetPubKeyHash()))

	addrPK, _ := NewAddressPubKey(keypair.GetPubKey(), "simnet")
	fmt.Println("addrPK : ", addrPK)

	addrPKH, _ := NewAddressPubKeyHash(keypair.GetPubKeyHash(), "simnet")
	fmt.Println("addrPKH : ", addrPKH)

	addrP2SH, _ := addrPKH.AddressScriptHash("simnet")
	fmt.Println("addrP2SH : ", addrP2SH)

	addrPWKH, _ := addrPK.AddressWitnessPubKeyHash("simnet")
	fmt.Println("addrPWKH : ", addrPWKH)

	addrP2WSH, _ := addrPKH.AddressWitnessScriptHash("simnet")
	fmt.Println("addrP2WSH : ", addrP2WSH)

	pkScript, _ := PayToAddrScript(addrPKH.EncodeAddress(), "simnet")
	asm := DisasmString(pkScript)
	fmt.Println("..... pkScript : ", hex.EncodeToString(pkScript))
	fmt.Println("........ asm : ", asm)
}

func TestMultiSig(t *testing.T) {

	/*
		--- PrivKey :  cUzWuNCTRzNvEV5Pm4i7hBxFRpAyusmv7igR5y997ZTBk7xpDPsi
		--- PubKey :  0270350bc5a36a27fda5aa61b101903c3e19292a3c27c3294c6466ad7745890066

		--- PrivKey :  cVWxhztXHFHkNzeSNBbjNi7AmfQGRMpeq1M3TxZkQh4KcGyAexgN
		--- PubKey :  03d6608f6d29f3594779ba9bef2216b7efa62481778158df49f7e446a5ca23fab2


		--- PrivKey :  cSL87rj1HtsbeoEGFJKm7vjeR8jmT7e1WyBzAK3G3zQ62qv78DEb
		--- PubKey :  022513ccc572fc432b4556e6321123cfaef53670b448b8ac3b9b9cb3e22a000d5c

		--- PrivKey :  cN9wnUGbVHR5N8jeL3GnRkkJaDj82RWnSeqrwSMExQ1FtFZchXFX
		--- PubKey :  026f9537ba54cae978e9501cf7ba37bcf2d03f28eda159713d721540a5a3678628
	*/

	addr, _ := NewAddressMultiSig("simnet")
	addr.AddPubKey("0270350bc5a36a27fda5aa61b101903c3e19292a3c27c3294c6466ad7745890066")
	addr.AddPubKey("03d6608f6d29f3594779ba9bef2216b7efa62481778158df49f7e446a5ca23fab2")
	addr.AddPubKey("022513ccc572fc432b4556e6321123cfaef53670b448b8ac3b9b9cb3e22a000d5c")
	addr.AddPubKey("026f9537ba54cae978e9501cf7ba37bcf2d03f28eda159713d721540a5a3678628")
	script := addr.MultiSigScript(2)
	fmt.Println("script : ", script)
	address := addr.EncodeAddress()
	fmt.Println("address : ", address)
}

/*
func Test_CLI(t *testing.T) {
	cli, err := NewClient("simnet", "localhost:18443", "123", "123")
	if err != nil {
		return
	}

	// fmt.Println("............blockCount :", cli.GetBlockCount())
	// fmt.Println("............blockHash :", cli.GetBlockHash(109))
	// fmt.Println("............betsblockHash :", cli.GetBestBlockHash())
	// fmt.Println("............block :", cli.GetBlock("7ec656095f40ede795180cb33e35c96682d0c2bba48b05458b2ef582e368f9c6"))
	// fmt.Println("............blockverbose :", cli.GetBlockVerbose("7ec656095f40ede795180cb33e35c96682d0c2bba48b05458b2ef582e368f9c6"))

	// fmt.Println("............balance :", cli.GetBalance("myuudnuiPrBPHz5eoCaDQ9QDKhKyoSApdd"))
	// fmt.Println("............GetRawTransactionVerbose :", cli.GetRawTransactionVerbose("fb04c43c98f81a55111560e396f5b934568d14428a5f53affa8cfdf9f381a664"))

	blockcount := cli.GetBlockCount()
	fmt.Println("............blockCount :", blockcount)
	SyncBlock(0, blockcount, cli)

	//---------------------------------------------------------------------//

	mnemoric := "matrix stay scale miss city dentist tired tide paper open guilt ozone"
	seed, _ := GenSeedWithMnemoric(mnemoric, "123")
	//seed, _ := GenSeed("123")

	fmt.Println("....Seed......", seed.Mnemoric, hex.EncodeToString(seed.Seed))

	w, _ := NewWallet("simnet", seed.Mnemoric, "123")
	for i := 0; i < 1; i++ {
		w.CreateAccount()
	}
	encrypted := w.BackUp()
	fmt.Println("........encrypted.....", encrypted)

	wS, _ := Restore(hex.EncodeToString(seed.Seed), encrypted)
	fmt.Println("........decrypt.....", wS.String())

	b, _ := json.MarshalIndent(ManagedAccount, "", " ")
	fmt.Println("........ManagedAccount......", string(b))

	b, _ = json.MarshalIndent(ManagedAddress, "", " ")
	fmt.Println("........ManagedAddress......", string(b))

	//---------------------------------------------------------------------//
	addr := wS.Accounts[0].AddressExternals[0].Address

	txs := ListTransactions(addr)
	fmt.Println("..........txs..........", txs)

	utxos := ListUTXOs("simnet", addr)
	fmt.Println("..........utxos..........", utxos)

	balance := GetBalance("simnet", addr)
	fmt.Println("..............balance............", balance)

	//---------------------------------------------------------------------//

	tx := wS.NewTX("simnet")
	tx.AddSender(addr)
	tx.AddReceipt("mohWPAZxv5uvjwQKSQsuH21zbgimnWwV3y", float64(0.03))
	tx.CreateRawTx(float64(0.00001))
	tx.SignTx()
	fmt.Println(".....tx.....", tx.String())

	txHex := SendRawTx(tx, false, cli)
	fmt.Println(".....txHex.....", txHex)

}
*/
