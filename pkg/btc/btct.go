package btc

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/btcsuite/btcd/btcec"
	"github.com/btcsuite/btcd/chaincfg"
	"github.com/btcsuite/btcd/chaincfg/chainhash"
	"github.com/btcsuite/btcd/rpcclient"
	"github.com/btcsuite/btcd/txscript"
	"github.com/btcsuite/btcutil"
)

var MainNetParams = "mainnet"
var TestNet3Params = "testnet"
var SimNetParams = "simnet"

func ChainConfig(net string) *chaincfg.Params {
	switch net {
	case MainNetParams:
		return &chaincfg.MainNetParams
	case TestNet3Params:
		return &chaincfg.TestNet3Params
	case SimNetParams:
		return &chaincfg.RegressionNetParams
	}

	return nil
}

func ToBTC(amount string) float64 {
	f, err := strconv.ParseFloat(amount, 64)
	if err != nil {
		return 0
	}

	value, err := btcutil.NewAmount(f)
	if err != nil {
		return 0
	}

	return value.ToBTC()
}

func ToSatoshi(amount string) float64 {
	f, err := strconv.ParseFloat(amount, 64)
	if err != nil {
		return 0
	}

	value, err := btcutil.NewAmount(f)
	if err != nil {
		return 0
	}

	return value.ToUnit(btcutil.AmountSatoshi)
}

func Sign(privHex, message string) string {
	privKeyBytes, err := hex.DecodeString(privHex)
	if err != nil {
		return ""
	}

	privKey, _ := btcec.PrivKeyFromBytes(btcec.S256(), privKeyBytes)
	messageHash := chainhash.DoubleHashB([]byte(message))
	signature, err := privKey.Sign(messageHash)
	if err != nil {

		return ""
	}

	return hex.EncodeToString(signature.Serialize())
}

func VerifySignature(pubKeyHex, sigHex, message string) bool {
	pubKeyBytes, err := hex.DecodeString(pubKeyHex)
	if err != nil {
		return false
	}
	pubKey, err := btcec.ParsePubKey(pubKeyBytes, btcec.S256())
	if err != nil {
		return false
	}

	sigBytes, err := hex.DecodeString(sigHex)
	if err != nil {
		return false
	}

	signature, err := btcec.ParseSignature(sigBytes, btcec.S256())
	if err != nil {
		return false
	}
	messageHash := chainhash.DoubleHashB([]byte(message))
	verified := signature.Verify(messageHash, pubKey)

	return verified
}

func EncryptMessage(pubKeyHex, message string) string {
	pubKeyBytes, err := hex.DecodeString(pubKeyHex) // uncompressed pubkey
	if err != nil {
		return ""
	}
	pubKey, err := btcec.ParsePubKey(pubKeyBytes, btcec.S256())
	if err != nil {
		return ""
	}

	ciphertext, err := btcec.Encrypt(pubKey, []byte(message))
	if err != nil {
		return ""
	}

	return string(ciphertext)
}

func DecryptMessage(privHex, ciphertext string) string {
	pkBytes, err := hex.DecodeString(privHex)
	if err != nil {
		return ""
	}

	privKey, _ := btcec.PrivKeyFromBytes(btcec.S256(), pkBytes)
	plaintext, err := btcec.Decrypt(privKey, []byte(ciphertext))
	if err != nil {
		return ""
	}

	return string(plaintext)
}

func PayToAddrScript(addr, net string) ([]byte, error) {
	address, err := btcutil.DecodeAddress(addr, ChainConfig(net))
	if err != nil {
		return []byte{}, err
	}

	pkScript, err := txscript.PayToAddrScript(address)
	if err != nil {
		return []byte{}, err
	}

	return pkScript, nil
}

func ExtractPkScriptAddrs(pkScript []byte, net string) (string, error) {
	conf := ChainConfig(net)

	_, addrs, _, err := txscript.ExtractPkScriptAddrs(pkScript, conf)
	if err != nil {
		return "", err
	}

	arr := make([]string, 0)
	for _, v := range addrs {
		arr = append(arr, v.String())
	}

	b, err := json.Marshal(arr)
	if err != nil {
		return "", err
	}
	//fmt.Println("ExtractPkScriptAddrs : ", sc, addrs, requires)

	return string(b), nil
}

func DisasmString(script []byte) string {
	disasm, err := txscript.DisasmString(script)
	if err != nil {
		return ""
	}

	return disasm
}

//---------------------------------------------------------------------//

type KeyPair struct {
	wif *btcutil.WIF
	//pubKey   *btcec.PublicKey
	chaincfg *chaincfg.Params
}

func NewKeyPairRandom(net string, compress bool) (*KeyPair, error) {
	conf := ChainConfig(net)
	privKey, err := btcec.NewPrivateKey(btcec.S256())
	if err != nil {
		return &KeyPair{}, err
	}

	wif, err := btcutil.NewWIF(privKey, conf, compress)
	if err != nil {
		return &KeyPair{}, err
	}

	kp := &KeyPair{}
	kp.wif = wif
	kp.chaincfg = conf

	return kp, nil

}

func NewKeyPairFromBytes(net, keyHex string, compress bool) (*KeyPair, error) {
	conf := ChainConfig(net)
	privKeyBytes, err := hex.DecodeString(keyHex)
	if err != nil {
		return &KeyPair{}, err
	}

	privKey, _ := btcec.PrivKeyFromBytes(btcec.S256(), privKeyBytes)
	wif, err := btcutil.NewWIF(privKey, conf, compress)
	if err != nil {
		return &KeyPair{}, err
	}

	kp := &KeyPair{}
	kp.wif = wif
	kp.chaincfg = conf

	return kp, nil
}

func NewKeyPairWIF(net, privHex string) (*KeyPair, error) {
	conf := ChainConfig(net)
	wif, err := btcutil.DecodeWIF(privHex)
	if err != nil {
		return &KeyPair{}, err
	}
	kp := &KeyPair{}
	kp.wif = wif
	kp.chaincfg = conf

	return kp, nil
}

func (kp KeyPair) GetPrivKey() string {
	//	wif, err := btcutil.NewWIF(kp.privKey, kp.chaincfg, false)
	//	if err != nil {
	//		return ""
	//	}
	//	return wif.String()

	return kp.wif.String()
}

func (kp KeyPair) GetPubKeyHash() []byte {
	return btcutil.Hash160(kp.GetPubKey())
}

func (kp KeyPair) GetPubKey() []byte {
	//return kp.wif.PrivKey.PubKey().SerializeUncompressed()
	return kp.wif.SerializePubKey()
}

//---------------------------------------------------------------------//

type AddressPubKey struct {
	address *btcutil.AddressPubKey
}

func NewAddressPubKey(serializedPubKey []byte, net string) (*AddressPubKey, error) {
	conf := ChainConfig(net)
	addr, err := btcutil.NewAddressPubKey(serializedPubKey, conf)
	if err != nil {
		return &AddressPubKey{}, err
	}
	kp := &AddressPubKey{}
	kp.address = addr

	return kp, nil
}

func (a *AddressPubKey) SerializeCompressed() []byte {
	return a.address.PubKey().SerializeCompressed()
}

func (a *AddressPubKey) SerializeUncompressed() []byte {
	return a.address.PubKey().SerializeUncompressed()
}

func (a *AddressPubKey) EncodeAddress() string {
	return a.address.EncodeAddress()
}

func (a *AddressPubKey) ScriptAddress() []byte {
	return a.address.ScriptAddress()
}

func (a *AddressPubKey) IsForNet(net string) bool {
	conf := ChainConfig(net)
	return a.address.IsForNet(conf)
}

func (a *AddressPubKey) String() string {
	return a.address.EncodeAddress()
}

func (a *AddressPubKey) AddressPubKeyHash() *AddressPubKeyHash {
	kp := &AddressPubKeyHash{}
	kp.address = a.address.AddressPubKeyHash()
	return kp
}

func (a *AddressPubKey) AddressWitnessPubKeyHash(net string) (*AddressWitnessPubKeyHash, error) {
	witnessProg := btcutil.Hash160(a.SerializeCompressed())
	return NewAddressWitnessPubKeyHash(witnessProg, net)
}

//---------------------------------------------------------------------//

type AddressPubKeyHash struct {
	address *btcutil.AddressPubKeyHash
}

func NewAddressPubKeyHash(pkHash []byte, net string) (*AddressPubKeyHash, error) {
	conf := ChainConfig(net)
	addr, err := btcutil.NewAddressPubKeyHash(pkHash, conf)
	if err != nil {
		return &AddressPubKeyHash{}, err
	}
	kp := &AddressPubKeyHash{}
	kp.address = addr

	return kp, nil
}

func (a *AddressPubKeyHash) EncodeAddress() string {
	return a.address.EncodeAddress()
}

func (a *AddressPubKeyHash) ScriptAddress() []byte {
	return a.address.ScriptAddress()
}

func (a *AddressPubKeyHash) IsForNet(net string) bool {
	conf := ChainConfig(net)
	return a.address.IsForNet(conf)
}

func (a *AddressPubKeyHash) String() string {
	return a.address.EncodeAddress()
}

func (a *AddressPubKeyHash) Hash160() []byte {
	return a.address.Hash160()[:]
}

func (a *AddressPubKeyHash) AddressScriptHash(net string) (*AddressScriptHash, error) {
	pkScript, err := txscript.PayToAddrScript(a.address)
	if err != nil {
		return &AddressScriptHash{}, err
	}

	bytes32 := make([]byte, 32)
	sum := sha256.Sum256(pkScript)
	copy(bytes32, sum[:])
	ripemd160 := btcutil.Hash160(bytes32)
	scriptAddr, err := NewAddressScriptHash(ripemd160, net)
	if err != nil {
		return &AddressScriptHash{}, err
	}

	return scriptAddr, nil
}

func (a *AddressPubKeyHash) AddressWitnessScriptHash(net string) (*AddressWitnessScriptHash, error) {
	pkScript, err := txscript.PayToAddrScript(a.address)
	if err != nil {
		return &AddressWitnessScriptHash{}, err
	}

	bytes32 := make([]byte, 32)
	sum := sha256.Sum256(pkScript)
	copy(bytes32, sum[:])
	scriptAddr, err := NewAddressWitnessScriptHash(bytes32, net)
	if err != nil {
		return &AddressWitnessScriptHash{}, err
	}

	return scriptAddr, nil

}

//---------------------------------------------------------------------//

type AddressScriptHash struct {
	address *btcutil.AddressScriptHash
}

// RIPEMD160(SHA256(script))
func NewAddressScriptHash(serializedScript []byte, net string) (*AddressScriptHash, error) {
	conf := ChainConfig(net)
	addr, err := btcutil.NewAddressScriptHash(serializedScript, conf)
	if err != nil {
		return &AddressScriptHash{}, err
	}
	kp := &AddressScriptHash{}
	kp.address = addr

	return kp, nil
}

func (a *AddressScriptHash) EncodeAddress() string {
	return a.address.EncodeAddress()
}

func (a *AddressScriptHash) ScriptAddress() []byte {
	return a.address.ScriptAddress()
}

func (a *AddressScriptHash) IsForNet(net string) bool {
	conf := ChainConfig(net)
	return a.address.IsForNet(conf)
}

func (a *AddressScriptHash) String() string {
	return a.address.EncodeAddress()
}

func (a *AddressScriptHash) Hash160() []byte {
	return a.address.Hash160()[:]
}

//---------------------------------------------------------------------//

type AddressWitnessPubKeyHash struct {
	address *btcutil.AddressWitnessPubKeyHash
}

// RIPEMD160(compressed public key)
func NewAddressWitnessPubKeyHash(witnessProg []byte, net string) (*AddressWitnessPubKeyHash, error) {
	conf := ChainConfig(net)
	addr, err := btcutil.NewAddressWitnessPubKeyHash(witnessProg, conf)
	if err != nil {
		return &AddressWitnessPubKeyHash{}, err
	}
	kp := &AddressWitnessPubKeyHash{}
	kp.address = addr

	return kp, nil
}

func (a *AddressWitnessPubKeyHash) EncodeAddress() string {
	return a.address.EncodeAddress()
}

func (a *AddressWitnessPubKeyHash) ScriptAddress() []byte {
	return a.address.ScriptAddress()
}

func (a *AddressWitnessPubKeyHash) IsForNet(net string) bool {
	conf := ChainConfig(net)
	return a.address.IsForNet(conf)
}

func (a *AddressWitnessPubKeyHash) String() string {
	return a.address.EncodeAddress()
}

func (a *AddressWitnessPubKeyHash) Hash160() []byte {
	return a.address.Hash160()[:]
}

func (a *AddressWitnessPubKeyHash) Hrp() string {
	return a.address.Hrp()
}

func (a *AddressWitnessPubKeyHash) WitnessVersion() byte {
	return a.address.WitnessVersion()
}

func (a *AddressWitnessPubKeyHash) WitnessProgram() []byte {
	return a.address.WitnessProgram()
}

//---------------------------------------------------------------------//

type AddressWitnessScriptHash struct {
	address *btcutil.AddressWitnessScriptHash
}

// SHA256(script)
func NewAddressWitnessScriptHash(witnessProg []byte, net string) (*AddressWitnessScriptHash, error) {
	conf := ChainConfig(net)
	addr, err := btcutil.NewAddressWitnessScriptHash(witnessProg, conf)
	if err != nil {
		return &AddressWitnessScriptHash{}, err
	}
	kp := &AddressWitnessScriptHash{}
	kp.address = addr

	return kp, nil
}

func (a *AddressWitnessScriptHash) EncodeAddress() string {
	return a.address.EncodeAddress()
}

func (a *AddressWitnessScriptHash) ScriptAddress() []byte {
	return a.address.ScriptAddress()
}

func (a *AddressWitnessScriptHash) IsForNet(net string) bool {
	conf := ChainConfig(net)
	return a.address.IsForNet(conf)
}

func (a *AddressWitnessScriptHash) String() string {
	return a.address.EncodeAddress()
}

func (a *AddressWitnessScriptHash) Hrp() string {
	return a.address.Hrp()
}

func (a *AddressWitnessScriptHash) WitnessVersion() byte {
	return a.address.WitnessVersion()
}

func (a *AddressWitnessScriptHash) WitnessProgram() []byte {
	return a.address.WitnessProgram()
}

//---------------------------------------------------------------------//

type AddressMultiSig struct {
	address         *btcutil.AddressScriptHash
	addressesPubKey []*btcutil.AddressPubKey
	Net             string
}

func NewAddressMultiSig(net string) (a *AddressMultiSig, err error) {
	a = new(AddressMultiSig)
	a.addressesPubKey = make([]*btcutil.AddressPubKey, 0)
	a.Net = net

	return
}

func (a *AddressMultiSig) AddPubKey(pubHex string) (addr string) {
	serializedPubKey, err := hex.DecodeString(pubHex)
	if err != nil {
		return
	}

	address, err := NewAddressPubKey(serializedPubKey, a.Net)
	if err != nil {
		return
	}

	for _, v := range a.addressesPubKey {
		if address.EncodeAddress() == v.EncodeAddress() {
			return
		}
	}

	a.addressesPubKey = append(a.addressesPubKey, address.address)
	return
}

func (a *AddressMultiSig) MultiSigScript(nrequire int) (addr string) {
	if nrequire > len(a.addressesPubKey) {
		return
	}

	pkScript, err := txscript.MultiSigScript(a.addressesPubKey, nrequire)
	if err != nil {
		return
	}

	scriptAddr, err := btcutil.NewAddressScriptHash(pkScript, ChainConfig(a.Net))
	if err != nil {
		return
	}

	a.address = scriptAddr
	addr = scriptAddr.EncodeAddress()
	return
}

func (a *AddressMultiSig) EncodeAddress() string {
	return a.address.EncodeAddress()
}

//---------------------------------------------------------------------//

type Client struct {
	client   *rpcclient.Client
	connCfg  *rpcclient.ConnConfig
	chaincfg *chaincfg.Params
	Net      string
	URL      string
}

func NewClient(net, host, username, password string) (*Client, error) {
	connCfg := &rpcclient.ConnConfig{}
	connCfg.Host = host
	connCfg.User = username
	connCfg.Pass = password
	connCfg.HTTPPostMode = true
	connCfg.DisableTLS = true

	client, err := rpcclient.New(connCfg, nil)
	if err != nil {
		return &Client{}, err
	}
	//defer client.Shutdown()

	cli := &Client{}
	cli.client = client
	cli.connCfg = connCfg
	cli.chaincfg = ChainConfig(net)
	cli.Net = net
	cli.URL = host

	return cli, nil
}

func (cli *Client) GetBlockCount() int64 {
	blockCount, err := cli.client.GetBlockCount()
	if err != nil {
		return 0
	}

	return blockCount
}

func (cli *Client) GetBestBlockHash() string {
	hs, err := cli.client.GetBestBlockHash()
	if err != nil {
		return ""
	}

	return hs.String()
}

func (cli *Client) GetBlock(blockHash string) string {
	hx, err := chainhash.NewHashFromStr(blockHash)
	if err != nil {
		return ""
	}

	block, err := cli.client.GetBlock(hx)
	if err != nil {
		return ""
	}

	b, err := json.MarshalIndent(block, "", " ")
	if err != nil {
		return ""
	}

	return string(b)
}

func (cli *Client) GetBlockVerbose(blockHash string) string {
	hx, err := chainhash.NewHashFromStr(blockHash)
	if err != nil {
		return ""
	}

	block, err := cli.client.GetBlockVerbose(hx)
	if err != nil {
		return ""
	}

	b, err := json.MarshalIndent(block, "", " ")
	if err != nil {
		return ""
	}

	return string(b)
}

func (cli *Client) GetBlockHash(blockHeight int64) string {
	hs, err := cli.client.GetBlockHash(blockHeight)
	if err != nil {
		return ""
	}

	return hs.String()
}

func (cli *Client) DecodeAddress(hex string) string {
	address, err := btcutil.DecodeAddress(hex, cli.chaincfg)
	if err != nil {
		return ""
	}

	return address.String()
}

func (cli *Client) WalletPassphrase(pass string, second int64) (bool, error) {
	//  WalletPassphrase
	err := cli.client.WalletPassphrase(pass, second)
	if err != nil {
		return false, err
	}

	return true, nil
}

// NOTE: This function requires to the wallet to be unlocked
func (cli *Client) DumpPrivKey(addr string) string {
	address, err := btcutil.DecodeAddress(addr, cli.chaincfg)
	if err != nil {
		return ""
	}

	// DumpPrivKey
	wif, err := cli.client.DumpPrivKey(address)
	if err != nil {
		return ""
	}

	return wif.String()
}

func (cli *Client) ImportPrivKey(prv, label string, rescan bool) error {
	wif, err := btcutil.DecodeWIF(prv)
	if err != nil {
		return err
	}

	return cli.client.ImportPrivKeyRescan(wif, label, rescan)
}

func (cli *Client) ImportAddress(addr string) error {
	return cli.client.ImportAddress(addr)
}

func (cli *Client) GetTransaction(txHex string) string {
	txHash, err := chainhash.NewHashFromStr(txHex)
	if err != nil {
		return ""
	}

	btcj, err := cli.client.GetTransaction(txHash)
	if err != nil {
		return ""
	}

	b, err := json.MarshalIndent(btcj, "", " ")
	if err != nil {
		return ""
	}

	return string(b)
}

func (cli *Client) GetRawTransaction(txHex string) string {
	txHash, err := chainhash.NewHashFromStr(txHex)
	if err != nil {
		return ""
	}

	btcj, err := cli.client.GetRawTransaction(txHash)
	if err != nil {
		return ""
	}

	b, err := json.MarshalIndent(btcj, "", " ")
	if err != nil {
		return ""
	}

	return string(b)
}

func (cli *Client) GetRawTransactionVerbose(txHex string) string {
	txHash, err := chainhash.NewHashFromStr(txHex)
	if err != nil {
		return ""
	}

	btcj, err := cli.client.GetRawTransactionVerbose(txHash)
	if err != nil {
		return ""
	}

	b, err := json.MarshalIndent(btcj, "", " ")
	if err != nil {
		return ""
	}

	return string(b)
}

func (cli *Client) Generate(num int32) string {
	bl, err := cli.client.Generate(uint32(num))
	if err != nil {
		return ""
	}

	b, err := json.MarshalIndent(bl, "", " ")
	if err != nil {
		return ""
	}

	return string(b)
}

func (cli *Client) SendToAddress(addr string, amount float64) string {
	address, err := btcutil.DecodeAddress(addr, cli.chaincfg)
	if err != nil {
		return ""
	}

	value, err := btcutil.NewAmount(amount)
	if err != nil {
		return ""
	}

	hs, err := cli.client.SendToAddress(address, value)
	if err != nil {
		fmt.Println("......error not SendToAddress.....")
		return ""
	}

	if cli.Net == "simnet" {
		cli.Generate(1)
	}

	return hs.String()
}
