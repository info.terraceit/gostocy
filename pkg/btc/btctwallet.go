package btc

import (
	"bytes"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"sync"

	"github.com/bartekn/go-bip39"
	"github.com/btcsuite/btcd/blockchain"
	"github.com/btcsuite/btcd/btcec"
	"github.com/btcsuite/btcd/btcjson"
	"github.com/btcsuite/btcd/chaincfg"
	"github.com/btcsuite/btcd/chaincfg/chainhash"
	"github.com/btcsuite/btcd/mempool"
	"github.com/btcsuite/btcd/txscript"
	"github.com/btcsuite/btcd/wire"
	"github.com/btcsuite/btcutil"
	"github.com/btcsuite/btcutil/hdkeychain"
	"gitlab.com/info.terraceit/gostocy/pkg/cryptocy"
)

const (
	// RedeemP2PKHSigScriptSize is the worst case (largest) serialize size
	// of a transaction input script that redeems a compressed P2PKH output.
	// It is calculated as:
	//
	//   - OP_DATA_73
	//   - 72 bytes DER signature + 1 byte sighash
	//   - OP_DATA_33
	//   - 33 bytes serialized compressed pubkey
	RedeemP2PKHSigScriptSize = 1 + 73 + 1 + 33

	// P2PKHPkScriptSize is the size of a transaction output script that
	// pays to a compressed pubkey hash.  It is calculated as:
	//
	//   - OP_DUP
	//   - OP_HASH160
	//   - OP_DATA_20
	//   - 20 bytes pubkey hash
	//   - OP_EQUALVERIFY
	//   - OP_CHECKSIG
	P2PKHPkScriptSize = 1 + 1 + 1 + 20 + 1 + 1

	// RedeemP2PKHInputSize is the worst case (largest) serialize size of a
	// transaction input redeeming a compressed P2PKH output.  It is
	// calculated as:
	//
	//   - 32 bytes previous tx
	//   - 4 bytes output index
	//   - 1 byte compact int encoding value 107
	//   - 107 bytes signature script
	//   - 4 bytes sequence
	RedeemP2PKHInputSize = 32 + 4 + 1 + RedeemP2PKHSigScriptSize + 4

	// P2PKHOutputSize is the serialize size of a transaction output with a
	// P2PKH output script.  It is calculated as:
	//
	//   - 8 bytes output value
	//   - 1 byte compact int encoding value 25
	//   - 25 bytes P2PKH output script
	P2PKHOutputSize = 8 + 1 + P2PKHPkScriptSize
)

func EstimateSerializeSize(inputCount int, outputStr string, addChangeOutput bool) int {
	var txOuts []*wire.TxOut
	err := json.Unmarshal([]byte(outputStr), &txOuts)
	if err != nil {
		return 0
	}

	changeSize := 0
	outputCount := len(txOuts)
	if addChangeOutput {
		changeSize = P2PKHOutputSize
		outputCount++
	}

	// 8 additional bytes are for version and locktime
	return 8 + wire.VarIntSerializeSize(uint64(inputCount)) +
		wire.VarIntSerializeSize(uint64(outputCount)) +
		inputCount*RedeemP2PKHInputSize +
		SumOutputSerializeSizes(outputStr) +
		changeSize
}

func EstimateSizePKH(inputCount, outputCount int) int {
	// 8 additional bytes are for version and locktime
	return 8 + wire.VarIntSerializeSize(uint64(inputCount)) +
		wire.VarIntSerializeSize(uint64(outputCount)) +
		inputCount*RedeemP2PKHInputSize +
		outputCount*P2PKHOutputSize
}

func FeeForSerializeSize(relayFeePerKb float64, txSerializeSize int) float64 {
	fee := relayFeePerKb * float64(txSerializeSize) / 1000

	if fee == 0 && relayFeePerKb > 0 {
		fee = relayFeePerKb
	}

	if fee < 0 || fee > btcutil.MaxSatoshi {
		fee = btcutil.MaxSatoshi
	}

	return fee
}

func SumOutputValues(outputStr string) float64 {
	var txOuts []*wire.TxOut
	err := json.Unmarshal([]byte(outputStr), &txOuts)
	if err != nil {
		return 0
	}

	totalOutput, _ := btcutil.NewAmount(0)
	for _, txOut := range txOuts {
		totalOutput += btcutil.Amount(txOut.Value)
	}
	return totalOutput.ToBTC()
}

func SumOutputSerializeSizes(outputStr string) (serializeSize int) {
	var txOuts []*wire.TxOut
	err := json.Unmarshal([]byte(outputStr), &txOuts)
	if err != nil {
		return 0
	}

	for _, txOut := range txOuts {
		serializeSize += txOut.SerializeSize()
	}
	return serializeSize
}

//---------------------------------------------------------------------//

var IsSyncing = true

var ManagedAccount = map[string]*Account{}              //	[addr]account
var ManagedAddress = map[string]*Address{}              //	[addr]pkscript
var ManagedTxs = map[string]map[string][]btcjson.Vout{} //	[addr]tx
var UTXOs = map[string][]btcjson.Vout{}                 //	[tx]{vout,vout}

func SaveSDK() string {
	mSave := make(map[string]string)
	b, err := json.MarshalIndent(ManagedAccount, "", " ")
	if err != nil {
		return ""
	}
	mSave["ManagedAccount"] = string(b)

	b, err = json.MarshalIndent(ManagedAddress, "", " ")
	if err != nil {
		return ""
	}
	mSave["ManagedAddress"] = string(b)

	b, err = json.MarshalIndent(ManagedTxs, "", " ")
	if err != nil {
		return ""
	}
	mSave["ManagedTxs"] = string(b)

	b, err = json.MarshalIndent(UTXOs, "", " ")
	if err != nil {
		return ""
	}
	mSave["UTXOs"] = string(b)

	mData, err := json.MarshalIndent(mSave, "", " ")
	if err != nil {
		return ""
	}

	return string(mData)
}

func LoadSDK(mData string) bool {
	mSave := make(map[string]string)
	err := json.Unmarshal([]byte(mData), &mSave)
	if err != nil {
		return false
	}

	b := mSave["ManagedAccount"]
	err = json.Unmarshal([]byte(b), &ManagedAccount)
	if err != nil {
		return false
	}

	b = mSave["ManagedAddress"]
	err = json.Unmarshal([]byte(b), &ManagedAddress)
	if err != nil {
		return false
	}

	b = mSave["ManagedTxs"]
	err = json.Unmarshal([]byte(b), &ManagedTxs)
	if err != nil {
		return false
	}

	b = mSave["UTXOs"]
	err = json.Unmarshal([]byte(b), &UTXOs)
	if err != nil {
		return false
	}

	return true
}

func SyncBlock(fromHeight, toHeight int64, cli *Client) {
	IsSyncing = true
	//ManagedTxs = make(map[string]map[string][]btcjson.Vout)

	// Syncing
	i := fromHeight
	for i <= toHeight {
		blockHash := cli.GetBlockHash(i)
		block := cli.GetBlockVerbose(blockHash)

		mblock := btcjson.GetBlockVerboseResult{}
		json.Unmarshal([]byte(block), &mblock)
		//fmt.Println(i, ".................. TXs : ", mblock.Tx)

		for _, tx := range mblock.Tx {
			txHash, err := chainhash.NewHashFromStr(tx)
			if err != nil {
				return
			}

			txRawResult, err := cli.client.GetRawTransactionVerbose(txHash)
			if err != nil {
				return
			}

			for _, vin := range txRawResult.Vin {
				if vin.IsCoinBase() {
					//fmt.Println(".................. IsCoinBase : ", tx)
					continue
				}

				if vouts, ok := UTXOs[vin.Txid]; ok {
					for i, vout := range vouts {
						if vin.Vout == vout.N {
							//fmt.Println("..............spendable........", vin.Txid, vout.N)
							UTXOs[vin.Txid] = append(UTXOs[vin.Txid][:i], UTXOs[vin.Txid][i+1:]...)

							if len(UTXOs[vin.Txid]) <= 0 {
								delete(UTXOs, vin.Txid)
							}
						}
					}
				}

				UTXOs[tx] = txRawResult.Vout
			}

			mVout := make(map[string][]btcjson.Vout)
			for _, vout := range txRawResult.Vout {
				for _, addr := range vout.ScriptPubKey.Addresses {
					//ManagedTxs[addr] = append(ManagedTxs[addr], tx)
					mVout[txRawResult.Txid] = txRawResult.Vout
					for txOld, arrV := range ManagedTxs[addr] {
						mVout[txOld] = arrV
					}
					ManagedTxs[addr] = mVout
				}
			}
		}
		i++
	}

	//b, _ := json.MarshalIndent(UTXOs, "", " ")
	//fmt.Println("..............utxos : ", string(b))

	IsSyncing = false
}

func ListUTXOs(net, addr string) (inputs string) {
	//	if IsSyncing {
	//		fmt.Println("........isSyncing........")
	//		return
	//	}

	// GO - Parse script by address
	pkScript, err := PayToAddrScript(addr, net)
	if err != nil {
		return
	}
	//asm := DisasmString(pkScript)
	//fmt.Println(".....address : ", addr, "..... pkScript : ", hex.EncodeToString(pkScript), "........ asm : ", asm)

	spendable := make(map[string]btcjson.Vout)
	for tx, vouts := range UTXOs {
		for _, vout := range vouts {
			if hex.EncodeToString(pkScript) == vout.ScriptPubKey.Hex {
				//fmt.Println(".........ListUTXOs........", tx)
				spendable[tx] = vout
				continue
			}

			if _, ok := spendable[tx]; !ok {
				// GO - check address is imported to wallet
				account, exitst := ManagedAccount[addr]
				if !exitst {
					//fmt.Println("........address not imported........", addr)
					continue
				}

				for i := account.IndexInternal - 1; i >= 0; i-- {
					addrInternal := account.AddressInternals[i].Address
					// GO - Parse script by address
					pkScript, err := PayToAddrScript(addrInternal, net)
					if err != nil {
						return
					}
					//asm := DisasmString(pkScript)
					//fmt.Println(".....address : ", addr, "..... pkScript : ", hex.EncodeToString(pkScript), "........ asm : ", asm)

					if hex.EncodeToString(pkScript) == vout.ScriptPubKey.Hex {
						//fmt.Println(".........ListUTXOs........", tx)
						spendable[tx] = vout
						continue
					}
				}
			}
		}
	}

	b, _ := json.MarshalIndent(spendable, "", " ")
	//fmt.Println(".........spendable........", string(b))
	inputs = string(b)
	return
}

func ListTransactions(addr string) string {
	// GO - check address is imported to wallet
	account, exitst := ManagedAccount[addr]
	if !exitst {
		//fmt.Println("........address not imported........", addr)
		return ""
	}

	mList := make([]map[string]interface{}, 0)
	for _, v := range account.AddressExternals {
		if mVout, ok := ManagedTxs[v.Address]; ok {
			for tx, vouts := range mVout {
				vList := make(map[string]interface{})
				vList["hash"] = tx
				for _, vout := range vouts {
					to := vout.ScriptPubKey.Addresses[0]
					if addr == to {
						vList["from"] = ""
						vList["to"] = to
						vList["value"] = vout.Value
					} else {
						vList["from"] = to
					}
				}
				mList = append(mList, vList)
			}
		}
	}

	for _, v := range account.AddressInternals {
		if mVout, ok := ManagedTxs[v.Address]; ok {
			for tx, vouts := range mVout {
				vList := make(map[string]interface{})
				vList["hash"] = tx
				for _, vout := range vouts {
					from := vout.ScriptPubKey.Addresses[0]
					if v.Address == from {
						//vList["from"] = from
						vList["from"] = addr
					} else {
						vList["to"] = from
						vList["value"] = vout.Value
					}
				}
				if _, ok := vList["from"]; ok {
					mList = append(mList, vList)
				}
			}
		}
	}

	b, _ := json.MarshalIndent(mList, "", " ")
	//fmt.Println("..............ListTransactions : ", string(b))
	return string(b)
}

func GetBalance(net, addr string) (balance float64) {
	//.......................Utxos..........................//
	inputs := ListUTXOs(net, addr)
	var utxos map[string]btcjson.Vout
	err := json.Unmarshal([]byte(inputs), &utxos)
	if err != nil {
		return
	}

	for _, utxo := range utxos {
		balance += utxo.Value
	}

	return
}

func SendRawTx(tx *Tx, allowHighFees bool, cli *Client) string {
	if !tx.Signed {
		fmt.Println("......error sendRawTX not Signed.....")
		return ""
	}

	hs, err := cli.client.SendRawTransaction(tx.MsgTx, allowHighFees)
	if err != nil {
		fmt.Println("......error not sendRawTX.....", err)
		return ""
	}

	if tx.Net == "simnet" {
		cli.Generate(1)
	}

	return hs.String()
}

//---------------------------------------------------------------------//

type Address struct {
	Address string `json:"address"`
	Pubkey  string `json:"pubkey"`
	Privkey string `json:"privkey"`
	Script  string `json:"script"`
}

type Account struct {
	Key              *hdkeychain.ExtendedKey `json:"key"`
	ExternalKey      *hdkeychain.ExtendedKey `json:"externalKey"`
	InternalKey      *hdkeychain.ExtendedKey `json:"internalKey"`
	AddressExternals []*Address              `json:"addressExternals"`
	AddressInternals []*Address              `json:"addressInternals"`

	Net           string `json:"net"`
	Salt          string `json:"salt"`
	IndexExternal int32  `json:"indexExternal"`
	IndexInternal int32  `json:"indexInternal"`
}

// m/44'/cointype'/0'/0/i
func NewAccount(net string, salt string, seed []byte) (acc *Account, err error) {
	acc = new(Account)
	acc.Net = net
	acc.Salt = salt
	acc.AddressExternals = make([]*Address, 0)
	acc.AddressInternals = make([]*Address, 0)
	indexs := cryptocy.MultiHash(salt)
	// gen key
	acc.GenKey(seed, indexs...)

	// gen first public address
	acc.NextAddress(false)

	return
}

func (acc *Account) GenKey(seed []byte, index ...uint32) error {
	conf := ChainConfig(acc.Net)

	//m/
	ext, err := hdkeychain.NewMaster(seed, conf)
	if err != nil {
		return err
	}

	// m/44'
	purpose, err := ext.Child(44 + hdkeychain.HardenedKeyStart)
	if err != nil {
		return err
	}

	// m/44'/altcointype'
	coinType, err := purpose.Child(uint32(0) + hdkeychain.HardenedKeyStart)
	if err != nil {
		return err
	}

	// m/44'/altcointype'/0'
	acct0, err := coinType.Child(uint32(0) + hdkeychain.HardenedKeyStart)
	if err != nil {
		return err
	}

	// m/44'/altcointype'/0'/0
	// 0 = external accounts for receive addresses
	acct0External, err := acct0.Child(0)
	if err != nil {
		return err
	}

	var receive *hdkeychain.ExtendedKey
	for i, v := range index {
		if i == 0 {
			receive, err = acct0External.Child(uint32(v))
			if err != nil {
				//"Failed to create %v receive address: %s"
				return err
			}
			continue
		}
		receive, err = receive.Child(uint32(v))
		if err != nil {
			//"Failed to create %v receive address: %s"
			return err
		}
	}
	acc.ExternalKey = receive

	// 1 = internal accounts for change
	acct0Internal, err := receive.Child(1)
	if err != nil {
		return err
	}
	acc.InternalKey = acct0Internal
	return nil
}

func (c *Account) NextAddress(isInternal bool) string {
	conf := ChainConfig(c.Net)
	index := 0
	key := &hdkeychain.ExtendedKey{}

	if isInternal {
		index = len(c.AddressInternals)
		receive, err := c.InternalKey.Child(uint32(index))
		if err != nil {
			return ""
		}
		key = receive
	} else {
		index = len(c.AddressExternals)
		receive, err := c.ExternalKey.Child(uint32(index))
		if err != nil {
			return ""
		}
		key = receive
	}

	privk, err := key.ECPrivKey()
	if err != nil {
		return ""
	}

	wif, err := btcutil.NewWIF(privk, conf, true)
	if err != nil {
		return ""
	}

	pubk, err := key.ECPubKey()
	if err != nil {
		return ""
	}

	address, err := key.Address(conf)
	if err != nil {
		return ""
	}

	addr := new(Address)
	addr.Address = address.String()
	addr.Pubkey = hex.EncodeToString(pubk.SerializeCompressed())
	addr.Privkey = wif.String()
	c.ImportAddress(addr, isInternal)

	return addr.Address
}

func (c *Account) ImportAddress(addr *Address, isInternal bool) {
	if isInternal {
		c.AddressInternals = append(c.AddressInternals, addr)
		c.IndexInternal++
	} else {
		c.AddressExternals = append(c.AddressExternals, addr)
		c.IndexExternal++
	}

	// GO : map[addr]account
	ManagedAccount[addr.Address] = c

	// GO - Parse script by address
	pkScript, err := PayToAddrScript(addr.Address, c.Net)
	if err == nil {
		addr.Script = hex.EncodeToString(pkScript)
	}
	ManagedAddress[addr.Address] = addr
}

func (c *Account) ListExAddress() string {
	b, err := json.MarshalIndent(c.AddressExternals, "", " ")
	if err != nil {
		return ""
	}

	return string(b)
}

func (c *Account) GetAddress() string {
	return c.AddressExternals[0].Address
}

func (c *Account) ListInAddress() string {
	b, err := json.MarshalIndent(c.AddressInternals, "", " ")
	if err != nil {
		return ""
	}

	return string(b)
}

func (c *Account) DumpprivKey() (privKey string) {
	if len(c.AddressExternals) <= 0 {
		return
	}

	privKey = c.AddressExternals[0].Privkey
	return
}

func (c *Account) GetPubKey() (pubKey string) {
	if len(c.AddressExternals) <= 0 {
		return
	}

	pubKey = c.AddressExternals[0].Pubkey
	return
}

func (c *Account) String() string {
	b, err := json.MarshalIndent(c, "", " ")
	if err != nil {
		return ""
	}
	return string(b)
}

//---------------------------------------------------------------------//

type Wallet struct {
	mux *sync.Mutex

	Net      string `json:"net"`
	Password string `json:"password"`
	Mnemoric string `json:"mnemoric"`
	Seed     []byte `json:"seed"`
	//Accounts      []*Account `json:"accounts"`
	Accounts      map[string]*Account `json:"accounts"`
	RelayFeePerKb float64             `json:"relayFeePerKb"`

	Block_genesis int64 `json:"block_genesis"`

	//
	Balance float64 `json:"balance"`
	ListTX  string  `json:"listTX"`
}

func NewWallet(net, mnemoric, password string) (w *Wallet, err error) {
	seed, err := bip39.NewSeedWithErrorChecking(mnemoric, password)
	if err != nil {
		return
	}

	w = new(Wallet)
	w.mux = &sync.Mutex{}
	w.Password = password
	w.Mnemoric = mnemoric
	w.Seed = seed
	w.Net = net
	w.RelayFeePerKb = float64(0.00001)
	//w.Accounts = make([]*Account, 0)
	w.Accounts = make(map[string]*Account)
	return
}

func NewWalletSeed(net string, seed []byte) (w *Wallet, err error) {
	w = new(Wallet)
	w.mux = &sync.Mutex{}
	w.Seed = seed
	w.Net = net
	w.RelayFeePerKb = float64(0.00001)
	//w.Accounts = make([]*Account, 0)
	w.Accounts = make(map[string]*Account)
	return
}

func (w *Wallet) Restore(seedHex string) {
	seed, err := hex.DecodeString(seedHex)
	if err != nil {
		return
	}

	for i, acc := range w.Accounts {

		indexs := cryptocy.MultiHash(i)
		acc.GenKey(seed, indexs...)

		for _, ext := range acc.AddressExternals {
			ManagedAccount[ext.Address] = acc
			ManagedAddress[ext.Address] = ext
		}

		for _, ints := range acc.AddressInternals {
			ManagedAccount[ints.Address] = acc
			ManagedAddress[ints.Address] = ints
		}
	}
}

func (w *Wallet) CreateAccount(salt string) (acc *Account, err error) {
	w.mux.Lock()
	defer w.mux.Unlock()

	acc, err = NewAccount(w.Net, salt, w.Seed)
	if err != nil {
		return
	}
	//w.Accounts = append(w.Accounts, acc)
	w.Accounts[salt] = acc
	return
}

func (w *Wallet) ImportAddress(address string, salt string, isInternal bool) bool {
	addr := &Address{}
	addr.Address = address
	w.Accounts[salt].ImportAddress(addr, isInternal)
	return true
}

func (w *Wallet) ListAccounts() string {
	b, err := json.MarshalIndent(w.Accounts, "", " ")
	if err != nil {
		return ""
	}

	return string(b)
}

func (w *Wallet) ListTransactions() string {
	return w.ListTX
}

func (w *Wallet) GetAccount(salt string) (acc *Account) {
	w.mux.Lock()
	defer w.mux.Unlock()

	acc = w.Accounts[salt]
	return
}

func (w *Wallet) GetBalance() float64 {
	return w.Balance
}

//---------------------------------------------------------------------//

type Tx struct {
	Signed bool
	Net    string
	From   string
	Tos    map[string]float64
	MsgTx  *wire.MsgTx
}

func (w *Wallet) NewTX(net string) *Tx {
	tx := new(Tx)
	tx.Signed = false
	tx.Net = net
	tx.Tos = make(map[string]float64)
	tx.MsgTx = new(wire.MsgTx)

	return tx
}

func (tx *Tx) AddSender(from string) {
	_, err := btcutil.DecodeAddress(from, ChainConfig(tx.Net))
	if err != nil {
		return
	}
	tx.From = from
}

func (tx *Tx) AddReceipt(to string, amount float64) {
	if _, ok := tx.Tos[to]; ok {
		return
	}

	_, err := btcutil.DecodeAddress(to, ChainConfig(tx.Net))
	if err != nil {
		return
	}
	tx.Tos[to] = amount
}

func (tx *Tx) CreateRawTx(relayFeePerKb float64) (mtx string) {
	//.......................Account..........................//
	account, exitst := ManagedAccount[tx.From]
	if !exitst {
		fmt.Println(".........external address not imported........", tx.From)
		return
	}

	//.......................Utxos..........................//
	inputs := ListUTXOs(tx.Net, tx.From)
	var utxos map[string]btcjson.Vout
	err := json.Unmarshal([]byte(inputs), &utxos)
	if err != nil {
		return
	}
	//b, _ := json.MarshalIndent(utxos, "", " ")
	//fmt.Println(".........utxos........", string(b))

	//.......................fetchInputs..........................//
	fetchInputs := func(target float64) (total float64, inputs []*wire.TxIn) {
		for txID, utxo := range utxos {
			txHash, err := chainhash.NewHashFromStr(txID)
			if err != nil {
				return
			}

			prevOut := wire.NewOutPoint(txHash, utxo.N)
			txIn := wire.NewTxIn(prevOut, []byte{}, nil)
			inputs = append(inputs, txIn)
			total += utxo.Value
			if total >= target {
				break
			}
		}
		return
	}

	//.......................TxOut..........................//
	outputs := make([]*wire.TxOut, 0)
	for to, amount := range tx.Tos {
		address, err := btcutil.DecodeAddress(to, ChainConfig(tx.Net))
		if err != nil {
			return
		}

		// Create a new script which pays to the provided address.
		pkScript, err := txscript.PayToAddrScript(address)
		if err != nil {
			return
		}

		// Convert the amount to satoshi.
		satoshi, err := btcutil.NewAmount(amount)
		if err != nil {
			return
		}

		txOut := wire.NewTxOut(int64(satoshi), pkScript)
		outputs = append(outputs, txOut)
	}

	bOut, errOut := json.MarshalIndent(outputs, "", " ")
	if errOut != nil {
		return
	}
	outputStr := string(bOut)

	//.......................TxIn..........................//
	targetAmount := SumOutputValues(outputStr)
	estimatedSize := EstimateSerializeSize(1, outputStr, true)
	targetFee := FeeForSerializeSize(relayFeePerKb, estimatedSize)

	for {
		inputAmount, inputs := fetchInputs(targetAmount + targetFee)
		//fmt.Println("..........inputAmount targetAmount targetFee ............", strconv.FormatFloat(inputAmount, 'f', -1, 64), strconv.FormatFloat(targetAmount, 'f', -1, 64), strconv.FormatFloat(targetFee, 'f', -1, 64))

		if inputAmount < targetAmount+targetFee {
			return
		}

		maxSignedSize := EstimateSerializeSize(len(inputs), outputStr, true)
		maxRequiredFee := FeeForSerializeSize(relayFeePerKb, maxSignedSize)
		remainingAmount := inputAmount - targetAmount
		//fmt.Println("..........maxRequiredFee remainingAmount......................", strconv.FormatFloat(maxRequiredFee, 'f', -1, 64), strconv.FormatFloat(remainingAmount, 'f', -1, 64))

		if remainingAmount < maxRequiredFee {
			targetFee = maxRequiredFee
			continue
		}

		//.......................MsgTx..........................//
		tx.MsgTx.Version = wire.TxVersion
		tx.MsgTx.TxIn = inputs
		tx.MsgTx.TxOut = outputs
		tx.MsgTx.LockTime = 0

		//.......................Change..........................//
		changeAmount := inputAmount - targetAmount - maxRequiredFee
		if changeAmount > 0 {
			changeAddr := account.NextAddress(true)
			if changeAddr == "" {
				return
			}

			pkScript := ManagedAddress[changeAddr].Script
			if pkScript == "" {
				return
			}

			script, err := hex.DecodeString(pkScript)
			if err != nil {
				return
			}

			// Convert the amount to satoshi.
			satoshi, err := btcutil.NewAmount(changeAmount)
			if err != nil {
				return
			}

			txOut := wire.NewTxOut(int64(satoshi), script)
			tx.MsgTx.AddTxOut(txOut)
			break
		}
	}

	b, _ := json.MarshalIndent(tx.MsgTx, "", " ")
	//fmt.Println(".........mtx........", string(b))
	mtx = string(b)
	return
}

func (tx *Tx) SignTx() (rawTx string) {
	if tx.Signed {
		return
	}

	if len(tx.MsgTx.TxIn) <= 0 {
		return
	}

	if len(tx.MsgTx.TxOut) <= 0 {
		return
	}

	//.......................SignTxOut..........................//
	getKey := txscript.KeyClosure(func(a btcutil.Address) (*btcec.PrivateKey, bool, error) {
		sender, ok := ManagedAddress[a.EncodeAddress()]
		if !ok {
			fmt.Println(".........address not imported........", a.EncodeAddress())
			return nil, false, errors.New("address not imported.")
		}

		wif, err := btcutil.DecodeWIF(sender.Privkey)
		if err != nil {
			return nil, false, errors.New("no key for address")
		}

		return wif.PrivKey, wif.CompressPubKey, nil
	})

	getScript := txscript.ScriptClosure(func(a btcutil.Address) ([]byte, error) {
		sender, ok := ManagedAddress[a.EncodeAddress()]
		if !ok {
			fmt.Println(".........address not imported........", a.EncodeAddress())
			return []byte{}, errors.New("address not imported.")
		}

		script, err := hex.DecodeString(sender.Script)
		if err != nil {
			return []byte{}, errors.New("no script for address")
		}

		return script, nil
	})

	//	switch *cmd.Flags {
	//	case "ALL":
	//		hashType = txscript.SigHashAll
	//	case "NONE":
	//		hashType = txscript.SigHashNone
	//	case "SINGLE":
	//		hashType = txscript.SigHashSingle
	//	case "ALL|ANYONECANPAY":
	//		hashType = txscript.SigHashAll | txscript.SigHashAnyOneCanPay
	//	case "NONE|ANYONECANPAY":
	//		hashType = txscript.SigHashNone | txscript.SigHashAnyOneCanPay
	//	case "SINGLE|ANYONECANPAY":
	//		hashType = txscript.SigHashSingle | txscript.SigHashAnyOneCanPay
	//	default:
	//		e := errors.New("Invalid sighash parameter")
	//		return nil, InvalidParameterError{e}
	//	}

	hashType := txscript.SigHashAll
	for i, txIn := range tx.MsgTx.TxIn {
		prevHash := &txIn.PreviousOutPoint.Hash
		prevIndex := txIn.PreviousOutPoint.Index
		preVout, ok := UTXOs[prevHash.String()]
		if !ok {
			return
		}

		prevOutScript := make([]byte, 0)
		prevAmount, _ := btcutil.NewAmount(0)
		for _, p := range preVout {
			if p.N == prevIndex {
				b, err := hex.DecodeString(p.ScriptPubKey.Hex)
				if err != nil {
					return
				}

				am, err := btcutil.NewAmount(p.Value)
				if err != nil {
					return
				}

				prevOutScript = b
				prevAmount = am
				break
			}
		}

		script := tx.MsgTx.TxIn[i].SignatureScript
		sigScript, err := txscript.SignTxOutput(ChainConfig(tx.Net), tx.MsgTx, i, prevOutScript, hashType, getKey, getScript, script)
		if err != nil {
			return
		}
		tx.MsgTx.TxIn[i].SignatureScript = sigScript

		// Either it was already signed or we just signed it.
		// Find out if it is completely satisfied or still needs more.
		vm, err := txscript.NewEngine(prevOutScript, tx.MsgTx, i, txscript.StandardVerifyFlags, nil, nil, int64(prevAmount))
		if err == nil {
			err = vm.Execute()
		}
		if err != nil {
			fmt.Println(".........Engine Fail........", err)
			return
		} else {
			fmt.Println(".........Engine Execute OK........")
		}
	}

	b, _ := json.MarshalIndent(tx.MsgTx, "", " ")
	//fmt.Println(".........rawTX........", string(b))
	rawTx = string(b)
	tx.Signed = true
	return
}

func (tx *Tx) String() string {
	// maxProtocolVersion is the max protocol version the server supports.
	maxProtocolVersion := uint32(70002)
	messageToHex := func(msg wire.Message) (string, error) {
		var buf bytes.Buffer
		if err := msg.BtcEncode(&buf, maxProtocolVersion, wire.WitnessEncoding); err != nil {
			return "", errors.New("Failed to encode msg")
		}

		return hex.EncodeToString(buf.Bytes()), nil
	}

	witnessToHex := func(witness wire.TxWitness) []string {
		if len(witness) == 0 {
			return nil
		}

		result := make([]string, 0, len(witness))
		for _, wit := range witness {
			result = append(result, hex.EncodeToString(wit))
		}

		return result
	}

	createVinList := func(mtx *wire.MsgTx) []btcjson.Vin {
		// Coinbase transactions only have a single txin by definition.
		vinList := make([]btcjson.Vin, len(mtx.TxIn))
		if blockchain.IsCoinBaseTx(mtx) {
			txIn := mtx.TxIn[0]
			vinList[0].Coinbase = hex.EncodeToString(txIn.SignatureScript)
			vinList[0].Sequence = txIn.Sequence
			vinList[0].Witness = witnessToHex(txIn.Witness)
			return vinList
		}

		for i, txIn := range mtx.TxIn {
			disbuf, _ := txscript.DisasmString(txIn.SignatureScript)
			vinEntry := &vinList[i]
			vinEntry.Txid = txIn.PreviousOutPoint.Hash.String()
			vinEntry.Vout = txIn.PreviousOutPoint.Index
			vinEntry.Sequence = txIn.Sequence
			vinEntry.ScriptSig = &btcjson.ScriptSig{
				Asm: disbuf,
				Hex: hex.EncodeToString(txIn.SignatureScript),
			}

			if mtx.HasWitness() {
				vinEntry.Witness = witnessToHex(txIn.Witness)
			}
		}
		return vinList
	}

	createVoutList := func(mtx *wire.MsgTx, chainParams *chaincfg.Params, filterAddrMap map[string]struct{}) []btcjson.Vout {
		voutList := make([]btcjson.Vout, 0, len(mtx.TxOut))
		for i, v := range mtx.TxOut {
			disbuf, _ := txscript.DisasmString(v.PkScript)
			scriptClass, addrs, reqSigs, _ := txscript.ExtractPkScriptAddrs(v.PkScript, chainParams)
			passesFilter := len(filterAddrMap) == 0
			encodedAddrs := make([]string, len(addrs))
			for j, addr := range addrs {
				encodedAddr := addr.EncodeAddress()
				encodedAddrs[j] = encodedAddr
				if passesFilter {
					continue
				}

				if _, exists := filterAddrMap[encodedAddr]; exists {
					passesFilter = true
				}
			}

			if !passesFilter {
				continue
			}

			var vout btcjson.Vout
			vout.N = uint32(i)
			vout.Value = btcutil.Amount(v.Value).ToBTC()
			vout.ScriptPubKey.Addresses = encodedAddrs
			vout.ScriptPubKey.Asm = disbuf
			vout.ScriptPubKey.Hex = hex.EncodeToString(v.PkScript)
			vout.ScriptPubKey.Type = scriptClass.String()
			vout.ScriptPubKey.ReqSigs = int32(reqSigs)
			voutList = append(voutList, vout)
		}

		return voutList
	}

	mtxHex, err := messageToHex(tx.MsgTx)
	if err != nil {
		return ""
	}

	txReply := new(btcjson.TxRawResult)
	txReply.Hex = mtxHex
	//Txid:     txHash,
	txReply.Hash = tx.MsgTx.WitnessHash().String()
	txReply.Size = int32(tx.MsgTx.SerializeSize())
	txReply.Vsize = int32(mempool.GetTxVirtualSize(btcutil.NewTx(tx.MsgTx)))
	txReply.Vin = createVinList(tx.MsgTx)
	txReply.Vout = createVoutList(tx.MsgTx, ChainConfig(tx.Net), nil)
	txReply.Version = tx.MsgTx.Version
	txReply.LockTime = tx.MsgTx.LockTime

	b, err := json.MarshalIndent(txReply, "", " ")
	if err != nil {
		return ""
	}

	return string(b)
}
