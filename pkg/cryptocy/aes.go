package cryptocy

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"encoding/hex"
	"fmt"

	//"encoding/hex"
	"errors"
	"io"

	//"log"

	"github.com/ethereum/go-ethereum/crypto"
)

func Keccak(message []byte) []byte {
	return crypto.Keccak256(message)
}

func XOR(input, key []byte) []byte {
	result := make([]byte, len(key))
	for i := 0; i < len(key); i++ {
		result[i] = key[i] ^ input[i]
	}
	return result
}

func Encrypt(key, message []byte) (string, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	//IV needs to be unique, but doesn't have to be secure.
	//It's common to put it at the beginning of the ciphertext.
	cipherText := make([]byte, aes.BlockSize+len(message))
	iv := cipherText[:aes.BlockSize]
	if _, err = io.ReadFull(rand.Reader, iv); err != nil {
		return "", err
	}

	//	log.Println("block size : ", aes.BlockSize)
	//	log.Println("cipher byte : ", hex.EncodeToString(key), "  size : ", len(key))
	//	log.Println("cipherText : ", hex.EncodeToString(cipherText))
	//	log.Println("iv : ", hex.EncodeToString(iv), "   size : ", len(iv))

	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(cipherText[aes.BlockSize:], message)

	//log.Println("cipherText : ", cipherText[aes.BlockSize:])

	//returns to base64 encoded string
	return base64.URLEncoding.EncodeToString(cipherText), nil
}

func Decrypt(key []byte, securemess string) (string, error) {
	cipherText, err := base64.URLEncoding.DecodeString(securemess)
	if err != nil {
		//log.Println(err)
		return "", err
	}

	block, err := aes.NewCipher(key)
	if err != nil {
		//log.Println(err)
		return "", err
	}

	if len(cipherText) < aes.BlockSize {
		err = errors.New("Ciphertext block size is too short!")
		//log.Println(err)
		return "", err
	}

	iv := cipherText[:aes.BlockSize]
	cipherText = cipherText[aes.BlockSize:]

	stream := cipher.NewCFBDecrypter(block, iv)
	// XORKeyStream can work in-place if the two arguments are the same.
	stream.XORKeyStream(cipherText, cipherText)

	return string(cipherText), nil
}

func Encrypt256(key, message []byte) (string, error) {
	//Since the key is in string, we need to convert decode it to bytes
	// key, _ := hex.DecodeString(keyString)
	// plaintext := []byte(stringToEncrypt)

	//Create a new Cipher Block from the key
	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	//Create a new GCM - https://en.wikipedia.org/wiki/Galois/Counter_Mode
	//https://golang.org/pkg/crypto/cipher/#NewGCM
	aesGCM, err := cipher.NewGCM(block)
	if err != nil {
		return "", err
	}

	//Create a nonce. Nonce should be from GCM
	nonce := make([]byte, aesGCM.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		return "", err
	}

	//Encrypt the data using aesGCM.Seal
	//Since we don't want to save the nonce somewhere else in this case, we add it as a prefix to the encrypted data. The first nonce argument in Seal is the prefix.
	ciphertext := aesGCM.Seal(nonce, nonce, message, nil)
	return fmt.Sprintf("%x", ciphertext), nil
}

func Decrypt256(key []byte, encryptedString string) (string, error) {
	//key, _ := hex.DecodeString(keyString)
	enc, _ := hex.DecodeString(encryptedString)

	//Create a new Cipher Block from the key
	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	//Create a new GCM
	aesGCM, err := cipher.NewGCM(block)
	if err != nil {
		return "", err

	}

	//Get the nonce size
	nonceSize := aesGCM.NonceSize()

	//Extract the nonce from the encrypted data
	nonce, ciphertext := enc[:nonceSize], enc[nonceSize:]

	//Decrypt the data
	plaintext, err := aesGCM.Open(nil, nonce, ciphertext, nil)
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("%s", plaintext), nil
}
