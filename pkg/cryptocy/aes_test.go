package cryptocy

import (
	"fmt"
	"log"

	"testing"
)

/*
func Test_1(t *testing.T) {
	CIPHER_KEY := []byte("01234567890123450123456789012345")

	js := map[string]interface{}{
		"amount": 100,
		"to":     "0x5e531bb813994f27f65d2d5f7dc7c51dbe5406f7",
	}
	bb, _ := json.MarshalIndent(js, " ", "")
	msg := string(bb)

	if encrypted, err := Encrypt(CIPHER_KEY, msg); err != nil {
		log.Println(err)
	} else {
		log.Printf("CIPHER KEY: %s\n", string(CIPHER_KEY))
		log.Printf("ENCRYPTED: %s\n", encrypted)

		if decrypted, err := Decrypt(CIPHER_KEY, encrypted); err != nil {
			log.Println(err)
		} else {
			log.Printf("DECRYPTED: %s\n", decrypted)
		}
	}
}
*/

func Test_2(t *testing.T) {
	CIPHER_KEY := Keccak([]byte("5KNaNVobFvHT7zQ4AX3hEQ7wGfxhWQxNguZU5p7brWG6VUR3hSB"))
	msg := "SCS4IITATPILLHQDGMWWZPJROJLWTZVPYTNEOG57ZPJXGBNXGUTRWMG3"

	if encrypted, err := Encrypt(CIPHER_KEY, []byte(msg)); err != nil {
		log.Println(err)
	} else {
		log.Printf("CIPHER KEY: %s\n", string(CIPHER_KEY))
		log.Printf("ENCRYPTED: %s\n", encrypted)

		if decrypted, err := Decrypt(CIPHER_KEY, encrypted); err != nil {
			log.Println(err)
		} else {
			log.Printf("DECRYPTED: %s\n", decrypted)
		}
	}
}

/*
func Test_3(t *testing.T) {

	iv, _ := base64.StdEncoding.DecodeString("AJf3QItKM7+Lkh/BZT2xNg==")
	key := []byte("1234567890abcdef")

	//text, _ := ioutil.ReadAll(os.Stdin)

	msg := []byte("SCS4IITATPILLHQDGMWWZPJROJLWTZVPYTNEOG57ZPJXGBNXGUTRWMG3")
	text := make([]byte, 0)

	cipherBlock, err := aes.NewCipher(key)
	if err != nil {
		log.Fatal(err)
	}

	cipher.NewCBCDecrypter(cipherBlock, iv).CryptBlocks(text, msg)
	fmt.Println("...x.x.x.x.x.x.x.x.....", string(text))

}
*/

func TestAesEncDec256(t *testing.T) {
	CIPHER_KEY := Keccak([]byte("1234567890"))
	msg := "Hello AES-256"

	encrypted, _ := Encrypt256(CIPHER_KEY, []byte(msg))
	fmt.Println("...... encrypted :", encrypted)
	decrypted, _ := Decrypt256(CIPHER_KEY, encrypted)
	fmt.Println("...... decrypted :", decrypted)
}
