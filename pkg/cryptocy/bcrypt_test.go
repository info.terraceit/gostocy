package cryptocy

import (
	"fmt"
	"testing"

	"golang.org/x/crypto/bcrypt"
)

func Test1(t *testing.T) {
	hash := "$2a$14$P195bOm6K34eyzNW.rtSh.mBC1HbgjY45doYPak3L5DvFf24gGF0m"
	pass := "123456789"
	b, _ := bcrypt.GenerateFromPassword([]byte(pass), 14)
	bStr := string(b)
	fmt.Println(string(bStr))

	if bStr != hash {
		fmt.Println("xxxxxxxxxxxxxxx")
	}
}

func Test2(t *testing.T) {
	pass := []byte("allmine")
	salt := []byte("3.14159265358979323846") // 22 bytes
	//expectedHash := []byte("$2a$10$XajjQvNhvvRt5GSeFk1xFeyqRrsxkhBkUiQeg0dt.wU1qD4aFDcga")

	hash, err := Bcrypt(pass, 10, salt)
	fmt.Println(err, string(base64Encode(hash)))
}
