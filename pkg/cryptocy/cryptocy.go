package cryptocy

import (
	"crypto/sha256"
	"errors"
	"strings"

	"github.com/ethereum/go-ethereum/common"
)

var (
	delimiter = "|"
	LOOP_PIN  = 1000
)

func genSecretPIN(PIN string) []byte {
	// GO-0 : input validator PIN
	if len(PIN) < 8 {
		//fmt.Println("error pin")
		return nil
	}

	// GO-1 : keccak pin
	pinKeccak := Keccak([]byte(PIN))
	if len(pinKeccak) <= 0 {
		//fmt.Println("error pinKeccak")
		return nil
	}
	//fmt.Println("..... pin :", PIN, " keccak :", hex.EncodeToString(pinKeccak))

	// GO-2 : bcrypt 1000 times
	// salt := []byte("3.141592653589793238462643383279") // 32 bytes -> 256 bits
	salt := []byte("3.14159265358979323846") // 22 bytes
	result := pinKeccak
	//fmt.Println(fmt.Sprintf("...... bscrypting %v times", n))
	for i := 0; i < LOOP_PIN; i++ {
		//fmt.Println(".....salt :", string(base64Encode(salt)), " len :", len(salt))
		// hash, err := Scrypt(pinKeccak, salt)
		hash, err := Bcrypt(result, 11, salt)
		if err != nil {
			//fmt.Println(err)
			return nil
		}
		result = hash
		// salt = XOR(hash, salt)
		salt = base64Encode(XOR(hash, salt))[:encodedSaltSize]

		//fmt.Println(fmt.Sprintf("... hash[%v] %s", i, string(base64Encode(hash))))
	}
	//fmt.Println("...... bscrypted time :", time.Now().Unix()-begin.Unix())
	//fmt.Println("...... cipherPass :", hex.EncodeToString(result))

	// GO-3 : blake hash
	blakeInput := append(result, salt...)
	blake := sha256.New()
	blake.Write([]byte(blakeInput))
	secret := blake.Sum(nil)
	//fmt.Println("...... secret:", hex.EncodeToString(secret))

	return secret
}

func EncryptMasterKeyPIN(PIN, masterSeed string) []byte {
	secret := genSecretPIN(PIN)
	if secret == nil {
		return nil
	}

	// Hash 32 bytes - 256 bits for masterSeed
	encrypted, err := Encrypt256(secret, Keccak([]byte(masterSeed)))
	if err != nil {
		return nil
	}
	//fmt.Println("...... encrypted :", encrypted)

	// GO-4 : Pure after use
	PIN = ""
	for i := 0; i < len(secret); i++ {
		secret[i] = 0
	}

	return []byte(encrypted)
}

func EncryptMasterKeySIGN(PIN, masterSeed string) []byte {
	secret := genSecretPIN(PIN)
	if secret == nil {
		return nil
	}

	encrypted, err := Encrypt256(secret, []byte(masterSeed))
	if err != nil {
		return nil
	}
	//fmt.Println("...... encrypted :", encrypted)

	// GO-4 : Pure after use
	PIN = ""
	for i := 0; i < len(secret); i++ {
		secret[i] = 0
	}

	return []byte(encrypted)
}

func DecryptMasterKeyPIN(PIN, encrypted string) ([]byte, error) {
	secret := genSecretPIN(PIN)
	if secret == nil {
		return nil, errors.New("error genSecretPIN")
	}

	// GO-5 : Decrypt
	decrypted, err := Decrypt256(secret, encrypted)
	if err != nil {
		return nil, errors.New("error decrypted")
	}
	// fmt.Println("...... decrypted :", decrypted)

	// GO-4 : Pure after use
	PIN = ""
	for i := 0; i < len(secret); i++ {
		secret[i] = 0
	}

	return []byte(decrypted), nil
}

func MultiHash(data string) []uint32 {
	elements := strings.Split(data, delimiter)
	hashedElements := make([]uint32, len(elements))
	for i, v := range elements {
		hashedElements[i] = computehash(v)
	}
	return hashedElements
}

func computehash(input string) uint32 {
	hash := common.BytesToHash(Keccak([]byte(input)))
	return uint32(hash.Big().Uint64())
}
