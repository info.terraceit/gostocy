package cryptocy

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"log"
	"testing"
	"time"

	"github.com/bartekn/go-bip39"
)

/*---------Test Encrypt Decrypt Bscrypt-Salt-------- */
func TestXscrypt(t *testing.T) {
	// GO-0 : input validator PIN
	PIN := "12345678"
	if len(PIN) < 8 {
		fmt.Println("error pin")
		return
	}

	// GO-1 : keccak pin
	pinKeccak := Keccak([]byte(PIN))
	if len(pinKeccak) <= 0 {
		fmt.Println("error pinKeccak")
		return
	}
	fmt.Println("..... pin :", PIN, " keccak :", hex.EncodeToString(pinKeccak))

	// GO-2 : bcrypt 1000 times
	begin := time.Now()
	// salt := []byte("3.141592653589793238462643383279") // 32 bytes -> 256 bits
	salt := []byte("3.14159265358979323846") // 22 bytes
	result := pinKeccak
	n := 1
	fmt.Println(fmt.Sprintf("...... bscrypting %v times", n))
	for i := 0; i < n; i++ {
		fmt.Println(".....salt :", string(base64Encode(salt)), " len :", len(salt))
		// hash, err := Scrypt(pinKeccak, salt)
		hash, err := Bcrypt(result, 11, salt)
		if err != nil {
			log.Fatal(err)
			return
		}
		result = hash
		// salt = XOR(hash, salt)
		salt = base64Encode(XOR(hash, salt))[:encodedSaltSize]

		fmt.Println(fmt.Sprintf("... hash[%v] %s", i, string(base64Encode(hash))))
	}
	fmt.Println("...... bscrypted time :", time.Now().Unix()-begin.Unix())
	fmt.Println("...... cipherPass :", hex.EncodeToString(result))

	// GO-3 : blake hash
	blakeInput := append(result, salt...)
	blake := sha256.New()
	blake.Write([]byte(blakeInput))
	secret := blake.Sum(nil)
	fmt.Println("...... secret:", hex.EncodeToString(secret))

	// GO-4 : aes encrypted
	mnemonic := "tag volcano eight thank tide danger coast health above argue embrace heavy"
	fmt.Println("...... mnemonic :", mnemonic)

	masterSeed, err := bip39.NewSeedWithErrorChecking(mnemonic, PIN)
	if err != nil {
		return
	}

	privKey := Keccak(masterSeed) //([]byte(mnemonic))
	//messagePrivKey64 := base64.URLEncoding.EncodeToString(privKey)
	//fmt.Println("...... message privKey64 :", messagePrivKey64)
	fmt.Println("...... privKey :", hex.EncodeToString(privKey))

	// encrypted, err := Encrypt(secret, []byte(messagePrivKey64))
	// if err != nil {
	// 	fmt.Println("error encrypt", err)
	// 	return
	// }
	encrypted, _ := Encrypt256(secret, privKey)
	fmt.Println("...... encrypted :", encrypted)

	// GO-5 : Decrypt
	decrypted, _ := Decrypt256(secret, encrypted)
	fmt.Println("...... decrypted :", decrypted)

	// GO-6 : compare decode mess with privK
	if decrypted != string(privKey) {
		fmt.Println("error not equal")
		return
	}
	fmt.Println("...... equal privKey :", string(privKey), "  decrypted :", decrypted)
}

func TestEncryptDecryptPIn(t *testing.T) {
	PIN := "1234567890"
	mnemonic := "tag volcano eight thank tide danger coast health above argue embrace heavy"
	LOOP_PIN = 10

	masterSeed, err := bip39.MnemonicToByteArray(mnemonic)
	if err != nil {
		return
	}

	encrypted := EncryptMasterKeyPIN(PIN, hex.EncodeToString(masterSeed)) // mnemonic)
	fmt.Println("...... encrypted :", string(encrypted))

	decrypted, _ := DecryptMasterKeyPIN(PIN, string(encrypted))
	fmt.Println("...... decrypted :", string(decrypted))
	fmt.Println("...... decrypted hex :", hex.EncodeToString(decrypted))

	// GO-4 : Pure after use
	PIN = ""
	for i := 0; i < len(masterSeed); i++ {
		masterSeed[i] = 0
	}
}
