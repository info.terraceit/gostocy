package cryptocy

import (
	"errors"

	"github.com/awnumar/memguard"
)

type Secret []byte

func (s *Secret) Destroy() {
	*s = make([]byte, 0)
}

var mem *memguard.Enclave

// max Goroutine = 10
var maxCon chan struct{} = make(chan struct{}, 10)

func MemguardStorage(data []byte) error {
	// clean memory
	MemguardPure()

	// set memmory
	mem = memguard.NewEnclave(data)
	if mem == nil {
		return errors.New("error new memguard")
	}

	return nil
}

func MemguardRetrieve() (*Secret, error) {
	// Wait for counter lower max Goroutine
	maxCon <- struct{}{}

	buffer, err := mem.Open()
	if err != nil {
		return nil, err
	}

	defer func(buffer *memguard.LockedBuffer) {
		buffer.Destroy()
		<-maxCon
	}(buffer)

	secret := Secret([]byte(string(buffer.Data())))
	return &secret, nil
}

func MemguardPure() {
	if mem == nil {
		return
	}

	// Clean Buffer
	buffer, err := mem.Open()
	if err != nil {
		return
	}
	buffer.Destroy()

	// Clean Memory
	memguard.Purge()
}
