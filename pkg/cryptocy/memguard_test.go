package cryptocy

import (
	"encoding/json"
	"fmt"
	"sync"
	"testing"

	"github.com/awnumar/memguard"
)

func TestNewEnclave(t *testing.T) {
	e := memguard.NewEnclave([]byte("yellow submarine !!!"))
	if e == nil {
		t.Error("got nil enclave")
	}
	data, err := e.Open()
	if err != nil {
		t.Error("unexpected error:", err)
	}
	fmt.Println(string(data.Data()))

	data.Destroy()
	fmt.Println(string(data.Data()))
}

func TestMemguard(t *testing.T) {
	input := []byte("Master Seed")
	MemguardStorage(input)

	output, err := mem.Open()
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println(string(input), string(output.Data()))
}

func TestMemStruct(t *testing.T) {
	type secrets struct {
		Key  string `json:"key"`
		Pass string `json:"pass"`
	}

	input := secrets{Key: "key", Pass: "pass"}
	b, err := json.MarshalIndent(input, "", " ")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(string(b))

	MemguardStorage(b)

	output, err := MemguardRetrieve()
	if err != nil {
		fmt.Println(err)
		return
	}
	defer output.Destroy()
	fmt.Println(string(b), string(*output))

	var secret secrets
	err = json.Unmarshal(*output, &secret)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(secret.Key, secret.Pass)
}

func TestMaxBuffer(t *testing.T) {
	type secrets struct {
		Key  string `json:"key"`
		Pass string `json:"pass"`
	}

	input := secrets{Key: "key", Pass: "pass"}
	b, err := json.MarshalIndent(input, "", " ")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(string(b))

	MemguardStorage(b)
	i := 0
	wg := &sync.WaitGroup{}
	for {
		i++
		if i >= 10000 {
			break
		}
		wg.Add(1)
		go func(i int, wg *sync.WaitGroup) {
			data, err := MemguardRetrieve()
			if err != nil {
				fmt.Println(err)
			}
			defer data.Destroy()
			fmt.Println(i, data)
			wg.Done()
		}(i, wg)
	}
	wg.Wait()
}
