package cryptocy

/*
import (
	"encoding/json"
	"fmt"
	"testing"
)

var publicKey = []byte(`
-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAncQYhmABP4wZT/hnWe2B
oljJOTFK4DMJP3LRrpVm6sqv894kgZf/JxfONwimkJ73H0jT5rdMLA4/pVx+tE/r
P+nchk0QMJKDMqG5RZf5+Ek+av8Nzkx8VwzLLy/ry6c4+hLw5+V5gWnn5DzqcD94
BsEBHAHBOyNKO3Dz6LbrcvLqnJU9UHmAoy+zRMR4L+BiJmUK4Pv4jF8U77ge3S/X
6i3ZYyxCW3XvKAkKy37Y7NsNdf+zGYolCG5KRDCwfC1C9ZjGY7pInb58CF+504a1
0E2mmbydJDgatjr4am2S2r0AM/iOQ1St8cwHS1kHSyquRFsQqs1J37mIbAqdUMB/
GQIDAQAB
-----END PUBLIC KEY-----
`)

var privateKey = []byte(`
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAncQYhmABP4wZT/hnWe2BoljJOTFK4DMJP3LRrpVm6sqv894k
gZf/JxfONwimkJ73H0jT5rdMLA4/pVx+tE/rP+nchk0QMJKDMqG5RZf5+Ek+av8N
zkx8VwzLLy/ry6c4+hLw5+V5gWnn5DzqcD94BsEBHAHBOyNKO3Dz6LbrcvLqnJU9
UHmAoy+zRMR4L+BiJmUK4Pv4jF8U77ge3S/X6i3ZYyxCW3XvKAkKy37Y7NsNdf+z
GYolCG5KRDCwfC1C9ZjGY7pInb58CF+504a10E2mmbydJDgatjr4am2S2r0AM/iO
Q1St8cwHS1kHSyquRFsQqs1J37mIbAqdUMB/GQIDAQABAoIBAQCMws6C7huPAEgB
Uwebc85q9D6tOa6ttLt2kdJFq2VE7Yzpz/TQb7YmwhWqgpxigoLItejie/6JfDkL
KCigZoHl55lrdkyQmdhXD3ZHgp50CZ2YR3Kg8xspa+/JbT872Dp83PN2ZAbyEXTl
7ffaKLbFh+SegW5PfmAYAuKMatBOIMnPSBBHxgf9oB/89L9uALLQ9FoQrMJrvoMb
H+Z6kP3QkjCQFTbi0nd7SFd5vnwdGtJhdVvHo+maW6c+P6h5wDpDHgX6p89oXSjP
GGrTxVCUPxv9laU+DD/zgprDwLbV9JaE8+fjqVlJjELtjsWZqtAZbbIllhB4K67a
MkrT+liBAoGBAMuJA27ljXvw8jC2ViZ/LUyIRYiiyOQ+EXq00gBUCJGvdQLxr/dH
yHClfpFe4L9YG1EeaDr2vkQqll8nnqm+SrC6a8UqZTm7mPTD8H22HEb4GiVJRcgH
IvkdqLpNxOdYSNVjE8YrStMGr/UEhvg/cugA2ITyZGCxfJn9PYPIZOSRAoGBAMZu
1xLwmeUqsJOc8OpyfZ/YHm+gkTQUcC+GRaWDuzVSt+MU5y4ZH1zRN0htwG3cboQC
D/4eW73Hhdb5CutyarZTtZiq92tWz7nQzKeitL/gSfR2udTE4j/ENamP06flDQWS
11ZSiHldw9iwJCN0WjYa9gLP2ERunf032MWIkBYJAoGAVeVq54BFJ4tWCqzIytJd
QbE49yML+RcgtVJeVfkg7KrJjQaGpqtKsg+FzL/nxZjfXmqgvlGnlokKkPndgfFn
ABKMrNbphWXgdVvDwKn8YQ8PqbC0nGg2oD+eF5H+iUu+4R9BFx7qIwjtdVAFzfqo
+bRxrBnHtu7wkORI87a8raECgYAlMYlmp2pxJftK79PIXgVl/KBX1ATjeiE9BzHu
wC5K5GJLUhOy9EqInz55ePjknrioTaqpuw7/F3LixHNjhmKyIiaboxAVMoRlE9oI
Ydr3TBcQl2BlnTG7Og/E5IyLn8c9EDUEnRuvg/+9keZ0Ls7p7UvHYyXqTcGXQOyE
ugAn4QKBgGl5REC1yXXvLGR3Zl6vhpOiIAKzoyvqMPajHY1z9qT6y4xIemDjcjm2
s33XH9z9w40+gur/Esv1USikM8RLbR7cn2B8CdrcqZ4TEXD98apyHNT4oW9YjVh6
UhLUG9VdN3FvuBj9j5daOkWvxiv4R2r/B6agXQlJDHK8v6QuBmFz
-----END RSA PRIVATE KEY-----
`)

func Test_RSA(t *testing.T) {

	// data
	//	resq := map[string]interface{}{
	//		"success": true,
	//		"data": map[string]string{
	//			"coin":     "BTC",
	//			"contract": "",
	//			"payload":  "92824e941ae0dd797ec4bb7bffd871590d623b22cb064ba6919d99ff680b8bb0"},
	//	}

	resq := map[string]interface{}{
		"result": 0,
		"api":    "balance",
		"data": map[string]interface{}{
			"address": "mou4W8Uz2FGD3PnfcJCU8X467KJ1H9Ho7E",
			"balance": 0,
			"payload": "24aeb0b1662c77091d5c2c6286d0ef21681367b3557accebe461b59e88e58a82"},
		"error": nil}

	// json string
	json_encode, err := json.Marshal(resq["data"])
	if err != nil {
		fmt.Println("....error..0..", err)
	}
	fmt.Println("......json_encode.....", string(json_encode))

	// opessl encrypt
	encrypt, err := RsaEncrypt(json_encode, publicKey)
	if err != nil {
		fmt.Println("....error..1..", err)
	}
	fmt.Println("....encrypt....", encrypt)

	// base64 encode
	msg := Base64Enc(encrypt)
	fmt.Println("....msg....", msg)

	fmt.Println("------------------------------------------------")

	bStr := `hH8VA9FtRxDlZR3TKWEaRppvJbxKg4lkRvTzj6rDlkEnZFTfROcPRulLl4sO/2yN+DS7dbTMqNWh
xVvpedSGu/rXCeiYz/swYDHqZ29xHUMKLxrB43hMIx27cZ9/ZATkW/cuz4IgboFb/X8uD5/kQbO9
y/BSI6YdBYknASxYfZDK1MC71c6s0PxsiPEYtMskM3slOMYkPwqjjKHatEK+qRmyyMJKvE8gaYED
FpGXMyBV+1sAViA1Wsy1r7/YAZoXKxTw0p/5QiPKj08XSQ8OsgzujGe7YhQruxHpARo53wthsscH
R0wo8dhiQemSE/pRD/lRKnReLaLVPX0oduh3jg==`

	// decode base64
	cipherText, err := Base64Dec(bStr)
	if err != nil {
		fmt.Println("....error..2..", err)
		return
	}
	fmt.Println(".....cipherText.....", cipherText)

	// openssl decrypt
	plaintext, err := RsaDecrypt(cipherText, privateKey)
	if err != nil {
		fmt.Println("....error..2..", err)
		return
	}
	fmt.Println(".....plaintext.....", string(plaintext))
}
*/

/*
func Test_RSA_DecryptPKCS1v15(t *testing.T) {

	// encrypted msg
	msg := "eYmH8calhJ1TFHhjYXkh3QopJz/tUyTb4ErPrxAwxcMfZ1dMhNT8FSR5GhQbuBiwmZO71pJCnjokoAxDHZlaj3XDJqXr5H1mbHbiRrM8U4CnAqKiWU8v91nw5PdmcOqGbmWC7YIqlW/9ZGzVKipISOY5J3fY6esd/XuCRbplcaJX6J8Hs9M99k9GFY97Y05Ix4nlpvVOsWFasUDnyGazb5jY83P1A5flk/Tdto0x8W9p0wd2NdttqEif0AZKbKS0Q3qwvxFkMpbZ1SirfdGNCiotpgK9DuszAiAkEnADEBHsOG3L1aQDbCxkn0tS7X5t2p04if2cTM1GEbnWD2vlDw=="
	cipherText, err := base64.StdEncoding.DecodeString(msg)
	if err != nil {
		fmt.Println("....error..2..", err)
		return
	}

	// kyeBytes
	//	keyBytes, err := ioutil.ReadFile("/Users/A/Desktop/twogap/twogateway/blockchain_private.key")
	//	if err != nil {
	//		fmt.Println("....error..1..", err)
	//		return
	//	}

	keyBytes := []byte(`
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAncQYhmABP4wZT/hnWe2BoljJOTFK4DMJP3LRrpVm6sqv894k
gZf/JxfONwimkJ73H0jT5rdMLA4/pVx+tE/rP+nchk0QMJKDMqG5RZf5+Ek+av8N
zkx8VwzLLy/ry6c4+hLw5+V5gWnn5DzqcD94BsEBHAHBOyNKO3Dz6LbrcvLqnJU9
UHmAoy+zRMR4L+BiJmUK4Pv4jF8U77ge3S/X6i3ZYyxCW3XvKAkKy37Y7NsNdf+z
GYolCG5KRDCwfC1C9ZjGY7pInb58CF+504a10E2mmbydJDgatjr4am2S2r0AM/iO
Q1St8cwHS1kHSyquRFsQqs1J37mIbAqdUMB/GQIDAQABAoIBAQCMws6C7huPAEgB
Uwebc85q9D6tOa6ttLt2kdJFq2VE7Yzpz/TQb7YmwhWqgpxigoLItejie/6JfDkL
KCigZoHl55lrdkyQmdhXD3ZHgp50CZ2YR3Kg8xspa+/JbT872Dp83PN2ZAbyEXTl
7ffaKLbFh+SegW5PfmAYAuKMatBOIMnPSBBHxgf9oB/89L9uALLQ9FoQrMJrvoMb
H+Z6kP3QkjCQFTbi0nd7SFd5vnwdGtJhdVvHo+maW6c+P6h5wDpDHgX6p89oXSjP
GGrTxVCUPxv9laU+DD/zgprDwLbV9JaE8+fjqVlJjELtjsWZqtAZbbIllhB4K67a
MkrT+liBAoGBAMuJA27ljXvw8jC2ViZ/LUyIRYiiyOQ+EXq00gBUCJGvdQLxr/dH
yHClfpFe4L9YG1EeaDr2vkQqll8nnqm+SrC6a8UqZTm7mPTD8H22HEb4GiVJRcgH
IvkdqLpNxOdYSNVjE8YrStMGr/UEhvg/cugA2ITyZGCxfJn9PYPIZOSRAoGBAMZu
1xLwmeUqsJOc8OpyfZ/YHm+gkTQUcC+GRaWDuzVSt+MU5y4ZH1zRN0htwG3cboQC
D/4eW73Hhdb5CutyarZTtZiq92tWz7nQzKeitL/gSfR2udTE4j/ENamP06flDQWS
11ZSiHldw9iwJCN0WjYa9gLP2ERunf032MWIkBYJAoGAVeVq54BFJ4tWCqzIytJd
QbE49yML+RcgtVJeVfkg7KrJjQaGpqtKsg+FzL/nxZjfXmqgvlGnlokKkPndgfFn
ABKMrNbphWXgdVvDwKn8YQ8PqbC0nGg2oD+eF5H+iUu+4R9BFx7qIwjtdVAFzfqo
+bRxrBnHtu7wkORI87a8raECgYAlMYlmp2pxJftK79PIXgVl/KBX1ATjeiE9BzHu
wC5K5GJLUhOy9EqInz55ePjknrioTaqpuw7/F3LixHNjhmKyIiaboxAVMoRlE9oI
Ydr3TBcQl2BlnTG7Og/E5IyLn8c9EDUEnRuvg/+9keZ0Ls7p7UvHYyXqTcGXQOyE
ugAn4QKBgGl5REC1yXXvLGR3Zl6vhpOiIAKzoyvqMPajHY1z9qT6y4xIemDjcjm2
s33XH9z9w40+gur/Esv1USikM8RLbR7cn2B8CdrcqZ4TEXD98apyHNT4oW9YjVh6
UhLUG9VdN3FvuBj9j5daOkWvxiv4R2r/B6agXQlJDHK8v6QuBmFz
-----END RSA PRIVATE KEY-----
`)

	fmt.Println("....privKey...", string(keyBytes))

	block, _ := pem.Decode(keyBytes)
	if block == nil {
		fmt.Println("....error..3..")
		return
	}
	b, _ := json.MarshalIndent(block, "", " ")
	fmt.Println(".......block......", string(b))

	priv, err := x509.ParsePKCS1PrivateKey(block.Bytes)
	if err != nil {
		fmt.Println("....error..4..", err)
		return
	}
	b, _ = json.MarshalIndent(priv, "", " ")
	fmt.Println(".......priv......", string(b))

	// DecryptPKCS1v15
	decryptedPKCS1v15, err := rsa.DecryptPKCS1v15(rand.Reader, priv, cipherText)
	if err != nil {
		fmt.Println("....error..5..", err)
		//return
	}
	fmt.Println("....decryptedPKCS1v15...", string(decryptedPKCS1v15))

	plaintext, err := rsa.DecryptOAEP(sha256.New(), rand.Reader, priv, cipherText, []byte(""))
	if err != nil {
		fmt.Println("....error..6..", err)
		//return
	}
	fmt.Println("....DecryptOAEP...", string(plaintext))

}

func Test_RSA_EncryptPKCS1v15(t *testing.T) {

	msg := []byte(`{"coin":"BTC","contract":"","payload":"92824e941ae0dd797ec4bb7bffd871590d623b22cb064ba6919d99ff680b8bb0"}`)
	fmt.Println("......msg......", string(msg))
	// public key from partner
	//	pubKey, err := ioutil.ReadFile("/Users/A/Desktop/twogap/twogateway/api_public.key")
	//	if err != nil {
	//		fmt.Println("....error....", err)
	//		return
	//	}

	pubKey := []byte(`
-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAncQYhmABP4wZT/hnWe2B
oljJOTFK4DMJP3LRrpVm6sqv894kgZf/JxfONwimkJ73H0jT5rdMLA4/pVx+tE/r
P+nchk0QMJKDMqG5RZf5+Ek+av8Nzkx8VwzLLy/ry6c4+hLw5+V5gWnn5DzqcD94
BsEBHAHBOyNKO3Dz6LbrcvLqnJU9UHmAoy+zRMR4L+BiJmUK4Pv4jF8U77ge3S/X
6i3ZYyxCW3XvKAkKy37Y7NsNdf+zGYolCG5KRDCwfC1C9ZjGY7pInb58CF+504a1
0E2mmbydJDgatjr4am2S2r0AM/iOQ1St8cwHS1kHSyquRFsQqs1J37mIbAqdUMB/
GQIDAQAB
-----END PUBLIC KEY-----
`)

	fmt.Println("....pubKey...", string(pubKey))

	block, _ := pem.Decode(pubKey)
	if block == nil {
		fmt.Println("....error..1..")
		return
	}

	pubInterface, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		fmt.Println("....error..2..")
		return
	}
	pub := pubInterface.(*rsa.PublicKey)

	data, err := rsa.EncryptPKCS1v15(rand.Reader, pub, msg)
	if err != nil {
		fmt.Println("....error..3..", err)
		return
	}

	encode := base64.StdEncoding.EncodeToString(data)
	fmt.Println(".........data.......", encode)

	ciphertext, err := rsa.EncryptOAEP(sha256.New(), rand.Reader, pub, msg, []byte(""))
	if err != nil {
		fmt.Println("....error..4..", err)
		return
	}

	encode = base64.StdEncoding.EncodeToString(ciphertext)
	fmt.Println(".......ciphertext.......", encode)
}
*/
