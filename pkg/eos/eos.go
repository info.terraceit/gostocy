package eos

import (
	"context"
	"encoding/json"
	"fmt"
	"math/rand"
	"strconv"
	"strings"
	"time"

	"github.com/bartekn/go-bip39"
	"github.com/btcsuite/btcd/chaincfg"
	"github.com/eoscanada/eos-go"
	"github.com/eoscanada/eos-go/ecc"
	"github.com/eoscanada/eos-go/system"
	"github.com/eoscanada/eos-go/token"
)

var MAIN_NET string = "mainnet"
var KYLIN_NET string = "kylin"
var JUNGLE_NET string = "jungle"
var SIMNET string = "simnet"

var URL_MAINNET string = "https://eos.greymass.com:443"
var URL_KYLIN string = "https://api-kylin.eosasia.one"
var URL_JUNGLE string = "https://jungle2.cryptolions.io:443"
var URL_SIMNET string = "http://35.221.118.61:8888"

type Seed struct {
	Mnemoric string `json:"mnemoric"`
	Seed     []byte `json:"seed"`
}

func GenSeedWithMnemoric(mnemonic, password string) (*Seed, error) {
	seed, err := bip39.NewSeedWithErrorChecking(mnemonic, password)
	if err != nil {
		return &Seed{}, err
	}
	s := &Seed{}
	s.Mnemoric = mnemonic
	s.Seed = seed

	return s, nil
}

type chainParams struct {
	Params *chaincfg.Params
}

func ChainConfig(net string) (Chaincfg *chainParams) {
	switch net {
	case MAIN_NET:
		Chaincfg = &chainParams{}
		Chaincfg.Params = &chaincfg.MainNetParams
	case JUNGLE_NET, KYLIN_NET, "testnet":
		Chaincfg = &chainParams{}
		Chaincfg.Params = &chaincfg.TestNet3Params
	case "simnet":
		//Chaincfg = &chainParams{Params: &chaincfg.SimNetParams}
		Chaincfg = &chainParams{}
		Chaincfg.Params = &chaincfg.RegressionNetParams
	}

	return
}

//-------------------------------------------------------------------------//

func GetURL(net string) (url string) {
	switch net {
	case MAIN_NET:
		{
			url = URL_MAINNET
		}
	case KYLIN_NET:
		{
			url = URL_KYLIN
		}
	case JUNGLE_NET, "testnet":
		{
			url = URL_JUNGLE
		}
	case SIMNET:
		{
			url = URL_SIMNET
		}
	default:
		url = URL_MAINNET
	}

	return
}

func SignPushActions(net, wif string, actions []*eos.Action) (tx string) {
	ctx := context.Background()
	signer := eos.NewKeyBag()
	err := signer.Add(wif)
	if err != nil {
		fmt.Println(err)
		return
	}

	api := eos.New(GetURL(net))
	api.SetSigner(signer)

	out, err := api.SignPushActions(ctx, actions...)
	if err != nil {
		fmt.Println(err)
		return
	}

	tx = out.TransactionID
	fmt.Println("SignPushActions : ", tx)
	return
}

func RandomAccount(net string) (account string) {
	ctx := context.Background()
	var letterBytes = []byte("abcdefghijklmnopqrstuvwxyz12345")
	max := len(letterBytes)
	b := make([]byte, 12)
	rand.Seed(time.Now().UnixNano())
	for i := range b {
		b[i] = letterBytes[rand.Intn(max)]
	}

	api := eos.New(GetURL(net))
	_, err := api.GetAccount(ctx, eos.AccountName(string(b)))
	if err == nil {
		fmt.Println("error random account existed : ", err)
		return RandomAccount(net)
	}

	account = string(b)
	fmt.Println("RandomAccount EOS : ", account)
	return
}

func SystemNewAccount(net, wif, from, to, owner, active, cpuStakeStr, netStakeStr string, buyRAMBytes int32) (tx string) {
	PK_owner, err := ecc.NewPublicKey(owner)
	if err != nil {
		return
	}

	PK_Active, err := ecc.NewPublicKey(active)
	if err != nil {
		return
	}

	Owner := eos.Authority{
		Threshold: 1,
		Keys: []eos.KeyWeight{
			{
				PublicKey: PK_owner,
				Weight:    1,
			},
		},
		Accounts: []eos.PermissionLevelWeight{},
	}
	Active := eos.Authority{
		Threshold: 1,
		Keys: []eos.KeyWeight{
			{
				PublicKey: PK_Active,
				Weight:    1,
			},
		},
		Accounts: []eos.PermissionLevelWeight{},
	}

	creator := eos.AccountName(from)
	newAccount := eos.AccountName(to)

	//cpuStakeStr := "0.01 EOS"
	cpuStake, err := eos.NewEOSAssetFromString(cpuStakeStr)
	if err != nil {
		return
	}

	//netStakeStr := "0.01 EOS"
	netStake, err := eos.NewEOSAssetFromString(netStakeStr)
	if err != nil {
		return
	}

	doTransfer := true

	//buyRAMBytes := 8

	actions := make([]*eos.Action, 0)
	actions = append(actions, system.NewCustomNewAccount(creator, newAccount, Owner, Active))
	actions = append(actions, system.NewDelegateBW(creator, newAccount, cpuStake, netStake, doTransfer))
	actions = append(actions, system.NewBuyRAMBytes(creator, newAccount, uint32(buyRAMBytes*1024)))
	tx = SignPushActions(net, wif, actions)

	return
}

func Transfer(net, wif, from, to, amount, contract, memo string) (tx string) {
	var err error
	var quantity eos.Asset

	if contract == "eosio.token" {
		quantity, err = eos.NewEOSAssetFromString(amount)
	} else {
		quantity, err = eos.NewAsset(amount)
	}

	if err != nil {
		fmt.Println("error : ", err)
		return
	}

	action := token.NewTransfer(eos.AccountName(from), eos.AccountName(to), quantity, memo)
	action.Account = eos.AccountName(contract)
	actions := make([]*eos.Action, 0)
	actions = append(actions, action)
	tx = SignPushActions(net, wif, actions)

	return
}

func PushAction(net, wif, actor_name, contract_name, action_name, payload string) (tx string) {
	ctx := context.Background()
	var dump map[string]interface{}
	err := json.Unmarshal([]byte(payload), &dump)
	if err != nil {
		fmt.Println(err)
		return
	}

	api := eos.New(GetURL(net))
	actionBinary, err := api.ABIJSONToBin(ctx, eos.AN(contract_name), eos.Name(action_name), eos.M(dump))
	if err != nil {
		fmt.Println(err)
		return
	}

	action := &eos.Action{
		Account: eos.AN(contract_name),
		Name:    eos.ActN(action_name),
		Authorization: []eos.PermissionLevel{
			{Actor: eos.AN(actor_name), Permission: eos.PermissionName("active")},
		},
		ActionData: eos.NewActionDataFromHexData([]byte(actionBinary)),
	}

	actions := make([]*eos.Action, 0)
	actions = append(actions, action)
	tx = SignPushActions(net, wif, actions)

	return
}

func SellRam(net, wif, seller string, ramBytes int64) (tx string) {
	action := system.NewSellRAM(eos.AccountName(seller), uint64(ramBytes))
	actions := make([]*eos.Action, 0)
	actions = append(actions, action)
	tx = SignPushActions(net, wif, actions)
	return
}

func BuyRAMBytes(net, wif, seller, receiver string, ramBytes int32) (tx string) {
	action := system.NewBuyRAMBytes(eos.AccountName(seller), eos.AccountName(receiver), uint32(ramBytes))
	actions := make([]*eos.Action, 0)
	actions = append(actions, action)
	tx = SignPushActions(net, wif, actions)
	return
}

func ListTransaction(net, account string) (list string) {
	ctx := context.Background()
	api := eos.New(GetURL(net))
	params := eos.GetActionsRequest{}
	params.AccountName = eos.AccountName(account)
	//params.Offset = 0
	//params.Pos = 0

	out, err := api.GetActions(ctx, params)
	if err != nil {
		fmt.Println("Error :", err)
		return
	}

	b, err := json.Marshal(out.Actions)
	if err != nil {
		fmt.Println("Error :", err)
		return
	}
	list = string(b)

	return
}

func GetBalance(net, account, symbol, contract string) (balance float64) {
	ctx := context.Background()
	api := eos.New(GetURL(net))
	balances, err := api.GetCurrencyBalance(ctx, eos.AccountName(account), symbol, eos.AccountName(contract))
	if err != nil {
		fmt.Printf("Error: get balance: %s\n", err)
		return
	}

	for _, asset := range balances {
		arr := strings.Split(asset.String(), " ")
		if arr[1] == symbol {
			balance, _ = strconv.ParseFloat(arr[0], 64)
			break
		}
	}

	return
}

func GetBlockCount(net string) (blockcount int32) {
	ctx := context.Background()
	api := eos.New(GetURL(net))
	out, err := api.GetInfo(ctx)
	if err != nil {
		return
	}
	blockcount = int32(out.HeadBlockNum)

	return
}

func GetBlockByID(net, id string) (result string) {
	ctx := context.Background()
	api := eos.New(GetURL(net))
	out, err := api.GetBlockByID(ctx, id)
	if err != nil {
		return
	}

	b, err := json.MarshalIndent(out, "", " ")
	if err != nil {
		return
	}
	result = string(b)

	return
}

func GetBlockByNum(net string, num int32) (result string) {
	ctx := context.Background()
	api := eos.New(GetURL(net))
	block, err := api.GetBlockByNum(ctx, uint32(num))
	if err != nil {
		return
	}

	//txResps := make([]*eos.TransactionResp, 0)
	// for _, txObj := range block.Transactions {
	// 	tx, err := api.GetTransaction(txObj.Transaction.ID.String())
	// 	if err != nil {
	// 		return
	// 	}
	// 	txResps = append(txResps, tx)
	// }

	b, err := json.MarshalIndent(block, "", " ")
	if err != nil {
		return
	}
	result = string(b)

	return
}

func GetTransaction(net, id string) (result string) {
	ctx := context.Background()
	api := eos.New(GetURL(net))
	out, err := api.GetTransaction(ctx, id)
	if err != nil {
		return
	}

	b, err := json.MarshalIndent(out, "", " ")
	if err != nil {
		fmt.Println(err)
		return
	}
	result = string(b)

	return
}

//-------------------------------------------------------------------------//

func SystemNewAccountMulSig(net, wif, from, to, cpuStakeStr, netStakeStr string, buyRAMBytes int32, ownerStr, activeStr string) (tx string) {
	owners := strings.Split(ownerStr, ",")
	actives := strings.Split(activeStr, ",")
	if len(owners) < 1 || len(actives) < 1 {
		return
	}

	thresholdOwners := uint32(len(owners))
	thresholdActives := uint32(len(actives))
	keyOwners := make([]eos.KeyWeight, 0)
	keyActives := make([]eos.KeyWeight, 0)

	createPubKey := func(key string) (keyWeight eos.KeyWeight, err error) {

		pubKey, err := ecc.NewPublicKey(key)
		if err != nil {
			return
		}

		keyWeight = eos.KeyWeight{}
		keyWeight.PublicKey = pubKey
		keyWeight.Weight = 1

		return
	}

	for _, key := range owners {
		keyWeight, err := createPubKey(key)
		if err != nil {
			return
		}

		keyOwners = append(keyOwners, keyWeight)
	}

	for _, key := range actives {
		keyWeight, err := createPubKey(key)
		if err != nil {
			return
		}

		keyActives = append(keyActives, keyWeight)
	}

	Owner := eos.Authority{
		Threshold: thresholdOwners,
		Keys:      keyOwners,
		Accounts:  []eos.PermissionLevelWeight{},
	}
	Active := eos.Authority{
		Threshold: thresholdActives,
		Keys:      keyActives,
		Accounts:  []eos.PermissionLevelWeight{},
	}

	creator := eos.AccountName(from)
	newAccount := eos.AccountName(to)

	//cpuStakeStr := "0.01 EOS"
	cpuStake, err := eos.NewEOSAssetFromString(cpuStakeStr)
	if err != nil {
		return
	}

	//netStakeStr := "0.01 EOS"
	netStake, err := eos.NewEOSAssetFromString(netStakeStr)
	if err != nil {
		return
	}

	doTransfer := true

	//buyRAMBytes := 8

	actions := make([]*eos.Action, 0)
	actions = append(actions, system.NewCustomNewAccount(creator, newAccount, Owner, Active))
	actions = append(actions, system.NewDelegateBW(creator, newAccount, cpuStake, netStake, doTransfer))
	actions = append(actions, system.NewBuyRAMBytes(creator, newAccount, uint32(buyRAMBytes*1024)))
	tx = SignPushActions(net, wif, actions)

	return
}

func NewAction(net, actors, contract_name, action_name, payload string) (action string) {
	ctx := context.Background()
	var dump map[string]interface{}
	err := json.Unmarshal([]byte(payload), &dump)
	if err != nil {
		fmt.Println(err)
		return
	}

	api := eos.New(GetURL(net))
	actionBinary, err := api.ABIJSONToBin(ctx, eos.AN(contract_name), eos.Name(action_name), eos.M(dump))
	if err != nil {
		fmt.Println(err)
		return
	}

	acts := strings.Split(actors, ",")
	permissions := make([]eos.PermissionLevel, 0)
	for _, actName := range acts {
		allow := eos.PermissionLevel{Actor: eos.AN(actName), Permission: eos.PermissionName("active")}
		permissions = append(permissions, allow)
	}

	act := &eos.Action{
		Account:       eos.AN(contract_name),
		Name:          eos.ActN(action_name),
		Authorization: permissions,
		ActionData:    eos.NewActionDataFromHexData([]byte(actionBinary)),
	}

	b, err := json.MarshalIndent(*act, "", " ")
	if err != nil {
		return
	}
	action = string(b)

	return
}

func NewTransaction(net, act string, expired int64) (tx string) {
	ctx := context.Background()
	var action eos.Action
	err := json.Unmarshal([]byte(act), &action)
	if err != nil {
		return
	}

	actions := make([]*eos.Action, 0)
	actions = append(actions, &action)
	api := eos.New(GetURL(net))
	opts := &eos.TxOptions{}
	opts.FillFromChain(ctx, api)
	if err != nil {
		return
	}

	trans := eos.NewTransaction(actions, opts)
	trans.SetExpiration(time.Duration(expired) * time.Second)
	b, err := json.MarshalIndent(*trans, "", " ")
	if err != nil {
		return
	}
	tx = string(b)

	return
}

func SignedTx(net, wif, tx string) (signedTx string) {
	ctx := context.Background()
	api := eos.New(GetURL(net))
	opts := &eos.TxOptions{}
	err := opts.FillFromChain(ctx, api)
	if err != nil {
		return
	}

	signer := eos.NewKeyBag()
	err = signer.Add(wif)
	if err != nil {
		return
	}

	keys, err := signer.AvailableKeys(ctx)
	if err != nil {
		return
	}

	var trans eos.Transaction
	err = json.Unmarshal([]byte(tx), &trans)
	if err != nil {
		return
	}

	signed := eos.NewSignedTransaction(&trans)
	signed, err = signer.Sign(ctx, signed, opts.ChainID, keys...)
	if err != nil {
		return
	}

	b, err := json.MarshalIndent(*signed, "", " ")
	if err != nil {
		return
	}
	signedTx = string(b)

	return
}

func AddSignedTx(net, wif, signedTx string) (addSignedTx string) {
	ctx := context.Background()
	api := eos.New(GetURL(net))
	opts := &eos.TxOptions{}
	err := opts.FillFromChain(ctx, api)
	if err != nil {
		return
	}

	signer := eos.NewKeyBag()
	err = signer.Add(wif)
	if err != nil {
		return
	}

	keys, err := signer.AvailableKeys(ctx)
	if err != nil {
		return
	}

	var trans eos.SignedTransaction
	err = json.Unmarshal([]byte(signedTx), &trans)
	if err != nil {
		return
	}

	signed, err := signer.Sign(ctx, &trans, opts.ChainID, keys...)
	if err != nil {
		return
	}

	b, err := json.MarshalIndent(*signed, "", " ")
	if err != nil {
		return
	}
	addSignedTx = string(b)

	return
}

func PackedTx(signedTx string) (packedTx string) {
	var signed eos.SignedTransaction
	err := json.Unmarshal([]byte(signedTx), &signed)
	if err != nil {
		return
	}

	packed, err := signed.Pack(eos.CompressionZlib)
	if err != nil {
		return
	}

	b, err := json.MarshalIndent(*packed, "", " ")
	if err != nil {
		return
	}
	packedTx = string(b)

	return
}

func PushedTx(net, packedTx string) (tx string) {
	ctx := context.Background()
	var packed eos.PackedTransaction
	err := json.Unmarshal([]byte(packedTx), &packed)
	if err != nil {
		return
	}

	api := eos.New(GetURL(net))
	resp, err := api.PushTransaction(ctx, &packed)
	if err != nil {
		fmt.Println(err)
		return
	}

	b, err := json.MarshalIndent(*resp, "", " ")
	if err != nil {
		return
	}
	tx = string(b)

	return
}
