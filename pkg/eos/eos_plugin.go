package eos

import (
	"context"
	"encoding/json"
	"time"

	"github.com/eoscanada/eos-go"
)

type EosSyncing struct {
	Net        string
	IsSyncing  bool
	FromHeight int32
	blockList  []*eos.BlockResp
	api        *eos.API
}

func NewEosSync(net string, fromHeight int32) (e *EosSyncing, err error) {
	e = new(EosSyncing)
	e.Net = net
	if fromHeight < 0 {
		fromHeight = 0
	}
	e.FromHeight = fromHeight
	e.blockList = make([]*eos.BlockResp, 0)
	e.api = eos.New(GetURL(net))

	return
}

func (e *EosSyncing) Run() {
	go func() {

		for {

			if !e.IsSyncing {
				toHeight := e.GetBlockCount()
				if toHeight > e.FromHeight {
					e.SyncBlock(toHeight)
				}
			}

			time.Sleep(500 * time.Millisecond)
		}

	}()

}

func (e *EosSyncing) GetBlockList() []*eos.BlockResp {
	return e.blockList
}

func (e *EosSyncing) GetBlockListJS() string {
	b, _ := json.MarshalIndent(e.blockList, "", " ")
	return string(b)
}

func (e *EosSyncing) GetBlockCount() int32 {
	ctx := context.Background()
	info, err := e.api.GetInfo(ctx)
	if err != nil {
		return 0
	}

	return int32(info.HeadBlockNum)
}

func (e *EosSyncing) SyncBlock(toHeight int32) {
	ctx := context.Background()
	for e.FromHeight <= toHeight {
		e.IsSyncing = true

		block, err := e.api.GetBlockByNum(ctx, uint32(e.FromHeight))
		if err != nil {
			return
		}
		e.blockList = append(e.blockList, block)

		e.FromHeight++
	}
	e.IsSyncing = false
}
