package eos

import (
	"fmt"
	"testing"
	"time"

	"github.com/eoscanada/eos-go"
)

var private_owner string = "5J2S7vw2YVhjGdXmCAsZXePX5saXnqQqhK5KvktuE5y7gZ2HmJW"
var public_owner string = "EOS4vj26egdBEvhwVcwVEWfHPCMmMX7ntTcNzennWeP1mrFrseciU"

var private_active string = "5JYgTAj53dGh8eVBVTMnxkx2PaYksnv3zsFpnk2xv38jxjckm6C"
var public_active string = "EOS5cDczF3HqThXQqVViZuuUFnmJL2cX15yHtuxyWevTHxwsMRct9"

/*
func Test(t *testing.T) {

	balances := GetBalance("twogapabc123", "EOS", "eosio.token")
	fmt.Println(balances)

	actions := ListTransaction("twogapabc123")
	fmt.Println(actions)

}

func TestSystemNewAccount(t *testing.T) {

	randomAccount := func() string {
		var letterBytes = []byte("abcdefghijklmnopqrstuvwxyz12345")

		max := len(letterBytes)
		b := make([]byte, 12)
		rand.Seed(time.Now().UnixNano())
		for i := range b {
			b[i] = letterBytes[rand.Intn(max)]
		}

		fmt.Println(string(b))

		return string(b)
	}

	wif := "5KNaNVobFvHT7zQ4AX3hEQ7wGfxhWQxNguZU5p7brWG6VUR3hSB"
	privKey, err := ecc.NewPrivateKey(wif)
	if err != nil {
		return
	}

	pubKey := privKey.PublicKey()
	//fmt.Println(pubKey.String())

	creator := eos.AccountName("twogapabc123")
	newAccount := eos.AccountName(randomAccount())
	//pubKey, _ := ecc.NewPublicKey("EOS8QUQm8Qe5Lj5bfGyAfypEDZQ8purPG78Kgq9nR2vHeG1tZpF55")

	cpuStakeStr := "0.01 EOS"
	cpuStake, err := eos.NewEOSAssetFromString(cpuStakeStr)
	if err != nil {
		return
	}

	netStakeStr := "0.01 EOS"
	netStake, err := eos.NewEOSAssetFromString(netStakeStr)
	if err != nil {
		return
	}

	doTransfer := true

	buyRAMBytes := 8

	actions := make([]*eos.Action, 0)
	actions = append(actions, system.NewNewAccount(creator, newAccount, pubKey))
	actions = append(actions, system.NewDelegateBW(creator, newAccount, cpuStake, netStake, doTransfer))
	actions = append(actions, system.NewBuyRAMBytes(creator, newAccount, uint32(buyRAMBytes*1024)))

	signer := eos.NewKeyBag()
	signer.Add(wif)

	url := "https://kylin.eoscanada.com"
	api := eos.New(url)
	api.SetSigner(signer)

	out, err := api.SignPushActions(actions...)
	fmt.Println("error : ", err)

	outTX, _ := json.Marshal(out)
	fmt.Println(string(outTX))
}


func TestTransfer(t *testing.T) {

	wif := "5KNaNVobFvHT7zQ4AX3hEQ7wGfxhWQxNguZU5p7brWG6VUR3hSB"
	privKey, err := ecc.NewPrivateKey(wif)
	if err != nil {
		return
	}

	pubKey := privKey.PublicKey()
	fmt.Println(pubKey.String())

	from := eos.AccountName("twogapabc123")
	to := eos.AccountName("jojojojojoj5")
	contract := eos.AccountName("eosio.token")
	amount := "1" // "1.0000 EOS"
	memo := ""

	var quantity eos.Asset
	if contract == "eosio.token" {
		quantity, err = eos.NewEOSAssetFromString(amount)
	} else {
		quantity, err = eos.NewAsset(amount)
	}

	actions := make([]*eos.Action, 0)
	actions = append(actions, token.NewTransfer(from, to, quantity, memo))
	//actions.Account = contract

	signer := eos.NewKeyBag()
	signer.Add(wif)

	url := "https://kylin.eoscanada.com"
	api := eos.New(url)
	api.SetSigner(signer)

	out, err := api.SignPushActions(actions...)
	fmt.Println("error : ", err)

	outTX, _ := json.Marshal(out)
	fmt.Println(string(outTX))
}


func TestSellRam(t *testing.T) {
	wif := "5KNaNVobFvHT7zQ4AX3hEQ7wGfxhWQxNguZU5p7brWG6VUR3hSB"
	privKey, err := ecc.NewPrivateKey(wif)
	if err != nil {
		return
	}

	pubKey := privKey.PublicKey()
	fmt.Println(pubKey.String())

	from := eos.AccountName("twogapabc123")
	ramBytes := uint64(1024)

	actions := make([]*eos.Action, 0)
	actions = append(actions, system.NewSellRAM(from, ramBytes))

	signer := eos.NewKeyBag()
	signer.Add(wif)

	url := "https://kylin.eoscanada.com"
	api := eos.New(url)
	api.SetSigner(signer)

	out, err := api.SignPushActions(actions...)
	fmt.Println("error : ", err)

	outTX, _ := json.Marshal(out)
	fmt.Println(string(outTX))
}

func TestBuyRAMBytes(t *testing.T) {
	wif := "5JStWw29uqZXsHnZqGkfjLpDDWRy5Uy3xVdc9krG4NphQEyqSY4" // seller
	privKey, err := ecc.NewPrivateKey(wif)
	if err != nil {
		return
	}

	pubKey := privKey.PublicKey()
	fmt.Println(pubKey.String())

	player := eos.AccountName("twogaptesti1") // seller
	receiver := eos.AccountName("twogapabc123")
	ramBytes := uint32(1024)

	actions := make([]*eos.Action, 0)
	actions = append(actions, system.NewBuyRAMBytes(player, receiver, ramBytes))

	signer := eos.NewKeyBag()
	signer.Add(wif)

	url := "https://kylin.eoscanada.com"
	api := eos.New(url)
	api.SetSigner(signer)

	out, err := api.SignPushActions(actions...)
	fmt.Println("error : ", err)

	outTX, _ := json.Marshal(out)
	fmt.Println(string(outTX))
}
*/

/*
func Test_Transaction(t *testing.T) {
	tx := GetTransaction(MAIN_NET, "7a631498958bb623596579eb2f0015a03f6950702a6f74225ad090761cbc4213")

	fmt.Println(tx)
}
*/

/*
func Test_SyncBlock(t *testing.T) {

	eosSync, err := NewEosSync(MAIN_NET, int32(57910700))
	if err != nil {
		fmt.Println(err)
		return
	}
	eosSync.Run()

	for {

		time.Sleep(1000 * time.Millisecond)

		fmt.Println(eosSync.GetBlockList())
	}
}
*/

type mApprove struct {
	Attestor eos.AccountName `json:"attestor"`
}

type mAttach struct {
	User     eos.AccountName `json:"user"`
	Meta     eos.Uint64      `json:"meta"`
	Hash     string          `json:"hash"`
	Attestor eos.AccountName `json:"attestor"`
}

/*
func TestAction(t *testing.T) {

	// ap := mApprove{}
	// ap.Attestor = eos.AccountName("credify1main")
	// Action(JUNGLE_NET, "5J2S7vw2YVhjGdXmCAsZXePX5saXnqQqhK5KvktuE5y7gZ2HmJW", "credifysuper", "cridcontract", "approve", ap)

	m := mAttach{}
	m.User = eos.AccountName("credifysuper")
	m.Meta = eos.Uint64(0x000000000000356B)
	m.Hash = "Qmbd5jx8YF1QLhvwfLbCTWXGyZLyEJHrPbtbpRESvYs4FS"
	m.Attestor = eos.AccountName("credify1main")

	b, _ := json.MarshalIndent(m, "", " ")
	fmt.Println(string(b))

	Action(JUNGLE_NET, "5J2S7vw2YVhjGdXmCAsZXePX5saXnqQqhK5KvktuE5y7gZ2HmJW", "credifysuper", "cridcontract", "attach", m)

}
*/

/*
func TestNewAccountMulSig(t *testing.T) {

	net := SIMNET
	if net == SIMNET {

		temp_priv := private_owner
		temp_pub := public_owner

		private_owner = private_active
		public_owner = public_active

		private_active = temp_priv
		public_active = temp_pub
	}
	actives := public_owner + "," + public_active

	tx := SystemNewAccountMulSig(net, private_owner, "credifysuper", "credifyuber1", "0.1 EOS", "0.1 EOS", 8, public_owner, actives)

	fmt.Println(tx)
}
*/

func TestMultiSig(t *testing.T) {

	net := SIMNET
	if net == SIMNET {

		temp_priv := private_owner
		temp_pub := public_owner

		private_owner = private_active
		public_owner = public_active

		private_active = temp_priv
		public_active = temp_pub
	}

	var data string
	data = `{"user":"credifyuber1", "meta": 0, "hash": "Qmbd5jx8YF1QLhvwfLbCTWXGyZLyEJHrPbtbpRESvYs4F7", "attestor": "credifysuper"}`
	fmt.Println("data \n", data)

	act := NewAction(net, "credifysuper,credifyuber1", "cridcontract", "attach", data)
	fmt.Println("act \n", act)

	tx := NewTransaction(net, act, int64(30))
	fmt.Println("tx \n", tx)

	signedTx := SignedTx(net, private_owner, tx)
	fmt.Println("SignedTx \n", signedTx)

	time.Sleep(1 * time.Second)

	addSignedTx := AddSignedTx(net, private_active, signedTx)
	fmt.Println("addSignedTx \n", addSignedTx)

	packedTx := PackedTx(addSignedTx)
	fmt.Println("packedTX \n", packedTx)

	pushedTx := PushedTx(net, packedTx)
	fmt.Println("pushedTx \n", pushedTx)
}
