package msig

import (
	"encoding/json"

	EOS "gitlab.com/info.terraceit/gostocy/pkg/eos"

	"github.com/eoscanada/eos-go"
	"github.com/eoscanada/eos-go/msig"
)

func Propose(net, wif, proposer, proposalName, permissionList, txData string) (tx string) {

	requested := make([]eos.PermissionLevel, 0)
	err := json.Unmarshal([]byte(permissionList), &requested)
	if err != nil {
		return
	}

	transaction := &eos.Transaction{}
	err = json.Unmarshal([]byte(txData), &transaction)
	if err != nil {
		return
	}

	action := msig.NewPropose(eos.AccountName(proposer), eos.Name(proposalName), requested, transaction)

	actions := make([]*eos.Action, 0)
	actions = append(actions, action)

	tx = EOS.SignPushActions(net, wif, actions)

	return
}

func Approve(net, wif, proposer, proposalName, actor, permission string) string {

	p := eos.PermissionLevel{Actor: eos.AccountName(actor), Permission: eos.PermissionName(permission)}

	action := msig.NewApprove(eos.AccountName(proposer), eos.Name(proposalName), p)

	actions := make([]*eos.Action, 0)
	actions = append(actions, action)

	return EOS.SignPushActions(net, wif, actions)
}

func UnApprove(net, wif, proposer, proposalName, actor, permission string) string {

	p := eos.PermissionLevel{Actor: eos.AccountName(actor), Permission: eos.PermissionName(permission)}

	action := msig.NewUnapprove(eos.AccountName(proposer), eos.Name(proposalName), p)

	actions := make([]*eos.Action, 0)
	actions = append(actions, action)

	return EOS.SignPushActions(net, wif, actions)
}

func Cancel(net, wif, proposer, proposalName, canceler string) string {
	action := msig.NewCancel(eos.AccountName(proposer), eos.Name(proposalName), eos.AccountName(canceler))

	actions := make([]*eos.Action, 0)
	actions = append(actions, action)

	return EOS.SignPushActions(net, wif, actions)
}

func Exec(net, wif, proposer, proposalName, executer string) string {
	action := msig.NewExec(eos.AccountName(proposer), eos.Name(proposalName), eos.AccountName(executer))

	actions := make([]*eos.Action, 0)
	actions = append(actions, action)

	return EOS.SignPushActions(net, wif, actions)
}
