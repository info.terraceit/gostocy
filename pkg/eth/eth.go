package eth

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"math"
	"math/big"
	"net/http"
	"reflect"
	"strconv"
	"strings"

	"github.com/ethereum/go-ethereum"
	Abi "github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/accounts/keystore"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/hexutil"
	mathETH "github.com/ethereum/go-ethereum/common/math"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"golang.org/x/net/context"
)

const (
	Ether  string = "Ether"
	Finney string = "Finney"
	Szabo  string = "Szabo"
	Gwei   string = "Gwei"
	Mwei   string = "Mwei"
	Kwei   string = "Kwei"
)

//---------------------------------------------------------------------//

func NewTransaction(nonce int64, to string, amount *big.Int, gasLimit int64, gasPrice *big.Int, data []byte) *types.Transaction {
	return types.NewTransaction(uint64(nonce), common.HexToAddress(to), amount, uint64(gasLimit), gasPrice, common.CopyBytes(data))
}

func NewRawTransaction(privKey string, tx *types.Transaction) *types.Transaction {
	key, err := crypto.HexToECDSA(privKey)
	if err != nil {
		return nil
	}

	signer := types.HomesteadSigner{}
	signature, err := crypto.Sign(signer.Hash(tx).Bytes(), key)
	if err != nil {
		return nil
	}
	rawTx, err := tx.WithSignature(signer, signature)
	if err != nil {
		return nil
	}

	return rawTx
}

func NewCallMsg(from, to string, value *big.Int, gasLimit int64, gasPrice *big.Int, data []byte) *ethereum.CallMsg {
	_to := common.HexToAddress(to)
	msg := &ethereum.CallMsg{
		From:     common.HexToAddress(from),
		To:       &_to,
		Gas:      uint64(gasLimit),
		GasPrice: gasPrice,
		Value:    value,
		Data:     data,
	}

	return msg
}

// method : "transfer"
func Pack(abi, method string, params ...interface{}) ([]byte, error) {
	parsed, err := Abi.JSON(strings.NewReader(abi))
	if err != nil {
		return []byte{}, err
	}

	b, err := parsed.Pack(method, params...)
	if err != nil {
		return []byte{}, err
	}

	return b, nil
}

func UnPack(abi string, fmts []*types.Log) (mEvent map[string]interface{}, err error) {
	parsed, err := Abi.JSON(strings.NewReader(abi))
	if err != nil {
		return
	}

	mEvent = make(map[string]interface{})
	for _, fmt := range fmts {
		for name, _ := range parsed.Events {
			ev := map[string]interface{}{}
			err := parsed.UnpackIntoMap(ev, name, fmt.Data)
			if err == nil {
				mEvent[name] = ev
				goto GO
			}
		}
	GO:
	}
	return
}

// TODO : int uint float ....
func PackParams(params ...interface{}) []byte {
	data := make([]byte, 0)
	for _, value := range params {
		r := reflect.ValueOf(value)
		if r.Kind() == reflect.String {
			v := common.HexToAddress(value.(string)).Bytes()
			data = append(data, common.BytesToHash(v).Bytes()...)
		} else if r.Kind() == reflect.Slice {

			if arrAdd, okAdd := value.([]common.Address); okAdd {
				for _, addr := range arrAdd {
					v := addr.Bytes()
					data = append(data, common.BytesToHash(v).Bytes()...)
				}
			}

			if arrStr, okStr := value.([]string); okStr {
				for _, str := range arrStr {
					v := common.HexToAddress(str).Bytes()
					data = append(data, common.BytesToHash(v).Bytes()...)
				}
			}

			if arrBig, okBig := value.([]*big.Int); okBig {
				for _, b := range arrBig {
					v := common.BigToHash(b).Bytes()
					data = append(data, v...)
				}
			}

		} else {
			v := common.BigToHash(value.(*big.Int)).Bytes()
			data = append(data, v...)
		}
	}
	return data
}

func PackInputData(method string, params ...interface{}) []byte {
	// Input //
	//method := `print(uint256)`
	//value := big.NewInt(11)
	event := Sha3FromEvent(method)
	sig := common.FromHex(event)[:4]
	data := make([]byte, 0)
	data = append(data, sig...)
	data = append(data, PackParams(params...)...)

	//input := common.ToHex(data)
	//	fmt.Println("method : ", method)
	//	fmt.Println("event : ", event)
	//	fmt.Println("sig : ", common.ToHex(sig))
	//	fmt.Println("value : ", params)
	//	fmt.Println("data : ", data)
	//fmt.Println("input : ", input)
	// Input //
	return data
}

/*
func (ec *Client) SolidityCompile(path string) (map[string]map[string]string, error) {
	if _, err := exec.LookPath("solc"); err != nil {
		fmt.Println("exec solc", err)
		return nil, err
	}
	fmt.Println("SolidityCompile : ", path)

	data, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}
	source := string(data)
	fmt.Println("source : ", source)

	contracts, err := Solc.CompileSolidityString("", source)
	if err != nil {
		return nil, err
	}
	fmt.Println("compile contracts : ", contracts)

	mContracts := make(map[string]map[string]string)
	for stdin, c := range contracts {
		//c, _ := contracts["<stdin>:Test"]
		name := strings.Split(stdin, "<stdin>:")[1]
		code := c.Code
		abi, _ := json.Marshal(c.Info.AbiDefinition)

		fmt.Println("name : ", name)
		fmt.Println("code : ", code)
		fmt.Println("abi : ", string(abi))

		mCon := map[string]string{
			"code": code,
			"abi":  string(abi),
		}
		mContracts[name] = mCon
	}

	return mContracts, nil
}
*/

//---------------------------------------------------------------------//

func ToWei(value float64, from string) float64 {
	switch from {
	case Ether:
		value *= math.Pow10(18)
	case Finney:
		value *= math.Pow10(15)
	case Szabo:
		value *= math.Pow10(12)
	case Gwei:
		value *= math.Pow10(9)
	case Mwei:
		value *= math.Pow10(6)
	case Kwei:
		value *= math.Pow10(6)
	}
	return value
}

func ToWeiString(value float64, from string) string {
	f := ToWei(value, from)
	return strconv.FormatFloat(f, 'f', -1, 64)
}

func FromWei(value float64, to string) float64 {
	switch to {
	case Ether:
		value /= math.Pow10(18)
	case Finney:
		value /= math.Pow10(15)
	case Szabo:
		value /= math.Pow10(12)
	case Gwei:
		value /= math.Pow10(9)
	case Mwei:
		value /= math.Pow10(6)
	case Kwei:
		value /= math.Pow10(6)
	}
	return value
}

func FromWeiString(value float64, from string) string {
	f := FromWei(value, from)
	return strconv.FormatFloat(f, 'f', -1, 64)
}

func ToBigNumber(value int64) string {
	return hexutil.EncodeUint64(uint64(value))
}

func ToBig256(s string) *big.Int {
	bi, ok := mathETH.ParseBig256(s)
	if !ok {
		return nil
	}
	////fmt.Println(bi, ok)
	return bi
}

func ToBigFloat256(b []byte) float64 {
	i := common.BytesToHash(b).Big()
	f := new(big.Float).SetInt(i)
	ff, _ := f.Float64()
	return ff
}

func ToString(f float64) string {
	return strconv.FormatFloat(f, 'f', -1, 64)
}

func FloatToBigInt(value float64, decimal int) *big.Int {
	bigval := new(big.Float)
	bigval.SetFloat64(value)

	coin := new(big.Float)
	// coin.SetInt(big.NewInt(1000000000000000000)) // wei 18 zero
	pow := int64(math.Pow10(decimal))
	coin.SetInt(big.NewInt(pow))

	bigval.Mul(bigval, coin)

	bi := new(big.Int)
	bigval.Int(bi)

	return bi
}

func ByteToFloat(data []byte, decimal int) (*big.Int, string) {
	bigI := new(big.Int).SetBytes(data)
	fL, _ := bigI.Float64()
	fL /= math.Pow10(decimal)
	return bigI, strconv.FormatFloat(fL, 'f', -1, 64)
}

func BigToFloat(bigI *big.Int, decimal int) (float64, string) {
	bigF, _ := bigI.Float64()
	fL := bigF / math.Pow10(decimal)
	return fL, strconv.FormatFloat(fL, 'f', -1, 64)
}

//---------------------------------------------------------------------//

type KeyPair struct {
	Privkey string
	PubKey  string
	Address string
}

func NewKeyPair() (*KeyPair, error) {
	// Generate a new random account and a funded simulator
	key, err := crypto.GenerateKey()
	if err != nil {
		return &KeyPair{}, err
	}
	addr := crypto.PubkeyToAddress(key.PublicKey)

	keyHex := common.Bytes2Hex(crypto.FromECDSA(key))
	pubHex := common.Bytes2Hex(crypto.FromECDSAPub(&key.PublicKey)) // common.ToHex(crypto.FromECDSAPub(&key.PublicKey))

	return &KeyPair{Privkey: keyHex, PubKey: strings.ToLower(pubHex), Address: strings.ToLower(addr.Hex())}, nil
}

func NewKeyPairFromPrivKey(hex string) (*KeyPair, error) {
	key, err := crypto.HexToECDSA(hex)
	if err != nil {
		return &KeyPair{}, err
	}

	keyHex := common.Bytes2Hex(crypto.FromECDSA(key))
	pubHex := common.Bytes2Hex(crypto.FromECDSAPub(&key.PublicKey)) //common.ToHex(crypto.FromECDSAPub(&key.PublicKey))
	addr := crypto.PubkeyToAddress(key.PublicKey)

	return &KeyPair{Privkey: keyHex, PubKey: strings.ToLower(pubHex), Address: strings.ToLower(addr.Hex())}, nil
}

func StoreAccount(keyHex, pass, path string) (string, error) {
	key, _ := crypto.HexToECDSA(keyHex)
	ks := keystore.NewKeyStore(path, keystore.StandardScryptN, keystore.StandardScryptP)
	account, err := ks.ImportECDSA(key, pass)

	//fmt.Println("ETH Accounts : ", account, "Error : ", err)
	return account.Address.Hex(), err
}

func IsHexAddress(address string) bool {
	return common.IsHexAddress(strings.ToLower(address))
}

func ValidateAmount(amount string) (float64, error) {
	f, err := strconv.ParseFloat(amount, 64)
	if err != nil || len(amount) > 19 {
		//fmt.Println("ValidateAmount ", err)
		return 0, errors.New(err.Error())
	}
	//fmt.Println("ValidateAmount : ", f)
	return f, nil
}

func Sha3FromHex(s string) string {
	return crypto.Keccak256Hash(common.FromHex(s)).Hex()
}

// eventSignature := []byte("ItemSet(bytes32,bytes32)")
// 0xe79e73da417710ae99aa2088575580a60415d359acfad9cdd3382d59c80281d4
// Transfer(address,address,uint256)
func Sha3FromEvent(s string) string {
	return crypto.Keccak256Hash([]byte(s)).Hex()
}

func AddressFromEvent(s string) string {
	data := common.FromHex(s)
	hex := common.Bytes2Hex(data[12:]) //common.ToHex(data[12:])
	return hex
}

func Sign(privateKey string, message []byte) ([]byte, error) {
	key, err := crypto.HexToECDSA(privateKey)
	if err != nil {
		return nil, err
	}

	// Message Hash
	messageHash := crypto.Keccak256(message)
	signature, err := crypto.Sign(messageHash, key)
	if err != nil {
		return nil, err
	}
	return signature, nil
}

func VerifySignature(pubkey, message, signature []byte) bool {
	// Message Hash
	messageHash := crypto.Keccak256(message)
	pubkeyFromHash, err := crypto.Ecrecover(messageHash, signature)
	if err != nil {
		return false
	}
	return bytes.Equal(pubkeyFromHash, pubkey)
}

func call_RPC(url, method string, paramsIn ...interface{}) map[string]interface{} {
	//fmt.Println("method : ", method, "  params : ", paramsIn)
	values := map[string]interface{}{"jsonrpc": "2.0", "method": method, "params": paramsIn, "id": 1}
	jsonStr, _ := json.Marshal(values)

	resp, err := http.Post(url, "application/json", bytes.NewBuffer(jsonStr))
	if err != nil {
		//fmt.Println("Error web3 call ", err)
		return nil
	}
	defer resp.Body.Close()

	result := map[string]interface{}{}
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		//fmt.Println("Error web3 call ", err)
		return nil
	}

	//fmt.Println(method, string(body))
	err = json.Unmarshal(body, &result)
	if err != nil {
		//fmt.Println("Error web3 call ", err)
		return nil
	}

	return result
}

// ---------------------------------------------------------------------//
var txList map[string][]int64

type Client struct {
	client  *ethclient.Client
	URL     string
	ChainId *big.Int
}

func NewClient(url string) (cli *Client, err error) {
	// connect to rpc
	rpc, err := ethclient.Dial(url)
	if err != nil {
		fmt.Println("Error ethClient ", err)
		return
	}
	txList = make(map[string][]int64)

	chainId, err := rpc.ChainID(context.Background())
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	cli = &Client{client: rpc, URL: url, ChainId: chainId}

	return
}

func (ec *Client) BlockNumber() (uint64, error) {
	return ec.client.BlockNumber(context.Background())
}

func (ec *Client) GetBlockNumber() string {
	result := call_RPC(ec.URL, "eth_blockNumber")
	if _, ok := result["result"]; !ok {
		return "0x"
	}
	return result["result"].(string)
}

func (ec *Client) GetBlockByHash(hex string) (*types.Block, error) {
	hash := common.HexToHash(hex)
	bl, err := ec.client.BlockByHash(context.Background(), hash)
	return bl, err
}

func (ec *Client) GetBlockHeaderByHash(hex string) (*types.Header, error) {
	hash := common.HexToHash(hex)
	h, err := ec.client.HeaderByHash(context.Background(), hash)
	return h, err
}

func (ec *Client) GetBlockByNumber(blockNumber string) (*types.Block, error) {
	numBig := ToBig256(blockNumber)
	bl, err := ec.client.BlockByNumber(context.Background(), numBig)
	return bl, err
}

func (ec *Client) GetBlockHeaderByNumber(blockNumber string) (*types.Header, error) {
	numBig := ToBig256(blockNumber)
	h, err := ec.client.HeaderByNumber(context.Background(), numBig)
	return h, err
}

func (ec *Client) GetBlockByNumber_Web3(number interface{}) (data map[string]interface{}, err error) {
	result := call_RPC(ec.URL, "eth_getBlockByNumber", number, true)
	if _, ok := result["result"]; !ok {
		err = errors.New("error GetBlockByNumber_Web3")
		return
	}
	data = result["result"].(map[string]interface{})
	return
}

func (ec *Client) GetTransactionByHash(hex string) (*types.Transaction, error) {
	tx, _, err := ec.client.TransactionByHash(context.Background(), common.HexToHash(hex))
	return tx, err
}

func (ec *Client) GetTransactionCount(hex string) (int, error) {
	hash := common.HexToHash(hex)
	rawCount, err := ec.client.TransactionCount(context.Background(), hash)
	return int(rawCount), err
}

func (ec *Client) GetTransactionInBlock(hex string, index int) (*types.Transaction, error) {
	tx, err := ec.client.TransactionInBlock(context.Background(), common.HexToHash(hex), uint(index))
	return tx, err
}

func (ec *Client) GetTransactionReceipt(hex string) (*types.Receipt, error) {
	rawReceipt, err := ec.client.TransactionReceipt(context.Background(), common.HexToHash(hex))
	if err != nil {
		return nil, err
	}
	return rawReceipt, err
}

func (ec *Client) GetBalance(addr string) (*big.Int, error) {
	numBig := ToBig256(ec.GetBlockNumber())
	balance, err := ec.client.BalanceAt(context.Background(), common.HexToAddress(addr), numBig)
	return balance, err
}

func (ec *Client) GetBalanceOf(addr, contract string, decimal int) (*big.Int, string) {
	hex, err := ec.SolidityCallRaw(addr, contract, `balanceOf(address)`, addr)
	if err != nil {
		fmt.Println("ERC20 Token not enough !!!")
		return nil, ""
	}

	// weiBigInt := new(big.Int).SetBytes(hex)
	// weBigIntCommon := common.BytesToHash(hex).Big()
	// fmt.Println("balanceOf BigInt :", weiBigInt, weBigIntCommon)
	// fmt.Println("balanceOf Int64 :", weiBigInt.Int64())
	// fmt.Println("balanceOf Uint64 :", weBigIntCommon.Uint64())

	bigI, fStr := ByteToFloat(hex, decimal)
	//fmt.Println("balanceOf :", bigI, fStr)
	return bigI, fStr
}

func (ec *Client) CodeAt(addr string) string {
	var aC common.Address = common.HexToAddress(addr)
	code, err := ec.client.CodeAt(context.Background(), aC, nil)
	if err != nil {
		return "0x0"
	}
	return common.Bytes2Hex(code) //common.ToHex(code)
}

func (ec *Client) GetNonceAt(addr string, number int64) (nonce int64, _ error) {
	address := common.HexToAddress(addr)
	if number < 0 {
		rawNonce, err := ec.client.NonceAt(context.Background(), address, nil)
		return int64(rawNonce), err
	}
	rawNonce, err := ec.client.NonceAt(context.Background(), address, big.NewInt(number))
	return int64(rawNonce), err
}

func (ec *Client) GetPendingBalanceAt(addr string) (*big.Int, error) {
	bi, err := ec.client.PendingBalanceAt(context.Background(), common.HexToAddress(addr))
	return bi, err
}

func (ec *Client) GetPendingStorageAt(addr, hex string) ([]byte, error) {
	address := common.HexToAddress(addr)
	hash := common.HexToHash(hex)
	return ec.client.PendingStorageAt(context.Background(), address, hash)
}

func (ec *Client) GetPendingCodeAt(addr string) ([]byte, error) {
	address := common.HexToAddress(addr)
	return ec.client.PendingCodeAt(context.Background(), address)
}

func (ec *Client) GetPendingNonceAt(addr string) (int64, error) {
	address := common.HexToAddress(addr)
	nonce, err := ec.client.PendingNonceAt(context.Background(), address)
	if err != nil {
		return 0, err
	}

	if len(txList[addr]) <= 0 {
		txList[addr] = make([]int64, 0)
	} else {
		max := len(txList[addr])
		nc := txList[addr][max-1] + 1
		if nc >= int64(nonce) {
			nonce = uint64(nc)
		}
	}
	txList[addr] = append(txList[addr], int64(nonce))

	return int64(nonce), err
}

func (ec *Client) GetPendingTransactionCount() (count int, _ error) {
	rawCount, err := ec.client.PendingTransactionCount(context.Background())
	return int(rawCount), err
}

func (ec *Client) SuggestGasPrice() (*big.Int, error) {
	price, err := ec.client.SuggestGasPrice(context.Background())
	return price, err
}

func (ec *Client) EstimateGas(msg ethereum.CallMsg) (int64, error) {
	rawGas, err := ec.client.EstimateGas(context.Background(), msg)
	return int64(rawGas), err
}

func (ec *Client) EsimateGasLimit(fromAddress, contractAddress, abi, method string, params ...interface{}) ([]byte, *big.Int, int64, int64) {
	// price
	gasPrice, err := ec.SuggestGasPrice()
	if err != nil {
		fmt.Println(err)
		return nil, nil, 0, 0
	}
	// fmt.Println("gasPrice : ", gasPrice)

	// bytecode
	byteCode, err := Pack(abi, method, params...)
	if err != nil {
		fmt.Println(err)
		return nil, nil, 0, 0
	}
	// fmt.Println("byteCode", hex.EncodeToString(byteCode))

	// estimate gas
	var from common.Address = common.HexToAddress(fromAddress)
	var to common.Address = common.HexToAddress(contractAddress)
	msg := ethereum.CallMsg{From: from, To: &to, Data: byteCode}

	// gasUsed
	gasUsed, err := ec.EstimateGas(msg)
	if err != nil {
		fmt.Println(err)
		return nil, nil, 0, 0
	}
	// fmt.Println("gasUsed : ", gasUsed)

	// gas
	gas := gasUsed * gasPrice.Int64()
	// fmt.Println("gas :", gas)

	return byteCode, gasPrice, gasUsed, gas
}

func (ec *Client) SendTransaction(tx *types.Transaction) error {
	return ec.client.SendTransaction(context.Background(), tx)
}

func (ec *Client) SendTransactionRaw(prvKey, to string, amount *big.Int, gasLimit uint64, gasPrice *big.Int, data []byte) (string, error) {
	key0, err := crypto.HexToECDSA(prvKey)
	if err != nil {
		fmt.Println("error estimateGas")
		return "", err
	}
	addr0 := crypto.PubkeyToAddress(key0.PublicKey)
	//opts := bind.NewKeyedTransactor(key0)

	//valueWei := ToWei(amETH, "ether")
	//f := strconv.FormatFloat(valueWei, 'f', -1, 64)
	//value := ToBig256(f)

	// gasLimit //
	//var aC common.Address = common.HexToAddress(to)
	//msg := ethereum.CallMsg{From: opts.From, To: &aC, Value: big.NewInt(value), Data: data}
	//gasLimit, erg := ec.client.EstimateGas(context.Background(), msg)
	//if erg != nil {
	//	fmt.Println("Error EstimateGas", erg)
	//	return ""
	//}
	//fmt.Println("msg : ", msg)

	// Nonce //
	nonce, err := ec.GetPendingNonceAt(addr0.Hex())
	if err != nil {
		n, _ := fmt.Printf("%v", err)
		fmt.Println(n)
		return "", err
	}
	//fmt.Println("nonce : ", nonce)

	// NewTransaction
	tx := types.NewTransaction(uint64(nonce), common.HexToAddress(to), amount, gasLimit, gasPrice, data)
	//fmt.Println("tx un signed : ", tx)

	signer := types.HomesteadSigner{}
	signature, err := crypto.Sign(signer.Hash(tx).Bytes(), key0)
	if err != nil {
		fmt.Println("error Sign")
		return "", err
	}
	rawTx, err := tx.WithSignature(signer, signature)
	if err != nil {
		fmt.Println("error withSignature")
		return "", err
	}
	//fmt.Println("rawTx : ", rawTx, rawTx.Hash().Hex())

	err = ec.client.SendTransaction(context.Background(), rawTx)
	if err != nil {
		fmt.Println("error sendTransaction :", err)
		return "", err
	}

	return rawTx.Hash().Hex(), nil
}

func (ec *Client) CallContract(msg ethereum.CallMsg, blockNumber int64) ([]byte, error) {
	return ec.client.CallContract(context.Background(), msg, big.NewInt(blockNumber))
}

func (ec *Client) SolidityCall(addr, abi, method string, params ...interface{}) (result []interface{}, err error) {
	parsed, err := Abi.JSON(strings.NewReader(abi))
	if err != nil {
		fmt.Println(err)
		return
	}

	contract := bind.NewBoundContract(common.HexToAddress(addr), parsed, ec.client, ec.client, ec.client)
	err = contract.Call(nil, &result, method, params...)
	if err != nil {
		fmt.Println(err)
		return
	}
	return
}

// method = 'transfer'
func (ec *Client) SolidityTransact(prvKey, addr, abi, method string, value, gasPrice, gasLimit int64, params ...interface{}) (string, error) {
	parsed, err := Abi.JSON(strings.NewReader(abi))
	if err != nil {
		return "", err
	}

	key0, err := crypto.HexToECDSA(prvKey)
	if err != nil {
		return "", err
	}
	addr0 := crypto.PubkeyToAddress(key0.PublicKey)

	// Nonce //
	nonce, err := ec.client.PendingNonceAt(context.Background(), addr0)
	if err != nil {
		return "", err
	}

	opts, err := bind.NewKeyedTransactorWithChainID(key0, ec.ChainId)
	if err != nil {
		fmt.Println(err)
		return "", err
	}
	opts.Nonce = big.NewInt(int64(nonce))
	opts.Value = big.NewInt(value)
	opts.GasPrice = big.NewInt(gasPrice)
	opts.GasLimit = uint64(gasLimit)

	contract := bind.NewBoundContract(common.HexToAddress(addr), parsed, ec.client, ec.client, ec.client)
	tx, err := contract.Transact(opts, method, params...)
	if err != nil {
		return "", err
	}

	return tx.Hash().Hex(), nil
}

func (ec *Client) SolidityDeploy(prvKey, abi, codeHex string, value, gasPrice *big.Int, gasLimit uint64, params ...interface{}) (string, string, *bind.BoundContract, error) {
	parsed, err := Abi.JSON(strings.NewReader(abi))
	if err != nil {
		fmt.Println(err)
		return "", "", nil, err
	}
	//fmt.Println("abi : ", parsed)

	key, err := crypto.HexToECDSA(prvKey)
	if err != nil {
		fmt.Println(err)
		return "", "", nil, err
	}
	opts, err := bind.NewKeyedTransactorWithChainID(key, ec.ChainId)
	if err != nil {
		fmt.Println(err)
		return "", "", nil, err
	}
	opts.Value = value
	opts.GasPrice = gasPrice
	opts.GasLimit = gasLimit
	//fmt.Println("params........", len(params), params)

	addr, tx, contract, err := bind.DeployContract(opts, parsed, common.FromHex(codeHex), ec.client, params...)
	if err != nil {
		fmt.Println(err)
		return "", "", nil, err
	}
	// fmt.Println("contract : ", contract)
	// fmt.Println("address : ", addr.Hex())
	// fmt.Println("tx : ", tx)

	return addr.Hex(), tx.Hash().Hex(), contract, nil
}

// SolRaw(prvKey, addressContract, `transfer(address,uint256)`, `0x5e531bb813994f27f65d2d5f7dc7c51dbe5406f7`, big.NewInt(100))
func (ec *Client) SolidityTransactRaw(prvKey, addr, method string, amount *big.Int, gasLimit uint64, gasPrice *big.Int, params ...interface{}) (string, error) {
	key0, _ := crypto.HexToECDSA(prvKey)
	addr0 := crypto.PubkeyToAddress(key0.PublicKey)
	//opts := bind.NewKeyedTransactor(key0)

	// gasLimit //
	//var aC common.Address = common.HexToAddress(addr)
	//msg := ethereum.CallMsg{From: opts.From, To: &aC, Value: amount, Data: data}
	//gasLimit, erg := ec.client.EstimateGas(context.Background(), msg)
	//if erg != nil {
	//	fmt.Println("Error gasLimit")
	//	return ""
	//}
	//fmt.Println("gasLimit", gasLimit)
	//
	//gasPrice, err := ec.client.SuggestGasPrice(context.Background())
	//if erg != nil {
	//	fmt.Println("Error gasPrice")
	//	return ""
	//}
	//fmt.Println("gasPrice", gasPrice)

	// Nonce //
	nonce, err := ec.client.PendingNonceAt(context.Background(), addr0)
	if err != nil {
		fmt.Println("error nonce", err)
		return "", err
	}
	//fmt.Println("nonce : ", nonce)

	// Input Data
	data := PackInputData(method, params...)
	if len(data) <= 0 {
		return "", errors.New("error data length empty")
	}

	// NewTransaction
	tx := types.NewTransaction(nonce, common.HexToAddress(addr), amount, gasLimit, gasPrice, data)
	//fmt.Println("tx un signed : ", tx)

	signer := types.HomesteadSigner{}
	signature, err := crypto.Sign(signer.Hash(tx).Bytes(), key0)
	if err != nil {
		fmt.Println("error sign", err)
		return "", err
	}
	rawTx, err := tx.WithSignature(signer, signature)
	if err != nil {
		fmt.Println("error withSignature", err)
		return "", err
	}

	// SendTraction to Contract //
	err = ec.client.SendTransaction(context.Background(), rawTx)
	if err != nil {
		fmt.Println("error sendTransacction ", err)
		return "", err
	}
	//fmt.Println("rawTx : ", rawTx, rawTx.Hash().Hex())

	return rawTx.Hash().Hex(), nil
}

// `balanceOf(address)`
func (ec *Client) SolidityCallRaw(_from, _to, method string, params ...interface{}) (hexutil.Bytes, error) {
	// Input Data
	data := PackInputData(method, params...)
	if len(data) <= 0 {
		return nil, errors.New("error data length empty")
	}

	var from common.Address = common.HexToAddress(_from)
	var to common.Address = common.HexToAddress(_to)
	msg := ethereum.CallMsg{From: from, To: &to, Data: data}
	hex, err := ec.client.CallContract(context.Background(), msg, nil)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	return hex, err
}
