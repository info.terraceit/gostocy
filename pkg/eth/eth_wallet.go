package eth

import (
	"encoding/hex"
	"encoding/json"
	"strconv"
	"sync"

	"gitlab.com/info.terraceit/gostocy/pkg/btc"
	"gitlab.com/info.terraceit/gostocy/pkg/cryptocy"

	"github.com/bartekn/go-bip39"
	"github.com/btcsuite/btcutil/hdkeychain"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
)

type Account struct {
	Net  string `json:"net"`
	Salt string `json:"salt"`

	KeyPair *KeyPair
}

// m/44'/cointype'/0'/0/i
func NewAccount(net string, salt string, coinType uint32, seed []byte) (acc *Account, err error) {
	acc = &Account{}
	acc.Net = net
	acc.Salt = salt
	acc.KeyPair = new(KeyPair)
	indexs := cryptocy.MultiHash(salt)

	// gen key
	acc.GenKey(coinType, seed, indexs...)

	return
}

func (acc *Account) GenKey(cointype uint32, seed []byte, index ...uint32) error {
	//m/
	ext, err := hdkeychain.NewMaster(seed, btc.ChainConfig(acc.Net))
	if err != nil {
		return err
	}

	// m/44'
	purpose, err := ext.Child(44 + hdkeychain.HardenedKeyStart)
	if err != nil {
		return err
	}

	// m/44'/altcointype'
	coinType, err := purpose.Child(cointype + hdkeychain.HardenedKeyStart)
	if err != nil {
		return err
	}

	// m/44'/altcointype'/0'
	acct0, err := coinType.Child(uint32(0) + hdkeychain.HardenedKeyStart)
	if err != nil {
		return err
	}

	// m/44'/altcointype'/0'/0
	// 0 = external accounts for receive addresses
	acct0External, err := acct0.Child(0)
	if err != nil {
		return err
	}

	var receive *hdkeychain.ExtendedKey
	for i, v := range index {
		if i == 0 {
			receive, err = acct0External.Child(uint32(v))
			if err != nil {
				//"Failed to create %v receive address: %s"
				return err
			}
			continue
		}
		receive, err = receive.Child(uint32(v))
		if err != nil {
			//"Failed to create %v receive address: %s"
			return err
		}
	}

	privk, err := receive.ECPrivKey()
	if err != nil {
		//"ECPrivKey() failed"
		return err
	}

	pubk, err := receive.ECPubKey()
	if err != nil {
		//"ECPubKey() failed"
		return err
	}
	address := crypto.PubkeyToAddress(*pubk.ToECDSA())

	kp := new(KeyPair)
	kp.Privkey = hex.EncodeToString(privk.Serialize())
	kp.PubKey = common.Bytes2Hex(crypto.FromECDSAPub(pubk.ToECDSA()))
	kp.Address = address.String()

	acc.KeyPair = kp
	return nil
}

func (c *Account) GetAddress() string {
	return c.KeyPair.Address
}

func (c *Account) GetPrivKey() string {
	return c.KeyPair.Privkey
}

func (c *Account) GetPubKey() string {
	return c.KeyPair.PubKey
}

//---------------------------------------------------------------------//

type Wallet struct {
	mux *sync.Mutex

	Net      string `json:"net"`
	Password string `json:"password"`
	Mnemoric string `json:"mnemoric"`
	Seed     []byte `json:"seed"`
	//Accounts      []*Account `json:"accounts"`
	Accounts map[string]*Account `json:"accounts"`

	//
	Balance float64 `json:"balance"`
	ListTX  string  `json:"listTX"`
}

func NewWallet(net, mnemoric, password string) (w *Wallet, err error) {
	seed, err := bip39.NewSeedWithErrorChecking(mnemoric, password)
	if err != nil {
		return
	}

	w = new(Wallet)
	w.mux = &sync.Mutex{}
	w.Password = password
	w.Mnemoric = mnemoric
	w.Seed = seed
	w.Net = net
	//w.Accounts = make([]*Account, 0)
	w.Accounts = make(map[string]*Account)
	return
}

func NewWalletSeed(net string, seed []byte) (w *Wallet, err error) {
	w = new(Wallet)
	w.mux = &sync.Mutex{}
	w.Seed = seed
	w.Net = net
	//w.Accounts = make([]*Account, 0)
	w.Accounts = make(map[string]*Account)
	return
}

func (w *Wallet) String() string {
	wall := make(map[string]interface{})
	wall["net"] = w.Net
	wall["password"] = w.Password
	wall["mnemoric"] = w.Mnemoric
	wall["seed"] = hex.EncodeToString(w.Seed)
	wall["accounts"] = w.Accounts

	b, err := json.MarshalIndent(wall, "", " ")
	if err != nil {
		return ""
	}

	return string(b)
}

func (w *Wallet) CreateAccount(coinType uint32, salt string) (acc *Account, err error) {
	w.mux.Lock()
	defer w.mux.Unlock()

	acc, err = NewAccount(w.Net, salt, coinType, w.Seed)
	if err != nil {
		return
	}
	//w.Accounts = append(w.Accounts, acc)
	w.Accounts[salt] = acc
	return
}

func (w *Wallet) ListAccounts() string {
	b, err := json.MarshalIndent(w.Accounts, "", " ")
	if err != nil {
		return ""
	}

	return string(b)
}

func (w *Wallet) ListTransactions() string {
	return w.ListTX
}

func (w *Wallet) GetAccount(salt string) (acc *Account) {
	w.mux.Lock()
	defer w.mux.Unlock()

	acc = w.Accounts[salt]
	return
}

func (w *Wallet) GetBalance() float64 {
	return w.Balance
}

// ---------------------------------------------------------------------//
func ListTransaction(addr string, fromHeight, toHeight int64, cli *Client) (list string) {
	txS := make([]map[string]interface{}, 0)
	i := fromHeight
	for i <= toHeight {
		numHash := ToBigNumber(i)
		block, err := cli.GetBlockByNumber_Web3(numHash)
		if err != nil {
			return
		}

		var trans []map[string]interface{}
		tranB, _ := json.Marshal(block["transactions"])
		json.Unmarshal(tranB, &trans)
		for _, txObj := range trans {
			if addr == txObj["from"] || addr == txObj["to"] {
				value := ToBig256(txObj["value"].(string)).String()
				valueF, _ := strconv.ParseFloat(value, 64)
				txObj["value"] = FromWeiString(valueF, "ether")
				gas := ToBig256(txObj["gas"].(string)).String()
				txObj["gas"] = gas
				gasPrice := ToBig256(txObj["gasPrice"].(string)).String()
				gasPriceF, _ := strconv.ParseFloat(gasPrice, 64)
				txObj["gasPrice"] = FromWeiString(gasPriceF, "gwei")
				nonce := ToBig256(txObj["nonce"].(string)).String()
				txObj["nonce"] = nonce
				txS = append(txS, txObj)

			}

			/*
							{
				  "blockHash": "0x8a34cf6b4bd5ab6ea40b87bb891bbb0e3f2135da9c80a60305dba84303e74e7f",
				  "blockNumber": "0xadc9",
				  "from": "0x8dd75f7c03a048c0a66a53dbf9ed76d04e9a9ea3",
				  "gas": "0x15f90",
				  "gasPrice": "0x430e23400",
				  "hash": "0xe3b1a7e2d60ab36314d6c5080244ff98b53b0b18b437d9705054218040a0f808",
				  "input": "0x",
				  "nonce": "0x708",
				  "r": "0x12a0d442d48842d6ed46f51d67d1b71ee49ce4cd7f535ce0a9c48f48f5e02f50",
				  "s": "0x78da8a6fbff2e9a1c75821258daa22b31af9a3d0133c70951f9f82e4641c708d",
				  "to": "0x56bfed926439bb905fca945f47d4e4ffff9a77b8",
				  "transactionIndex": "0x0",
				  "v": "0x3f",
				  "value": "0x1"
				 }
			*/
		}
		i++
	}

	b, _ := json.MarshalIndent(txS, "", " ")
	list = string(b)

	return
}
