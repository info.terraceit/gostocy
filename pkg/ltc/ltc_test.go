package ltc

import (
	"encoding/hex"
	//"encoding/json"
	"fmt"
	"testing"
)

func Test_Addr(t *testing.T) {
	keypair, _ := NewKeyPairWIF("simnet", "cNcJL3jMJrnBr5DBrJn3TBvE9v86cb76g1i4bwdkEESHzobKA39u")
	//keypair, _ := NewKeyPairRandom("simnet", true)
	fmt.Println("--- PrivKey : ", keypair.GetPrivKey())
	fmt.Println("--- PubKey : ", hex.EncodeToString(keypair.GetPubKey()))
	fmt.Println("--- PubKeyHash : ", hex.EncodeToString(keypair.GetPubKeyHash()))

	addrPK, _ := NewAddressPubKey(keypair.GetPubKey(), "simnet")
	fmt.Println("addrPK : ", addrPK)

	addrPKH, _ := NewAddressPubKeyHash(keypair.GetPubKeyHash(), "simnet")
	fmt.Println("addrPKH : ", addrPKH)

	addrP2SH, _ := addrPKH.AddressScriptHash("simnet")
	fmt.Println("addrP2SH : ", addrP2SH)

	addrPWKH, _ := addrPK.AddressWitnessPubKeyHash("simnet")
	fmt.Println("addrPWKH : ", addrPWKH)

	addrP2WSH, _ := addrPKH.AddressWitnessScriptHash("simnet")
	fmt.Println("addrP2WSH : ", addrP2WSH)

	pkScript, _ := PayToAddrScript(addrPKH.EncodeAddress(), "simnet")
	asm := DisasmString(pkScript)
	fmt.Println("..... pkScript : ", hex.EncodeToString(pkScript))
	fmt.Println("........ asm : ", asm)
}

/*
func Test_CLI(t *testing.T) {
	cli, err := NewClient("simnet", "localhost:19443", "123", "123")
	if err != nil {
		return
	}

	blockcount := cli.GetBlockCount()
	fmt.Println("............blockCount :", blockcount)
	SyncBlock(100, blockcount, cli)

	//---------------------------------------------------------------------//

	mnemoric := "frog toy sugar bar dinner approve gospel define youth jacket dolphin phone"
	seed, _ := GenSeedWithMnemoric(mnemoric, "123")
	//seed, _ := GenSeed("123")

	fmt.Println("....Seed......", seed.Mnemoric, hex.EncodeToString(seed.Seed))

	w, _ := NewWallet("simnet", seed.Mnemoric, "123")
	for i := 0; i < 1; i++ {
		w.CreateAccount()
	}

	encrypted := w.BackUp()
	fmt.Println("........encrypted.....", encrypted)

	wS, _ := Restore(hex.EncodeToString(seed.Seed), encrypted)
	fmt.Println("........decrypt.....", wS.String())

	//---------------------------------------------------------------------//
	addr := wS.Accounts[0].AddressExternals[0].Address

	txs := ListTransactions(addr)
	fmt.Println("..........txs..........", txs)

	utxos := ListUTXOs("simnet", addr)
	fmt.Println("..........utxos..........", utxos)

	balance := GetBalance("simnet", addr)
	fmt.Println("..............balance............", balance)

	//	b, _ := json.MarshalIndent(ManagedAccount, "", " ")
	//	fmt.Println("........ManagedAccount......", string(b))

	//	b, _ = json.MarshalIndent(ManagedAddress, "", " ")
	//	fmt.Println("........ManagedAddress......", string(b))

	//---------------------------------------------------------------------//

	tx := wS.NewTX("simnet")
	tx.AddSender(addr)
	tx.AddReceipt("mmnrzc3H7BtAr2WtqjWGX1C5RPrucxS79z", float64(0.01))
	tx.CreateRawTx(float64(0.001))
	tx.SignTx()
	fmt.Println(".....tx.....", tx.String())

	txHex := SendRawTx(tx, false, cli)
	fmt.Println(".....txHex.....", txHex)

}
*/
