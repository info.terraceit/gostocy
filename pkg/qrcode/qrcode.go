package qrcode

import (
	"fmt"
	"os"
	"runtime"

	"rsc.io/qr"
)

func WriteFile(size int, name, content, path string) string {
	url := ""
	if runtime.GOOS == "android" {
		url = path + "/" + name + ".png" // android
	} else if runtime.GOOS == "darwin" {
		url = path + name + ".png" // ios
	}

	fmt.Println("........qrcode File......", url)

	c, err := qr.Encode(content, qr.H)
	if err != nil {
		url = ""
	}

	os.WriteFile(url, c.PNG(), 0666)

	return url
}

func Encode(size int, content string) (png []byte, err error) {
	c, err := qr.Encode(content, qr.H)
	if err != nil {
		return
	}

	png = c.PNG()

	return
}
