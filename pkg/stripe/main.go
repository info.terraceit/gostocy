package stripe

import (
	"fmt"
	"os"

	"github.com/stripe/stripe-go"
	"github.com/stripe/stripe-go/token"
)

func main() {

	stripe.Key = "sk_test_zQSuhqob9Go8kH4gRrcxDB8f"

	tokenParams := &stripe.TokenParams{
		Card: &stripe.CardParams{
			Number:   stripe.String(os.Args[1]),
			ExpMonth: stripe.String(os.Args[2]),
			ExpYear:  stripe.String(os.Args[3]),
			CVC:      stripe.String(os.Args[4]),
		},
	}
	to, err := token.New(tokenParams)
	fmt.Println("...australia...", err, to.ID)

}
