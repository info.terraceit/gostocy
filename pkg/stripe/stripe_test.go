package stripe

import (
	//	"encoding/json"
	"fmt"
	"testing"

	"github.com/stripe/stripe-go"
	//	"github.com/stripe/stripe-go/client"

	//"github.com/stripe/stripe-go/order"
	//"github.com/stripe/stripe-go/product"
	//"github.com/stripe/stripe-go/sku"
	"github.com/stripe/stripe-go/token"
)

func TestCard(t *testing.T) {

	stripe.Key = "sk_test_zQSuhqob9Go8kH4gRrcxDB8f"

	/*
		tokenParams := &stripe.TokenParams{
			Card: &stripe.CardParams{
				Number:   stripe.String("4242424242424242"),
				ExpMonth: stripe.String("12"),
				ExpYear:  stripe.String("2019"),
				CVC:      stripe.String("123"),
			},
		}
		to, err := token.New(tokenParams)
		fmt.Println("...visa...", err, to.ID)

		tokenParams = &stripe.TokenParams{
			Card: &stripe.CardParams{
				Number:   stripe.String("4000056655665556"),
				ExpMonth: stripe.String("12"),
				ExpYear:  stripe.String("2019"),
				CVC:      stripe.String("123"),
			},
		}
		to, err = token.New(tokenParams)
		fmt.Println("...visa debit...", err, to.ID)

		tokenParams = &stripe.TokenParams{
			Card: &stripe.CardParams{
				Number:   stripe.String("5555555555554444"),
				ExpMonth: stripe.String("12"),
				ExpYear:  stripe.String("2019"),
				CVC:      stripe.String("123"),
			},
		}
		to, err = token.New(tokenParams)
		fmt.Println("...master card...", err, to.ID)

		tokenParams = &stripe.TokenParams{
			Card: &stripe.CardParams{
				Number:   stripe.String("2223003122003222"),
				ExpMonth: stripe.String("12"),
				ExpYear:  stripe.String("2019"),
				CVC:      stripe.String("123"),
			},
		}
		to, err = token.New(tokenParams)
		fmt.Println("...master card 2series...", err, to.ID)

		tokenParams = &stripe.TokenParams{
			Card: &stripe.CardParams{
				Number:   stripe.String("5200828282828210"),
				ExpMonth: stripe.String("12"),
				ExpYear:  stripe.String("2019"),
				CVC:      stripe.String("123"),
			},
		}
		to, err = token.New(tokenParams)
		fmt.Println("...master card debit...", err, to.ID)

		tokenParams = &stripe.TokenParams{
			Card: &stripe.CardParams{
				Number:   stripe.String("5105105105105100"),
				ExpMonth: stripe.String("12"),
				ExpYear:  stripe.String("2019"),
				CVC:      stripe.String("123"),
			},
		}
		to, err = token.New(tokenParams)
		fmt.Println("...master card prepaid...", err, to.ID)

		tokenParams = &stripe.TokenParams{
			Card: &stripe.CardParams{
				Number:   stripe.String("378282246310005"),
				ExpMonth: stripe.String("12"),
				ExpYear:  stripe.String("2019"),
				CVC:      stripe.String("123"),
			},
		}
		to, err = token.New(tokenParams)
		fmt.Println("...american express...", err, to.ID)

		tokenParams = &stripe.TokenParams{
			Card: &stripe.CardParams{
				Number:   stripe.String("371449635398431"),
				ExpMonth: stripe.String("12"),
				ExpYear:  stripe.String("2019"),
				CVC:      stripe.String("123"),
			},
		}
		to, err = token.New(tokenParams)
		fmt.Println("...american express...", err, to.ID)

		tokenParams = &stripe.TokenParams{
			Card: &stripe.CardParams{
				Number:   stripe.String("6011111111111117"),
				ExpMonth: stripe.String("12"),
				ExpYear:  stripe.String("2019"),
				CVC:      stripe.String("123"),
			},
		}
		to, err = token.New(tokenParams)
		fmt.Println("...discover...", err, to.ID)

		tokenParams = &stripe.TokenParams{
			Card: &stripe.CardParams{
				Number:   stripe.String("6011000990139424"),
				ExpMonth: stripe.String("12"),
				ExpYear:  stripe.String("2019"),
				CVC:      stripe.String("123"),
			},
		}
		to, err = token.New(tokenParams)
		fmt.Println("...discover...", err, to.ID)

		tokenParams = &stripe.TokenParams{
			Card: &stripe.CardParams{
				Number:   stripe.String("30569309025904"),
				ExpMonth: stripe.String("12"),
				ExpYear:  stripe.String("2019"),
				CVC:      stripe.String("123"),
			},
		}
		to, err = token.New(tokenParams)
		fmt.Println("...dinner club...", err, to.ID)

		tokenParams = &stripe.TokenParams{
			Card: &stripe.CardParams{
				Number:   stripe.String("38520000023237"),
				ExpMonth: stripe.String("12"),
				ExpYear:  stripe.String("2019"),
				CVC:      stripe.String("123"),
			},
		}
		to, err = token.New(tokenParams)
		fmt.Println("...dinner club...", err, to.ID)

		tokenParams = &stripe.TokenParams{
			Card: &stripe.CardParams{
				Number:   stripe.String("3566002020360505"),
				ExpMonth: stripe.String("12"),
				ExpYear:  stripe.String("2019"),
				CVC:      stripe.String("123"),
			},
		}
		to, err = token.New(tokenParams)
		fmt.Println("...jcb...", err, to.ID)

		tokenParams = &stripe.TokenParams{
			Card: &stripe.CardParams{
				Number:   stripe.String("6200000000000005"),
				ExpMonth: stripe.String("12"),
				ExpYear:  stripe.String("2019"),
				CVC:      stripe.String("123"),
			},
		}
		to, err = token.New(tokenParams)
		fmt.Println("...union pay...", err, to.ID)
	*/

	tokenParams := &stripe.TokenParams{
		Card: &stripe.CardParams{
			Number:   stripe.String("4000000360000006"),
			ExpMonth: stripe.String("12"),
			ExpYear:  stripe.String("2019"),
			CVC:      stripe.String("123"),
		},
	}
	to, err := token.New(tokenParams)
	fmt.Println("...australia...", err, to.ID)
}

/*
func TestCharge(t *testing.T) {

	stripe.Key = "sk_test_zQSuhqob9Go8kH4gRrcxDB8f"

	cli := client.API{}
	cli.Init(stripe.Key, nil)
	fmt.Println("........stripe.Key......", cli.Charges.Key)

	tokenParams := &stripe.TokenParams{
		Card: &stripe.CardParams{
			Number:   stripe.String("4000000360000006"),
			ExpMonth: stripe.String("12"),
			ExpYear:  stripe.String("2019"),
			CVC:      stripe.String("123"),
		},
	}
	to, err := token.New(tokenParams)
	fmt.Println("......", err, to)

	chargeParams := &stripe.ChargeParams{
		Amount:      stripe.Int64(100),
		Currency:    stripe.String(string(stripe.CurrencyUSD)),
		Description: stripe.String("Charge for jenny.rosen@example.com"),
	}
	chargeParams.SetSource(to.ID) //
	ch, err := cli.Charges.New(chargeParams)

	fmt.Println("......", err, ch)
}
*/

/*
func TestChargeCustomerID(t *testing.T) {

	stripe.Key = "sk_test_zQSuhqob9Go8kH4gRrcxDB8f"

	cli := client.API{}
	cli.Init(stripe.Key, nil)
	fmt.Println("........stripe.Key......", cli.Charges.Key)

	tokenParams := &stripe.TokenParams{
		Card: &stripe.CardParams{
			Number:   stripe.String("4242424242424242"),
			ExpMonth: stripe.String("12"),
			ExpYear:  stripe.String("2019"),
			CVC:      stripe.String("123"),
		},
	}
	to, err := token.New(tokenParams)
	fmt.Println("......", err, to)

	// Create a Customer:
	customerParams := &stripe.CustomerParams{
		Email: stripe.String(to.Email),
	}
	customerParams.SetSource(to.ID)
	//cus, _ := customer.New(customerParams)
	cus, _ := cli.Customers.New(customerParams)
	fmt.Println("......", err, cus)

	// Create Charge
	chargeParams := &stripe.ChargeParams{
		Amount:      stripe.Int64(50),
		Currency:    stripe.String(string(stripe.CurrencyUSD)),
		Description: stripe.String("Example charge"),
		Customer:    &cus.ID,
	}
	//chargeParams.SetSource(to.ID)
	//ch, _ := charge.New(chargeParams)
	ch, _ := cli.Charges.New(chargeParams)
	fmt.Println("......", err, ch)

	b, _ := json.MarshalIndent(ch, "", " ")
	fmt.Println(string(b))
}
*/

/*
func TestProductTypeService(t *testing.T) {

	stripe.Key = "sk_test_zQSuhqob9Go8kH4gRrcxDB8f"

	// Create Product service
	paramsProduct := &stripe.ProductParams{
		Name: stripe.String("XXX Service"),
		Type: stripe.String(string(stripe.ProductTypeService)),
	}
	prod, _ := product.New(paramsProduct)
	fmt.Println(".........", prod)

	// Create Plan
	paramsPlan1 := &stripe.PlanParams{
		Nickname:  stripe.String("Standard Monthly"),
		ProductID: stripe.String(prod.ID),
		Amount:    stripe.Int64(100),
		Currency:  stripe.String(string(stripe.CurrencyUSD)),
		Interval:  stripe.String(string(stripe.PlanIntervalMonth)),
		UsageType: stripe.String(string(stripe.PlanUsageTypeLicensed)),
	}
	pl1, _ := plan.New(paramsPlan1)
	fmt.Println(".........", pl1)

	// Create Plan
	paramsPlan2 := &stripe.PlanParams{
		Nickname:  stripe.String("Standard Monthly"),
		ProductID: stripe.String(prod.ID),
		Amount:    stripe.Int64(50),
		Currency:  stripe.String(string(stripe.CurrencyUSD)),
		Interval:  stripe.String(string(stripe.PlanIntervalMonth)),
		UsageType: stripe.String(string(stripe.PlanUsageTypeLicensed)),
	}
	pl2, _ := plan.New(paramsPlan2)
	fmt.Println(".........", pl2)

	// Subcription
	paramsSub := &stripe.SubscriptionParams{
		Customer: stripe.String("cus_DViZTMpE87VJGX"),
		Items: []*stripe.SubscriptionItemsParams{
			{
				Plan:     stripe.String(pl1.ID),
				Quantity: stripe.Int64(1),
			},

			{
				Plan:     stripe.String(pl2.ID),
				Quantity: stripe.Int64(2),
			},
		},
	}
	s, _ := sub.New(paramsSub)
	fmt.Println(".........", s)
}
*/

/*
func TestProductTypeGood(t *testing.T) {

	stripe.Key = "sk_test_zQSuhqob9Go8kH4gRrcxDB8f"

	// Create product goods
	paramsProduct := &stripe.ProductParams{
		Name:        stripe.String("T-shirt"),
		Type:        stripe.String(string(stripe.ProductTypeGood)),
		Description: stripe.String("Comfortable cotton t-shirt"),
		Attributes: []*string{
			stripe.String("size"),
			stripe.String("gender"),
		},
	}
	prod, _ := product.New(paramsProduct)
	fmt.Println(".........", prod)

	// Create sku
	paramsSku := &stripe.SKUParams{
		Product: stripe.String(prod.ID),
		Attributes: map[string]string{
			"size":   "Medium",
			"gender": "Unisex",
		},
		Price:    stripe.Int64(1500),
		Currency: stripe.String(string(stripe.CurrencyUSD)),
		Inventory: &stripe.InventoryParams{
			Quantity: stripe.Int64(1),
			Type:     stripe.String(string(stripe.SKUInventoryTypeFinite)),
		},
	}
	s, _ := sku.New(paramsSku)
	fmt.Println(".........", s)

	// Create order
	paramsOrder := &stripe.OrderParams{
		Currency: stripe.String(string(stripe.CurrencyUSD)),
		Items: []*stripe.OrderItemParams{
			&stripe.OrderItemParams{
				Type:   stripe.String(string(stripe.OrderItemTypeSKU)),
				Parent: stripe.String(s.ID),
			},
		},
		Shipping: &stripe.ShippingParams{
			Name: stripe.String("Jenny Rosen"),
			Address: &stripe.AddressParams{
				Line1:      stripe.String("1234 Main Street"),
				City:       stripe.String("San Francisco"),
				State:      stripe.String("CA"),
				Country:    stripe.String("US"),
				PostalCode: stripe.String("94111"),
			},
		},
		Email: stripe.String("jenny.rosen@example.com"),
	}
	oc, _ := order.New(paramsOrder)
	fmt.Println(".........", oc)

	// Pay order
	paramsPay := &stripe.OrderPayParams{}
	paramsPay.SetSource("tok_mastercard")
	op, _ := order.Pay(oc.ID, paramsPay)
	fmt.Println(".........", op)

	b, _ := json.MarshalIndent(op, "", " ")
	fmt.Println(string(b))
}
*/
