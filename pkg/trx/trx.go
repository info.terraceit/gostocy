package trx

import (
	"bytes"
	"context"
	"crypto/ecdsa"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"math"
	"math/big"
	"net/http"
	"strconv"
	"strings"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	tronGRPC "github.com/fbsobreira/gotron-sdk/pkg/client"
	"github.com/fbsobreira/gotron-sdk/pkg/proto/api"
	"github.com/fbsobreira/gotron-sdk/pkg/proto/core"
	"github.com/shengdoushi/base58"
	"gitlab.com/info.terraceit/gostocy/pkg/eth"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/proto"
)

const (
	// HashLength is the expected length of the hash
	HashLength = 32
	// AddressLength is the expected length of the address
	AddressLength = 20
	// AddressLengthBase58 is the expected length of the address in base58format
	AddressLengthBase58 = 34
	// TronBytePrefix is the hex prefix to address
	TronBytePrefix = byte(0x41)
)

// Address represents the 21 byte address of an Tron account.
type Address []byte

func (a Address) Bytes() []byte {
	return a[:]
}

func (a Address) Hex() string {
	return "0x" + common.Bytes2Hex(a[1:])
}

func (a Address) String() string {
	if len(a) == 0 {
		return ""
	}

	if a[0] == 0 {
		return new(big.Int).SetBytes(a.Bytes()).String()
	}
	return EncodeCheck(a.Bytes())
}

func BigToAddress(b *big.Int) Address {
	id := b.Bytes()
	base := bytes.Repeat([]byte{0}, (AddressLength+1)-len(id))
	return append(base, id...)
}

func HexToAddress(s string) Address {
	return common.FromHex(s)
}

// TRX --> ETH  address
func Base58ToAddress(s string) (Address, error) {
	addr, err := DecodeCheck(s)
	if err != nil {
		return nil, err
	}
	return addr, nil
}

func Base64ToAddress(s string) (Address, error) {
	decoded, err := base64.StdEncoding.DecodeString(s)
	if err != nil {
		return nil, err
	}
	return Address(decoded), nil
}

// ETH --> TRX  address
func AddressToBase58(s string) string {
	ethAddr := common.HexToAddress(s)
	return HexToAddress("0x41" + ethAddr.String()[2:]).String()
}

func PubkeyToAddress(p ecdsa.PublicKey) Address {
	address := crypto.PubkeyToAddress(p)

	addressTron := make([]byte, 0)
	addressTron = append(addressTron, TronBytePrefix)
	addressTron = append(addressTron, address.Bytes()...)
	return addressTron
}

func EthAddressToTronAddress(address common.Address) string {
	return HexToAddress("0x41" + address.String()[2:]).String()
}

func TronAddressToEthAddress(address string) (common.Address, error) {
	decoded, err := Decode(address)
	if err != nil {
		return common.Address{}, err
	}
	return common.HexToAddress(common.Bytes2Hex(decoded[:len(decoded)-4])), nil
}

// ---------------------------------------------------------------------//

type KeyPair struct {
	Privkey string
	PubKey  string
	Address string
}

func NewKeyPair() (*KeyPair, error) {
	// Generate a new random account and a funded simulator
	key, err := crypto.GenerateKey()
	if err != nil {
		return &KeyPair{}, err
	}
	addr := PubkeyToAddress(key.PublicKey)

	keyHex := common.Bytes2Hex(crypto.FromECDSA(key))
	pubHex := common.Bytes2Hex(crypto.FromECDSAPub(&key.PublicKey)) // common.ToHex(crypto.FromECDSAPub(&key.PublicKey))

	return &KeyPair{Privkey: keyHex, PubKey: pubHex, Address: addr.String()}, nil
}

func NewKeyPairFromPrivKey(hex string) (*KeyPair, error) {
	key, err := crypto.HexToECDSA(hex)
	if err != nil {
		return &KeyPair{}, err
	}

	keyHex := common.Bytes2Hex(crypto.FromECDSA(key))
	pubHex := common.Bytes2Hex(crypto.FromECDSAPub(&key.PublicKey)) //common.ToHex(crypto.FromECDSAPub(&key.PublicKey))
	addr := PubkeyToAddress(key.PublicKey)

	return &KeyPair{Privkey: keyHex, PubKey: pubHex, Address: addr.String()}, nil
}

/*----------------------------------------------------------*/
func Encode(input []byte) string {
	return base58.Encode(input, base58.BitcoinAlphabet)
}

func EncodeCheck(input []byte) string {
	h256h0 := sha256.New()
	h256h0.Write(input)
	h0 := h256h0.Sum(nil)

	h256h1 := sha256.New()
	h256h1.Write(h0)
	h1 := h256h1.Sum(nil)

	inputCheck := input
	inputCheck = append(inputCheck, h1[:4]...)

	return Encode(inputCheck)
}

func Decode(input string) ([]byte, error) {
	return base58.Decode(input, base58.BitcoinAlphabet)
}

func DecodeCheck(input string) ([]byte, error) {
	decodeCheck, err := Decode(input)
	if err != nil {
		return nil, err
	}

	if len(decodeCheck) < 4 {
		return nil, fmt.Errorf("b58 check error")
	}

	// tron address should should have 20 bytes + 4 checksum + 1 Prefix
	if len(decodeCheck) != AddressLength+4+1 {
		return nil, fmt.Errorf("invalid address length: %d", len(decodeCheck))
	}

	// check prefix
	if decodeCheck[0] != TronBytePrefix {
		return nil, fmt.Errorf("invalid prefix")
	}

	decodeData := decodeCheck[:len(decodeCheck)-4]

	h256h0 := sha256.New()
	h256h0.Write(decodeData)
	h0 := h256h0.Sum(nil)

	h256h1 := sha256.New()
	h256h1.Write(h0)
	h1 := h256h1.Sum(nil)

	if h1[0] == decodeCheck[len(decodeData)] &&
		h1[1] == decodeCheck[len(decodeData)+1] &&
		h1[2] == decodeCheck[len(decodeData)+2] &&
		h1[3] == decodeCheck[len(decodeData)+3] {

		return decodeData, nil
	}

	return nil, fmt.Errorf("b58 check error")
}

// BytesToHexString encodes bytes as a hex string.
func BytesToHexString(bytes []byte) string {
	encode := make([]byte, len(bytes)*2)
	hex.Encode(encode, bytes)

	//return "0x" + string(encode)
	return string(encode)
}

/*----------------------------------------------------------*/
func ToSun(value float64) float64 {
	return value * math.Pow10(6)
}

func ToSunString(value float64) string {
	f := ToSun(value)
	return strconv.FormatFloat(f, 'f', -1, 64)
}

func FromSun(value float64) float64 {
	return value / math.Pow10(6)
}

func FromSunString(value float64) string {
	f := FromSun(value)
	return strconv.FormatFloat(f, 'f', -1, 64)
}

func IsHexAddress(address string) bool {
	addrETH, err := Base58ToAddress(address)
	if err != nil {
		return false
	}
	return common.IsHexAddress(strings.ToLower(addrETH.Hex()))
}

func Sha3FromHex(s string) string {
	return crypto.Keccak256Hash(common.FromHex(s)).Hex()
}

// eventSignature := []byte("ItemSet(bytes32,bytes32)")
// 0xe79e73da417710ae99aa2088575580a60415d359acfad9cdd3382d59c80281d4
// Transfer(address,address,uint256)
func Sha3FromEvent(s string) string {
	return crypto.Keccak256Hash([]byte(s)).Hex()
}

func AddressFromEvent(s string) string {
	data := common.FromHex(s)
	hex := common.Bytes2Hex(data[12:])
	return "0x41" + hex
}

func Sign(privateKey string, message []byte) ([]byte, error) {
	key, err := crypto.HexToECDSA(privateKey)
	if err != nil {
		return nil, err
	}

	// Message Hash
	messageHash := crypto.Keccak256(message)
	signature, err := crypto.Sign(messageHash, key)
	if err != nil {
		return nil, err
	}

	return signature, nil
}

func VerifySignature(pubkey, message, signature []byte) bool {
	// Message Hash
	messageHash := crypto.Keccak256(message)
	pubkeyFromHash, err := crypto.Ecrecover(messageHash, signature)
	if err != nil {
		return false
	}

	// matches := bytes.Equal(pubkeyFromHash, pubkey)
	// if matches {
	// 	signatureNoRecoverID := signature[:len(signature)-1]
	// 	return crypto.VerifySignature(pubkey, messageHash, signatureNoRecoverID)
	// }
	// return false

	return bytes.Equal(pubkeyFromHash, pubkey)
}

/*----------------------------------------------------------*/

type Transaction struct {
	BlockHash        string `json:"blockHash"`
	BlockNumber      string `json:"blockNumber"`
	From             string `json:"from"`
	To               string `json:"to"`
	Value            string `json:"value"`
	Gas              string `json:"gas"`      // unused
	GasPrice         string `json:"gasPrice"` // energy price
	Hash             string `json:"hash"`
	Input            string `json:"input"`
	Nonce            string `json:"nonce"` // unused
	TransactionIndex string `json:"transactionIndex"`
	Type             string `json:"type"`
	V                string `json:"v"`
	R                string `json:"r"`
	S                string `json:"s"`
}

type TransactionInfo struct {
	ID              string          `json:"id"`
	Fee             int64           `json:"fee"`
	BlockNumber     int64           `json:"blockNumber"`
	BlockTimeStamp  int64           `json:"blockTimeStamp"`
	ContractResult  []string        `json:"contractResult"`
	ContractAddress string          `json:"contract_address"`
	Receipt         ResourceReceipt `json:"receipt"`
	Result          int             `json:"result,omitempty"`
}

type ResourceReceipt struct {
	EnergyUsage        int64  `json:"energy_usage"`
	EnergyFee          int64  `json:"energy_fee"`
	OriginEnergyUsage  int64  `json:"origin_energy_usage"`
	EnergyUsageTotal   int64  `json:"energy_usage_total"`
	NetUsage           int64  `json:"net_usage"`
	NetFee             int64  `json:"net_fee"`
	Result             string `json:"result"`
	EnergyPenaltyTotal int64  `json:"energy_penalty_total"`
}

type TransactionReceipt struct {
	TransactionHash   string      `json:"transactionHash"`
	TransactionIndex  string      `json:"transactionIndex"`
	BlockHash         string      `json:"blockHash"`
	BlockNumber       string      `json:"blockNumber"`
	From              string      `json:"from"`
	To                string      `json:"to"`
	CumulativeGasUsed string      `json:"cumulativeGasUsed"` // The total amount of gas used when this transaction was executed in the block.s
	EffectiveGasPrice string      `json:"effectiveGasPrice"`
	GasUsed           string      `json:"gasUsed"` // The amount of gas used by this specific transaction alone.
	ContractAddress   string      `json:"contractAddress"`
	Logs              []types.Log `json:"logs"`
	LogsBloom         string      `json:"logsBloom"`
	Root              string      `json:"root"`
	Status            string      `json:"status"`
	Type              string      `json:"type"`
}

type RawDataContract struct {
	Contract []struct {
		Parameter struct {
			Value struct {
				Data            string `json:"data,omitempty"`
				Amount          int    `json:"amount,omitempty"`
				OwnerAddress    string `json:"owner_address,omitempty"`
				ContractAddress string `json:"contract_address,omitempty"`
				ToAddress       string `json:"to_address,omitempty"`
				CallValue       int64  `json:"call_value,omitempty"`
				CallTokenValue  int64  `json:"call_token_value,omitempty"`
				TokenId         int64  `json:"token_id,omitempty"`
				NewContract     struct {
					ByteCode                   string                 `json:"bytecode,omitempty"`
					ConsumeUserResourcePercent int64                  `json:"consume_user_resource_percent,omitempty"`
					Name                       string                 `json:"name,omitempty"`
					OriginAddress              string                 `json:"origin_address,omitempty"`
					Abi                        map[string]interface{} `json:"abi,omitempty"`
					OriginEnergyLimit          int64                  `json:"origin_energy_limit,omitempty"`
					CallValue                  int64                  `json:"call_value,omitempty"`
				} `json:"new_contract,omitempty"`
			} `json:"value"`
			TypeURL string `json:"type_url"`
		} `json:"parameter"`
		Type string `json:"type"`
	} `json:"contract"`
	RefBlockBytes string `json:"ref_block_bytes"`
	RefBlockHash  string `json:"ref_block_hash"`
	Expiration    int64  `json:"expiration"`
	Timestamp     int64  `json:"timestamp"`
	FeeLimit      int64  `json:"fee_limit"`
}

type RawData struct {
	Contract []struct {
		Parameter struct {
			Value struct {
				Data            string `json:"data,omitempty"`
				Amount          int    `json:"amount,omitempty"`
				OwnerAddress    string `json:"owner_address,omitempty"`
				ContractAddress string `json:"contract_address,omitempty"`
				ToAddress       string `json:"to_address,omitempty"`
				CallValue       int64  `json:"call_value,omitempty"`
				CallTokenValue  int64  `json:"call_token_value,omitempty"`
			} `json:"value"`
			TypeURL string `json:"type_url"`
		} `json:"parameter"`
		Type string `json:"type"`
	} `json:"contract"`
	RefBlockBytes string `json:"ref_block_bytes"`
	RefBlockHash  string `json:"ref_block_hash"`
	Expiration    int64  `json:"expiration"`
	Timestamp     int64  `json:"timestamp"`
	FeeLimit      int64  `json:"fee_limit"`
}

type RawTransaction struct {
	Visible    bool    `json:"visible"`
	TxID       string  `json:"txID"`
	RawData    RawData `json:"raw_data"`
	RawDataHex string  `json:"raw_data_hex"`
}

type RawTransactionContract struct {
	Visible    bool            `json:"visible"`
	TxID       string          `json:"txID"`
	RawData    RawDataContract `json:"raw_data"`
	RawDataHex string          `json:"raw_data_hex"`
}

type SignedTransaction struct {
	RawTransaction
	Signature []string `json:"signature,omitempty"`
}

type SignedTransactionContract struct {
	RawTransactionContract
	Signature []string `json:"signature,omitempty"`
}

type ResultTransasction struct {
	Result  bool   `json:"result"`
	Txid    string `json:"txid"`
	Code    string `json:"code"`
	Message string `json:"message"`
}

type Uncle struct{}

type Block struct {
	BaseFeePerGas    string        `json:"baseFeePerGas"`
	Difficulty       string        `json:"difficulty"`
	ExtraData        string        `json:"extraData"`
	GasLimit         string        `json:"gasLimit"`
	GasUsed          string        `json:"gasUsed"`
	Hash             string        `json:"hash"`
	LogsBloom        string        `json:"logsBloom"`
	Miner            string        `json:"miner"`
	MixHash          string        `json:"mixHash"`
	Nonce            string        `json:"nonce"`
	Number           string        `json:"number"`
	ParentHash       string        `json:"parentHash"`
	ReceiptsRoot     string        `json:"receiptsRoot"`
	Sha3Uncles       string        `json:"sha3Uncles"`
	Size             string        `json:"size"`
	StateRoot        string        `json:"stateRoot"`
	Timestamp        string        `json:"timestamp"`
	TotalDifficulty  string        `json:"totalDifficulty"`
	Transactions     []Transaction `json:"transactions"`
	TransactionsRoot string        `json:"transactionsRoot"`
	Uncles           []Uncle       `json:"uncles"`
}

type AccountResource struct {
	FreeNetUsed          int64              `json:"freeNetUsed"`
	FreeNetLimit         int64              `json:"freeNetLimit"`
	NetUsed              int64              `json:"NetUsed"`
	NetLimit             int64              `json:"NetLimit"`
	TotalNetLimit        int64              `json:"TotalNetLimit"`
	TotalNetWeight       int64              `json:"TotalNetWeight"`
	TotalTronPowerWeight int64              `json:"totalTronPowerWeight"`
	TronPowerLimit       int64              `json:"tronPowerLimit"`
	TronPowerUsed        int64              `json:"tronPowerUsed"`
	EnergyUsed           int64              `json:"EnergyUsed"`
	EnergyLimit          int64              `json:"EnergyLimit"`
	TotalEnergyLimit     int64              `json:"TotalEnergyLimit"`
	TotalEnergyWeight    int64              `json:"TotalEnergyWeight"`
	AssetNetUsed         []map[string]int64 `json:"assetNetUsed"`
	AssetNetLimit        []map[string]int64 `json:"assetNetLimit"`
}

/*----------------------------------------------------------*/

func call_RPC(url, method string, paramsIn ...interface{}) map[string]interface{} {
	//fmt.Println("method : ", method, "  params : ", paramsIn)
	values := map[string]interface{}{"jsonrpc": "2.0", "method": method, "params": paramsIn, "id": 1}
	jsonStr, err := json.Marshal(values)
	if err != nil {
		//fmt.Println("error marshal values :", err)
		return nil
	}
	//fmt.Println(".....jsonStr :", string(jsonStr))

	resp, err := http.Post(url+"/jsonrpc", "application/json", bytes.NewBuffer(jsonStr))
	if err != nil {
		//fmt.Println("error call rpc :", err)
		return nil
	}
	defer resp.Body.Close()

	result := map[string]interface{}{}
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		//fmt.Println("error readall body :", err)
		return nil
	}
	//fmt.Println(".....body :", string(body))

	//fmt.Println(method, string(body))
	err = json.Unmarshal(body, &result)
	if err != nil {
		//fmt.Println("error unmarshal result :", err)
		return nil
	}

	return result
}

func broadcastTx(url string, signedTx *SignedTransaction) *ResultTransasction {
	jsonStr, err := json.Marshal(signedTx)
	if err != nil {
		return nil
	}

	resp, err := http.Post(url+"/wallet/broadcasttransaction", "application/json", bytes.NewBuffer(jsonStr))
	if err != nil {
		return nil
	}
	defer resp.Body.Close()

	var result *ResultTransasction
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil
	}

	err = json.Unmarshal(body, &result)
	if err != nil {
		return nil
	}

	return result
}

func broadcastTxContract(url string, signedTx *SignedTransactionContract) *ResultTransasction {
	jsonStr, err := json.Marshal(signedTx)
	if err != nil {
		return nil
	}

	resp, err := http.Post(url+"/wallet/broadcasttransaction", "application/json", bytes.NewBuffer(jsonStr))
	if err != nil {
		return nil
	}
	defer resp.Body.Close()

	var result *ResultTransasction
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil
	}

	err = json.Unmarshal(body, &result)
	if err != nil {
		return nil
	}

	return result
}

func SignRawTx(privHex string, tx *RawTransaction) (*SignedTransaction, error) {
	txIDbytes, err := hex.DecodeString(tx.TxID)
	if err != nil {
		return nil, err
	}

	priv, err := crypto.HexToECDSA(privHex)
	if err != nil {
		return nil, err
	}

	signature, err := crypto.Sign(txIDbytes, priv)
	if err != nil {
		return nil, err
	}

	signedTransaction := &SignedTransaction{
		RawTransaction: *tx,
		Signature:      make([]string, 0),
	}
	signedTransaction.Signature = append(signedTransaction.Signature, hex.EncodeToString(signature))

	return signedTransaction, nil
}

func SignRawTxContract(privHex string, tx *RawTransactionContract) (*SignedTransactionContract, error) {
	txIDbytes, err := hex.DecodeString(tx.TxID)
	if err != nil {
		return nil, err
	}

	priv, err := crypto.HexToECDSA(privHex)
	if err != nil {
		return nil, err
	}

	signature, err := crypto.Sign(txIDbytes, priv)
	if err != nil {
		return nil, err
	}

	signedTransaction := &SignedTransactionContract{
		RawTransactionContract: *tx,
		Signature:              make([]string, 0),
	}
	signedTransaction.Signature = append(signedTransaction.Signature, hex.EncodeToString(signature))

	return signedTransaction, nil
}

func SignTxGrpc(privHex string, tx *api.TransactionExtention) (*api.TransactionExtention, error) {
	rawData, err := proto.Marshal(tx.Transaction.GetRawData())
	if err != nil {
		return nil, err
	}

	h256h := sha256.New()
	h256h.Write(rawData)
	hash := h256h.Sum(nil)

	priv, err := crypto.HexToECDSA(privHex)
	if err != nil {
		return nil, err
	}

	signature, err := crypto.Sign(hash, priv)
	if err != nil {
		return nil, err
	}
	tx.Transaction.Signature = append(tx.Transaction.Signature, signature)

	return tx, nil
}

// unit price : 1 bandwith = 1000sun = 0.001TRX
// Bandwidth Points Recovery within 24 hours
// Burned TRX =  the amount of bandwidth consumed * the unit price of bandwidth
func BurnBandwidth(consumedSun, priceSun float64) float64 {
	return FromSun(consumedSun * priceSun)
}

// The amount of bandwidth obtained = the amount of TRX staked for obtaining bandwidth / the total amount of TRX staked for obtaining bandwidth in the whole network  * 43_200_000_000
// = stakeTRX / TotalNetWeight * TotalNetLimit
func StakeBandwidth(stakeTRX, networkTRX, networkLimit float64) int64 {
	return int64(stakeTRX / networkTRX * networkLimit)
}

// unit price : 1 energy = 420sun = 0.00042TRX
// Energy Recovery : After the energy resource of the account is consumed, it will gradually recover within 24 hours
// Burned TRX = Energy quantity * the unit price of Energy
func BurnEnergy(quantitySun, priceSun float64) float64 {
	return FromSun(quantitySun * priceSun)
}

// The amount of energy obtained = the amount of TRX staked for obtaining energy / the total amount of TRX staked for obtaining energy in the whole network * 90_000_000_000
// = stakeTRX / TotalEnergyWeight * TotalEnergyLimit
func StakeEnergy(stakeTRX, networkTRX, networkLimit float64) int64 {
	return int64(stakeTRX / networkTRX * networkLimit)
}

/*----------------------------------------------------------*/
// 1 TRX = 1000000 sun
// Bandwidth (Normal Transaction) = (200->300 bytes) * 10 SUN = 0.002
// Energy (Trigger Smart Contract)

type Client struct {
	URL        string
	client     *ethclient.Client
	GRPC       string
	clientGrpc *tronGRPC.GrpcClient

	txList map[string][]int64
}

func NewClient(url, grpcUrl string) (cli *Client, err error) {
	// connect to rpc
	rpc, err := ethclient.Dial(url)
	if err != nil {
		fmt.Println("Error Dial ", err)
		return
	}

	// connect to grpc
	gc := tronGRPC.NewGrpcClient(grpcUrl)
	err = gc.Start(grpc.WithInsecure())
	if err != nil {
		fmt.Println("Error Dial ", err)
		return nil, err
	}

	cli = &Client{
		client:     rpc,
		clientGrpc: gc,
		URL:        url,
		GRPC:       grpcUrl,
		txList:     make(map[string][]int64)}
	return
}

func (c *Client) GetClientGRPC() *tronGRPC.GrpcClient {
	return c.clientGrpc
}

func (c *Client) GetClientETH() *ethclient.Client {
	return c.client
}

func (c *Client) BlockNumber() string {
	data := call_RPC(c.URL, "eth_blockNumber")
	result, ok := data["result"]
	if !ok {
		return ""
	}

	return result.(string)
}

func (c *Client) Call(from, to, gas, gasPrice, value, data string) string {
	fromAddr, err := Base58ToAddress(from)
	if err != nil {
		return ""
	}

	toAddr, err := Base58ToAddress(to)
	if err != nil {
		return ""
	}

	param := map[string]string{
		"from":     fromAddr.Hex(),
		"to":       toAddr.Hex(),
		"gas":      gas,
		"gasPrice": gasPrice,
		"value":    value,
		"data":     data,
	}

	resp := call_RPC(c.URL, "eth_call", param, "latest")
	result, ok := resp["result"]
	if !ok {
		return ""
	}

	return result.(string)
}

// Get the required energy through triggerConstantContract
// Returns The amount of energy used
func (c *Client) EstimateGas(from, to, data string, value int64) string {
	fromAddr, err := Base58ToAddress(from)
	if err != nil {
		return ""
	}

	toAddr, err := Base58ToAddress(to)
	if err != nil {
		return ""
	}

	param := map[string]string{
		"from": fromAddr.Hex(),
		"to":   toAddr.Hex(),
		//"gas":      gas, //unused
		//"gasPrice": gasPrice, //unused
		"value": eth.ToBigNumber(value),
		"data":  data, //Hash of the method signature and encoded parameters
	}

	resp := call_RPC(c.URL, "eth_estimateGas", param)
	result, ok := resp["result"]
	if !ok {
		return ""
	}

	return result.(string)
}

// Returns Integer of the current energy price in sun
func (c *Client) GasPrice() string {
	resp := call_RPC(c.URL, "eth_gasPrice")
	result, ok := resp["result"]
	if !ok {
		return ""
	}

	return result.(string)
}

// Latest same gasPrice
func (c *Client) EnergyPrice() int64 {
	resp, err := http.Get(c.URL + "/wallet/getenergyprices")
	if err != nil {
		//fmt.Println("error call rpc :", err)
		return 0
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		//fmt.Println("error readall body :", err)
		return 0
	}
	//fmt.Println(".....body :", string(body))

	var result map[string]string
	err = json.Unmarshal(body, &result)
	if err != nil {
		//fmt.Println("error unmarshal result :", err)
		return 0
	}

	/*
		{
		  "prices": "0:100,1575871200000:10,1606537680000:40,1614238080000:140,1635739080000:280,1681895880000:420"
		}
	*/

	prices := strings.Split(result["prices"], ",")
	price := strings.Split(prices[len(prices)-1], ":")
	energy, _ := strconv.ParseInt(price[1], 10, 64)

	return energy
}

func (c *Client) BandwidthPrice() int64 {
	resp, err := http.Get(c.URL + "/wallet/getbandwidthprices")
	if err != nil {
		//fmt.Println("error call rpc :", err)
		return 0
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		//fmt.Println("error readall body :", err)
		return 0
	}
	//fmt.Println(".....body :", string(body))

	var result map[string]string
	err = json.Unmarshal(body, &result)
	if err != nil {
		//fmt.Println("error unmarshal result :", err)
		return 0
	}

	/*
		{
		"prices": "0:10,1606537680000:40,1614238080000:140,1626581880000:1000,1626925680000:140,1627731480000:1000"
		}
	*/

	prices := strings.Split(result["prices"], ",")
	price := strings.Split(prices[len(prices)-1], ":")
	bandwidth, _ := strconv.ParseInt(price[1], 10, 64)

	return bandwidth
}

func (c *Client) EstimateBandwithSignedTx(signedTxn *SignedTransaction) int {
	var DATA_HEX_PROTOBUF_EXTRA int = 3
	var MAX_RESULT_SIZE_IN_TX int = 64
	var A_SIGNATURE int = 67

	length := len(signedTxn.RawDataHex)/2 + DATA_HEX_PROTOBUF_EXTRA + MAX_RESULT_SIZE_IN_TX
	signatureListSize := len(signedTxn.Signature)
	for i := 0; i < signatureListSize; i++ {
		length += A_SIGNATURE
	}
	return length
}

func (c *Client) EstimateBandwithRawTx(rawTx *RawTransaction, numSignature int) int {
	var DATA_HEX_PROTOBUF_EXTRA int = 3
	var MAX_RESULT_SIZE_IN_TX int = 64
	var A_SIGNATURE int = 67

	length := len(rawTx.RawDataHex)/2 + DATA_HEX_PROTOBUF_EXTRA + MAX_RESULT_SIZE_IN_TX
	for i := 0; i < numSignature; i++ {
		length += A_SIGNATURE
	}
	return length
}

func (c *Client) EstimateEnergyTrigger(ownerAddr, contractAddr, method, data string) (float64, float64) {
	param := map[string]interface{}{
		"owner_address":     ownerAddr,
		"contract_address":  contractAddr,
		"function_selector": method, // "transfer(address,uint256)",
		"parameter":         data,   // data[8:] - remove signature
		"visible":           true,
	}
	payload, _ := json.Marshal(param)

	resp, err := http.Post(c.URL+"/wallet/triggerconstantcontract", "application/json", bytes.NewBuffer(payload))
	if err != nil {
		//fmt.Println("error call rpc :", err)
		return 0, 0
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		//fmt.Println("error readall body :", err)
		return 0, 0
	}
	fmt.Println(".....body :", string(body))

	var result map[string]interface{}
	err = json.Unmarshal(body, &result)
	if err != nil {
		//fmt.Println("error unmarshal result :", err)
		return 0, 0
	}

	used := 0.0
	energy_used, ok := result["energy_used"]
	if ok {
		used = energy_used.(float64)
	}

	penalty := 0.0
	energy_penalty, ok := result["energy_penalty"]
	if ok {
		penalty = energy_penalty.(float64)
	}

	return used, penalty
}

func (c *Client) EstimateEnergy(ownerAddr, contractAddr, method, params, data string, amount int64) float64 {
	param := map[string]interface{}{
		"owner_address":     ownerAddr,
		"contract_address":  contractAddr,
		"function_selector": method, // "transfer(address,uint256)",
		"parameter":         params, // data[8:] - remove signature
		"data":              data,   // for deploy smart contract
		"call_value":        amount, // amount TRX
		"visible":           true,
	}
	payload, _ := json.Marshal(param)

	resp, err := http.Post(c.URL+"/wallet/estimateenergy", "application/json", bytes.NewBuffer(payload))
	if err != nil {
		//fmt.Println("error call rpc :", err)
		return 0
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		//fmt.Println("error readall body :", err)
		return 0
	}
	//fmt.Println(".....body :", string(body))

	var result map[string]interface{}
	err = json.Unmarshal(body, &result)
	if err != nil {
		//fmt.Println("error unmarshal result :", err)
		return 0
	}

	engergy, ok := result["energy_required"]
	if !ok {
		return 0
	}

	return engergy.(float64)
}

// If a contract uses too many resources in one maintenance cycle, then in the next maintenance cycle, a certain percentage of punitive consumption will be added
// Ccurrently it is 6 hours, and the first 6 seconds
// energy consumption by a contract invocation transaction  = the basic energy consumption generated by the transaction * （1 +  energy_factor）
func (c *Client) EnergyConsume(energyUsedSun, energyFactor float64) float64 {
	return energyUsedSun * (1 + energyFactor)
}

func (c *Client) StakeBandwidth(addr string, amountTRX float64) int64 {
	accResource, err := c.GetAccountResource(addr)
	if err != nil {
		return 0
	}

	return StakeBandwidth(amountTRX, float64(accResource.TotalNetWeight), float64(accResource.TotalNetLimit))
}

func (c *Client) StakeEnergy(addr string, amountTRX float64) int64 {
	accResource, err := c.GetAccountResource(addr)
	if err != nil {
		return 0
	}

	return StakeEnergy(amountTRX, float64(accResource.TotalEnergyWeight), float64(accResource.TotalEnergyLimit))
}

func (c *Client) GetEnergyFactor(contractAddr string) float64 {
	param := map[string]interface{}{
		"value":   contractAddr,
		"visible": true,
	}
	payload, _ := json.Marshal(param)

	resp, err := http.Post(c.URL+"/wallet/getcontractinfo", "application/json", bytes.NewBuffer(payload))
	if err != nil {
		//fmt.Println("error call rpc :", err)
		return 0
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		//fmt.Println("error readall body :", err)
		return 0
	}
	//fmt.Println(".....body :", string(body))

	var result map[string]interface{}
	err = json.Unmarshal(body, &result)
	if err != nil {
		//fmt.Println("error unmarshal result :", err)
		return 0
	}

	contractState, ok := result["contract_state"]
	if !ok {
		return 0
	}

	energyFactor, ok := contractState.(map[string]interface{})["energy_factor"]
	if !ok {
		return 0
	}

	return energyFactor.(float64)
}

func (c *Client) GetAccountResource(addr string) (*AccountResource, error) {
	param := map[string]interface{}{
		"address": addr,
		"visible": true,
	}
	payload, _ := json.Marshal(param)

	resp, err := http.Post(c.URL+"/wallet/getaccountresource", "application/json", bytes.NewBuffer(payload))
	if err != nil {
		//fmt.Println("error call rpc :", err)
		return nil, err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		//fmt.Println("error readall body :", err)
		return nil, err
	}
	//fmt.Println(".....body :", string(body))

	var result AccountResource
	err = json.Unmarshal(body, &result)
	if err != nil {
		//fmt.Println("error unmarshal result :", err)
		return nil, err
	}

	return &result, nil
}

func (c *Client) GetTransactionInfo(txHash string) (*TransactionInfo, error) {
	payload := strings.NewReader(fmt.Sprintf("{\"value\":\"%v\"}", txHash))

	resp, err := http.Post(c.URL+"/walletsolidity/gettransactioninfobyid", "application/json", payload)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var result *TransactionInfo
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(body, &result)
	if err != nil {
		return nil, err
	}

	return result, nil

}

// milliseconds
func (c *Client) GetNextMaintenanceTime() int64 {
	resp, err := http.Get(c.URL + "/wallet/getnextmaintenancetime")
	if err != nil {
		//fmt.Println("error call rpc :", err)
		return 0
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		//fmt.Println("error readall body :", err)
		return 0
	}
	//fmt.Println(".....body :", string(body))

	var result map[string]int64
	err = json.Unmarshal(body, &result)
	if err != nil {
		//fmt.Println("error unmarshal result :", err)
		return 0
	}

	/*
		{
		  "num": 1711523880000
		}
	*/

	return result["num"]
}

func (c *Client) GetBalance(address string) string {
	addr, err := Base58ToAddress(address)
	if err != nil {
		return ""
	}

	resp := call_RPC(c.URL, "eth_getBalance", addr.Hex(), "latest")
	result, ok := resp["result"]
	if !ok {
		return ""
	}

	return result.(string)
}

func (c *Client) GetBalanceOf(from, contractAddress, abi string) string {
	fromAddr, err := Base58ToAddress(from)
	if err != nil {
		return ""
	}

	data, err := eth.Pack(abi, "balanceOf", common.HexToAddress(fromAddr.Hex()))
	if err != nil {
		return ""
	}
	dataHex := common.Bytes2Hex(data)
	return c.Call(from, contractAddress, "0x0", "0x0", "0x0", dataHex)
}

func (c *Client) GetBlockByHash(hex string) *Block {
	resp := call_RPC(c.URL, "eth_getBlockByHash", hex, true)
	result, ok := resp["result"]
	if !ok {
		return nil
	}

	b, err := json.Marshal(result)
	if err != nil {
		return nil
	}

	var block *Block
	err = json.Unmarshal(b, &block)
	if err != nil {
		return nil
	}

	return block
}

func (c *Client) GetBlockByNumber(blockNumber string) *Block {
	resp := call_RPC(c.URL, "eth_getBlockByNumber", blockNumber, true)
	result, ok := resp["result"]
	if !ok {
		return nil
	}

	b, err := json.Marshal(result)
	if err != nil {
		return nil
	}

	var block *Block
	err = json.Unmarshal(b, &block)
	if err != nil {
		return nil
	}

	return block
}

func (c *Client) GetBlockTransactionCountByHash(blockHash string) string {
	resp := call_RPC(c.URL, "eth_getBlockTransactionCountByHash", blockHash)
	result, ok := resp["result"]
	if !ok {
		return ""
	}
	return result.(string)
}

func (c *Client) GetBlockTransactionCountByNumber(blockNumber string) string {
	resp := call_RPC(c.URL, "eth_getBlockTransactionCountByNumber", blockNumber)
	result, ok := resp["result"]
	if !ok {
		return ""
	}

	return result.(string)
}

func (c *Client) GetCode(contracAddresss string) string {
	addr, err := Base58ToAddress(contracAddresss)
	if err != nil {
		return ""
	}

	resp := call_RPC(c.URL, "eth_getCode", addr.Hex(), "latest")
	result, ok := resp["result"]
	if !ok {
		return ""
	}

	return result.(string)
}

func (c *Client) GetStorageAt(address, position string) string {
	addr, err := Base58ToAddress(address)
	if err != nil {
		return ""
	}

	resp := call_RPC(c.URL, "eth_getStorageAt", addr.Hex(), position, "latest")
	result, ok := resp["result"]
	if !ok {
		return ""
	}

	return result.(string)
}

func (c *Client) GetTransactionByBlockHashAndIndex(blockHash, index string) *Transaction {
	resp := call_RPC(c.URL, "eth_getTransactionByBlockHashAndIndex", blockHash, index)
	result, ok := resp["result"]
	if !ok {
		return nil
	}

	b, err := json.Marshal(result)
	if err != nil {
		return nil
	}

	var tx *Transaction
	err = json.Unmarshal(b, &tx)
	if err != nil {
		return nil
	}

	return tx
}

func (c *Client) GetTransactionByBlockNumberAndIndex(blockNumber, index string) *Transaction {
	resp := call_RPC(c.URL, "eth_getTransactionByBlockNumberAndIndex", blockNumber, index)
	result, ok := resp["result"]
	if !ok {
		return nil
	}

	b, err := json.Marshal(result)
	if err != nil {
		return nil
	}

	var tx *Transaction
	err = json.Unmarshal(b, &tx)
	if err != nil {
		return nil
	}

	return tx
}

func (c *Client) GetTransactionByHash(txHash string) *Transaction {
	resp := call_RPC(c.URL, "eth_getTransactionByHash", txHash)
	result, ok := resp["result"]
	if !ok {
		return nil
	}

	b, err := json.Marshal(result)
	if err != nil {
		return nil
	}

	var tx *Transaction
	err = json.Unmarshal(b, &tx)
	if err != nil {
		return nil
	}

	return tx
}

func (c *Client) GetTransactionReceipt(txHash string) *TransactionReceipt {
	resp := call_RPC(c.URL, "eth_getTransactionReceipt", txHash)
	result, ok := resp["result"]
	if !ok {
		return nil
	}

	b, err := json.Marshal(result)
	if err != nil {
		return nil
	}

	var tx *TransactionReceipt
	err = json.Unmarshal(b, &tx)
	if err != nil {
		return nil
	}

	return tx
}

func (c *Client) CreateSendTrx(from, to string, amount int64) (*RawTransaction, error) {
	fromAddr, err := Base58ToAddress(from)
	if err != nil {
		return nil, err
	}

	toAddr, err := Base58ToAddress(to)
	if err != nil {
		return nil, err
	}

	param := map[string]string{
		"from":  fromAddr.Hex(),
		"to":    toAddr.Hex(),
		"value": eth.ToBigNumber(amount),
	}
	resp := call_RPC(c.URL, "buildTransaction", param)
	result, ok := resp["result"].(map[string]interface{})
	if !ok {
		return nil, errors.New("error buildTransaction result")
	}

	b, err := json.Marshal(result["transaction"])
	if err != nil {
		return nil, err
	}

	var tx *RawTransaction
	err = json.Unmarshal(b, &tx)
	if err != nil {
		return nil, err
	}

	return tx, nil
}

func (c *Client) CreateSendToken(from, to string, tokenId, tokenValue int64) (*RawTransaction, error) {
	fromAddr, err := Base58ToAddress(from)
	if err != nil {
		return nil, err
	}

	toAddr, err := Base58ToAddress(to)
	if err != nil {
		return nil, err
	}

	param := map[string]interface{}{
		"from":       fromAddr.Hex(),
		"to":         toAddr.Hex(),
		"tokenId":    tokenId,
		"tokenValue": tokenValue,
	}
	resp := call_RPC(c.URL, "buildTransaction", param)
	result, ok := resp["result"].(map[string]interface{})
	if !ok {
		return nil, err
	}

	b, err := json.Marshal(result["transaction"])
	if err != nil {
		return nil, err
	}

	var tx *RawTransaction
	err = json.Unmarshal(b, &tx)
	if err != nil {
		return nil, err
	}

	return tx, nil
}

// tokenId := ""
// fee_limit = energy_required x energy unit price
func (c *Client) CreateTriggerSmartContract(from, contract, data string, gas, value, tokenId, tokenValue int64) (*RawTransaction, error) {
	fromAddr, err := Base58ToAddress(from)
	if err != nil {
		return nil, err
	}

	contractAddr, err := Base58ToAddress(contract)
	if err != nil {
		return nil, err
	}

	param := map[string]interface{}{
		"from":       fromAddr.Hex(),
		"to":         contractAddr.Hex(),
		"data":       data,
		"gas":        eth.ToBigNumber(gas), // fee_limit = energy_required x energy unit price
		"value":      eth.ToBigNumber(value),
		"tokenId":    tokenId,
		"tokenValue": tokenValue,
	}
	resp := call_RPC(c.URL, "buildTransaction", param)
	result, ok := resp["result"].(map[string]interface{})
	if !ok {
		return nil, errors.New("error buildTransaction result")
	}

	b, err := json.Marshal(result["transaction"])
	if err != nil {
		return nil, err
	}

	var tx *RawTransaction
	err = json.Unmarshal(b, &tx)
	if err != nil {
		return nil, err
	}

	return tx, nil
}

func (c *Client) CreateSmartContract(from, name, abi, data string, gas, value, consumeUserResourcePercent, originEnergyLimit, tokenId, tokenValue int64) (*RawTransactionContract, error) {
	fromAddr, err := Base58ToAddress(from)
	if err != nil {
		return nil, err
	}

	param := map[string]interface{}{
		"from":                       fromAddr.Hex(),
		"name":                       name,
		"gas":                        eth.ToBigNumber(gas), // "0x245498" 0xf4240 // fee_limit = energy_required x energy unit price
		"abi":                        abi,
		"data":                       hex.EncodeToString(common.FromHex(data)),
		"consumeUserResourcePercent": consumeUserResourcePercent,
		"originEnergyLimit":          originEnergyLimit,
		"value":                      eth.ToBigNumber(value),
		"tokenId":                    tokenId,
		"tokenValue":                 tokenValue,
	}
	resp := call_RPC(c.URL, "buildTransaction", param)
	result, ok := resp["result"].(map[string]interface{})
	if !ok {
		return nil, errors.New("error buildTransaction result")
	}

	b, err := json.Marshal(result["transaction"])
	if err != nil {
		return nil, err
	}

	var tx *RawTransactionContract
	err = json.Unmarshal(b, &tx)
	if err != nil {
		return nil, err
	}

	return tx, nil
}

func (c *Client) Broadcast(signedTx *SignedTransaction) (*ResultTransasction, error) {
	tx := broadcastTx(c.URL, signedTx)
	if !tx.Result {
		return nil, errors.New(tx.Code + ":" + tx.Message)
	}
	return tx, nil
}

func (c *Client) BroadcastContract(signedTx *SignedTransactionContract) (*ResultTransasction, error) {
	tx := broadcastTxContract(c.URL, signedTx)
	if !tx.Result {
		return nil, errors.New(tx.Code + ":" + tx.Message)
	}
	return tx, nil
}

/*--------------------------GRPC--------------------------------*/

func (c *Client) CreateSendTrxGRPC(from, to string, amount int64) (*api.TransactionExtention, error) {
	return c.clientGrpc.Transfer(from, to, amount)
}

func (c *Client) CreateSendTokenGRPC(from, to, contract string, amount, feeLimit int64) (*api.TransactionExtention, error) {
	number := big.NewInt(amount)
	return c.clientGrpc.TRC20Send(from, to, contract, number, feeLimit)
}

// tokenId := ""
// method : transfer(address,uint256)
// jsonString : `[{"address": "TE4c73WubeWPhSF1nAovQDmQytjcaLZyY9"},{"uint256": "100"}]`
func (c *Client) CreateTriggerSmartContractGRPC(from, contractAddress, method string, params []map[string]interface{}, feeLimit, value int64, tokenID string, tokenAmount int64) (*api.TransactionExtention, error) {
	b, _ := json.Marshal(params)
	jsonString := string(b)

	return c.clientGrpc.TriggerContract(from, contractAddress, method, jsonString, feeLimit, value, tokenID, tokenAmount)
}

func (c *Client) BroadcastGRPC(tx *api.TransactionExtention) (*api.Return, error) {
	return c.clientGrpc.Broadcast(tx.Transaction)
}

func (c *Client) BalanceCheck(from, to, method, params, data string, amount int64) bool {
	balance := c.GetBalance(from)
	if balance == "" {
		return false
	}

	ePrice := c.EnergyPrice()
	eCost := c.EstimateEnergy(from, to, method, params, data, amount)
	total := ePrice * int64(eCost)

	return eth.ToBig256(balance).Int64() >= total
}

func (c *Client) TriggerSmartContract(from, to, signature, method, params string, amount, feeLimit int64, view bool) (*api.TransactionExtention, error) {
	var err error

	tsc := &core.TriggerSmartContract{}
	addrFrom, err := Base58ToAddress(from)
	if err != nil {
		return nil, err
	}
	addrTo, err := Base58ToAddress(to)
	if err != nil {
		return nil, err
	}
	tsc.OwnerAddress = addrFrom.Bytes()
	tsc.ContractAddress = addrTo.Bytes()

	// TODO : sometime wrong, cannot get balance
	// if !c.BalanceCheck(from, to, method, params, "", amount) {
	// 	return nil, errors.New("insufficient funds")
	// }

	input := signature + params

	tsc.Data = common.FromHex(input)

	var result *api.TransactionExtention
	ctx := context.Background()
	if view {
		result, err = c.clientGrpc.Client.TriggerConstantContract(ctx, tsc)
	} else {
		result, err = c.clientGrpc.Client.TriggerContract(ctx, tsc)

		if result.Result.Code > 0 {
			return nil, fmt.Errorf("%s", string(result.Result.Message))
		}
		if feeLimit > 0 {
			result.Transaction.RawData.FeeLimit = feeLimit
			c.clientGrpc.UpdateHash(result)
		}
	}

	return result, err
}

func (c *Client) DeploySmartContract(from, name, byteCode string, abi *core.SmartContract_ABI, feeLimit, curPercent, oeLimit int64) (*api.TransactionExtention, error) {
	balance := c.GetBalance(from)

	// Default 30 TRX / 1 Smart Contract
	if eth.ToBig256(balance).Int64() < 30000000 {
		return nil, errors.New("insufficient funds")
	}

	return c.clientGrpc.DeployContract(from, name, abi, byteCode, feeLimit, curPercent, oeLimit)
}

/*------------------------JSON RPC----------------------------------*/
func (c *Client) Send(prv, from, to string, amountTRX float64) (string, error) {
	// Create Raw Transaction
	amountSun := ToSun(amountTRX)
	rawTx, err := c.CreateSendTrx(from, to, int64(amountSun))
	if err != nil {
		fmt.Println(err)
		return "", err
	}

	// Estimate Bandwidth
	// bandwidth := c.EstimateBandwithRawTx(rawTx, 1)
	// fmt.Println("......bandwidth rawTx......", bandwidth)

	// Sign Transaction
	signedTx, err := SignRawTx(prv, rawTx)
	if err != nil {
		fmt.Println(err)
		return "", err
	}

	// b, err := json.MarshalIndent(signedTx, "", " ")
	// if err != nil {
	// 	fmt.Println(err)
	// 	return "", err

	// }
	// fmt.Println("......signedTx......", string(b))

	// Estimate Bandwidth
	// bandwidth = c.EstimateBandwithSignedTx(signedTx)
	// fmt.Println("......bandwidth signedTx......", bandwidth)

	// Broadcast Transaction
	_, err = c.Broadcast(signedTx)
	if err != nil {
		fmt.Println(err)
		return "", err

	}

	// b, err = json.MarshalIndent(tx, "", " ")
	// if err != nil {
	// 	fmt.Println(err)
	// 	return "", err
	// }
	// fmt.Println("......tx......", string(b))

	// ONLY GRPC
	// txHash := BytesToHexString([]byte(signedTx.TxID))
	// fmt.Println("...txHash...", txHash)

	return signedTx.TxID, nil
}

/*--------ERC20---------*/
func (c *Client) DeploySolidity(prv, from, name, abi, data string, gas, value, consumeUserResourcePercent, originEnergyLimit, tokenId, tokenValue int64) (string, error) {
	// CreateSmartContract
	rawTx, err := c.CreateSmartContract(from, name, abi, data, gas, value, consumeUserResourcePercent, originEnergyLimit, tokenId, tokenValue)
	if err != nil {
		return "", err
	}

	// Sign Transaction
	signedTx, err := SignRawTxContract(prv, rawTx)
	if err != nil {
		fmt.Println(err)
		return "", err
	}
	// b, err := json.MarshalIndent(signedTx, "", " ")
	// if err != nil {
	// 	fmt.Println(err)
	// 	return "", err
	// }
	// fmt.Println("......signedTx......", string(b))

	// Broadcast Transaction
	_, err = c.BroadcastContract(signedTx)
	if err != nil {
		fmt.Println(err)
		return "", err
	}

	// b, err := json.MarshalIndent(tx, "", " ")
	// if err != nil {
	// 	fmt.Println(err)
	// 	return "", err
	// }
	// fmt.Println("......tx......", string(b))

	// ONLY GRPC
	// txHash := BytesToHexString([]byte(signedTx.TxID))
	// fmt.Println("...txHash...", txHash)

	return signedTx.TxID, nil
}

func (c *Client) NewSoliditySignedTX(prv, fromAddr, contractTRC20, abi, method string, decimal int, gasLimitSun int64, params ...interface{}) (*SignedTransaction, error) {
	// params
	data, err := eth.Pack(abi, method, params)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	dataHex := common.Bytes2Hex(data)
	fmt.Println("...data...", dataHex)

	if gasLimitSun <= 0 {
		// gasPrice
		gasPriceHex := c.GasPrice()
		gasPriceSun := eth.ToBig256(gasPriceHex).Int64()
		gasPriceTrx := FromSun(float64(gasPriceSun))
		fmt.Println("......gasPrice......", gasPriceHex, gasPriceSun, gasPriceTrx)

		// gasUsed engergy
		gasUsedHex := c.EstimateGas(fromAddr, contractTRC20, dataHex, 0)
		gasUsedSun := eth.ToBig256(gasUsedHex).Int64()
		gasUsedTrx := FromSun(float64(gasUsedSun))
		fmt.Println("......gasUsed......", gasUsedHex, gasUsedSun, gasUsedTrx)

		// fee_limit = energy_required x energy unit price
		gasLimitSun = gasUsedSun * gasPriceSun
		fmt.Println("......gasLimit......", gasLimitSun, FromSun(float64(gasLimitSun)))
	}

	// Create Raw Transaction
	rawTx, err := c.CreateTriggerSmartContract(fromAddr, contractTRC20, dataHex, gasLimitSun, int64(0), int64(0), int64(0))
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	// Estimate Bandwidth
	// bandwidth := c.EstimateBandwithRawTx(rawTx, 1)
	// fmt.Println("......bandwidth rawTx......", bandwidth)

	// Sign Transaction
	signedTx, err := SignRawTx(prv, rawTx)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	// b, err := json.MarshalIndent(signedTx, "", " ")
	// if err != nil {
	// 	fmt.Println(err)
	// 	return nil, err
	// }
	// fmt.Println("......signedTx......", string(b))

	// Estimate Bandwidth
	// bandwidth = c.EstimateBandwithSignedTx(signedTx)
	// fmt.Println("......bandwidth signedTx......", bandwidth)

	return signedTx, nil
}

func (c *Client) Approve(prv, spender string, contractTRC20, abi string, decimal int, gasLimitSun int64, sunToken int64) (string, error) {
	// params
	//tokenValue := FloatToBigInt(numToken, decimal)
	to, err := Base58ToAddress(spender)
	if err != nil {
		fmt.Println(err)
		return "", err
	}

	// get address from private key
	kp, err := NewKeyPairFromPrivKey(prv)
	if err != nil {
		fmt.Println(err)
		return "", err
	}

	// signedTx
	signedTx, err := c.NewSoliditySignedTX(prv, kp.Address, contractTRC20, abi, "approve", decimal, gasLimitSun, common.HexToAddress(to.Hex()), big.NewInt(sunToken))
	if err != nil {
		return "", err
	}

	// Broadcast Transaction
	_, err = c.Broadcast(signedTx)
	if err != nil {
		fmt.Println(err)
		return "", err

	}

	// b, err := json.MarshalIndent(tx, "", " ")
	// if err != nil {
	// 	fmt.Println(err)
	// 	return "", err
	// }
	// fmt.Println("......tx......", string(b))

	// ONLY GRPC
	// txHash := BytesToHexString([]byte(signedTx.TxID))
	// fmt.Println("...txHash...", txHash)

	return signedTx.TxID, nil
}

func (c *Client) Allowance(owner, spender string, contractTRC20, abi string) string {
	from, err := Base58ToAddress(owner)
	if err != nil {
		return ""
	}

	to, err := Base58ToAddress(spender)
	if err != nil {
		return ""
	}

	data, err := eth.Pack(abi, "allowance", common.HexToAddress(from.Hex()), common.HexToAddress(to.Hex()))
	if err != nil {
		return ""
	}
	dataHex := common.Bytes2Hex(data)
	return c.Call(owner, contractTRC20, "0x0", "0x0", "0x0", dataHex)
}

func (c *Client) Transfer(prv, recipient string, contractTRC20, abi string, decimal int, gasLimitSun int64, sunToken int64) (string, error) {
	// params
	//tokenValue := FloatToBigInt(numToken, decimal)
	to, err := Base58ToAddress(recipient)
	if err != nil {
		fmt.Println(err)
		return "", err
	}

	// get address from private key
	kp, err := NewKeyPairFromPrivKey(prv)
	if err != nil {
		fmt.Println(err)
		return "", err
	}

	// signedTx
	signedTx, err := c.NewSoliditySignedTX(prv, kp.Address, contractTRC20, abi, "transfer", decimal, gasLimitSun, common.HexToAddress(to.Hex()), big.NewInt(sunToken))
	if err != nil {
		return "", err
	}

	// Broadcast Transaction
	_, err = c.Broadcast(signedTx)
	if err != nil {
		fmt.Println(err)
		return "", err

	}

	// b, err := json.MarshalIndent(tx, "", " ")
	// if err != nil {
	// 	fmt.Println(err)
	// 	return "", err
	// }
	// fmt.Println("......tx......", string(b))

	// ONLY GRPC
	// txHash := BytesToHexString([]byte(signedTx.TxID))
	// fmt.Println("...txHash...", txHash)

	return signedTx.TxID, nil
}

func (c *Client) TransferFrom(prv, sender, recipient string, contractTRC20, abi string, decimal int, gasLimitSun int64, sunToken int64) (string, error) {
	// params
	// tokenValue := FloatToBigInt(numToken, decimal)
	from, err := Base58ToAddress(sender)
	if err != nil {
		fmt.Println(err)
		return "", err
	}

	to, err := Base58ToAddress(recipient)
	if err != nil {
		fmt.Println(err)
		return "", err
	}

	// get address from private key
	kp, err := NewKeyPairFromPrivKey(prv)
	if err != nil {
		fmt.Println(err)
		return "", err
	}

	// signedTx
	signedTx, err := c.NewSoliditySignedTX(prv, kp.Address, contractTRC20, abi, "transferFrom", decimal, gasLimitSun, common.HexToAddress(from.Hex()), common.HexToAddress(to.Hex()), big.NewInt(sunToken))
	if err != nil {
		return "", err
	}

	// Broadcast Transaction
	_, err = c.Broadcast(signedTx)
	if err != nil {
		fmt.Println(err)
		return "", err

	}

	// b, err := json.MarshalIndent(tx, "", " ")
	// if err != nil {
	// 	fmt.Println(err)
	// 	return "", err
	// }
	// fmt.Println("......tx......", string(b))

	// ONLY GRPC
	// txHash := BytesToHexString([]byte(signedTx.TxID))
	// fmt.Println("...txHash...", txHash)

	return signedTx.TxID, nil
}
