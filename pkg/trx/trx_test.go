package trx

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"math"
	"math/big"
	"strings"
	"testing"

	"github.com/btcsuite/btcd/btcec/v2"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	tronGRPC "github.com/fbsobreira/gotron-sdk/pkg/client"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/info.terraceit/gostocy/pkg/eth"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/proto"
)

var Mainnet = "https://api.trongrid.io"
var Shasta = "https://api.shasta.trongrid.io"
var Nile = "https://nile.trongrid.io"

var GprcMain = "grpc.trongrid.io:50051"
var GprcShasta = "grpc.shasta.trongrid.io:50051"
var GprcNile = "grpc.nile.trongrid.io:50051"

var privKey = "47de15108b35169c4aff4826d5c413fe117e361a900325f6d3df1f0e04cbd706"
var fromAddr = "TNuCHV47zLuQWxSTJbU5Mdb8tiyCTRtm3e"
var contractUSDT = "TXLAQ63Xg1NAzckPwKHvzw7CSEmLMEqcdj"
var abiUSDT = `[{"constant":true,"inputs":[],"name":"name","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_upgradedAddress","type":"address"}],"name":"deprecate","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_value","type":"uint256"}],"name":"approve","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"deprecated","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_evilUser","type":"address"}],"name":"addBlackList","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_from","type":"address"},{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transferFrom","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"upgradedAddress","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"decimals","outputs":[{"name":"","type":"uint8"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"maximumFee","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"_totalSupply","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"unpause","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"_maker","type":"address"}],"name":"getBlackListStatus","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"paused","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_subtractedValue","type":"uint256"}],"name":"decreaseApproval","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"who","type":"address"}],"name":"balanceOf","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_value","type":"uint256"}],"name":"calcFee","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[],"name":"pause","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"owner","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"symbol","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_to","type":"address"},{"name":"_value","type":"uint256"}],"name":"transfer","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"who","type":"address"}],"name":"oldBalanceOf","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"newBasisPoints","type":"uint256"},{"name":"newMaxFee","type":"uint256"}],"name":"setParams","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"amount","type":"uint256"}],"name":"issue","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_spender","type":"address"},{"name":"_addedValue","type":"uint256"}],"name":"increaseApproval","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"amount","type":"uint256"}],"name":"redeem","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"_owner","type":"address"},{"name":"_spender","type":"address"}],"name":"allowance","outputs":[{"name":"remaining","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"basisPointsRate","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"isBlackListed","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_clearedUser","type":"address"}],"name":"removeBlackList","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"MAX_UINT","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_blackListedUser","type":"address"}],"name":"destroyBlackFunds","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"inputs":[{"name":"_initialSupply","type":"uint256"},{"name":"_name","type":"string"},{"name":"_symbol","type":"string"},{"name":"_decimals","type":"uint8"}],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_blackListedUser","type":"address"},{"indexed":false,"name":"_balance","type":"uint256"}],"name":"DestroyedBlackFunds","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"amount","type":"uint256"}],"name":"Issue","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"amount","type":"uint256"}],"name":"Redeem","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"newAddress","type":"address"}],"name":"Deprecate","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_user","type":"address"}],"name":"AddedBlackList","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_user","type":"address"}],"name":"RemovedBlackList","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"feeBasisPoints","type":"uint256"},{"indexed":false,"name":"maxFee","type":"uint256"}],"name":"Params","type":"event"},{"anonymous":false,"inputs":[],"name":"Pause","type":"event"},{"anonymous":false,"inputs":[],"name":"Unpause","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"previousOwner","type":"address"},{"indexed":true,"name":"newOwner","type":"address"}],"name":"OwnershipTransferred","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"owner","type":"address"},{"indexed":true,"name":"spender","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"from","type":"address"},{"indexed":true,"name":"to","type":"address"},{"indexed":false,"name":"value","type":"uint256"}],"name":"Transfer","type":"event"}]`

var abiVault = `[{"inputs":[{"internalType":"address","name":"_asset","type":"address"},{"internalType":"address","name":"_owner","type":"address"},{"internalType":"uint8","name":"_ownerType","type":"uint8"}],"stateMutability":"nonpayable","type":"constructor"},{"inputs":[],"name":"AccessControlBadConfirmation","type":"error"},{"inputs":[{"internalType":"address","name":"account","type":"address"},{"internalType":"bytes32","name":"neededRole","type":"bytes32"}],"name":"AccessControlUnauthorizedAccount","type":"error"},{"inputs":[],"name":"EnforcedPause","type":"error"},{"inputs":[],"name":"ExpectedPause","type":"error"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint256","name":"wad","type":"uint256"},{"indexed":false,"internalType":"address","name":"to","type":"address"}],"name":"EAdminTransfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint256","name":"wad","type":"uint256"},{"indexed":false,"internalType":"address","name":"to","type":"address"}],"name":"EAdminTransferAll","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"bytes32","name":"ticketID","type":"bytes32"},{"indexed":false,"internalType":"address","name":"owner","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"}],"name":"EExit","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"oldOwner","type":"address"},{"indexed":false,"internalType":"address","name":"newOwner","type":"address"}],"name":"EOwnerChanged","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint256","name":"wad","type":"uint256"},{"indexed":false,"internalType":"address","name":"to","type":"address"}],"name":"EOwnerTransfer","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint256","name":"wad","type":"uint256"},{"indexed":false,"internalType":"address","name":"to","type":"address"}],"name":"EOwnerWithdrawAll","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"bytes32","name":"ticketID","type":"bytes32"},{"indexed":false,"internalType":"address","name":"owner","type":"address"}],"name":"ESettlement","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"owner","type":"address"},{"indexed":false,"internalType":"uint8","name":"ownerType","type":"uint8"},{"indexed":false,"internalType":"address","name":"asset","type":"address"},{"indexed":false,"internalType":"address","name":"vault","type":"address"},{"indexed":false,"internalType":"address","name":"admin","type":"address"}],"name":"EVaultCreated","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"account","type":"address"}],"name":"Paused","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"bytes32","name":"role","type":"bytes32"},{"indexed":true,"internalType":"bytes32","name":"previousAdminRole","type":"bytes32"},{"indexed":true,"internalType":"bytes32","name":"newAdminRole","type":"bytes32"}],"name":"RoleAdminChanged","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"bytes32","name":"role","type":"bytes32"},{"indexed":true,"internalType":"address","name":"account","type":"address"},{"indexed":true,"internalType":"address","name":"sender","type":"address"}],"name":"RoleGranted","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"bytes32","name":"role","type":"bytes32"},{"indexed":true,"internalType":"address","name":"account","type":"address"},{"indexed":true,"internalType":"address","name":"sender","type":"address"}],"name":"RoleRevoked","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"account","type":"address"}],"name":"Unpaused","type":"event"},{"inputs":[{"components":[{"internalType":"uint256","name":"tokenLocked","type":"uint256"},{"internalType":"uint256","name":"pendingToken","type":"uint256"},{"internalType":"uint256","name":"fee","type":"uint256"},{"internalType":"address","name":"owner","type":"address"},{"internalType":"address","name":"admin","type":"address"}],"internalType":"struct VaultInfo","name":"newVault","type":"tuple"}],"name":"ApproveTicket","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"newOwner","type":"address"}],"name":"ChangeOwner","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"CreateSettlement","outputs":[{"internalType":"bytes32","name":"","type":"bytes32"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"DEFAULT_ADMIN_ROLE","outputs":[{"internalType":"bytes32","name":"","type":"bytes32"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"Exit","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"OWNER_ROLE","outputs":[{"internalType":"bytes32","name":"","type":"bytes32"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"wad","type":"uint256"},{"internalType":"address","name":"to","type":"address"}],"name":"OwnerTransfer","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"to","type":"address"}],"name":"OwnerWithdrawAll","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"wad","type":"uint256"},{"internalType":"address","name":"to","type":"address"}],"name":"Transfer","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"wad","type":"uint256"},{"internalType":"address","name":"to","type":"address"}],"name":"TransferAll","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bytes4","name":"command","type":"bytes4"},{"internalType":"bytes","name":"data","type":"bytes"}],"name":"UpdateVault","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"asset","outputs":[{"internalType":"contract IERC20","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"bytes32","name":"role","type":"bytes32"}],"name":"getRoleAdmin","outputs":[{"internalType":"bytes32","name":"","type":"bytes32"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"bytes32","name":"role","type":"bytes32"},{"internalType":"address","name":"account","type":"address"}],"name":"grantRole","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bytes32","name":"role","type":"bytes32"},{"internalType":"address","name":"account","type":"address"}],"name":"hasRole","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"bytes32","name":"","type":"bytes32"}],"name":"mTicketTTL","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"pause","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"paused","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"bytes32","name":"role","type":"bytes32"},{"internalType":"address","name":"callerConfirmation","type":"address"}],"name":"renounceRole","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bytes32","name":"role","type":"bytes32"},{"internalType":"address","name":"account","type":"address"}],"name":"revokeRole","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"slot0","outputs":[{"internalType":"uint256","name":"tokenLocked","type":"uint256"},{"internalType":"uint256","name":"pendingToken","type":"uint256"},{"internalType":"uint256","name":"fee","type":"uint256"},{"internalType":"address","name":"owner","type":"address"},{"internalType":"address","name":"admin","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"bytes4","name":"interfaceId","type":"bytes4"}],"name":"supportsInterface","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"ticket","outputs":[{"internalType":"bytes32","name":"","type":"bytes32"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"unpause","outputs":[],"stateMutability":"nonpayable","type":"function"}]`
var bytecodeVault = `0x60a060405234801561000f575f80fd5b5060405161235c38038061235c83398181016040528101906100319190610386565b5f60015f6101000a81548160ff02191690831515021790555061005c5f801b3361019360201b60201c565b5061009160405160200161006f9061042a565b604051602081830303815290604052805190602001208361019360201b60201c565b508273ffffffffffffffffffffffffffffffffffffffff1660808173ffffffffffffffffffffffffffffffffffffffff16815250508160026003015f6101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055503360026004015f6101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055507fe3dea5429c3bc457b0c2213f063ebc4a64bf3b7f5ff70135979c7bf4591ac4c8828285303360405161018395949392919061045c565b60405180910390a15050506104ad565b5f6101a4838361028860201b60201c565b61027e5760015f808581526020019081526020015f205f015f8473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020015f205f6101000a81548160ff02191690831515021790555061021b6102eb60201b60201c565b73ffffffffffffffffffffffffffffffffffffffff168273ffffffffffffffffffffffffffffffffffffffff16847f2f8788117e7eff1d82e926ec794901d17c78024a50270940304540a733656f0d60405160405180910390a460019050610282565b5f90505b92915050565b5f805f8481526020019081526020015f205f015f8373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020015f205f9054906101000a900460ff16905092915050565b5f33905090565b5f80fd5b5f73ffffffffffffffffffffffffffffffffffffffff82169050919050565b5f61031f826102f6565b9050919050565b61032f81610315565b8114610339575f80fd5b50565b5f8151905061034a81610326565b92915050565b5f60ff82169050919050565b61036581610350565b811461036f575f80fd5b50565b5f815190506103808161035c565b92915050565b5f805f6060848603121561039d5761039c6102f2565b5b5f6103aa8682870161033c565b93505060206103bb8682870161033c565b92505060406103cc86828701610372565b9150509250925092565b5f81905092915050565b7f4f574e45525f524f4c45000000000000000000000000000000000000000000005f82015250565b5f610414600a836103d6565b915061041f826103e0565b600a82019050919050565b5f61043482610408565b9150819050919050565b61044781610315565b82525050565b61045681610350565b82525050565b5f60a08201905061046f5f83018861043e565b61047c602083018761044d565b610489604083018661043e565b610496606083018561043e565b6104a3608083018461043e565b9695505050505050565b608051611e666104f65f395f8181610527015281816106dd0152818161074e015281816109d101528181610a6b01528181610b5701528181610d220152610dda0152611e665ff3fe608060405234801561000f575f80fd5b5060043610610156575f3560e01c806376a98ebc116100c1578063bbf5d9711161007a578063bbf5d9711461038e578063cff410b4146103be578063d547741f146103dc578063e58378bb146103f8578063f285329214610416578063fbef8a9b1461043257610156565b806376a98ebc146102ce5780638456cb59146102fe57806391d14854146103085780639510000b14610338578063a217fddf14610354578063abe1dcf91461037257610156565b806338d52e0f1161011357806338d52e0f146102305780633f4ba83a1461024e5780634fe71e67146102585780635ac4e148146102745780635c975abb146102925780636cc25db7146102b057610156565b806301ffc9a71461015a578063248a9ca31461018a5780632f2ff15d146101ba57806333ab640f146101d657806336568abe146101f25780633850c7bd1461020e575b5f80fd5b610174600480360381019061016f91906114c8565b610462565b604051610181919061150d565b60405180910390f35b6101a4600480360381019061019f9190611559565b6104db565b6040516101b19190611593565b60405180910390f35b6101d460048036038101906101cf9190611606565b6104f7565b005b6101f060048036038101906101eb9190611677565b610519565b005b61020c60048036038101906102079190611606565b6105ff565b005b61021661067a565b6040516102279594939291906116d3565b60405180910390f35b6102386106db565b604051610245919061177f565b60405180910390f35b6102566106ff565b005b610272600480360381019061026d9190611677565b610716565b005b61027c610826565b6040516102899190611593565b60405180910390f35b61029a6108f0565b6040516102a7919061150d565b60405180910390f35b6102b8610905565b6040516102c59190611593565b60405180910390f35b6102e860048036038101906102e391906118ad565b61090b565b6040516102f5919061150d565b60405180910390f35b61030661091e565b005b610322600480360381019061031d9190611606565b610935565b60405161032f919061150d565b60405180910390f35b610352600480360381019061034d91906118d8565b610998565b005b61035c610b43565b6040516103699190611593565b60405180910390f35b61038c60048036038101906103879190611677565b610b49565b005b6103a860048036038101906103a39190611964565b610c2f565b6040516103b5919061150d565b60405180910390f35b6103c6610c44565b6040516103d3919061150d565b60405180910390f35b6103f660048036038101906103f19190611606565b610eba565b005b610400610edc565b60405161040d9190611593565b60405180910390f35b610430600480360381019061042b91906118d8565b610f04565b005b61044c60048036038101906104479190611559565b611017565b60405161045991906119c1565b60405180910390f35b5f7f7965db0b000000000000000000000000000000000000000000000000000000007bffffffffffffffffffffffffffffffffffffffffffffffffffffffff1916827bffffffffffffffffffffffffffffffffffffffffffffffffffffffff191614806104d457506104d38261102c565b5b9050919050565b5f805f8381526020019081526020015f20600101549050919050565b610500826104db565b61050981611095565b61051383836110a9565b50505050565b5f801b61052581611095565b7f000000000000000000000000000000000000000000000000000000000000000073ffffffffffffffffffffffffffffffffffffffff1663a9059cbb83856040518363ffffffff1660e01b81526004016105809291906119da565b6020604051808303815f875af115801561059c573d5f803e3d5ffd5b505050506040513d601f19601f820116820180604052508101906105c09190611a2b565b507ff32964dae370fe3aefec124d2afcee4b5abedad5b28bfb0f3639884fe651bf3483836040516105f2929190611a56565b60405180910390a1505050565b610607611192565b73ffffffffffffffffffffffffffffffffffffffff168173ffffffffffffffffffffffffffffffffffffffff161461066b576040517f6697b23200000000000000000000000000000000000000000000000000000000815260040160405180910390fd5b6106758282611199565b505050565b6002805f015490806001015490806002015490806003015f9054906101000a900473ffffffffffffffffffffffffffffffffffffffff1690806004015f9054906101000a900473ffffffffffffffffffffffffffffffffffffffff16905085565b7f000000000000000000000000000000000000000000000000000000000000000081565b5f801b61070b81611095565b610713611282565b50565b61071e6112e3565b60405160200161072d90611ad1565b6040516020818303038152906040528051906020012061074c81611095565b7f000000000000000000000000000000000000000000000000000000000000000073ffffffffffffffffffffffffffffffffffffffff1663a9059cbb83856040518363ffffffff1660e01b81526004016107a79291906119da565b6020604051808303815f875af11580156107c3573d5f803e3d5ffd5b505050506040513d601f19601f820116820180604052508101906107e79190611a2b565b507f632031794f506ce78594dc4046cbba98a82f94b8e8b765fb23bb1795aecf8fa38383604051610819929190611a56565b60405180910390a1505050565b5f61082f6112e3565b60405160200161083e90611ad1565b6040516020818303038152906040528051906020012061085d81611095565b5f801b600754146108a3576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040161089a90611b3f565b60405180910390fd5b6108ac33611324565b7f6e29f79aff439f6fb9dd443c7da9535de3edb0ecc2d8dc5e24577c8f43668a64600754336040516108df929190611b5d565b60405180910390a160075491505090565b5f60015f9054906101000a900460ff16905090565b60075481565b5f805f1b61091881611095565b50919050565b5f801b61092a81611095565b610932611370565b50565b5f805f8481526020019081526020015f205f015f8373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020015f205f9054906101000a900460ff16905092915050565b6109a06112e3565b6040516020016109af90611ad1565b604051602081830303815290604052805190602001206109ce81611095565b5f7f000000000000000000000000000000000000000000000000000000000000000073ffffffffffffffffffffffffffffffffffffffff166370a08231306040518263ffffffff1660e01b8152600401610a289190611b84565b602060405180830381865afa158015610a43573d5f803e3d5ffd5b505050506040513d601f19601f82011682018060405250810190610a679190611bb1565b90507f000000000000000000000000000000000000000000000000000000000000000073ffffffffffffffffffffffffffffffffffffffff1663a9059cbb84836040518363ffffffff1660e01b8152600401610ac49291906119da565b6020604051808303815f875af1158015610ae0573d5f803e3d5ffd5b505050506040513d601f19601f82011682018060405250810190610b049190611a2b565b507f86f3f3ff14dda039b78d5b01546bb373acbc6c2e471b6bd8f2d2684e2a05e8ec8184604051610b36929190611a56565b60405180910390a1505050565b5f801b81565b5f801b610b5581611095565b7f000000000000000000000000000000000000000000000000000000000000000073ffffffffffffffffffffffffffffffffffffffff1663a9059cbb83856040518363ffffffff1660e01b8152600401610bb09291906119da565b6020604051808303815f875af1158015610bcc573d5f803e3d5ffd5b505050506040513d601f19601f82011682018060405250810190610bf09190611a2b565b507f8dbef736551930d9fc29cc0d2094c7de32fb90aede118714ca0e696aedb3c91c8383604051610c22929190611a56565b60405180910390a1505050565b5f805f1b610c3c81611095565b509392505050565b5f604051602001610c5490611ad1565b60405160208183030381529060405280519060200120610c7381611095565b5f801b60075403610cb9576040517f08c379a0000000000000000000000000000000000000000000000000000000008152600401610cb090611c26565b60405180910390fd5b60085f60075481526020019081526020015f2054421015610d0f576040517f08c379a0000000000000000000000000000000000000000000000000000000008152600401610d0690611c8e565b60405180910390fd5b5f60025f015460026001015460028001547f000000000000000000000000000000000000000000000000000000000000000073ffffffffffffffffffffffffffffffffffffffff166370a08231306040518263ffffffff1660e01b8152600401610d799190611b84565b602060405180830381865afa158015610d94573d5f803e3d5ffd5b505050506040513d601f19601f82011682018060405250810190610db89190611bb1565b610dc29190611cd9565b610dcc9190611cd9565b610dd69190611cd9565b90507f000000000000000000000000000000000000000000000000000000000000000073ffffffffffffffffffffffffffffffffffffffff1663a9059cbb33836040518363ffffffff1660e01b8152600401610e339291906119da565b6020604051808303815f875af1158015610e4f573d5f803e3d5ffd5b505050506040513d601f19601f82011682018060405250810190610e739190611a2b565b507f576dd17524ac03db1026e5d7e2f81bbe56b3bd2cec19ddeb29e2f78eafe2081e6007543383604051610ea993929190611d0c565b60405180910390a160019250505090565b610ec3826104db565b610ecc81611095565b610ed68383611199565b50505050565b604051602001610eeb90611ad1565b6040516020818303038152906040528051906020012081565b5f801b610f1081611095565b5f60026003015f9054906101000a900473ffffffffffffffffffffffffffffffffffffffff169050610f66604051602001610f4a90611ad1565b60405160208183030381529060405280519060200120846110a9565b50610f95604051602001610f7990611ad1565b6040516020818303038152906040528051906020012082611199565b508260026003015f6101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055507f2f0f41e268e5a8d5efdf58dabc50490bfb6fcf65606eeebbc6ea0704ad5f95c1818460405161100a929190611d41565b60405180910390a1505050565b6008602052805f5260405f205f915090505481565b5f7f01ffc9a7000000000000000000000000000000000000000000000000000000007bffffffffffffffffffffffffffffffffffffffffffffffffffffffff1916827bffffffffffffffffffffffffffffffffffffffffffffffffffffffff1916149050919050565b6110a6816110a1611192565b6113d1565b50565b5f6110b48383610935565b6111885760015f808581526020019081526020015f205f015f8473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020015f205f6101000a81548160ff021916908315150217905550611125611192565b73ffffffffffffffffffffffffffffffffffffffff168273ffffffffffffffffffffffffffffffffffffffff16847f2f8788117e7eff1d82e926ec794901d17c78024a50270940304540a733656f0d60405160405180910390a46001905061118c565b5f90505b92915050565b5f33905090565b5f6111a48383610935565b15611278575f805f8581526020019081526020015f205f015f8473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020015f205f6101000a81548160ff021916908315150217905550611215611192565b73ffffffffffffffffffffffffffffffffffffffff168273ffffffffffffffffffffffffffffffffffffffff16847ff6391f5c32d9c69d2a47ea670b442974b53935d1edc7fd64eb21e047a839171b60405160405180910390a46001905061127c565b5f90505b92915050565b61128a611422565b5f60015f6101000a81548160ff0219169083151502179055507f5db9ee0a495bf2e6ff9c91a7834c1ba4fdd244a5e8aa4e537bd38aeae4b073aa6112cc611192565b6040516112d99190611b84565b60405180910390a1565b6112eb6108f0565b15611322576040517fd93c066500000000000000000000000000000000000000000000000000000000815260040160405180910390fd5b565b80424460405160200161133993929190611dcd565b604051602081830303815290604052805190602001206007819055504260085f60075481526020019081526020015f208190555050565b6113786112e3565b6001805f6101000a81548160ff0219169083151502179055507f62e78cea01bee320cd4e420270b5ea74000d11b0c9f74754ebdbfc544b05a2586113ba611192565b6040516113c79190611b84565b60405180910390a1565b6113db8282610935565b61141e5780826040517fe2517d3f000000000000000000000000000000000000000000000000000000008152600401611415929190611e09565b60405180910390fd5b5050565b61142a6108f0565b611460576040517f8dfc202b00000000000000000000000000000000000000000000000000000000815260040160405180910390fd5b565b5f604051905090565b5f80fd5b5f80fd5b5f7fffffffff0000000000000000000000000000000000000000000000000000000082169050919050565b6114a781611473565b81146114b1575f80fd5b50565b5f813590506114c28161149e565b92915050565b5f602082840312156114dd576114dc61146b565b5b5f6114ea848285016114b4565b91505092915050565b5f8115159050919050565b611507816114f3565b82525050565b5f6020820190506115205f8301846114fe565b92915050565b5f819050919050565b61153881611526565b8114611542575f80fd5b50565b5f813590506115538161152f565b92915050565b5f6020828403121561156e5761156d61146b565b5b5f61157b84828501611545565b91505092915050565b61158d81611526565b82525050565b5f6020820190506115a65f830184611584565b92915050565b5f73ffffffffffffffffffffffffffffffffffffffff82169050919050565b5f6115d5826115ac565b9050919050565b6115e5816115cb565b81146115ef575f80fd5b50565b5f81359050611600816115dc565b92915050565b5f806040838503121561161c5761161b61146b565b5b5f61162985828601611545565b925050602061163a858286016115f2565b9150509250929050565b5f819050919050565b61165681611644565b8114611660575f80fd5b50565b5f813590506116718161164d565b92915050565b5f806040838503121561168d5761168c61146b565b5b5f61169a85828601611663565b92505060206116ab858286016115f2565b9150509250929050565b6116be81611644565b82525050565b6116cd816115cb565b82525050565b5f60a0820190506116e65f8301886116b5565b6116f360208301876116b5565b61170060408301866116b5565b61170d60608301856116c4565b61171a60808301846116c4565b9695505050505050565b5f819050919050565b5f61174761174261173d846115ac565b611724565b6115ac565b9050919050565b5f6117588261172d565b9050919050565b5f6117698261174e565b9050919050565b6117798161175f565b82525050565b5f6020820190506117925f830184611770565b92915050565b5f80fd5b5f601f19601f8301169050919050565b7f4e487b71000000000000000000000000000000000000000000000000000000005f52604160045260245ffd5b6117e28261179c565b810181811067ffffffffffffffff82111715611801576118006117ac565b5b80604052505050565b5f611813611462565b905061181f82826117d9565b919050565b5f60a0828403121561183957611838611798565b5b61184360a061180a565b90505f61185284828501611663565b5f83015250602061186584828501611663565b602083015250604061187984828501611663565b604083015250606061188d848285016115f2565b60608301525060806118a1848285016115f2565b60808301525092915050565b5f60a082840312156118c2576118c161146b565b5b5f6118cf84828501611824565b91505092915050565b5f602082840312156118ed576118ec61146b565b5b5f6118fa848285016115f2565b91505092915050565b5f80fd5b5f80fd5b5f80fd5b5f8083601f84011261192457611923611903565b5b8235905067ffffffffffffffff81111561194157611940611907565b5b60208301915083600182028301111561195d5761195c61190b565b5b9250929050565b5f805f6040848603121561197b5761197a61146b565b5b5f611988868287016114b4565b935050602084013567ffffffffffffffff8111156119a9576119a861146f565b5b6119b58682870161190f565b92509250509250925092565b5f6020820190506119d45f8301846116b5565b92915050565b5f6040820190506119ed5f8301856116c4565b6119fa60208301846116b5565b9392505050565b611a0a816114f3565b8114611a14575f80fd5b50565b5f81519050611a2581611a01565b92915050565b5f60208284031215611a4057611a3f61146b565b5b5f611a4d84828501611a17565b91505092915050565b5f604082019050611a695f8301856116b5565b611a7660208301846116c4565b9392505050565b5f81905092915050565b7f4f574e45525f524f4c45000000000000000000000000000000000000000000005f82015250565b5f611abb600a83611a7d565b9150611ac682611a87565b600a82019050919050565b5f611adb82611aaf565b9150819050919050565b5f82825260208201905092915050565b7f416c7068763a205469636b6574204578697374210000000000000000000000005f82015250565b5f611b29601483611ae5565b9150611b3482611af5565b602082019050919050565b5f6020820190508181035f830152611b5681611b1d565b9050919050565b5f604082019050611b705f830185611584565b611b7d60208301846116c4565b9392505050565b5f602082019050611b975f8301846116c4565b92915050565b5f81519050611bab8161164d565b92915050565b5f60208284031215611bc657611bc561146b565b5b5f611bd384828501611b9d565b91505092915050565b7f416c7068763a20496e76616c6964205469636b657421000000000000000000005f82015250565b5f611c10601683611ae5565b9150611c1b82611bdc565b602082019050919050565b5f6020820190508181035f830152611c3d81611c04565b9050919050565b7f416c7068763a2049742773206e6f742074696d652079657421000000000000005f82015250565b5f611c78601983611ae5565b9150611c8382611c44565b602082019050919050565b5f6020820190508181035f830152611ca581611c6c565b9050919050565b7f4e487b71000000000000000000000000000000000000000000000000000000005f52601160045260245ffd5b5f611ce382611644565b9150611cee83611644565b9250828203905081811115611d0657611d05611cac565b5b92915050565b5f606082019050611d1f5f830186611584565b611d2c60208301856116c4565b611d3960408301846116b5565b949350505050565b5f604082019050611d545f8301856116c4565b611d6160208301846116c4565b9392505050565b5f8160601b9050919050565b5f611d7e82611d68565b9050919050565b5f611d8f82611d74565b9050919050565b611da7611da2826115cb565b611d85565b82525050565b5f819050919050565b611dc7611dc282611644565b611dad565b82525050565b5f611dd88286611d96565b601482019150611de88285611db6565b602082019150611df88284611db6565b602082019150819050949350505050565b5f604082019050611e1c5f8301856116c4565b611e296020830184611584565b939250505056fea26469706673582212208c37995c3239919110a9f48d0d011db5db437fdc347bf6f12db049a793d9be3064736f6c634300081a0033`

/*
GetAssets List
curl --request GET \
     --url https://nile.trongrid.io/v1/assets/USDT/list \
     --header 'accept: application/json'

GetAssets By Owner
curl --request GET \
     --url https://nile.trongrid.io/v1/assets/TGQVLckg1gDZS5wUwPTrPgRG4U8MKC4jcP \
     --header 'accept: application/json'

GetAssets By Id
curl --request GET \
     --url https://nile.trongrid.io/v1/assets/1000308 \
     --header 'accept: application/json'

TriggerSmartContract
curl -X POST 'https://nile.trongrid.io/jsonrpc' --data '{"id": 1,
    "jsonrpc": "2.0",
    "method": "buildTransaction",
    "params": [
        {
            "from": "0x8dD75F7c03A048C0a66a53dbf9ED76d04E9a9eA3",
            "to": "0xEa51342dAbbb928aE1e576bd39eFf8aaf070A8c6",
            "data": "a9059cbb00000000000000000000000073e2af370078dd5d242da6785cc033395496e8fb000000000000000000000000000000000000000000000000000000000000000a",
            "gas": "0xf4240",
            "value": "0x0",
            "tokenId": 1000308,
            "tokenValue": 10
        }
    ]
  }'
*/

func TestToBigNumberHex(t *testing.T) {
	gasLimit := eth.ToBigNumber(int64(1000000))
	fmt.Println("gasLimit : ", gasLimit)

	value := eth.ToBigNumber(int64(0))
	fmt.Println("value :", value)

	f := eth.FloatToBigInt(1, 6)
	fmt.Println(f.Int64(), f.String())

	f = eth.FloatToBigInt(0.5, 6)
	fmt.Println(f.Int64(), f.String())

	fmt.Println(eth.ToBig256("0x245498").String())

	ff := ToSun(1000.0)
	ffB := eth.ToBigNumber(int64(ff))
	fmt.Println(ffB, eth.ToBig256(ffB).String())
}

func TestAddressConvert(t *testing.T) {
	addrTron := "TLoKVZMCPcAqmAVNV8ZjRetRMNrYnW6h9r"

	// Tron to Eth address
	addrETH, err := Base58ToAddress(addrTron)
	assert.Equal(t, nil, err)
	fmt.Println("address Tron :", addrTron, "address Eth:", addrETH.Hex())

	// Eth to Tron address
	addrTronConvert := AddressToBase58(addrETH.Hex())
	assert.Equal(t, addrTron, addrTronConvert)
	fmt.Println("address Tron :", addrTron, "address Tron convert :", addrTronConvert)

	// Validate Tron address
	ok := IsHexAddress(addrTron)
	assert.Equal(t, true, ok)

	// Not Validate Tron address
	noOk := IsHexAddress("0x0000000xxxxx0000000")
	assert.Equal(t, false, noOk)
}

func TestGetBlock(t *testing.T) {
	cli, err := NewClient(Nile, GprcNile)
	if err != nil {
		fmt.Println(err)
		return
	}

	block := cli.GetBlockByHash("0000000003005e978481561db9f52e9b767c7abc9e51e2c2aa5fb451775d5fa3")
	if block == nil {
		fmt.Println("err GetBlockByHash :", err)
		return
	}
	b, err := json.MarshalIndent(block, "", " ")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("......GetBlockByHash..... :", string(b))

	blockNumber := cli.BlockNumber()
	fmt.Println("............blockNumber : ", blockNumber, eth.ToBig256(blockNumber).String(), eth.ToBig256(blockNumber).Int64())

	block = cli.GetBlockByNumber(blockNumber)
	if block == nil {
		fmt.Println("err GetBlockByNumber :", err)
		return
	}
	b, err = json.MarshalIndent(block, "", " ")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("......GetBlockByNumber..... :", string(b))
}

func TestGetBalance(t *testing.T) {
	cli, err := NewClient(Nile, GprcNile)
	if err != nil {
		fmt.Println(err)
		return
	}

	availableSunHex := cli.GetBalance(fromAddr)
	availableSunInt := eth.ToBig256(availableSunHex).Int64()
	availableSunFloat, _ := eth.ToBig256(availableSunHex).Float64()
	availableTrx := FromSun(availableSunFloat)

	fmt.Println("......GetBalance.....", "balanceSunHex :", availableSunHex, "balanaceSunInt :", availableSunInt, "balanceSunFloat :", availableSunFloat, "balanceTrx :", availableTrx)
}

func TestGetByteCode(t *testing.T) {
	from := fromAddr
	fromAddr, err := Base58ToAddress(from)
	if err != nil {
		fmt.Println(err)
		return
	}

	data, err := eth.Pack(abiUSDT, "balanceOf", common.HexToAddress(fromAddr.Hex()))
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("...data...", common.Bytes2Hex(data))
}

func TestFlowGetBalanceOf(t *testing.T) {
	cli, err := NewClient(Mainnet, GprcNile)
	if err != nil {
		fmt.Println(err)
		return
	}

	from := fromAddr
	fromAddr, err := Base58ToAddress(from)
	if err != nil {
		fmt.Println(err)
		return
	}

	data, err := eth.Pack(abiUSDT, "balanceOf", common.HexToAddress(fromAddr.Hex()))
	if err != nil {
		fmt.Println("err GetByteCode :", err)
		return
	}
	dataHex := common.Bytes2Hex(data)
	fmt.Println("...data...", dataHex)

	balance := cli.Call(from, contractUSDT, "0x0", "0x0", "0x0", dataHex)
	fmt.Println("......GetBalanceOf..... :", balance, eth.ToBig256(balance))
}

func TestGetBalanceOf(t *testing.T) {
	cli, err := NewClient(Nile, GprcNile)
	if err != nil {
		fmt.Println(err)
		return
	}

	tokenSunHex := cli.GetBalanceOf(fromAddr, contractUSDT, abiUSDT)
	tokenSunInt64 := eth.ToBig256(tokenSunHex).Int64()
	tokenSunFloat64, _ := eth.ToBig256(tokenSunHex).Float64()
	tokenTrx := FromSunString(tokenSunFloat64)

	fmt.Println("......GetBalanceOf.....", "tokenSunHex :", tokenSunHex, "tokenSunInt64 :", tokenSunInt64, "tokenSunFloat64 :", tokenSunFloat64, "tokenTrx :", tokenTrx)
}

func TestGetTransactionReceipt(t *testing.T) {
	cli, err := NewClient(Nile, GprcNile)
	if err != nil {
		fmt.Println(err)
		return
	}

	var contractUSDT = "TB3Fj87psi6zgunj4AkKZDRzbkCUmRkdKY"
	var txHash = "d0b2594e7df47c90421a0709c32d4fa0c440da21776bdffb40fe067ea12c47ac"
	receipt := cli.GetTransactionReceipt(txHash)
	if receipt.TransactionHash == "" {
		fmt.Println("nil")
		return
	}

	b, err := json.MarshalIndent(receipt, "", " ")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("...Tx...", string(b))

	if len(receipt.Logs) <= 0 {
		fmt.Println("empty topics")
		return
	}

	addrUSDT, err := Base58ToAddress(contractUSDT)
	if err != nil {
		fmt.Println(err)
		return
	}

	index := -1
	for _, lg := range receipt.Logs {
		//if strings.ToLower(lg.Address.Hex()) != strings.ToLower(addrUSDT.Hex()) {
		if !strings.EqualFold(lg.Address.Hex(), addrUSDT.Hex()) {
			// fmt.Println("not same contract address")
			continue
		}

		topics := lg.Topics
		eventSignature := "Transfer(address,address,uint256)"
		topicSignature := Sha3FromEvent(eventSignature)
		if topicSignature != topics[0].Hex() {
			// fmt.Println("different event sig")
			continue
		}
		fmt.Println("event :", eventSignature, "hex :", topicSignature)

		from := HexToAddress(AddressFromEvent(topics[1].Hex()))
		fmt.Println("from : ", from.Hex(), from.String())

		to := HexToAddress(AddressFromEvent(topics[2].Hex()))
		fmt.Println("to :", to.Hex(), to.String())

		amount := eth.ToBigFloat256(lg.Data)
		fmt.Println("value SUN :", amount, " Trx :", FromSun(amount))

		index++
		fmt.Println("index :", index, "log index :", lg.Index)
	}
}

func TestGetDeployContract(t *testing.T) {
	cli, err := NewClient(Nile, GprcNile)
	if err != nil {
		fmt.Println(err)
		return
	}

	var txHash = "0696b7074a12e705d547baa7c1c26452284e7581629a44a66688eb3e927c1ccf"
	receipt := cli.GetTransactionReceipt(txHash)
	if receipt.TransactionHash == "" {
		fmt.Println("nil")
		return
	}

	b, err := json.MarshalIndent(receipt, "", " ")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("...Tx...", string(b))

	if len(receipt.Logs) <= 0 {
		fmt.Println("empty topics")
		return
	}

	if receipt.ContractAddress == "" {
		fmt.Println("not contract")
		return
	}

	oldest := len(receipt.Logs) - 1
	topics := receipt.Logs[oldest].Topics
	eventSignature := "EVaultCreated(address,uint8,address,address,address)"
	topicSignature := Sha3FromEvent(eventSignature)
	fmt.Println("topicSignature :", topicSignature, "topics[0].Hex :", topics[0].Hex())
	if topicSignature != topics[0].Hex() {
		fmt.Println("different event sig")
		return
	}
	fmt.Println("event :", eventSignature, "hex :", topicSignature)

	from := AddressToBase58(receipt.From)
	fmt.Println("from : ", receipt.From, from)

	contract := AddressToBase58(receipt.ContractAddress)
	fmt.Println("contract : ", contract)
}

func TestSendTRX(t *testing.T) {
	cli, err := NewClient(Nile, GprcNile)
	if err != nil {
		fmt.Println(err)
		return
	}

	// Create Raw Transaction
	toAddr := "TLoKVZMCPcAqmAVNV8ZjRetRMNrYnW6h9r"
	amounTRX := float64(1000)
	amountSun := ToSun(amounTRX)
	rawTx, err := cli.CreateSendTrx(fromAddr, toAddr, int64(amountSun))
	if err != nil {
		fmt.Println(err)
		return
	}

	// Estimate Bandwidth
	bandwidth := cli.EstimateBandwithRawTx(rawTx, 1)
	fmt.Println("......bandwidth rawTx......", bandwidth)

	// Sign Transaction
	signedTx, err := SignRawTx(privKey, rawTx)
	if err != nil {
		fmt.Println(err)
		return
	}

	b, err := json.MarshalIndent(signedTx, "", " ")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("......signedTx......", string(b))

	// Estimate Bandwidth
	bandwidth = cli.EstimateBandwithSignedTx(signedTx)
	fmt.Println("......bandwidth signedTx......", bandwidth)

	// Broadcast Transaction
	tx, err := cli.Broadcast(signedTx)
	if err != nil {
		fmt.Println(err)
		return

	}

	b, err = json.MarshalIndent(tx, "", " ")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("......tx......", string(b))

	// ONLY GRPC
	// txHash := BytesToHexString([]byte(signedTx.TxID))
	// fmt.Println("...txHash...", txHash)
}

func TestTriggerSmartContract(t *testing.T) {
	cli, err := NewClient(Nile, GprcNile)
	if err != nil {
		fmt.Println(err)
		return
	}

	// params
	value := int64(0)
	tokenValue := big.NewInt(10)

	toAddr := "TLoKVZMCPcAqmAVNV8ZjRetRMNrYnW6h9r"
	to, err := Base58ToAddress(toAddr)
	if err != nil {
		fmt.Println(err)
		return
	}

	data, err := eth.Pack(abiUSDT, "transfer", common.HexToAddress(to.Hex()), tokenValue)
	if err != nil {
		fmt.Println(err)
		return
	}
	dataHex := common.Bytes2Hex(data)
	fmt.Println("...data...", dataHex)

	// gasPrice
	gasPriceHex := cli.GasPrice()
	gasPriceSun := eth.ToBig256(gasPriceHex).Int64()
	gasPriceTrx := FromSun(float64(gasPriceSun))
	fmt.Println("......gasPrice......", gasPriceHex, gasPriceSun, gasPriceTrx)

	// gasUsed engergy
	gasUsedHex := cli.EstimateGas(fromAddr, contractUSDT, dataHex, 0)
	gasUsedSun := eth.ToBig256(gasUsedHex).Int64()
	gasUsedTrx := FromSun(float64(gasUsedSun))
	fmt.Println("......gasUsed......", gasUsedHex, gasUsedSun, gasUsedTrx)

	// fee_limit = energy_required x energy unit price
	gasLimitSun := gasUsedSun * gasPriceSun
	fmt.Println("......gasLimit......", gasLimitSun, FromSun(float64(gasLimitSun)))

	// Create Raw Transaction
	rawTx, err := cli.CreateTriggerSmartContract(fromAddr, contractUSDT, dataHex, gasLimitSun, value, int64(0), int64(0))
	if err != nil {
		fmt.Println(err)
		return
	}

	// Estimate Bandwidth
	bandwidth := cli.EstimateBandwithRawTx(rawTx, 1)
	fmt.Println("......bandwidth rawTx......", bandwidth)

	// Sign Transaction
	signedTx, err := SignRawTx(privKey, rawTx)
	if err != nil {
		fmt.Println(err)
		return
	}

	b, err := json.MarshalIndent(signedTx, "", " ")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("......signedTx......", string(b))

	// Estimate Bandwidth
	bandwidth = cli.EstimateBandwithSignedTx(signedTx)
	fmt.Println("......bandwidth signedTx......", bandwidth)

	// Broadcast Transaction
	tx, err := cli.Broadcast(signedTx)
	if err != nil {
		fmt.Println(err)
		return
	}

	b, err = json.MarshalIndent(tx, "", " ")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("......tx......", string(b))

	// ONLY GRPC
	// txHash := BytesToHexString([]byte(signedTx.TxID))
	// fmt.Println("...txHash...", txHash)
}

func TestSendTrxGrpcFlow(t *testing.T) {
	//t.Skip()
	toAddr := "TLoKVZMCPcAqmAVNV8ZjRetRMNrYnW6h9r"

	privateKeyBytes, _ := hex.DecodeString(privKey)

	c := tronGRPC.NewGrpcClient("grpc.nile.trongrid.io:50051")
	err := c.Start(grpc.WithInsecure())
	require.Nil(t, err)

	valueTrx := float64(10)
	valueSun := ToSun(valueTrx)
	tx, err := c.Transfer(fromAddr, toAddr, int64(valueSun))
	require.Nil(t, err)

	rawData, err := proto.Marshal(tx.Transaction.GetRawData())
	require.Nil(t, err)
	h256h := sha256.New()
	h256h.Write(rawData)
	hash := h256h.Sum(nil)

	// btcec.PrivKeyFromBytes only returns a secret key and public key
	sk, _ := btcec.PrivKeyFromBytes(privateKeyBytes)

	signature, err := crypto.Sign(hash, sk.ToECDSA())
	require.Nil(t, err)
	tx.Transaction.Signature = append(tx.Transaction.Signature, signature)

	result, err := c.Broadcast(tx.Transaction)
	require.Nil(t, err)
	require.NotNil(t, result)

	b, _ := json.MarshalIndent(tx, "", " ")
	fmt.Println("xxxxxxxxxxxx", string(b))
}

func TestSendTrxGrpc(t *testing.T) {
	// connect
	cli, err := NewClient(Nile, GprcNile)
	require.Nil(t, err)

	// data
	from := "TNuCHV47zLuQWxSTJbU5Mdb8tiyCTRtm3e"
	to := "TLoKVZMCPcAqmAVNV8ZjRetRMNrYnW6h9r"
	priv := "47de15108b35169c4aff4826d5c413fe117e361a900325f6d3df1f0e04cbd706"
	valueTrx := float64(10)
	valueSun := ToSun(valueTrx)

	// raw
	rawTx, err := cli.CreateSendTrxGRPC(from, to, int64(valueSun))
	require.Nil(t, err)

	// signature
	signedTx, err := SignTxGrpc(priv, rawTx)
	require.Nil(t, err)

	b, _ := json.MarshalIndent(signedTx, "", " ")
	fmt.Println("xxxxxxxxxxxx", string(b))

	// send
	result, err := cli.BroadcastGRPC(signedTx)
	require.Nil(t, err)
	require.NotNil(t, result)

	// ONLY GRPC
	txHash := BytesToHexString(signedTx.Txid)
	fmt.Println("...txHash...", txHash)
}

func TestTRC20GRPC(t *testing.T) {
	// connect
	cli, err := NewClient(Nile, GprcNile)
	require.Nil(t, err)

	// data
	from := "TNuCHV47zLuQWxSTJbU5Mdb8tiyCTRtm3e"
	to := "TLoKVZMCPcAqmAVNV8ZjRetRMNrYnW6h9r"
	priv := "47de15108b35169c4aff4826d5c413fe117e361a900325f6d3df1f0e04cbd706"

	tokenTrx := float64(10)
	tokenSun := ToSun(tokenTrx)
	feeLimitTrx := float64(1000)
	feeLimitSun := ToSun(feeLimitTrx)

	// raw
	rawTx, err := cli.CreateSendTokenGRPC(from, to, contractUSDT, int64(tokenSun), int64(feeLimitSun))
	require.Nil(t, err)

	// signature
	signedTx, err := SignTxGrpc(priv, rawTx)
	require.Nil(t, err)

	// send
	result, err := cli.BroadcastGRPC(signedTx)
	require.Nil(t, err)
	require.NotNil(t, result)

	b, _ := json.MarshalIndent(signedTx, "", " ")
	fmt.Println("xxxxxxxxxxxx", string(b))

	// ONLY GRPC
	txHash := BytesToHexString(signedTx.Txid)
	fmt.Println("...txHash...", txHash)
}

func TestTriggerGRPC(t *testing.T) {
	// connect
	cli, err := NewClient(Nile, GprcNile)
	require.Nil(t, err)

	// data
	from := "TNuCHV47zLuQWxSTJbU5Mdb8tiyCTRtm3e"
	to := "TLoKVZMCPcAqmAVNV8ZjRetRMNrYnW6h9r"
	priv := "47de15108b35169c4aff4826d5c413fe117e361a900325f6d3df1f0e04cbd706"

	feeLimitTrx := float64(1000)
	feeLimitSun := ToSun(feeLimitTrx)

	valueTrx := float64(0)
	valueSun := ToSun(valueTrx)

	tokenId := ""

	tokenTrx := float64(5)
	tokenSun := ToSun(tokenTrx)
	tokenStr := ToSunString(tokenTrx)

	method := "transfer(address,uint256)"
	params := make([]map[string]interface{}, 0)
	params = append(params, map[string]interface{}{"address": to})
	params = append(params, map[string]interface{}{"uint256": tokenStr})

	// raw
	rawTx, err := cli.CreateTriggerSmartContractGRPC(from, contractUSDT, method, params, int64(feeLimitSun), int64(valueSun), tokenId, int64(tokenSun))
	require.Nil(t, err)

	// signature
	signedTx, err := SignTxGrpc(priv, rawTx)
	require.Nil(t, err)

	// send
	result, err := cli.BroadcastGRPC(signedTx)
	require.Nil(t, err)
	require.NotNil(t, result)

	b, _ := json.MarshalIndent(signedTx, "", " ")
	fmt.Println("xxxxxxxxxxxx", string(b))

	// ONLY GRPC
	txHash := BytesToHexString(signedTx.Txid)
	fmt.Println("...txHash...", txHash)
}

func TestGasNile(t *testing.T) {
	// connect
	cli, err := NewClient(Nile, GprcNile)
	require.Nil(t, err)

	// gasPrice
	gasPriceHex := cli.GasPrice()
	gasPriceSun := eth.ToBig256(gasPriceHex).Int64()
	gasPriceTrx := FromSun(float64(gasPriceSun))
	fmt.Println("......gasPrice......", gasPriceHex, gasPriceSun, gasPriceTrx)

	// energyPrice
	energyPriceSun := cli.EnergyPrice()
	energyPriceTrx := FromSun(float64(energyPriceSun))
	fmt.Println("......energyPrice......", energyPriceSun, energyPriceTrx)

	// bandwithPrice
	bandwithPricePrice := cli.BandwidthPrice()
	fmt.Println("......bandwithPricePrice......", bandwithPricePrice)

	// byte code
	toAddr := "TLoKVZMCPcAqmAVNV8ZjRetRMNrYnW6h9r"
	to, _ := Base58ToAddress(toAddr)
	tokenValue := big.NewInt(10)
	data, _ := eth.Pack(abiUSDT, "transfer", common.HexToAddress(to.Hex()), tokenValue)
	dataHex := common.Bytes2Hex(data)
	fmt.Println("...data...", dataHex)

	// gasUsed engergy
	gasUsedHex := cli.EstimateGas(fromAddr, contractUSDT, dataHex, 0)
	gasUsedSun := eth.ToBig256(gasUsedHex).Int64()
	gasUsedTrx := FromSun(float64(gasUsedSun))
	fmt.Println("......gasUsed......", gasUsedHex, gasUsedSun, gasUsedTrx)

	// fee_limit = energy_required x energy unit price
	gasLimitSun := gasUsedSun * gasPriceSun
	fmt.Println("......gasLimit......", gasLimitSun, FromSun(float64(gasLimitSun)))
}

func TestGasMain(t *testing.T) {
	// connect
	cli, err := NewClient(Mainnet, GprcMain)
	require.Nil(t, err)

	// gasPrice
	gasPriceHex := cli.GasPrice()
	gasPriceSun := eth.ToBig256(gasPriceHex).Int64()
	gasPriceTrx := FromSun(float64(gasPriceSun))
	fmt.Println("......gasPrice......", gasPriceHex, gasPriceSun, gasPriceTrx)

	// energyPrice
	energyPriceSun := cli.EnergyPrice()
	energyPriceTrx := FromSun(float64(energyPriceSun))
	fmt.Println("......energyPrice......", energyPriceSun, energyPriceTrx)

	// bandwithPrice
	bandwithPricePrice := cli.BandwidthPrice()
	fmt.Println("......bandwithPricePrice......", bandwithPricePrice)
}

/*
curl -X POST  https://nile.trongrid.io/wallet/triggerconstantcontract -d '{
    "owner_address": "TNuCHV47zLuQWxSTJbU5Mdb8tiyCTRtm3e",
    "contract_address": "TXLAQ63Xg1NAzckPwKHvzw7CSEmLMEqcdj",
    "function_selector": "transfer(address,uint256)",
    "parameter": "00000000000000000000000073e2af370078dd5d242da6785cc033395496e8fb000000000000000000000000000000000000000000000000000000000000000a",
    "visible": true
}'

curl -X POST  https://nile.trongrid.io/wallet/estimateenergy -d '{
    "owner_address": "TNuCHV47zLuQWxSTJbU5Mdb8tiyCTRtm3e",
    "contract_address": "TXLAQ63Xg1NAzckPwKHvzw7CSEmLMEqcdj",
    "function_selector": "transfer(address,uint256)",
    "parameter": "00000000000000000000000073e2af370078dd5d242da6785cc033395496e8fb000000000000000000000000000000000000000000000000000000000000000a",
    "visible": true
}'
*/

func TestEnergy(t *testing.T) {
	// connect
	cli, err := NewClient(Nile, GprcNile)
	require.Nil(t, err)

	// byte code
	toAddr := "TLoKVZMCPcAqmAVNV8ZjRetRMNrYnW6h9r"
	to, _ := Base58ToAddress(toAddr)
	tokenValue := big.NewInt(10)
	data, _ := eth.Pack(abiUSDT, "transfer", common.HexToAddress(to.Hex()), tokenValue)
	dataHex := common.Bytes2Hex(data)
	fmt.Println("...data...", dataHex)

	// energy
	energy_used, energy_penalty := cli.EstimateEnergyTrigger(fromAddr, contractUSDT, "transfer(address,uint256)", dataHex[8:])
	energy := energy_used - energy_penalty
	fmt.Println("...energy trigger...", energy)

	// energy
	energy = cli.EstimateEnergy(fromAddr, contractUSDT, "transfer(address,uint256)", dataHex[8:], "", 0)
	fmt.Println("...energy...", energy)
}

func TestStakeNile(t *testing.T) {
	// connect
	cli, err := NewClient(Nile, GprcNile)
	require.Nil(t, err)

	accResource, _ := cli.GetAccountResource("TLoKVZMCPcAqmAVNV8ZjRetRMNrYnW6h9r")
	b, _ := json.MarshalIndent(accResource, "", " ")
	fmt.Println(".....account resource....", string(b))

	bandwidth := StakeBandwidth(1.0, float64(accResource.TotalNetWeight), float64(accResource.TotalNetLimit))
	fmt.Println(".....bandwidth.....", bandwidth)

	energy := StakeEnergy(1.0, float64(accResource.TotalEnergyWeight), float64(accResource.TotalEnergyLimit))
	fmt.Println(".....energy.....", energy)

	bandwidth = cli.StakeBandwidth("TLoKVZMCPcAqmAVNV8ZjRetRMNrYnW6h9r", 1)
	fmt.Println(".....bandwidth.....", bandwidth)

	energy = cli.StakeEnergy("TLoKVZMCPcAqmAVNV8ZjRetRMNrYnW6h9r", 1)
	fmt.Println(".....energy.....", energy)
}

func TestStakeMain(t *testing.T) {
	// connect
	cli, err := NewClient(Mainnet, GprcMain)
	require.Nil(t, err)

	accResource, _ := cli.GetAccountResource("TZ4UXDV5ZhNW7fb2AMSbgfAEZ7hWsnYS2g")
	b, _ := json.MarshalIndent(accResource, "", " ")
	fmt.Println(".....account resource....", string(b))

	bandwidth := StakeBandwidth(1.0, float64(accResource.TotalNetWeight), float64(accResource.TotalNetLimit))
	fmt.Println(".....bandwidth.....", bandwidth)

	energy := StakeEnergy(1.0, float64(accResource.TotalEnergyWeight), float64(accResource.TotalEnergyLimit))
	fmt.Println(".....energy.....", energy)

	bandwidth = cli.StakeBandwidth("TLoKVZMCPcAqmAVNV8ZjRetRMNrYnW6h9r", 1)
	fmt.Println(".....bandwidth.....", bandwidth)

	energy = cli.StakeEnergy("TLoKVZMCPcAqmAVNV8ZjRetRMNrYnW6h9r", 1)
	fmt.Println(".....energy.....", energy)
}

// ---------------------------------------------------------------------//

func TestJsonRpcSend(t *testing.T) {
	cli, err := NewClient(Nile, GprcNile)
	if err != nil {
		fmt.Println(err)
		return
	}

	txHash, err := cli.Send(privKey, fromAddr, "TLoKVZMCPcAqmAVNV8ZjRetRMNrYnW6h9r", 0.5)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("...txHash..", txHash)
}

func TestJsonRpcTransfer(t *testing.T) {
	cli, err := NewClient(Nile, GprcNile)
	if err != nil {
		fmt.Println(err)
		return
	}

	decimal := 6
	txHash, err := cli.Transfer(privKey, "TLoKVZMCPcAqmAVNV8ZjRetRMNrYnW6h9r", contractUSDT, abiUSDT, decimal, 0, 900000)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("...txHash..", txHash)
}

func TestJsonRpcDeployContract(t *testing.T) {
	cli, err := NewClient(Nile, GprcNile)
	if err != nil {
		fmt.Println(err)
		return
	}

	// Check Balance
	balance := cli.GetBalance(fromAddr)
	// Default 30 TRX / 1 Smart Contract
	if eth.ToBig256(balance).Int64() < 30000000 {
		fmt.Println("insufficient funds")
		return
	}

	// TODO : estimate gas & feeLimit
	//  feeLimit must be >= 0 and <= 15000000000
	feeLimitTrx := float64(1) // => 1.000.000 sun ==> $0.1562 USD
	feeLimitSun := ToSun(feeLimitTrx)

	tx, err := cli.DeploySolidity(privKey, fromAddr, "GostocyVault", abiVault, bytecodeVault, int64(feeLimitSun), 0, 50, 1000000, 0, 0)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("......tx......", tx)
}
func TestJsonRpcApprove(t *testing.T) {
	cli, err := NewClient(Nile, GprcNile)
	if err != nil {
		fmt.Println(err)
		return
	}

	spender := "TLoKVZMCPcAqmAVNV8ZjRetRMNrYnW6h9r"
	decimal := 6
	gasLimit := int64(1000000)

	// MaxUint256 = new(big.Int).Sub(new(big.Int).Lsh(common.Big1, 256), common.Big1)
	// MaxInt256 = new(big.Int).Sub(new(big.Int).Lsh(common.Big1, 255), common.Big1)
	// MaxHash = HexToHash("0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff")
	txHash, err := cli.Approve(privKey, spender, contractUSDT, abiUSDT, decimal, gasLimit, math.MaxInt64)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("...approve..", txHash)

	allowance := cli.Allowance(fromAddr, spender, contractUSDT, abiUSDT)
	fmt.Println("...allowance...", allowance, eth.ToBig256(allowance).Int64())
}

func TestJsonRpcAllowance(t *testing.T) {
	cli, err := NewClient(Nile, GprcNile)
	if err != nil {
		fmt.Println(err)
		return
	}

	owner := "TPQSbeAZwpreWeBFU5A6PRUHpkbrzEWG6P"
	spender := "TWmQj4zENowm5wn6FPcsXPGoPfgNfv9MU8"
	contractUSDT = "TB3Fj87psi6zgunj4AkKZDRzbkCUmRkdKY"

	allowance := cli.Allowance(owner, spender, contractUSDT, abiUSDT)
	fmt.Println("...allowance...", eth.ToBig256(allowance))
}
func TestJsonRpcTransferFrom(t *testing.T) {
	cli, err := NewClient(Nile, GprcNile)
	if err != nil {
		fmt.Println(err)
		return
	}

	spender := "TLoKVZMCPcAqmAVNV8ZjRetRMNrYnW6h9r"
	receipt := "TH4bVxm6SZxhGoL2civd2rhT8vECumeojP"
	decimal := 6
	gasLimit := int64(1000000)

	txHash, err := cli.TransferFrom(privKey, spender, receipt, contractUSDT, abiUSDT, decimal, gasLimit, 900000)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("...transferfrom..", txHash)
}

func TestGetTransactionInfo(t *testing.T) {
	cli, err := NewClient(Nile, GprcNile)
	if err != nil {
		fmt.Println(err)
		return
	}

	var txHash = "9cbc00ae08ff74da8b7b4ccb44a47e8142fe36478fe68766f00b2915b03c132b"
	receipt, err := cli.GetTransactionInfo(txHash)
	assert.Nil(t, err)

	fmt.Printf("ID: %s\n", receipt.ID)
	fmt.Printf("Fee: %d\n", receipt.Fee)
	fmt.Printf("Block Number: %d\n", receipt.BlockNumber)
	fmt.Printf("Block Timestamp: %d\n", receipt.BlockTimeStamp)
	fmt.Printf("Contract Result: %v\n", receipt.ContractResult)
	fmt.Printf("Contract Address: %s\n", receipt.ContractAddress)
	fmt.Printf("Result: %d\n", receipt.Result)

	// Logging ResourceReceipt details
	fmt.Println("Receipt:")
	fmt.Printf("  Energy Usage: %d\n", receipt.Receipt.EnergyUsage)
	fmt.Printf("  Energy Fee: %d\n", receipt.Receipt.EnergyFee)
	fmt.Printf("  Origin Energy Usage: %d\n", receipt.Receipt.OriginEnergyUsage)
	fmt.Printf("  Energy Usage Total: %d\n", receipt.Receipt.EnergyUsageTotal)
	fmt.Printf("  Net Usage: %d\n", receipt.Receipt.NetUsage)
	fmt.Printf("  Net Fee: %d\n", receipt.Receipt.NetFee)
	fmt.Printf("  Result: %s\n", receipt.Receipt.Result)
	fmt.Printf("  Energy Penalty Total: %d\n", receipt.Receipt.EnergyPenaltyTotal)
}
