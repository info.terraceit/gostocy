package xlm

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"math"
	"net/http"
	"strconv"

	"github.com/stellar/go/amount"
	"github.com/stellar/go/clients/horizonclient"
	"github.com/stellar/go/hash"
	"github.com/stellar/go/keypair"
	networkStellar "github.com/stellar/go/network"
	"github.com/stellar/go/protocols/horizon"
	"github.com/stellar/go/strkey"
	"github.com/stellar/go/txnbuild"
	"github.com/stellar/go/xdr"
	"golang.org/x/crypto/ed25519"
)

var ORDER_ASC = `asc`
var ORDER_DESC = `desc`

var ASSET_TYPE_NATIVE = `native`
var ASSET_TYPE_CREDIT_ALPHANUM4 = `credit_alphanum4`
var ASSET_TYPE_CREDIT_ALPHANUM12 = `credit_alphanum12`

var MEMO_NONE = int32(0)
var MEMO_TEXT = int32(1)
var MEMO_ID = int32(2)
var MEMO_HASH = int32(3)
var MEMO_RETURN = int32(4)

func HorizonNetwork(net string) (network *horizonclient.Client) {
	switch net {
	case "public", "mainnet":
		network = horizonclient.DefaultPublicNetClient
	case "test", "testnet", "simnet":
		network = horizonclient.DefaultTestNetClient
	}
	return
}

func HorizonPassPhrase(net string) (pass string) {
	switch net {
	case "public", "mainnet":
		pass = networkStellar.PublicNetworkPassphrase
	case "test", "testnet", "simnet":
		pass = networkStellar.TestNetworkPassphrase
	}
	return
}

//---------------------------------------------------------------------//

func AmountStringFromInt64(v int64) string {
	return amount.StringFromInt64(v)
}

func ToLumens(v string) float64 {
	stroops, err := amount.Parse(v)
	if err != nil {
		return 0
	}

	return float64(stroops) / math.Pow10(7)
}

func ToStroops(lumen float64) float64 {
	return lumen * math.Pow10(7)
}

//---------------------------------------------------------------------//

func VerifyAmount(v string) bool {
	lumens := ToLumens(v)
	if lumens == 0 {
		return false
	}

	return true
}

func VerifyAddress(addr string) bool {
	// prefix : G
	_, err := strkey.Decode(strkey.VersionByteAccountID, addr)
	if err != nil {
		//fmt.Println("Error VerifyAddress", err)
		return false
	}

	return true
}

func VerifySeed(seed string) bool {
	// prefix : S
	_, err := strkey.Decode(strkey.VersionByteSeed, seed)
	if err != nil {
		//fmt.Println("Error VerifySeed", err)
		return false
	}

	return true
}

func VerifyHashTx(tx string) bool {
	// prefix : T
	_, err := strkey.Decode(strkey.VersionByteHashTx, tx)
	if err != nil {
		//fmt.Println("Error VerifyHashTx", err)
		return false
	}

	return true
}

func VerifyHashX(hx string) bool {
	// prefix : X
	_, err := strkey.Decode(strkey.VersionByteHashX, hx)
	if err != nil {
		//fmt.Println("Error VerifyHashX", err)
		return false
	}

	return true
}

//---------------------------------------------------------------------//

type KeyPair struct {
	Seed    string
	Address string
}

func KeyPairRandom() (*KeyPair, error) {
	full, err := keypair.Random()
	if err != nil {
		//fmt.Println("Error KeyPairRandom", err)
	}

	//fmt.Println("KeyPairRandom", full.Seed(), full.Address())
	kp := &KeyPair{Seed: full.Seed(), Address: full.Address()}

	return kp, err
}

func KeyPairFromPassphrase(passphrase string) (*KeyPair, error) {
	rawSeed := hash.Hash([]byte(passphrase))
	full, err := keypair.FromRawSeed(rawSeed)
	if err != nil {
		//fmt.Println("Error KeyPairFromPassphrase", err)
	}

	//fmt.Println("KeyPairFromPassphrase", full.Seed(), full.Address())
	kp := &KeyPair{Seed: full.Seed(), Address: full.Address()}

	return kp, err
}

func KeySignSeed(seed string, input []byte) ([]byte, error) {
	rawSeed, err := strkey.Decode(strkey.VersionByteSeed, seed)
	if err != nil {
		return []byte{}, err
	}

	reader := bytes.NewReader(rawSeed)
	_, priv, err := ed25519.GenerateKey(reader)
	if err != nil {
		return []byte{}, err
	}

	return xdr.Signature(ed25519.Sign(priv, input)[:]), nil
}

//---------------------------------------------------------------------//

type Assest struct {
	Asset_Type   string
	Asset_Code   string
	Asset_issuer string
	asset        *xdr.Asset
}

func NewAssest(asset_type, asset_code, asset_issuer string) (ass *Assest, err error) {
	if asset_type != ASSET_TYPE_NATIVE && asset_type !=
		ASSET_TYPE_CREDIT_ALPHANUM4 && asset_type !=
		ASSET_TYPE_CREDIT_ALPHANUM12 {

		err = errors.New("Error asset_type")
		return
	}

	var accountID xdr.AccountId
	err = accountID.SetAddress(asset_issuer)
	if err != nil {
		return
	}

	var asset xdr.Asset
	err = asset.SetCredit(asset_code, accountID)
	if err != nil {
		return
	}

	switch asset_type {
	case ASSET_TYPE_NATIVE:
		asset.Type = xdr.AssetTypeAssetTypeNative
	case ASSET_TYPE_CREDIT_ALPHANUM4:
		asset.Type = xdr.AssetTypeAssetTypeCreditAlphanum4
	case ASSET_TYPE_CREDIT_ALPHANUM12:
		asset.Type = xdr.AssetTypeAssetTypeCreditAlphanum12
	}

	ass = &Assest{Asset_Type: asset_type, Asset_Code: asset_code, Asset_issuer: asset_issuer}

	return
}

func NewNative() (ass *Assest, err error) {
	err = ass.asset.SetNative()
	return
}

//---------------------------------------------------------------------//

type MeMo struct {
	Type    int32
	Id      int64
	Text    string
	Hash    string
	RetHash string
}

func NewMemoText(aType int32, value string) (*MeMo, error) {
	memo, err := xdr.NewMemo(xdr.MemoType(aType), value)
	if err != nil {
		return &MeMo{}, err
	}
	mType := int32(memo.Type)
	mText := *memo.Text

	return &MeMo{Type: mType, Text: mText}, nil
}

func NewMemoID(aType int32, value int64) (*MeMo, error) {
	memo, err := xdr.NewMemo(xdr.MemoType(aType), value)
	if err != nil {
		return &MeMo{}, err
	}
	mType := int32(memo.Type)
	mId := int64(*memo.Id)

	return &MeMo{Type: mType, Id: mId}, nil
}

func NewMemoHash(aType int32, value string) (*MeMo, error) {
	memo, err := xdr.NewMemo(xdr.MemoType(aType), value)
	if err != nil {
		return &MeMo{}, err
	}
	mType := int32(memo.Type)
	mHash, _ := json.Marshal(*memo.Hash)

	return &MeMo{Type: mType, Hash: string(mHash)}, nil
}

func NewMemoReturn(aType int32, value string) (*MeMo, error) {
	memo, err := xdr.NewMemo(xdr.MemoType(aType), value)
	if err != nil {
		return &MeMo{}, err
	}
	mType := int32(memo.Type)
	mRetHash, _ := json.Marshal(*memo.RetHash)

	return &MeMo{Type: mType, RetHash: string(mRetHash)}, nil
}

//---------------------------------------------------------------------//

func SendNative(net, fromSeed, toAddress, lumens string) (txID string) {
	if !VerifySeed(fromSeed) {
		return
	}

	if !VerifyAddress(toAddress) {
		return
	}

	if !VerifyAmount(lumens) {
		return
	}

	// Load the source account
	sourceKP := keypair.MustParseFull(fromSeed)
	sourceAccountRequest := horizonclient.AccountRequest{AccountID: sourceKP.Address()}
	sourceAccount, err := HorizonNetwork(net).AccountDetail(sourceAccountRequest)
	if err != nil {
		return
	}

	// Build transaction
	tx, err := txnbuild.NewTransaction(
		txnbuild.TransactionParams{
			SourceAccount:        &sourceAccount,
			IncrementSequenceNum: true,
			BaseFee:              txnbuild.MinBaseFee,
			Preconditions: txnbuild.Preconditions{
				TimeBounds: txnbuild.NewInfiniteTimeout(), // Use a real timeout in production!
			},
			Operations: []txnbuild.Operation{
				&txnbuild.Payment{
					Destination: toAddress,
					Amount:      lumens,
					Asset:       txnbuild.NativeAsset{},
				},
			},
		},
	)
	if err != nil {
		//fmt.Println("Error TxBuilder", err)
		return
	}

	// Sign the transaction to prove you are actually the person sending it.
	tx, err = tx.Sign(HorizonPassPhrase(net), sourceKP)
	if err != nil {
		return
	}

	// And finally, send it off to Stellar!
	TxSubmit, err := HorizonNetwork(net).SubmitTransaction(tx)
	if err != nil {
		return
	}
	txID = TxSubmit.Hash

	return txID
}

func SendCredit(net, fromSeed, toAddress, code, issuer, amount string) (txID string) {
	if !VerifySeed(fromSeed) {
		return
	}

	if !VerifyAddress(toAddress) {
		return
	}

	if !VerifyAmount(amount) {
		return
	}

	// Load the source account
	sourceKP := keypair.MustParseFull(fromSeed)
	sourceAccountRequest := horizonclient.AccountRequest{AccountID: sourceKP.Address()}
	sourceAccount, err := HorizonNetwork(net).AccountDetail(sourceAccountRequest)
	if err != nil {
		return
	}

	// Build transaction
	tx, err := txnbuild.NewTransaction(
		txnbuild.TransactionParams{
			SourceAccount:        &sourceAccount,
			IncrementSequenceNum: true,
			BaseFee:              txnbuild.MinBaseFee,
			Preconditions: txnbuild.Preconditions{
				TimeBounds: txnbuild.NewInfiniteTimeout(), // Use a real timeout in production!
			},
			Operations: []txnbuild.Operation{
				&txnbuild.Payment{
					Destination: toAddress,
					Amount:      amount,
					Asset:       txnbuild.CreditAsset{Code: code, Issuer: issuer},
				},
			},
		},
	)
	if err != nil {
		//fmt.Println("Error TxBuilder", err)
		return
	}

	// Sign the transaction to prove you are actually the person sending it.
	tx, err = tx.Sign(HorizonPassPhrase(net), sourceKP)
	if err != nil {
		return
	}

	// And finally, send it off to Stellar!
	TxSubmit, err := HorizonNetwork(net).SubmitTransaction(tx)
	if err != nil {
		return
	}
	txID = TxSubmit.Hash

	return txID
}

//---------------------------------------------------------------------//
/*
func TxDecode(data string) (tx xdr.TransactionEnvelope) {
	rawr := strings.NewReader(data)
	b64r := base64.NewDecoder(base64.StdEncoding, rawr)
	xdr.Unmarshal(b64r, &tx)
	//	bytesRead, err := xdr.Unmarshal(b64r, &tx)
	//	if err != nil {
	//		fmt.Println("Error txDecode", err)
	//	}
	//	fmt.Printf("read %d bytes\n", bytesRead)

	//fmt.Printf("This tx has %d operations\n", len(tx.Tx.Operations))
	return
}

func TxAddFee(tx *xdr.Transaction, fee xdr.Uint32) {
	tx.Fee = fee
}

func TxAddMemo(tx *xdr.Transaction, memo xdr.Memo) {
	tx.Memo = memo
}

func TxAddTimeBound(tx *xdr.Transaction, timebound xdr.TimeBounds) {
	tx.Cond.TimeBounds = &timebound
}

func TxAddPaymentOp(tx *xdr.Transaction, to, amount string, asset xdr.Asset) {
	if !VerifyAddress(to) {
		return
	}

	var destination xdr.MuxedAccount
	err := destination.SetAddress(to)
	if err != nil {
		return
	}

	am, err := strconv.ParseInt(amount, 0, 64)
	if err != nil {
		return
	}
	lumens := xdr.Int64(am)

	op := xdr.PaymentOp{
		Destination: destination,
		Asset:       asset,
		Amount:      lumens,
	}

	body, err := xdr.NewOperationBody(xdr.OperationTypePayment, op)
	if err != nil {
		return
	}

	operation := xdr.Operation{Body: body}
	tx.Operations = append(tx.Operations, operation)
}

func TxAddCreateAccountOp(tx *xdr.Transaction, to, balance string) {
	if !VerifyAddress(to) {
		return
	}

	var destination xdr.AccountId
	err := destination.SetAddress(to)
	if err != nil {
		return
	}

	am, err := strconv.ParseInt(balance, 0, 64)
	if err != nil {
		return
	}
	lumens := xdr.Int64(am)

	op := xdr.CreateAccountOp{
		Destination:     destination,
		StartingBalance: lumens,
	}

	body, err := xdr.NewOperationBody(xdr.OperationTypeCreateAccount, op)
	if err != nil {
		return
	}

	operation := xdr.Operation{Body: body}
	tx.Operations = append(tx.Operations, operation)
}

func TxAddPathPaymentOp(tx *xdr.Transaction) {}

func TxAddManageBuyOfferOp(tx *xdr.Transaction, selling, buying xdr.Asset, amount xdr.Int64, price xdr.Price, offerID xdr.Int64) {
	op := xdr.ManageBuyOfferOp{
		Selling:   selling,
		Buying:    buying,
		BuyAmount: amount,
		Price:     price,
		OfferId:   offerID,
	}

	body, err := xdr.NewOperationBody(xdr.OperationTypeManageBuyOffer, op)
	if err != nil {
		return
	}

	operation := xdr.Operation{Body: body}
	tx.Operations = append(tx.Operations, operation)
}

func TxAddManageSellOfferOp(tx *xdr.Transaction, selling, buying xdr.Asset, amount xdr.Int64, price xdr.Price, offerID xdr.Int64) {
	op := xdr.ManageSellOfferOp{
		Selling: selling,
		Buying:  buying,
		Amount:  amount,
		Price:   price,
		OfferId: offerID,
	}

	body, err := xdr.NewOperationBody(xdr.OperationTypeManageSellOffer, op)
	if err != nil {
		return
	}

	operation := xdr.Operation{Body: body}
	tx.Operations = append(tx.Operations, operation)
}

func TxAddCreatePassiveOfferOp(tx *xdr.Transaction, selling, buying xdr.Asset, amount xdr.Int64, price xdr.Price) {
	op := xdr.CreatePassiveSellOfferOp{
		Selling: selling,
		Buying:  buying,
		Amount:  amount,
		Price:   price,
	}

	body, err := xdr.NewOperationBody(xdr.OperationTypeCreatePassiveSellOffer, op)
	if err != nil {
		return
	}

	operation := xdr.Operation{Body: body}
	tx.Operations = append(tx.Operations, operation)
}

func TxAddSetOptionsOp(tx *xdr.Transaction, op *xdr.SetOptionsOp) {
	body, err := xdr.NewOperationBody(xdr.OperationTypeSetOptions, op)
	if err != nil {
		return
	}

	operation := xdr.Operation{Body: body}
	tx.Operations = append(tx.Operations, operation)
}

func TxAddChangeTrustOp(tx *xdr.Transaction, asset_type, asset_code, asset_issuer, limit string) {
	if limit == "" {
		limit = string(txnbuild.MaxLimit)
	}

	lim, err := strconv.ParseInt(limit, 0, 64)
	if err != nil {
		return
	}
	tokens := xdr.Int64(lim)

	ass, errA := NewAssest(asset_type, asset_code, asset_issuer)
	if errA != nil {
		return
	}

	op := xdr.ChangeTrustOp{Line: ass.asset.ToChangeTrustAsset(), Limit: tokens}
	body, err := xdr.NewOperationBody(xdr.OperationTypeChangeTrust, op)
	if err != nil {
		return
	}

	operation := xdr.Operation{Body: body}
	tx.Operations = append(tx.Operations, operation)
}

func TxAddAllowTrustOp(tx *xdr.Transaction, trustor, asset_code string, asset_type xdr.AssetType, authorize bool) {
	if !VerifyAddress(trustor) {
		return
	}

	var destination xdr.AccountId
	err := destination.SetAddress(trustor)
	if err != nil {
		return
	}

	ass, errA := xdr.NewAllowTrustOpAsset(asset_type, asset_code)
	if errA != nil {
		return
	}

	op := xdr.AllowTrustOp{Trustor: destination, Asset: ass, Authorize: authorize}
	body, err := xdr.NewOperationBody(xdr.OperationTypeAllowTrust, op)
	if err != nil {
		return
	}

	operation := xdr.Operation{Body: body}
	tx.Operations = append(tx.Operations, operation)
}

func TxAddAccountMergeOp(tx *xdr.Transaction, to string) {
	if !VerifyAddress(to) {
		return
	}

	accountMergeBuilder := txnbuild.AccountMerge{Destination: to}
	tx.Operations = append(tx.Operations, accountMergeBuilder)
}

func TxAddInflationOp(tx *xdr.Transaction, to string, lumens xdr.Int64) {
	if !VerifyAddress(to) {
		return
	}

	var destination xdr.AccountId
	err := destination.SetAddress(to)
	if err != nil {
		return
	}

	op := xdr.InflationPayout{Destination: destination, Amount: lumens}
	body, err := xdr.NewOperationBody(xdr.OperationTypeInflation, op)
	if err != nil {
		return
	}

	operation := xdr.Operation{Body: body}
	tx.Operations = append(tx.Operations, operation)
}

func TxAddManageDataOp(tx *xdr.Transaction, key string, value []byte) {
	dataBuilder := build.SetData(key, value)
	tx.Operations = append(tx.Operations, dataBuilder.O)
}

//---------------------------------------------------------------------//

func TxEnvelopBuilder(tx xdr.Transaction) *xdr.TransactionEnvelope {
	txe := &xdr.TransactionEnvelope{
		Tx:         tx,
		Signatures: []xdr.DecoratedSignature{},
	}

	return txe
}

func TxAddSignature(seed string, tx *xdr.Transaction, txe *xdr.TransactionEnvelope) {
	if !VerifySeed(seed) {
		return
	}

	skp, err := keypair.Parse(seed)
	if err != nil {
		return
	}

	if skp.Address() != tx.SourceAccount.Address() {
		return
	}

	var txBytes bytes.Buffer
	_, err1 := xdr.Marshal(&txBytes, tx)
	if err1 != nil {
		return
	}

	txHash := hash.Hash(txBytes.Bytes())
	signature, err2 := skp.Sign(txHash[:])
	if err2 != nil {
		return
	}

	ds := xdr.DecoratedSignature{
		Hint:      skp.Hint(),
		Signature: xdr.Signature(signature[:]),
	}

	txe.Signatures = append(txe.Signatures, ds)
}

func TxEnvelopEncode(txe *xdr.TransactionEnvelope) (txeB64 string) {
	var txeBytes bytes.Buffer
	_, err := xdr.Marshal(&txeBytes, txe)
	if err != nil {
		return
	}
	txeB64 = base64.StdEncoding.EncodeToString(txeBytes.Bytes())
	//fmt.Printf("tx base64: %s", txeB64)

	return
}
*/

//---------------------------------------------------------------------//

func cursor_limit_order(cursor string, limit int, order string) string {
	curs := "?cursor=" + cursor
	lim := "&limit=" + strconv.Itoa(int(limit))
	ordr := "&order=" + order

	return curs + lim + ordr
}

func call(url string) ([]byte, bool) {
	//fmt.Println(".....url......", url)

	res, err := http.Get(url)
	if err != nil {
		fmt.Println(err)
		return nil, false
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		fmt.Println(err)
		return nil, false
	}

	return body, true
}

//---------------------------------------------------------------------//

func FriendBot(addr string) (result string) {
	url := "https://horizon-testnet.stellar.org/friendbot?addr=" + addr
	body, ok := call(url)
	if !ok {
		return
	}
	//json.Unmarshal(body, &result)
	result = string(body)

	////fmt.Println(".......FriendBot........")
	////fmt.Println(string(body))

	return
}

func AccountDetails(net, id string) (result horizon.Account) {
	url := HorizonNetwork(net).HorizonURL + "/accounts/" + id
	body, ok := call(url)
	if !ok {
		return
	}
	json.Unmarshal(body, &result)

	////fmt.Println(".......AccountDetails........")
	////fmt.Println(string(body))
	return
}

func GetBalance(net, id, asset_type, asset_code string) (balance float64) {
	info := AccountDetails(net, id)
	for _, v := range info.Balances {
		if asset_type == v.Type && asset_code == v.Code {
			balance, _ = strconv.ParseFloat(v.Balance, 64)
			break
		}
	}

	//fmt.Println(".........GetBalance........", balance)
	return
}

func AssetDetails(net, code, issuer string, cursor string, limit int, order string) (result string) {
	asset_code := "&asset_code=" + code
	asset_issuer := "&asset_issuer=" + issuer
	url := HorizonNetwork(net).HorizonURL + "/assets" + cursor_limit_order(cursor, limit, order) + asset_code + asset_issuer
	body, ok := call(url)
	if !ok {
		return
	}
	//json.Unmarshal(body, &result)
	result = string(body)

	//fmt.Println(".......AssetCodeIssuer........")
	//fmt.Println(string(body))

	return
}

//---------------------------------------------------------------------//

func LedgerAll(net string, cursor string, limit int, order string) (result string) {
	url := HorizonNetwork(net).HorizonURL + "/ledgers" + cursor_limit_order(cursor, limit, order)

	body, ok := call(url)
	if !ok {
		return
	}
	//json.Unmarshal(body, &result)
	result = string(body)

	////fmt.Println(".......LedgerAll........")
	////fmt.Println(string(body))

	return
}

func LedgerByID(net, id string) (result string) {
	url := HorizonNetwork(net).HorizonURL + "/ledgers/" + id
	body, ok := call(url)
	if !ok {
		return
	}
	//json.Unmarshal(body, &result)
	result = string(body)

	////fmt.Println(".......LedgerID........")
	////fmt.Println(string(body))

	return
}

func GetBlockCount(net string) float64 {
	url := HorizonNetwork(net).HorizonURL + "/ledgers" + cursor_limit_order("", 1, ORDER_DESC)
	var result map[string]interface{}
	body, ok := call(url)
	if !ok {
		return 0
	}
	json.Unmarshal(body, &result)

	block_embeded := result["_embedded"].(map[string]interface{})
	block_record, _ := json.Marshal(block_embeded["records"])
	var recordsBlock []map[string]interface{}
	json.Unmarshal(block_record, &recordsBlock)

	return recordsBlock[0]["sequence"].(float64)
}

//---------------------------------------------------------------------//

func OfferForAccount(net, id string, cursor string, limit int, order string) (result string) { //(result horizon.OffersPage) {
	url := HorizonNetwork(net).HorizonURL + "/accounts/" + id + "/offers" + cursor_limit_order(cursor, limit, order)
	body, ok := call(url)
	if !ok {
		return
	}
	//json.Unmarshal(body, &result)
	result = string(body)

	//fmt.Println(".......OfferForAccount........")
	//fmt.Println(string(body))

	return
}

func OperationsAll(net string, cursor string, limit int, order string) (result string) { //(result map[string]interface{}) {
	url := HorizonNetwork(net).HorizonURL + "/operations" + cursor_limit_order(cursor, limit, order)
	body, ok := call(url)
	if !ok {
		return
	}
	//json.Unmarshal(body, &result)
	result = string(body)

	//fmt.Println(".......OperationsAll........")
	//fmt.Println(string(body))

	return
}

func OperationsByID(net, id string) (result string) { //(result map[string]interface{}) {
	url := HorizonNetwork(net).HorizonURL + "/operations/" + id
	body, ok := call(url)
	if !ok {
		return
	}
	//json.Unmarshal(body, &result)
	result = string(body)

	//fmt.Println(".......OperationsByID........")
	//fmt.Println(string(body))

	return
}

func OperationsForAccount(net, id string, cursor string, limit int, order string) (result string) { //(result map[string]interface{}) {
	url := HorizonNetwork(net).HorizonURL + "/accounts/" + id + "/operations" + cursor_limit_order(cursor, limit, order)
	body, ok := call(url)
	if !ok {
		return
	}
	//json.Unmarshal(body, &result)
	result = string(body)

	//fmt.Println(".......OperationsForAccount........")
	//fmt.Println(string(body))

	return
}

func OperationsForLedger(net, id string, cursor string, limit int, order string) (result string) { //(result map[string]interface{}) {
	url := HorizonNetwork(net).HorizonURL + "/ledgers/" + id + "/operations" + cursor_limit_order(cursor, limit, order)
	body, ok := call(url)
	if !ok {
		return
	}
	//json.Unmarshal(body, &result)
	result = string(body)

	//fmt.Println(".......OperationsForLedger........")
	//fmt.Println(string(body))

	return
}

func OperationsForTx(net, txHash string, cursor string, limit int, order string) (result string) { //(result map[string]interface{}) {
	url := HorizonNetwork(net).HorizonURL + "/transactions/" + txHash + "/operations" + cursor_limit_order(cursor, limit, order)
	body, ok := call(url)
	if !ok {
		return
	}
	//json.Unmarshal(body, &result)
	result = string(body)

	//fmt.Println(".......OperationsForTx........")
	//fmt.Println(string(body))

	return
}

//---------------------------------------------------------------------//

func OrderBookDetails(net, selling_asset_type, selling_asset_code, selling_asset_issuer,
	buying_asset_type, buying_asset_code, buying_asset_issuer, limit string) (result string) { //(result horizon.OrderBookSummary) {

	selling_asset_type = "?selling_asset_type=" + selling_asset_type
	selling_asset_code = "&selling_asset_code=" + selling_asset_code
	selling_asset_issuer = "&selling_asset_issuer=" + selling_asset_issuer

	buying_asset_type = "&buying_asset_type=" + buying_asset_type
	buying_asset_code = "&buying_asset_code=" + buying_asset_code
	buying_asset_issuer = "&buying_asset_issuer=" + buying_asset_issuer

	limit = "&limit=" + limit

	params := selling_asset_type + selling_asset_code + selling_asset_issuer + buying_asset_type + buying_asset_code + buying_asset_issuer + limit

	url := HorizonNetwork(net).HorizonURL + "/order_book" + params
	body, ok := call(url)
	if !ok {
		return
	}
	//json.Unmarshal(body, &result)
	result = string(body)

	//fmt.Println(".......OrderBookDetails........")
	//fmt.Println(string(body))

	return
}

func FindPaymentPath(net, destination_account, destination_asset_type, destination_asset_code, destination_asset_issuer, destination_amount,
	source_account string) (result string) { //(result map[string]interface{}) {

	destination_account = "?destination_account=" + destination_account
	destination_asset_type = "&destination_asset_type=" + destination_asset_type
	destination_asset_code = "&destination_asset_code=" + destination_asset_code
	destination_asset_issuer = "&destination_asset_issuer=" + destination_asset_issuer
	destination_amount = "&destination_amount=" + destination_amount
	source_account = "&destination_amount=" + destination_amount

	params := destination_account + destination_asset_type + destination_asset_code + destination_asset_issuer + destination_amount + source_account

	url := HorizonNetwork(net).HorizonURL + "/paths" + params
	body, ok := call(url)
	if !ok {
		return
	}
	//json.Unmarshal(body, &result)
	result = string(body)

	//fmt.Println(".......OrderBookDetails........")
	//fmt.Println(string(body))

	return
}

func TradeAggregations(net, base_asset_type, base_asset_code, base_asset_issuer,
	counter_asset_type, counter_asset_code, counter_asset_issuer,
	order string,
	limit, start_time, end_time, resolution int64) (result string) { //(result map[string]interface{}) {

	base_asset_type = "?base_asset_type=" + base_asset_type
	base_asset_code = "&base_asset_code=" + base_asset_code
	base_asset_issuer = "&base_asset_issuer=" + base_asset_issuer

	counter_asset_type = "&counter_asset_type=" + counter_asset_type
	counter_asset_code = "&counter_asset_code=" + counter_asset_code
	counter_asset_issuer = "&counter_asset_issuer=" + counter_asset_issuer
	order = "&order=" + order

	limitStr := "&limit=" + strconv.FormatInt(limit, 10)
	start_timeStr := "&start_time=" + strconv.FormatInt(start_time, 10)
	end_timeStr := "&end_time=" + strconv.FormatInt(end_time, 10)
	resolutionStr := "&resolution=" + strconv.FormatInt(resolution, 10)

	params := base_asset_type + base_asset_code + base_asset_issuer +
		counter_asset_type + counter_asset_code + counter_asset_issuer +
		order + limitStr + start_timeStr + end_timeStr + resolutionStr

	url := HorizonNetwork(net).HorizonURL + "/trade_aggregations" + params
	body, ok := call(url)
	if !ok {
		return
	}
	//json.Unmarshal(body, &result)
	result = string(body)

	//fmt.Println(".......OrderBookDetails........")
	//fmt.Println(string(body))

	return
}

func TradeAll(net, base_asset_type, base_asset_code, base_asset_issuer,
	counter_asset_type, counter_asset_code, counter_asset_issuer,
	offer_id, cursor, order, limit string) (result string) { //(result map[string]interface{}) {

	base_asset_type = "?base_asset_type=" + base_asset_type
	base_asset_code = "&base_asset_code=" + base_asset_code
	base_asset_issuer = "&base_asset_issuer" + base_asset_issuer
	counter_asset_type = "&counter_asset_type=" + counter_asset_type
	counter_asset_code = "&counter_asset_code=" + counter_asset_code
	counter_asset_issuer = "&counter_asset_issuer=" + counter_asset_issuer
	offer_id = "&offer_id=" + offer_id
	cursor = "&cursor=" + cursor
	order = "&order=" + order
	limit = "&limit=" + limit

	params := base_asset_type + base_asset_code + base_asset_issuer +
		counter_asset_type + counter_asset_code + counter_asset_issuer +
		offer_id + cursor + order + limit

	url := HorizonNetwork(net).HorizonURL + "/trades" + params

	body, ok := call(url)
	if !ok {
		return
	}
	//json.Unmarshal(body, &result)
	result = string(body)

	//fmt.Println(".......TradeAll........")
	//fmt.Println(string(body))

	return
}

func TradeForAccount(net, id string, cursor string, limit int, order string) (result string) { //(result map[string]interface{}) {
	url := HorizonNetwork(net).HorizonURL + "/accounts/" + id + "/trades" + cursor_limit_order(cursor, limit, order)
	body, ok := call(url)
	if !ok {
		return
	}
	//json.Unmarshal(body, &result)
	result = string(body)

	//fmt.Println(".......TradeForAccount........")
	//fmt.Println(string(body))

	return
}

//---------------------------------------------------------------------//

func PaymentAll(net string, cursor string, limit int, order string) (result string) { //(result map[string]interface{}) {
	url := HorizonNetwork(net).HorizonURL + "/payments" + cursor_limit_order(cursor, limit, order)
	body, ok := call(url)
	if !ok {
		return
	}
	//json.Unmarshal(body, &result)
	result = string(body)

	//fmt.Println(".......PaymentAll........")
	//fmt.Println(string(body))

	return
}

func PaymentForAccount(net, id string, cursor string, limit int, order string) (result string) { //(result map[string]interface{}) {
	url := HorizonNetwork(net).HorizonURL + "/accounts/" + id + "/payments" + cursor_limit_order(cursor, limit, order)
	body, ok := call(url)
	if !ok {
		return
	}
	//json.Unmarshal(body, &result)
	result = string(body)

	//	fmt.Println(".......PaymentForAccount........")
	//	fmt.Println(string(body))

	return
}

func PaymentForLedger(net, id string, cursor string, limit int, order string) (result string) { //(result map[string]interface{}) {
	url := HorizonNetwork(net).HorizonURL + "/ledgers/" + id + "/payments" + cursor_limit_order(cursor, limit, order)
	body, ok := call(url)
	if !ok {
		return
	}
	//json.Unmarshal(body, &result)
	result = string(body)

	//fmt.Println(".......PaymentForLedger........")
	//fmt.Println(string(body))

	return
}

func PaymentForTx(net, txHash string, cursor string, limit int, order string) (result string) { //(result map[string]interface{}) {
	url := HorizonNetwork(net).HorizonURL + "/transactions/" + txHash + "/payments" + cursor_limit_order(cursor, limit, order)
	body, ok := call(url)
	if !ok {
		return
	}
	//json.Unmarshal(body, &result)
	result = string(body)

	////fmt.Println(".......PaymentForTx........")
	////fmt.Println(string(body))

	return
}

//---------------------------------------------------------------------//

func TxAll(net string, cursor string, limit int, order string) (result string) { //(result map[string]interface{}) {
	url := HorizonNetwork(net).HorizonURL + "/transactions" + cursor_limit_order(cursor, limit, order)
	body, ok := call(url)
	if !ok {
		return
	}
	//json.Unmarshal(body, &result)
	result = string(body)

	//fmt.Println(".......TxAll........")
	//fmt.Println(string(body))

	return
}

func TxByHash(net, txHash string) (result string) { //(result horizon.Transaction) {
	url := HorizonNetwork(net).HorizonURL + "/transactions/" + txHash
	body, ok := call(url)
	if !ok {
		return
	}
	//json.Unmarshal(body, &result)
	result = string(body)

	//fmt.Println(".......TxByHash........")
	//fmt.Println(string(body))

	return
}

func TxForAccount(net, id string, cursor string, limit int, order string) (result string) { //(result map[string]interface{}) {
	url := HorizonNetwork(net).HorizonURL + "/accounts/" + id + "/transactions" + cursor_limit_order(cursor, limit, order)
	body, ok := call(url)
	if !ok {
		return
	}
	//json.Unmarshal(body, &result)
	result = string(body)

	//	fmt.Println(".......TxForAccount........")
	//	fmt.Println(string(body))

	return
}

func TxForLedger(net, id string, cursor string, limit int, order string) (result string) { //(result map[string]interface{}) {
	url := HorizonNetwork(net).HorizonURL + "/ledgers/" + id + "/transactions" + cursor_limit_order(cursor, limit, order)
	body, ok := call(url)
	if !ok {
		return
	}
	//json.Unmarshal(body, &result)
	result = string(body)

	////fmt.Println(".......TxForLedger........")
	////fmt.Println(string(body))
	return
}
