package xlm

import (
	"encoding/hex"
	"encoding/json"
	"sync"

	"gitlab.com/info.terraceit/gostocy/pkg/btc"
	"gitlab.com/info.terraceit/gostocy/pkg/cryptocy"

	"github.com/bartekn/go-bip39"
	"github.com/btcsuite/btcutil/hdkeychain"
)

type Account struct {
	Net  string `json:"net"`
	Salt string `json:"salt"`

	KeyPair *KeyPair
}

// m/44'/cointype'/0'/0/i
func NewAccount(net string, salt string, seed []byte) (acc *Account, err error) {
	acc = &Account{}
	acc.Net = net
	acc.Salt = salt
	acc.KeyPair = new(KeyPair)
	indexs := cryptocy.MultiHash(salt)

	// gen key
	acc.GenKey(seed, indexs...)

	return
}

func (acc *Account) GenKey(seed []byte, index ...uint32) error {
	//m/
	ext, err := hdkeychain.NewMaster(seed, btc.ChainConfig(acc.Net))
	if err != nil {
		return err
	}

	// m/44'
	purpose, err := ext.Child(44 + hdkeychain.HardenedKeyStart)
	if err != nil {
		return err
	}

	// m/44'/altcointype'
	coinType, err := purpose.Child(uint32(148) + hdkeychain.HardenedKeyStart)
	if err != nil {
		return err
	}

	// m/44'/altcointype'/0'
	acct0, err := coinType.Child(uint32(0) + hdkeychain.HardenedKeyStart)
	if err != nil {
		return err
	}

	// m/44'/altcointype'/0'/0
	// 0 = external accounts for receive addresses
	acct0External, err := acct0.Child(0)
	if err != nil {
		return err
	}

	var receive *hdkeychain.ExtendedKey
	for i, v := range index {
		if i == 0 {
			receive, err = acct0External.Child(uint32(v))
			if err != nil {
				//"Failed to create %v receive address: %s"
				return err
			}
			continue
		}
		receive, err = receive.Child(uint32(v))
		if err != nil {
			//"Failed to create %v receive address: %s"
			return err
		}
	}

	privk, err := receive.ECPrivKey()
	if err != nil {
		//"ECPrivKey() failed"
		return err
	}

	//	pubk, err := receive.ECPubKey()
	//	if err != nil {
	//		//"ECPubKey() failed"
	//		return
	//	}

	keyHex := hex.EncodeToString(privk.Serialize())

	keypair, err := KeyPairFromPassphrase(keyHex)
	if err != nil {
		return err
	}

	acc.KeyPair = keypair
	return nil
}

func (c *Account) GetAddress() string {
	return c.KeyPair.Address
}

func (c *Account) GetPrivKey() string {
	return c.KeyPair.Seed
}

type Wallet struct {
	mux *sync.Mutex

	Net      string `json:"net"`
	Password string `json:"password"`
	Mnemoric string `json:"mnemoric"`
	Seed     []byte `json:"seed"`
	//Accounts      []*Account `json:"accounts"`
	Accounts map[string]*Account `json:"accounts"`

	//
	Balance float64 `json:"balance"`
	ListTX  string  `json:"listTX"`
}

func NewWallet(net, mnemoric, password string) (w *Wallet, err error) {
	seed, err := bip39.NewSeedWithErrorChecking(mnemoric, password)
	if err != nil {
		return
	}

	w = new(Wallet)
	w.mux = &sync.Mutex{}
	w.Password = password
	w.Mnemoric = mnemoric
	w.Seed = seed
	w.Net = net
	//w.Accounts = make([]*Account, 0)
	w.Accounts = make(map[string]*Account)
	return
}

func NewWalletSeed(net string, seed []byte) (w *Wallet, err error) {
	w = new(Wallet)
	w.mux = &sync.Mutex{}
	w.Seed = seed
	w.Net = net
	//w.Accounts = make([]*Account, 0)
	w.Accounts = make(map[string]*Account)
	return
}

func (w *Wallet) String() string {
	wall := make(map[string]interface{})
	wall["net"] = w.Net
	wall["password"] = w.Password
	wall["mnemoric"] = w.Mnemoric
	wall["seed"] = hex.EncodeToString(w.Seed)
	wall["accounts"] = w.Accounts

	b, err := json.MarshalIndent(wall, "", " ")
	if err != nil {
		return ""
	}

	return string(b)
}

func (w *Wallet) CreateAccount(salt string) (acc *Account, err error) {
	w.mux.Lock()
	defer w.mux.Unlock()

	acc, err = NewAccount(w.Net, salt, w.Seed)
	if err != nil {
		return
	}
	//w.Accounts = append(w.Accounts, acc)
	w.Accounts[salt] = acc
	return
}

func (w *Wallet) ListAccounts() string {
	b, err := json.MarshalIndent(w.Accounts, "", " ")
	if err != nil {
		return ""
	}

	return string(b)
}

func (w *Wallet) ListTransactions() string {
	return w.ListTX
}

func (w *Wallet) GetAccount(salt string) (acc *Account) {
	w.mux.Lock()
	defer w.mux.Unlock()

	acc = w.Accounts[salt]
	return
}

func (w *Wallet) GetBalance() float64 {
	return w.Balance
}
