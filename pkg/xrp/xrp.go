package xrp

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/rubblelabs/ripple/crypto"
	"github.com/rubblelabs/ripple/data"
	"github.com/rubblelabs/ripple/websockets"
)

var Explorer_testnet string = "https://test.bithomp.com/explorer"
var Rpc_testnet string = "https://testnet.data.api.ripple.com/v2/"
var Faucet_testnet string = "https://faucet.altnet.rippletest.net/accounts"
var Ws_testnet string = "wss://s.altnet.rippletest.net:51233"

var Rpc_mainnet string = "https://data.api.ripple.com/v2/"
var Ws_mainnet string = "wss://s-east.ripple.com:443"

var LedgerSquence uint32

type KeyPair struct {
	Seed    string
	Address string
}

func NewKeyPairFromPassphrase(passphrase string) (kp *KeyPair, err error) {
	seed, err := crypto.GenerateFamilySeed(passphrase)
	if err != nil {
		return
	}

	key, err := crypto.NewECDSAKey(seed.Payload())
	if err != nil {
		return
	}

	addr, err := crypto.AccountId(key, nil)
	if err != nil {
		return
	}

	kp = new(KeyPair)
	kp.Seed = seed.String()
	kp.Address = addr.String()

	return
}

func NewKeyPairFromSeed(s string) (kp *KeyPair, err error) {
	seed, err := data.NewSeedFromAddress(s)
	if err != nil {
		return
	}

	sequenceZero := uint32(0)
	key, err := crypto.NewECDSAKey(seed.Bytes())
	if err != nil {
		return
	}

	addr, err := crypto.AccountId(key, &sequenceZero)
	if err != nil {
		return
	}

	kp = new(KeyPair)
	kp.Seed = seed.String()
	kp.Address = addr.String()

	return
}

type Client struct {
	r   *websockets.Remote
	URL string
}

func NewClient(url string) (cli *Client, err error) {
	r, err := websockets.NewRemote(url)
	if err != nil {
		fmt.Println("Error NewRemote", err)
		return
	}

	cli = &Client{r: r, URL: url}
	return
}

func (cli *Client) Tx(tx string) (result string) {
	txhash, err := data.NewHash256(tx)
	if err != nil {
		fmt.Println(err)
	}

	resp, err := cli.r.Tx(*txhash)
	if err != nil {
		fmt.Println(err)
	}

	return unmarshall(resp)
}

func (cli *Client) Submit(tx string) (result string) {
	b, err := json.Marshal(tx)
	if err != nil {
		fmt.Println(err)
	}

	var trans data.Transaction
	err = json.Unmarshal(b, &trans)
	if err != nil {
		fmt.Println(err)
	}

	resp, err := cli.r.Submit(trans)
	if err != nil {
		fmt.Println(err)
	}

	return unmarshall(resp)
}

func (cli *Client) SubmitBatch(txs string) (result string) {
	b, err := json.Marshal(txs)
	if err != nil {
		fmt.Println(err)
	}

	var trans []data.Transaction
	err = json.Unmarshal(b, &trans)
	if err != nil {
		fmt.Println(err)
	}

	resp, err := cli.r.SubmitBatch(trans)
	if err != nil {
		fmt.Println(err)
	}

	return unmarshall(resp)
}

func (cli *Client) LedgerBySequence(seq int32, trans bool) (result string) {
	resp, err := cli.r.Ledger(uint32(seq), trans)
	if err != nil {
		fmt.Println(err)
	}

	return unmarshall(resp)
}

func (cli *Client) AccountTx(account string, pageSize int, minLedger, maxLedger int64) (result string) {
	addr, err := data.NewAccountFromAddress(account)
	if err != nil {
		fmt.Println(err)
	}

	resp := cli.r.AccountTx(*addr, pageSize, minLedger, maxLedger)
	return unmarshall(<-resp)
}

func (cli *Client) AccountInfo(account string) (result string) {
	addr, err := data.NewAccountFromAddress(account)
	if err != nil {
		fmt.Println(err)
	}

	resp, err := cli.r.AccountInfo(*addr)
	if err != nil {
		fmt.Println(err)
	}

	return unmarshall(resp)
}

func (cli *Client) AccountLines(account, ledgerIndex string) (result string) {
	addr, err := data.NewAccountFromAddress(account)
	if err != nil {
		fmt.Println(err)
	}

	resp, err := cli.r.AccountLines(*addr, ledgerIndex)
	if err != nil {
		fmt.Println(err)
	}

	return unmarshall(resp)
}

func (cli *Client) AccountOffers(account, ledgerIndex string) (result string) {
	addr, err := data.NewAccountFromAddress(account)
	if err != nil {
		fmt.Println(err)
	}

	resp, err := cli.r.AccountOffers(*addr, ledgerIndex)
	if err != nil {
		fmt.Println(err)
	}

	return unmarshall(resp)
}

func (cli *Client) BookOffers(account, ledgerIndex, pays, gets string) (result string) {
	addr, err := data.NewAccountFromAddress(account)
	if err != nil {
		fmt.Println(err)
	}

	pay, err := data.NewAsset(pays)
	if err != nil {
		fmt.Println(err)
	}

	get, err := data.NewAsset(pays)
	if err != nil {
		fmt.Println(err)
	}

	resp, err := cli.r.BookOffers(*addr, ledgerIndex, *pay, *get)
	if err != nil {
		fmt.Println(err)
	}

	return unmarshall(resp)
}

func (cli *Client) Subscribe(ledger, transactions, transactionsProposed, server bool) (result string) {
	confirmation, err := cli.r.Subscribe(ledger, transactions, transactionsProposed, server)
	if err != nil {
		fmt.Println(err)
	}

	return unmarshall(confirmation)
}

func (cli *Client) SubscribeOrderBooks(books string) (result string) {
	b, err := json.Marshal(books)
	if err != nil {
		fmt.Println(err)
	}

	var orderbooks []websockets.OrderBookSubscription
	err = json.Unmarshal(b, &orderbooks)
	if err != nil {
		fmt.Println(err)
	}

	resp, err := cli.r.SubscribeOrderBooks(orderbooks)
	if err != nil {
		fmt.Println(err)
	}

	return unmarshall(resp)
}

func (cli *Client) Streming() {
	for {
		msg, ok := <-cli.r.Incoming
		if !ok {
			return
		}

		switch msg := msg.(type) {
		case *websockets.LedgerStreamMsg:
			{
				// fmt.Println("LedgerStreamMsg")
				// fmt.Println(unmarshall(msg))

				LedgerSquence = msg.LedgerSequence
				//	fmt.Println(LedgerSquence)
			}

		case *websockets.TransactionStreamMsg:
			{
				// fmt.Println("LedgerStreamMsg")
				// fmt.Println(unmarshall(msg.Transaction))

				// fmt.Println("..................................")
				// for _, path := range msg.Transaction.PathSet() {
				// 	fmt.Println(unmarshall(path))
				// }

				// fmt.Println("..................................")
				// trades, err := data.NewTradeSlice(&msg.Transaction)
				// if err != nil {
				// 	fmt.Println(err)
				// }
				// for _, trade := range trades {
				// 	fmt.Println(unmarshall(trade))
				// }

				// fmt.Println("..................................")
				// balances, err := msg.Transaction.Balances()
				// checkErr(err, false)
				// for _, balance := range balances {
				// 	fmt.Println(unmarshall(balance))
				// }
			}

		case *websockets.ServerStreamMsg:
			{
				// fmt.Println("ServerStreamMsg")
				// fmt.Println(unmarshall(msg))
			}
		}
		time.Sleep(1 * time.Second)
	}
}

func (cli *Client) Fee() (result string) {
	resp, err := cli.r.Fee()
	if err != nil {
		fmt.Println(err)
	}

	return unmarshall(resp)
}

func (cli *Client) GetBlockCount() uint32 {
	return LedgerSquence
}

func (cli *Client) GetBalance(addr string) (balance float64) {
	acc := cli.AccountInfo(addr)
	if acc == "" {
		return
	}

	var info websockets.AccountInfoResult
	err := json.Unmarshal([]byte(acc), &info)
	if err != nil {
		fmt.Println(err)
		return
	}

	if info.AccountData.Balance == nil {
		fmt.Println("Error GetBalance AccountData")
		return
	}

	balance = info.AccountData.Balance.Float()
	return
}

func (cli *Client) ListTransactions(start, end int32, addr string) (list string) {
	_, err := data.NewAccountFromAddress(addr)
	if err != nil {
		fmt.Println("Error filter", err)
		return
	}

	txS := make([]map[string]interface{}, 0)
	for ledger := uint32(start); ledger <= uint32(end); ledger++ {
		result, err := cli.r.Ledger(ledger, true)
		if err != nil {
			fmt.Println("Error Ledger", err)
			return
		}

		for _, tx := range result.Ledger.Transactions {
			txObj := make(map[string]interface{})
			if addr == tx.GetBase().Account.String() {
				txObj["type"] = tx.GetTransactionType()
				txObj["hash"] = tx.GetBase().Hash.String()
				txObj["from"] = addr
				txObj["fee"] = tx.GetBase().Fee.String()
				txObj["memo"] = tx.GetBase().Memos

				switch tx.GetTransactionType() {
				case data.PAYMENT:
					{
						var txParse data.Payment
						b, _ := json.MarshalIndent(tx, "", " ")
						err := json.Unmarshal(b, &txParse)
						if err == nil {
							txObj["to"] = txParse.Destination.String()
							txObj["value"] = txParse.Amount.String()
						}
					}
				case data.ESCROW_CREATE:
					{
						var txParse data.EscrowCreate
						b, _ := json.MarshalIndent(tx, "", " ")
						err := json.Unmarshal(b, &txParse)
						if err == nil {
							txObj["to"] = txParse.Destination.String()
							txObj["value"] = txParse.Amount.String()
						}
					}
				case data.PAYCHAN_CREATE:
					{
						var txParse data.PaymentChannelCreate
						b, _ := json.MarshalIndent(tx, "", " ")
						err := json.Unmarshal(b, &txParse)
						if err == nil {
							txObj["to"] = txParse.Destination.String()
							txObj["value"] = txParse.Amount.String()
						}
					}
				case data.CHECK_CREATE:
					{
						var txParse data.CheckCreate
						b, _ := json.MarshalIndent(tx, "", " ")
						err := json.Unmarshal(b, &txParse)
						if err == nil {
							txObj["to"] = txParse.Destination.String()
						}
					}
				}

				txS = append(txS, txObj)

			} else {
				var dest string
				switch tx.GetTransactionType() {
				case data.PAYMENT:
					{
						var txParse data.Payment
						b, _ := json.MarshalIndent(tx, "", " ")
						err := json.Unmarshal(b, &txParse)
						if err == nil {
							dest = txParse.Destination.String()
							txObj["value"] = txParse.Amount.String()
						}
					}
				case data.ESCROW_CREATE:
					{
						var txParse data.EscrowCreate
						b, _ := json.MarshalIndent(tx, "", " ")
						err := json.Unmarshal(b, &txParse)
						if err == nil {
							dest = txParse.Destination.String()
							txObj["value"] = txParse.Amount.String()
						}
					}
				case data.PAYCHAN_CREATE:
					{
						var txParse data.PaymentChannelCreate
						b, _ := json.MarshalIndent(tx, "", " ")
						err := json.Unmarshal(b, &txParse)
						if err == nil {
							dest = txParse.Destination.String()
							txObj["value"] = txParse.Amount.String()
						}
					}
				case data.CHECK_CREATE:
					{
						var txParse data.CheckCreate
						b, _ := json.MarshalIndent(tx, "", " ")
						err := json.Unmarshal(b, &txParse)
						if err == nil {
							dest = txParse.Destination.String()
						}
					}
				}

				if addr == dest {
					txObj["type"] = tx.GetTransactionType()
					txObj["hash"] = tx.GetBase().Hash.String()
					txObj["from"] = tx.GetBase().Account.String()
					txObj["to"] = addr
					txObj["fee"] = tx.GetBase().Fee.String()
					txObj["memo"] = tx.GetBase().Memos

					txS = append(txS, txObj)
				}
			}
		}
	}

	b, _ := json.MarshalIndent(txS, "", " ")
	list = string(b)

	return
}

func (cli *Client) SendNative(prvKey, to string, drops int64, fees string) (txID string) {
	//Go : Generate
	keySequence := uint32(0)
	seed, err := data.NewSeedFromAddress(prvKey)
	if err != nil {
		fmt.Println("Error NewRippleHashCheck", err)
		return
	}

	key, err := crypto.NewECDSAKey(seed.Bytes())
	if err != nil {
		fmt.Println("Error NewECDSAKey", err)
		return
	}

	addr, err := crypto.AccountId(key, &keySequence)
	if err != nil {
		fmt.Println("Error AccountId", err)
		return
	}
	fmt.Println("source account : ", addr.String())

	//Go : Create payment
	from, err := data.NewAccountFromAddress(addr.String())
	if err != nil {
		fmt.Println("Error From", err)
		return
	}

	destination, err := data.NewAccountFromAddress(to)
	if err != nil {
		fmt.Println("Error To", err)
		return
	}
	fmt.Println("destination : ", destination.String())

	amount, err := data.NewAmount(drops) // 1*10^6
	if err != nil {
		fmt.Println("Error NewAmount", err)
		return
	}

	tx := &data.Payment{
		Destination: *destination,
		Amount:      *amount,
	}
	tx.TransactionType = data.PAYMENT

	// Go : Sequence
	//sequence := uint32(4)
	acc, err := cli.r.AccountInfo(*from)
	if err != nil {
		fmt.Println("Error AccountInfo", err)
		return
	}
	sequence := *acc.AccountData.Sequence

	//Go : Sign
	base := tx.GetBase()
	base.Sequence = sequence
	copy(tx.Account[:], key.Id(&keySequence))
	if base.Flags == nil {
		base.Flags = new(data.TransactionFlag)
	}
	fee, _ := data.NewValue(fees, true)
	base.Fee = *fee

	err = data.Sign(tx, key, &keySequence)
	if err != nil {
		fmt.Println("Error Sign", err)
	}

	//Go : outTX
	out, err := json.Marshal(tx)
	if err != nil {
		fmt.Println("Error Marshal", err)
	}
	fmt.Println(string(out))

	//Go : Submit
	result, err := cli.r.Submit(tx)
	if err != nil {
		fmt.Println("Error Submit", err)
	}
	fmt.Println("Submit", result.EngineResult, result.EngineResultMessage)

	txID = tx.Hash.String()

	return
}

//----------------------------------------------------------------------------//

func unmarshall(v interface{}) (str string) {
	b, err := json.MarshalIndent(v, "", " ")
	if err != nil {
		fmt.Println(err)
	}

	str = string(b)

	return
}
