package xrp

import (
	"fmt"
	"os"
	"testing"
)

func checkErr(err error, quit bool) {
	if err != nil {
		fmt.Println(err)
		if quit {
			os.Exit(1)
		}
	}
}

/*
func Test_NewEd25519Key(t *testing.T) {
	var key crypto.Key
	var err error
	password := "masterpassphrase"

	seed, _ := crypto.GenerateFamilySeed(password)
	fmt.Println("seed", seed.String())

	key, err = crypto.NewEd25519Key(seed.Payload())
	//prv, _ := crypto.AccountPrivateKey(key, &sequenceOne)
	pub, _ := crypto.AccountPublicKey(key, nil)
	addr, _ := crypto.AccountId(key, nil)
	fmt.Println("NewEd25519Key", err, addr.String(), hex.EncodeToString(key.Private(nil)), pub.String())
}

func Test_NewECDSAKey(t *testing.T) {
	var key crypto.Key
	var err error
	//sequenceZero, sequenceOne := uint32(0), uint32(1)
	password := "masterpassphrase"

	seed, _ := crypto.GenerateFamilySeed(password)
	fmt.Println("seed", seed.String())

	key, err = crypto.NewECDSAKey(seed.Payload())
	prv, _ := crypto.AccountPrivateKey(key, nil)
	pub, _ := crypto.AccountPublicKey(key, nil)
	addr, _ := crypto.AccountId(key, nil)
	fmt.Println("NewECDSAKey", err, addr.String(), prv.String(), pub.String())
}

func Test_NewRippleHashCheck(t *testing.T) {
	var key crypto.Key
	var err error
	seed, err := data.NewSeedFromAddress("shSwNyScPmvaBzKdxBdj9Cuc9x3bn")
	fmt.Println("NewRippleHashCheck", err, seed.String())

	key, err = crypto.NewEd25519Key(seed.Bytes())
	addr, _ := crypto.AccountId(key, nil)
	fmt.Println("NewEd25519Key", err, addr.String())

	sequenceZero := uint32(0)
	key, err = crypto.NewECDSAKey(seed.Bytes())
	addr, _ = crypto.AccountId(key, &sequenceZero)
	fmt.Println("NewECDSAKey", err, addr.String())
}

func Test_Payment(t *testing.T) {
	//Go : Generate
	keySequence := uint32(0)
	seed, err := data.NewSeedFromAddress("shSwNyScPmvaBzKdxBdj9Cuc9x3bn")
	if err != nil {
		fmt.Println("Error NewRippleHashCheck", err)
		return
	}

	key, err := crypto.NewECDSAKey(seed.Bytes())
	if err != nil {
		fmt.Println("Error NewECDSAKey", err)
		return
	}

	addr, err := crypto.AccountId(key, &keySequence)
	if err != nil {
		fmt.Println("Error AccountId", err)
		return
	}
	fmt.Println("source account : ", addr.String())

	//Go : Connect ws
	r, err := websockets.NewRemote(Ws_testnet)
	if err != nil {
		fmt.Println("Error NewRemote", err)
		return
	}

	//Go : Create payment
	from, err := data.NewAccountFromAddress(addr.String())
	if err != nil {
		fmt.Println("Error From", err)
		return
	}

	destination, err := data.NewAccountFromAddress("rLyJD1YHKM6bCXdcUY9n5xn3vFcAVhS2re")
	if err != nil {
		fmt.Println("Error To", err)
		return
	}
	fmt.Println("destination : ", destination.String())

	amount, err := data.NewAmount("1000000") // 1*10^6
	if err != nil {
		fmt.Println("Error NewAmount", err)
		return
	}

	tx := &data.Payment{
		Destination: *destination,
		Amount:      *amount,
	}
	tx.TransactionType = data.PAYMENT

	// Go : Sequence
	//sequence := uint32(4)
	acc, err := r.AccountInfo(*from)
	if err != nil {
		fmt.Println("Error AccountInfo", err)
		return
	}
	sequence := *acc.AccountData.Sequence

	//Go : Sign
	base := tx.GetBase()
	base.Sequence = sequence
	copy(tx.Account[:], key.Id(&keySequence))
	if base.Flags == nil {
		base.Flags = new(data.TransactionFlag)
	}
	fee, _ := data.NewValue("12", true)
	base.Fee = *fee

	err = data.Sign(tx, key, &keySequence)
	if err != nil {
		fmt.Println("Error Sign", err)
	}

	//Go : outTX
	out, err := json.Marshal(tx)
	if err != nil {
		fmt.Println("Error Marshal", err)
	}
	fmt.Println(string(out))

	//Go : Submit
	result, err := r.Submit(tx)
	if err != nil {
		fmt.Println("Error Submit", err)
	}
	fmt.Println("Submit", result.EngineResult, result.EngineResultMessage)
}
*/

func Test_CLI(t *testing.T) {
	addr := "rLyJD1YHKM6bCXdcUY9n5xn3vFcAVhS2re"
	cli, _ := NewClient(Ws_testnet)
	balance := cli.GetBalance(addr)
	fmt.Println("Balance", balance)

	//actTx := cli.AccountTx(addr, 20, -1, -1)
	//fmt.Println("AccountTx", actTx)

	end := int32(21021896)
	start := end - int32(10)
	txs := cli.ListTransactions(start, end, addr)
	fmt.Println("ListTransactions", txs)

	cli.Subscribe(true, false, false, false)
	go func() {
		cli.Streming()
	}()
	for {

	}
}
