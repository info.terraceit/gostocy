package gostocy

import (
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"math/big"
	"net/http"
	"strconv"
	"sync"
	"sync/atomic"
	"time"

	"gitlab.com/info.terraceit/gostocy/pkg/avax"
	"gitlab.com/info.terraceit/gostocy/pkg/bch"
	"gitlab.com/info.terraceit/gostocy/pkg/bsv"
	"gitlab.com/info.terraceit/gostocy/pkg/btc"
	"gitlab.com/info.terraceit/gostocy/pkg/cryptocy"
	"gitlab.com/info.terraceit/gostocy/pkg/eos"
	"gitlab.com/info.terraceit/gostocy/pkg/eth"
	"gitlab.com/info.terraceit/gostocy/pkg/ltc"
	"gitlab.com/info.terraceit/gostocy/pkg/qrcode"
	"gitlab.com/info.terraceit/gostocy/pkg/trx"
	"gitlab.com/info.terraceit/gostocy/pkg/xlm"
	"gitlab.com/info.terraceit/gostocy/pkg/xrp"

	"github.com/bartekn/go-bip39"
	"github.com/btcsuite/btcd/chaincfg"
	"github.com/btcsuite/btcutil"
	"github.com/btcsuite/btcutil/hdkeychain"
)

const (
	MAIN_NET = `mainnet`
	TEST_NET = `testnet`
	SIM_NET  = `simnet`
)

const (
	COIN_BTC  = "BTC"
	COIN_LTC  = "LTC"
	COIN_BCH  = "BCH"
	COIN_BSV  = "BSV"
	COIN_ETH  = "ETH"
	COIN_EOS  = "EOS"
	COIN_XLM  = "XLM"
	COIN_XRP  = "XRP"
	COIN_TRX  = "TRX"
	COIN_AVAX = "AVAX"
)

//---------------------------------------------------------------------//

var Mnemonic string
var Password string
var Seed []byte
var walletManager = map[string]*Wallet{}
var mu sync.Mutex
var count atomic.Uint32

func chainConfig(net string) (conf *chaincfg.Params) {
	switch net {
	case MAIN_NET:
		conf = &chaincfg.MainNetParams
	case TEST_NET:
		conf = &chaincfg.TestNet3Params
	case SIM_NET:
		//&chainParams{Params: &chaincfg.SimNetParams}
		conf = &chaincfg.RegressionNetParams
	}

	return
}

func GenMnemonic() string {
	entropy, err := bip39.NewEntropy(256)
	if err != nil {
		return ""
	}

	mnemonic, err := bip39.NewMnemonic(entropy)
	if err != nil {
		return ""
	}

	Mnemonic = mnemonic
	return Mnemonic
}

func GenMnemonicFromKey(keyHex string) string {
	entropy, err := hex.DecodeString(keyHex)
	if err != nil {
		return ""
	}

	mnemonic, err := bip39.NewMnemonic(entropy)
	if err != nil {
		return ""
	}

	Mnemonic = mnemonic
	return Mnemonic
}

func GenSeed(mnemonic, password string) []byte {
	seed, err := bip39.NewSeedWithErrorChecking(mnemonic, password)
	if err != nil {
		return []byte{}
	}

	Mnemonic = mnemonic
	Password = password
	Seed = seed
	return Seed
}

func GenSeedRandom64() []byte {
	entropy, _ := bip39.NewEntropy(256)

	// Because seed has 64bytes, need 64bytes mask too
	mnemonic, _ := bip39.NewMnemonic(entropy)
	seed := bip39.NewSeed(mnemonic, "")
	return seed
}

func GenSeedRandom32() []byte {
	entropy, _ := bip39.NewEntropy(256)
	return entropy
}

func GenSeedOnly(mnemonic string) ([]byte, error) {
	seed, err := bip39.MnemonicToByteArray(mnemonic)
	if err != nil {
		return []byte{}, err
	}

	return seed, nil
}

func CreateSDK(password string) error {
	// GO : If existed --> new
	// if len(Seed) > 0 {
	// 	return errors.New("Error Wallet Exist !!!")
	// }

	GenMnemonic()
	GenSeed(Mnemonic, password)
	return nil
}

func CreateSdkFromKey(keyHex string) error {
	// GO : If existed --> new
	// if len(Seed) > 0 {
	// 	return errors.New("Error Wallet Exist !!!")
	// }

	GenMnemonicFromKey(keyHex)
	GenSeed(Mnemonic, "")
	return nil
}

func GenMasterPrivKey(net string, seed []byte) (prv string) {
	conf := chainConfig(net)
	ext, err := hdkeychain.NewMaster(seed, conf)
	if err != nil {
		return
	}

	privk, err := ext.ECPrivKey()
	if err != nil {
		return ""
	}

	wif, err := btcutil.NewWIF(privk, conf, true)
	if err != nil {
		return ""
	}

	prv = wif.String()
	return
}

func GenMasterPubKey(net string, seed []byte) (pub string) {
	conf := chainConfig(net)
	ext, err := hdkeychain.NewMaster(seed, conf)
	if err != nil {
		return
	}

	pubk, err := ext.ECPubKey()
	if err != nil {
		return ""
	}

	pub = hex.EncodeToString(pubk.SerializeCompressed())
	return
}

func GenMasterXpub(net string, seed []byte) (pub string) {
	conf := chainConfig(net)
	ext, err := hdkeychain.NewMaster(seed, conf)
	if err != nil {
		return
	}

	xpub, err := ext.Neuter()
	if err != nil {
		return
	}
	pub = xpub.String()

	return
}

func RemoveAll() {
	count.Store(count.Load() - 1)
	if count.Load() == 0 {
		mu.Lock()
		defer mu.Unlock()

		Mnemonic = ""
		Password = ""
		Seed = make([]byte, 0)
		walletManager = make(map[string]*Wallet)
	}
}

func Encrypted() string {
	CIPHER_KEY := cryptocy.Keccak(Seed)
	b, err := json.MarshalIndent(walletManager, "", " ")
	if err != nil {
		return ""
	}
	msg := string(b)

	encrypted, err := cryptocy.Encrypt(CIPHER_KEY, []byte(msg))
	if err != nil {
		return ""
	}

	return encrypted
}

func Decrypted(seedHex, encrypted string) string {
	seed, errs := hex.DecodeString(seedHex)
	if errs != nil {
		return ""
	}

	CIPHER_KEY := cryptocy.Keccak(seed)
	decrypted, err := cryptocy.Decrypt(CIPHER_KEY, encrypted)
	if err != nil {
		return ""
	}

	err = json.Unmarshal([]byte(decrypted), &walletManager)
	if err != nil {
		return ""
	}

	b, err := json.MarshalIndent(walletManager, "", " ")
	if err != nil {
		return ""
	}

	return string(b)
}

func SeedToHex() string {
	return hex.EncodeToString(Seed)
}

func BackUp() string {
	return Mnemonic
}

func Restore(mnemonic, password, name, net string) (*Wallet, error) {
	RemoveAll()
	GenSeed(mnemonic, password)
	return AddWallet(net, name)
}

func SaveSDK() string {
	b, err := json.MarshalIndent(walletManager, "", " ")
	if err != nil {
		return ""
	}

	mSave := make(map[string]string)
	mSave["walletManager"] = string(b)
	mSave["mnemonic"] = Mnemonic
	mSave["seed"] = SeedToHex()
	mSave[COIN_BTC] = btc.SaveSDK()
	mSave[COIN_BCH] = bch.SaveSDK()
	mSave[COIN_LTC] = ltc.SaveSDK()
	mSave[COIN_BSV] = bsv.SaveSDK()

	mData, err := json.MarshalIndent(mSave, "", " ")
	if err != nil {
		return ""
	}

	return string(mData)
}

func LoadSDK(mData string) bool {
	mSave := make(map[string]string)
	err := json.Unmarshal([]byte(mData), &mSave)
	if err != nil {
		fmt.Println("..........Err LoadSDK mData...........")
		return false
	}

	Mnemonic = mSave["mnemonic"]
	Seed, _ = hex.DecodeString(mSave["seed"])

	b := mSave["walletManager"]
	err = json.Unmarshal([]byte(b), &walletManager)
	if err != nil {
		fmt.Println("..........Err LoadSDK mData...........")
		return false
	}

	ok := btc.LoadSDK(mSave[COIN_BTC])
	if !ok {
		fmt.Println("..........Err LoadSDK b..tc.........")
		return false
	}

	ok = bch.LoadSDK(mSave[COIN_BCH])
	if !ok {
		fmt.Println("..........Err LoadSDK bch...........")
		return false
	}

	ok = ltc.LoadSDK(mSave[COIN_LTC])
	if !ok {
		fmt.Println("..........Err LoadSDK ltc...........")
		return false
	}

	ok = bsv.LoadSDK(mSave[COIN_BSV])
	if !ok {
		fmt.Println("..........Err LoadSDK bsv...........")
		return false
	}

	//

	for _, w := range walletManager {
		w.Restore(mSave["seed"])
		//w.Listening()
	}

	return true
}

func QrCodeFile(size int, name, content, path string) string {
	return qrcode.WriteFile(size, name, content, path)
}

func QrCodePNG(size int, content string) (png []byte, err error) {
	return qrcode.Encode(size, content)
}

//---------------------------------------------------------------------//

type Wallet struct {
	Index    int32  `json:"index"`
	Net      string `json:"net"`
	Name     string `json:"name"`
	Password string `json:"password"`
	Mnemonic string `json:"mnemonic"`
	Seed     []byte `json:"seed"`

	BTC  *btc.Wallet `json:"BTC"`
	BCH  *bch.Wallet `json:"BCH"`
	BSV  *bsv.Wallet `json:"BSV"`
	LTC  *ltc.Wallet `json:"LTC"`
	ETH  *eth.Wallet `json:"ETH"`
	EOS  *eos.Wallet `json:"EOS"`
	XLM  *xlm.Wallet `json:"XLM"`
	XRP  *xrp.Wallet `json:"XRP"`
	TRX  *trx.Wallet `json:"TRX"`
	AVAX *eth.Wallet `json:"AVAX"`

	cliBTC  *btc.Client
	cliBCH  *bch.Client
	cliBSV  *bsv.Client
	cliLTC  *ltc.Client
	cliETH  *eth.Client
	cliXRP  *xrp.Client
	cliTRX  *trx.Client
	cliAVAX *avax.Client

	isListening bool
}

//---------------------------------------------------------------------//

func AddWallet(net, name string) (w *Wallet, err error) {
	mu.Lock()
	defer mu.Unlock()

	count.Add(1)
	if len(Mnemonic) <= 0 || len(Seed) <= 0 {
		err = errors.New("error not mnemonic")
		return
	}

	w, ok := walletManager[name]
	if ok {
		return
	}

	index := len(walletManager)
	w = new(Wallet)
	w.Name = name
	w.Net = net
	w.Mnemonic = Mnemonic
	w.Password = Password
	w.Seed = Seed
	w.Index = int32(index)
	w.BTC, err = btc.NewWallet(net, w.Mnemonic, w.Password)
	w.BCH, err = bch.NewWallet(net, w.Mnemonic, w.Password)
	w.BSV, err = bsv.NewWallet(net, w.Mnemonic, w.Password)
	w.LTC, err = ltc.NewWallet(net, w.Mnemonic, w.Password)
	w.ETH, err = eth.NewWallet(net, w.Mnemonic, w.Password)
	w.EOS, err = eos.NewWallet(net, w.Mnemonic, w.Password)
	w.XLM, err = xlm.NewWallet(net, w.Mnemonic, w.Password)
	w.XRP, err = xrp.NewWallet(net, w.Mnemonic, w.Password)
	w.TRX, err = trx.NewWallet(net, w.Mnemonic, w.Password)
	w.AVAX, err = eth.NewWallet(net, w.Mnemonic, w.Password)
	walletManager[name] = w
	return
}

func AddWalletSeed(net, name string, seed []byte) (w *Wallet, err error) {
	mu.Lock()
	defer mu.Unlock()

	count.Add(1)
	Seed = seed
	if len(seed) <= 0 {
		return nil, fmt.Errorf("error mnemonic: %v", Mnemonic)
	}

	if walletManager == nil {
		return nil, fmt.Errorf("error walletManager: %v", walletManager)
	}

	index := len(walletManager)
	w = new(Wallet)
	w.Name = name
	w.Net = net
	w.Mnemonic = Mnemonic
	w.Password = Password
	w.Seed = seed
	w.Index = int32(index)
	w.BTC, err = btc.NewWalletSeed(net, Seed)
	if err != nil {
		return nil, err
	}

	w.BCH, err = bch.NewWalletSeed(net, Seed)
	if err != nil {
		return nil, err
	}

	w.BSV, err = bsv.NewWalletSeed(net, Seed)
	if err != nil {
		return nil, err
	}

	w.LTC, err = ltc.NewWalletSeed(net, Seed)
	if err != nil {
		return nil, err
	}

	w.ETH, err = eth.NewWalletSeed(net, Seed)
	if err != nil {
		return nil, err
	}

	w.EOS, err = eos.NewWalletSeed(net, Seed)
	if err != nil {
		return nil, err
	}

	w.XLM, err = xlm.NewWalletSeed(net, Seed)
	if err != nil {
		return nil, err
	}

	w.XRP, err = xrp.NewWalletSeed(net, Seed)
	if err != nil {
		return nil, err
	}

	w.TRX, err = trx.NewWalletSeed(net, Seed)
	if err != nil {
		return nil, err
	}

	w.AVAX, err = eth.NewWalletSeed(net, Seed)
	if err != nil {
		return nil, err
	}

	walletManager[name] = w
	return
}

func GetWallet(name string) (*Wallet, error) {
	var err error
	v, ok := walletManager[name]
	if !ok {
		err = errors.New("Error GetWallet")
	}

	return v, err
}

//---------------------------------------------------------------------//

func (w *Wallet) Restore(seedHex string) {
	w.BTC.Restore(seedHex)
	w.LTC.Restore(seedHex)
	w.BCH.Restore(seedHex)
	w.BSV.Restore(seedHex)
}

func (w *Wallet) Stop() {
	w.isListening = false
}

func (w *Wallet) Listening() {
	w.isListening = true

	// BTC
	go func() {
		for w.isListening {
			if w.cliBTC != nil {
				blockcount := w.GetBlockCount(COIN_BTC)
				if blockcount != w.BTC.Block_genesis && blockcount != 0 {
					addr := w.GetPublicAddress(COIN_BTC, "")
					if addr != "" {
						btc.SyncBlock(w.BTC.Block_genesis, blockcount, w.cliBTC)
						w.BTC.Balance = btc.GetBalance(w.Net, addr)
						w.BTC.ListTX = btc.ListTransactions(addr)
						//fmt.Println("..... balance btc :", w.BTC.Balance)
						w.BTC.Block_genesis = blockcount
					}
				}
			}
			time.Sleep(1 * time.Second)
		}
	}()

	// LTC
	go func() {
		for w.isListening {
			if w.cliLTC != nil {
				blockcount := w.GetBlockCount(COIN_LTC)
				if blockcount != w.LTC.Block_genesis && blockcount != 0 {
					addr := w.GetPublicAddress(COIN_LTC, "")
					if addr != "" {
						ltc.SyncBlock(w.LTC.Block_genesis, blockcount, w.cliLTC)
						w.LTC.Balance = ltc.GetBalance(w.Net, addr)
						w.LTC.ListTX = ltc.ListTransactions(addr)
						//fmt.Println("..... balance ltc :", w.LTC.Balance)
						w.LTC.Block_genesis = blockcount
					}
				}
			}
			time.Sleep(1 * time.Second)
		}
	}()

	// BCH
	go func() {
		for w.isListening {
			if w.cliBCH != nil {
				blockcount := w.GetBlockCount(COIN_BCH)
				if blockcount != w.BCH.Block_genesis && blockcount != 0 {
					addr := w.GetPublicAddress(COIN_BCH, "")
					if addr != "" {
						bch.SyncBlock(w.BCH.Block_genesis, blockcount, w.cliBCH)
						w.BCH.Balance = bch.GetBalance(w.Net, addr)
						w.BCH.ListTX = bch.ListTransactions(addr)
						//fmt.Println("..... balance bch :", w.BCH.Balance)
						w.BCH.Block_genesis = blockcount
					}
				}
			}
			time.Sleep(1 * time.Second)
		}
	}()

	// BSV
	go func() {
		for w.isListening {
			if w.cliBSV != nil {
				blockcount := w.GetBlockCount(COIN_BSV)
				if blockcount != w.BSV.Block_genesis && blockcount != 0 {
					addr := w.GetPublicAddress(COIN_BSV, "")
					if addr != "" {
						bsv.SyncBlock(w.BSV.Block_genesis, blockcount, w.cliBSV)
						w.BSV.Balance = bsv.GetBalance(w.Net, addr)
						w.BSV.ListTX = bsv.ListTransactions(addr)
						//fmt.Println("..... balance bsv :", w.BSV.Balance)
						w.BSV.Block_genesis = blockcount
					}
				}
			}
			time.Sleep(1 * time.Second)
		}
	}()

	// ETH
	go func() {
		for w.isListening {
			if w.cliETH != nil {
				addr := w.GetPublicAddress(COIN_ETH, "")
				if addr != "" {
					wei, err := w.cliETH.GetBalance(addr)
					if err == nil {
						eF, _ := wei.Float64()
						w.ETH.Balance = eF
						//fmt.Println("..... balance eth :", w.ETH.Balance)
					}

					toHeight := w.GetBlockCount(COIN_ETH)
					fromHeight := toHeight - 100
					if fromHeight > 0 {
						list := eth.ListTransaction(addr, fromHeight, toHeight, w.cliETH)
						w.ETH.ListTX = list
					}
				}
			}
			time.Sleep(1 * time.Second)
		}
	}()

	// EOS
	go func() {
		for w.isListening {
			addr := w.GetPublicAddress(COIN_EOS, "")
			if addr != "" {
				w.EOS.Balance = eos.GetBalance(w.Net, addr, "EOS", "eosio.token")
				list := eos.ListTransaction(w.Net, addr)
				w.EOS.ListTX = list
			}
			time.Sleep(1 * time.Second)
		}
	}()

	// XLM
	go func() {
		for w.isListening {
			addr := w.GetPublicAddress(COIN_XLM, "")
			if addr != "" {
				w.XLM.Balance = xlm.GetBalance(w.Net, addr, xlm.ASSET_TYPE_NATIVE, "")
				//fmt.Println("..... balance xlm :", w.XLM.Balance)

				list := xlm.PaymentForAccount(w.Net, addr, "", 200, xlm.ORDER_ASC)
				w.XLM.ListTX = list
			}
			time.Sleep(1 * time.Second)
		}
	}()

	// XRP
	go func() {
		for w.isListening {
			if w.cliXRP != nil {
				addr := w.GetPublicAddress(COIN_XRP, "")
				if addr != "" {
					w.XRP.Balance = w.cliXRP.GetBalance(addr)
					//fmt.Println("..... balance xrp :", w.XRP.Balance)

					toHeight := w.GetBlockCount(COIN_XRP)
					fromHeight := toHeight - 100
					if fromHeight > 0 {
						list := w.cliXRP.ListTransactions(int32(fromHeight), int32(toHeight), addr)
						w.XRP.ListTX = list
					}
				}
			}
			time.Sleep(1 * time.Second)
		}
	}()
}

func (w *Wallet) String() string {
	b, err := json.MarshalIndent(w, "", " ")
	if err != nil {
		return ""
	}

	return string(b)
}

func (w *Wallet) AddCoin(coinType string, salt string) (addr string) {
	switch coinType {
	case COIN_BTC:
		{
			acc, err := w.BTC.CreateAccount(salt)
			if err != nil {
				fmt.Println("....Error AddCoin BTC....")
				return
			}
			addr = acc.GetAddress()
		}
	case COIN_LTC:
		{
			acc, err := w.LTC.CreateAccount(salt)
			if err != nil {
				fmt.Println("....Error AddCoin LTC....")
				return
			}
			addr = acc.GetAddress()
		}
	case COIN_BCH:
		{
			acc, err := w.BCH.CreateAccount(salt)
			if err != nil {
				fmt.Println("....Error AddCoin BCH....")
				return
			}
			addr = acc.GetAddress()
		}
	case COIN_BSV:
		{
			acc, err := w.BSV.CreateAccount(salt)
			if err != nil {
				fmt.Println("....Error AddCoin BSV....")
				return
			}
			addr = acc.GetAddress()
		}
	case COIN_ETH:
		{
			acc, err := w.ETH.CreateAccount(uint32(60), salt)
			if err != nil {
				fmt.Println("....Error AddCoin ETH....")
				return
			}
			addr = acc.GetAddress()
		}
	case COIN_EOS:
		{
			acc, err := w.EOS.CreateAccount(salt)
			if err != nil {
				fmt.Println("....Error AddCoin EOS....")
				return
			}
			addr = acc.GetPubKey()
		}
	case COIN_XLM:
		{
			acc, err := w.XLM.CreateAccount(salt)
			if err != nil {
				fmt.Println("....Error AddCoin XLM....")
				return
			}
			addr = acc.GetAddress()
		}
	case COIN_XRP:
		{
			acc, err := w.XRP.CreateAccount(salt)
			if err != nil {
				fmt.Println("....Error AddCoin XRP....")
				return
			}
			addr = acc.GetAddress()
		}
	case COIN_TRX:
		{
			acc, err := w.TRX.CreateAccount(salt)
			if err != nil {
				fmt.Println("....Error AddCoin TRX....")
				return
			}
			addr = acc.GetAddress()
		}
	case COIN_AVAX:
		{
			acc, err := w.AVAX.CreateAccount(uint32(9005), salt)
			if err != nil {
				fmt.Println("....Error AddCoin AVAX....")
				return
			}
			addr = acc.GetAddress()
		}
	}

	return
}

func (w *Wallet) GetPublicAddress(coinType string, salt string) (addr string) {
	switch coinType {
	case COIN_BTC:
		{
			acc := w.BTC.GetAccount(salt)
			if acc == nil {
				return
			}
			addr = acc.GetAddress()
		}
	case COIN_LTC:
		{
			acc := w.LTC.GetAccount(salt)
			if acc == nil {
				return
			}
			addr = acc.GetAddress()
		}
	case COIN_BCH:
		{
			acc := w.BCH.GetAccount(salt)
			if acc == nil {
				return
			}
			addr = acc.GetAddress()
		}
	case COIN_BSV:
		{
			acc := w.BSV.GetAccount(salt)
			if acc == nil {
				return
			}
			addr = acc.GetAddress()
		}
	case COIN_ETH:
		{
			acc := w.ETH.GetAccount(salt)
			if acc == nil {
				return
			}
			addr = acc.GetAddress()
		}
	case COIN_EOS:
		{
			acc := w.EOS.GetAccount(salt)
			if acc == nil {
				return
			}
			addr = acc.GetAddress()
		}
	case COIN_XLM:
		{
			acc := w.XLM.GetAccount(salt)
			if acc == nil {
				return
			}
			addr = acc.GetAddress()
		}
	case COIN_XRP:
		{
			acc := w.XRP.GetAccount(salt)
			if acc == nil {
				return
			}
			addr = acc.GetAddress()
		}
	case COIN_TRX:
		{
			acc := w.TRX.GetAccount(salt)
			if acc == nil {
				return
			}
			addr = acc.GetAddress()
		}
	case COIN_AVAX:
		{
			acc := w.AVAX.GetAccount(salt)
			if acc == nil {
				return
			}
			addr = acc.GetAddress()
		}
	}

	return
}

func (w *Wallet) GetPrivKey(coinType string, salt string) (privKey string) {
	switch coinType {
	case COIN_BTC:
		{
			acc := w.BTC.GetAccount(salt)
			if acc == nil {
				return
			}
			privKey = acc.DumpprivKey()
		}
	case COIN_LTC:
		{
			acc := w.LTC.GetAccount(salt)
			if acc == nil {
				return
			}
			privKey = acc.DumpprivKey()
		}
	case COIN_BCH:
		{
			acc := w.BCH.GetAccount(salt)
			if acc == nil {
				return
			}
			privKey = acc.DumpprivKey()
		}
	case COIN_BSV:
		{
			acc := w.BSV.GetAccount(salt)
			if acc == nil {
				return
			}
			privKey = acc.DumpprivKey()
		}
	case COIN_ETH:
		{
			acc := w.ETH.GetAccount(salt)
			if acc == nil {
				return
			}
			privKey = acc.GetPrivKey()
		}
	case COIN_EOS:
		{
			acc := w.EOS.GetAccount(salt)
			if acc == nil {
				return
			}
			privKey = acc.GetPrivKey()
		}
	case COIN_XLM:
		{
			acc := w.XLM.GetAccount(salt)
			if acc == nil {
				return
			}
			privKey = acc.GetPrivKey()
		}
	case COIN_XRP:
		{
			acc := w.XRP.GetAccount(salt)
			if acc == nil {
				return
			}
			privKey = acc.GetPrivKey()
		}
	case COIN_TRX:
		{
			acc := w.TRX.GetAccount(salt)
			if acc == nil {
				return
			}
			privKey = acc.GetPrivKey()
		}
	case COIN_AVAX:
		{
			acc := w.AVAX.GetAccount(salt)
			if acc == nil {
				return
			}
			privKey = acc.GetPrivKey()
		}
	}

	return
}

func (w *Wallet) GetPubKey(coinType string, salt string) (pubKey string) {
	switch coinType {
	case COIN_BTC:
		{
			acc := w.BTC.GetAccount(salt)
			if acc == nil {
				return
			}
			pubKey = acc.GetPubKey()
		}
	case COIN_LTC:
		{
			acc := w.LTC.GetAccount(salt)
			if acc == nil {
				return
			}
			pubKey = acc.GetPubKey()
		}
	case COIN_BCH:
		{
			acc := w.BCH.GetAccount(salt)
			if acc == nil {
				return
			}
			pubKey = acc.GetPubKey()
		}
	case COIN_BSV:
		{
			acc := w.BSV.GetAccount(salt)
			if acc == nil {
				return
			}
			pubKey = acc.GetPubKey()
		}
	case COIN_ETH:
		{
			acc := w.ETH.GetAccount(salt)
			if acc == nil {
				return
			}
			pubKey = acc.GetPubKey()
		}
	case COIN_EOS:
		{
			acc := w.EOS.GetAccount(salt)
			if acc == nil {
				return
			}
			pubKey = acc.GetPubKey()
		}
	case COIN_XLM:
		{
			acc := w.XLM.GetAccount(salt)
			if acc == nil {
				return
			}
			pubKey = acc.GetAddress()
		}
	case COIN_XRP:
		{
			acc := w.XRP.GetAccount(salt)
			if acc == nil {
				return
			}
			pubKey = acc.GetAddress()
		}
	case COIN_TRX:
		{
			acc := w.TRX.GetAccount(salt)
			if acc == nil {
				return
			}
			pubKey = acc.GetPubKey()
		}
	}

	return
}

//---------------------------------------------------------------------//

func (w *Wallet) ConnectBTC(url, username, pass string) bool {
	//	if w.cliBTC != nil {
	//		fmt.Println(".....Connect_BTC...exist")
	//		return true
	//	}

	cli, err := btc.NewClient(w.Net, url, username, pass)
	if err != nil {
		return false
	}
	w.cliBTC = cli

	blockcount := w.BTC.Block_genesis
	if blockcount <= 0 {
		blockcount = w.GetBlockCount(COIN_BTC)
		w.BTC.Block_genesis = blockcount
	}
	return true
}

func (w *Wallet) ConnectLTC(url, username, pass string) bool {
	//	if w.cliLTC != nil {
	//		fmt.Println(".....Connect_BTC...exist")
	//		return true
	//	}

	cli, err := ltc.NewClient(w.Net, url, username, pass)
	if err != nil {
		return false
	}
	w.cliLTC = cli

	blockcount := w.LTC.Block_genesis
	if blockcount <= 0 {
		blockcount = w.GetBlockCount(COIN_LTC)
		w.LTC.Block_genesis = blockcount
	}
	return true
}

func (w *Wallet) ConnectBCH(url, username, pass string) bool {
	//	if w.cliBCH != nil {
	//		fmt.Println(".....Connect_BTC...exist")
	//		return true
	//	}

	cli, err := bch.NewClient(w.Net, url, username, pass)
	if err != nil {
		return false
	}
	w.cliBCH = cli

	blockcount := w.BCH.Block_genesis
	if blockcount <= 0 {
		blockcount = w.GetBlockCount(COIN_BCH)
		w.BCH.Block_genesis = blockcount
	}
	return true
}

func (w *Wallet) ConnectBSV(url, username, pass string) bool {
	//	if w.cliBSV != nil {
	//		fmt.Println(".....Connect_BSV...exist")
	//		return true
	//	}

	cli, err := bsv.NewClient(w.Net, url, username, pass)
	if err != nil {
		return false
	}
	w.cliBSV = cli

	blockcount := w.BSV.Block_genesis
	if blockcount <= 0 {
		blockcount = w.GetBlockCount(COIN_BSV)
		w.BSV.Block_genesis = blockcount
	}
	return true
}

func (w *Wallet) ConnectETH(url string) bool {
	//	if w.cliETH != nil {
	//		fmt.Println(".....Connect_BTC...exist")
	//		return true
	//	}

	cli, err := eth.NewClient(url)
	if err != nil {
		return false
	}
	w.cliETH = cli
	return true
}

func (w *Wallet) ConnectXRP() bool {
	//	if w.cliXRP != nil {
	//		fmt.Println(".....Connect_XRP...exist")
	//		return true
	//	}

	url := ""
	if w.Net == MAIN_NET {
		url = xrp.Ws_mainnet
	} else {
		url = xrp.Ws_testnet
	}

	cli, err := xrp.NewClient(url)
	if err != nil {
		return false
	}
	w.cliXRP = cli

	confirm := cli.Subscribe(true, false, false, false)
	fmt.Println("Subscribe", confirm)

	go func() {
		cli.Streming()
	}()
	return true
}

func (w *Wallet) ConnectTRX(urlRPC, urlGRPC string) bool {
	cli, err := trx.NewClient(urlRPC, urlGRPC)
	if err != nil {
		return false
	}
	w.cliTRX = cli
	return true
}

func (w *Wallet) ConnectAVAX(url string) bool {
	cli, err := avax.NewClient(url)
	if err != nil {
		return false
	}
	w.cliAVAX = cli
	return true
}

//---------------------------------------------------------------------//

func (w *Wallet) SyncBlock(coinType string, fromHeight, toHeight int64) {
	switch coinType {
	case COIN_BTC:
		{
			if w.cliBTC == nil {
				return
			}
			btc.SyncBlock(fromHeight, toHeight, w.cliBTC)
		}
	case COIN_LTC:
		{
			if w.cliLTC == nil {
				return
			}
			ltc.SyncBlock(fromHeight, toHeight, w.cliLTC)
		}
	case COIN_BCH:
		{
			if w.cliBCH == nil {
				return
			}
			bch.SyncBlock(fromHeight, toHeight, w.cliBCH)
		}
	case COIN_BSV:
		{
			if w.cliBSV == nil {
				return
			}
			bsv.SyncBlock(fromHeight, toHeight, w.cliBSV)
		}
	}
}

func (w *Wallet) GetBlockCount(coinType string) (blockcount int64) {
	switch coinType {
	case COIN_BTC:
		{
			if w.cliBTC == nil {
				return
			}
			blockcount = w.cliBTC.GetBlockCount()
			//fmt.Println("......blockcount btc..... :", blockcount)
		}
	case COIN_LTC:
		{
			if w.cliLTC == nil {
				return
			}
			blockcount = w.cliLTC.GetBlockCount()
			//fmt.Println("......blockcount ltc..... :", blockcount)
		}
	case COIN_BCH:
		{
			if w.cliBCH == nil {
				return
			}
			blockcount = w.cliBCH.GetBlockCount()
			//fmt.Println("......blockcount bch..... :", blockcount)
		}
	case COIN_BSV:
		{
			if w.cliBSV == nil {
				return
			}
			blockcount = w.cliBSV.GetBlockCount()
			//fmt.Println("......blockcount bsv..... :", blockcount)
		}
	case COIN_ETH:
		{
			if w.cliETH == nil {
				return
			}
			numHash := w.cliETH.GetBlockNumber()
			numInt, err := strconv.ParseInt(numHash, 0, 64)
			if err != nil {
				return
			}
			blockcount = numInt
			//fmt.Println("......blockcount eth..... :", blockcount)
		}
	case COIN_EOS:
		{
			blockcount = int64(eos.GetBlockCount(w.Net))
			//fmt.Println("......blockcount eos..... :", blockcount)
		}
	case COIN_XLM:
		{
			blockcount = int64(xlm.GetBlockCount(w.Net))
			//fmt.Println("......blockcount xlm..... :", blockcount)
		}
	case COIN_XRP:
		{
			if w.cliXRP == nil {
				return
			}
			blockcount = int64(w.cliXRP.GetBlockCount())
			//fmt.Println("......blockcount xrp..... :", blockcount)
		}
	}
	return
}

func (w *Wallet) GetBalance(coinType string) (balance float64) {
	switch coinType {
	case COIN_BTC:
		{
			balance = w.BTC.Balance
			//fmt.Println("..... balance btc :", balance)
		}
	case COIN_LTC:
		{
			balance = w.LTC.Balance
			//fmt.Println("..... balance ltc :", balance)
		}
	case COIN_BCH:
		{
			balance = w.BCH.Balance
			//fmt.Println("..... balance bch :", balance)
		}
	case COIN_BSV:
		{
			balance = w.BSV.Balance
			//fmt.Println("..... balance bsv :", balance)
		}
	case COIN_ETH:
		{
			balance = w.ETH.Balance
			//fmt.Println("..... balance eth :", balance)
		}
	case COIN_EOS:
		{
			balance = w.EOS.Balance
			//fmt.Println("..... balance eos :", balance)
		}
	case COIN_XLM:
		{
			balance = w.XLM.Balance
			//fmt.Println("..... balance xlm :", balance)
		}
	case COIN_XRP:
		{
			balance = w.XRP.Balance
			//fmt.Println("..... balance xrp :", balance)
		}
	}
	return
}

func (w *Wallet) ListTransaction(coinType string) (list string) {
	switch coinType {
	case COIN_BTC:
		{
			list = w.BTC.ListTX
		}
	case COIN_LTC:
		{
			list = w.LTC.ListTX
		}
	case COIN_BCH:
		{
			list = w.BCH.ListTX
		}
	case COIN_BSV:
		{
			list = w.BSV.ListTX
		}
	case COIN_ETH:
		{
			list = w.ETH.ListTX
		}
	case COIN_EOS:
		{
			list = w.EOS.ListTX
		}
	case COIN_XLM:
		{
			list = w.XLM.ListTX
		}
	case COIN_XRP:
		{
			list = w.XRP.ListTX
		}
	}

	var txs []map[string]interface{}
	//var max int = 0
	if coinType == COIN_XLM {
		listData := map[string]interface{}{}
		json.Unmarshal([]byte(list), &listData)
		if _, ok := listData["_embedded"]; ok {
			records := listData["_embedded"].(map[string]interface{})["records"]
			b, _ := json.Marshal(records)
			json.Unmarshal(b, &txs)
		}

	} else {
		listData := []map[string]interface{}{}
		json.Unmarshal([]byte(list), &listData)
		txs = listData
	}

	obj := make(map[string]interface{})
	listObj := make([]map[string]interface{}, 0)
	for _, txObj := range txs {
		if coinType == COIN_XLM {
			obj["tx"] = txObj["transaction_hash"]
			if txObj["type"] == "create_account" {
				obj["from"] = txObj["funder"]
				obj["to"] = txObj["account"]
				obj["value"] = txObj["starting_balance"]
			} else if txObj["type"] == "payment" {
				obj["from"] = txObj["from"]
				obj["to"] = txObj["to"]
				obj["value"] = txObj["amount"]
			}
		} else {
			obj["tx"] = txObj["hash"]
			obj["from"] = txObj["from"]
			obj["to"] = txObj["to"]
			obj["value"] = txObj["value"]
		}

		if at, ok := txObj["created_at"]; ok {
			obj["timeStamp"] = at
		} else {
			obj["timeStamp"] = ""
		}

		listObj = append(listObj, obj)
	}

	jc, _ := json.MarshalIndent(listObj, "", " ")
	//fmt.Println(".......listtrans.....", string(jc))
	list = string(jc)

	return
}

//---------------------------------------------------------------------//

func (w *Wallet) SendBTC(from, to string, relayFeePerKb, amount float64) (txHex string) {
	if w.cliBTC == nil {
		return
	}
	//	blockcount := w.GetBlockCount(COIN_BTC)
	//	btc.SyncBlock(w.BTC.Block_genesis, blockcount, w.cliBTC)

	tx := w.BTC.NewTX(w.Net)
	tx.AddSender(from)
	tx.AddReceipt(to, amount)
	tx.CreateRawTx(relayFeePerKb)
	tx.SignTx()
	//fmt.Println(".....tx.....", tx.String())

	txHex = btc.SendRawTx(tx, false, w.cliBTC)
	fmt.Println(".....txHex.....", txHex)

	return
}

func (w *Wallet) SendLTC(from, to string, relayFeePerKb, amount float64) (txHex string) {
	if w.cliLTC == nil {
		return
	}
	//	blockcount := w.GetBlockCount(COIN_BTC)
	//	ltc.SyncBlock(w.LTC.Block_genesis, blockcount, w.cliLTC)

	tx := w.LTC.NewTX(w.Net)
	tx.AddSender(from)
	tx.AddReceipt(to, amount)
	tx.CreateRawTx(relayFeePerKb)
	tx.SignTx()
	//fmt.Println(".....tx.....", tx.String())
	txHex = ltc.SendRawTx(tx, false, w.cliLTC)
	fmt.Println(".....txHex.....", txHex)

	return
}

func (w *Wallet) SendBCH(from, to string, relayFeePerKb, amount float64) (txHex string) {
	if w.cliBCH == nil {
		return
	}
	//	blockcount := w.GetBlockCount(COIN_BCH)
	//	bch.SyncBlock(w.BCH.Block_genesis, blockcount, w.cliBCH)

	tx := w.BCH.NewTX(w.Net)
	tx.AddSender(from)
	tx.AddReceipt(to, amount)
	tx.CreateRawTx(relayFeePerKb)
	tx.SignTx()
	//fmt.Println(".....tx.....", tx.String())
	txHex = bch.SendRawTx(tx, false, w.cliBCH)
	fmt.Println(".....txHex.....", txHex)

	return
}

func (w *Wallet) SendBSV(from, to string, relayFeePerKb, amount float64) (txHex string) {
	if w.cliBSV == nil {
		return
	}
	//	blockcount := w.GetBlockCount(COIN_BCH)
	//	bch.SyncBlock(w.BCH.Block_genesis, blockcount, w.cliBCH)

	tx := w.BSV.NewTX(w.Net)
	tx.AddSender(from)
	tx.AddReceipt(to, amount)
	tx.CreateRawTx(relayFeePerKb)
	tx.SignTx()
	//fmt.Println(".....tx.....", tx.String())
	txHex = bsv.SendRawTx(tx, false, w.cliBSV)
	fmt.Println(".....txHex.....", txHex)

	return
}

func (w *Wallet) SendETH(prvKey, to string, amETH float64, data string) (tx string) {
	if w.cliETH == nil {
		return
	}
	amount := eth.FloatToBigInt(amETH, 18)
	var gasLimit = uint64(2000000)
	var gasPrice = big.NewInt(int64(30000000000)) // 30 gwei
	tx, _ = w.cliETH.SendTransactionRaw(prvKey, to, amount, gasLimit, gasPrice, []byte(data))
	return
}

func (w *Wallet) SendEOS(prvKey, from, to, amount, contract, memo string) (tx string) {
	tx = eos.Transfer(w.Net, prvKey, from, to, amount, contract, memo)
	return
}

func (w *Wallet) SendXLM(prvKey, to string, amLumens float64) (tx string) {
	lumens := strconv.FormatFloat(amLumens, 'f', -1, 64)
	tx = xlm.SendNative(w.Net, prvKey, to, lumens)
	return
}

func (w *Wallet) SendXRP(prvKey, to string, drops int64, fees string) (tx string) {
	if w.cliXRP == nil {
		return
	}
	tx = w.cliXRP.SendNative(prvKey, to, drops, fees)
	return
}

//---------------------------------------------------------------------//

func (w *Wallet) NewAccountEOS(wif, owner_account, owner_pubKey,
	cpuStakeStr, netStakeStr string, buyRAMBytes int32) (tx string) {
	acc := w.EOS.GetAccount("")
	if acc == nil {
		return
	}
	tx = acc.SystemNewAccount(w.Net, wif, owner_account, owner_pubKey, cpuStakeStr, netStakeStr, buyRAMBytes)
	return
}

func (w *Wallet) NewAccountEOSMulSig(wif, from, to, cpuStakeStr, netStakeStr string,
	buyRAMBytes int32, ownerStr, activeStr string) (tx string) {
	tx = eos.SystemNewAccountMulSig(w.Net, wif, from, to, cpuStakeStr, netStakeStr, buyRAMBytes, ownerStr, activeStr)
	return
}

func (w *Wallet) PushActionEOS(wif, actor_name, contract_name, action_name, data string) (tx string) {
	tx = eos.PushAction(w.Net, wif, actor_name, contract_name, action_name, data)
	return
}

func (w *Wallet) NewAction(actor_name, contract_name, action_name, payload string) string {
	return eos.NewAction(w.Net, actor_name, contract_name, action_name, payload)
}

func (w *Wallet) NewTransactionEOS(act string, expire int64) string {
	return eos.NewTransaction(w.Net, act, expire)
}

func (w *Wallet) SignedTxEOS(wif, tx string) string {
	return eos.SignedTx(w.Net, wif, tx)
}

func (w *Wallet) AddSignedTxEOS(wif, signedTx string) string {
	return eos.AddSignedTx(w.Net, wif, signedTx)
}

func (w *Wallet) PackedTxEOS(signedTx string) string {
	return eos.PackedTx(signedTx)
}

func (w *Wallet) PushedTx(packedTx string) string {
	return eos.PushedTx(w.Net, packedTx)
}

//---------------------------------------------------------------------//

func (w *Wallet) Faucet(coinType, addr string) (result string) {
	if w.Net != SIM_NET {
		return
	}

	switch coinType {
	case COIN_BTC:
		{
			if w.cliBTC == nil {
				return
			}
			result = w.cliBTC.SendToAddress(addr, float64(10))
		}
	case COIN_LTC:
		{
			if w.cliLTC == nil {
				return
			}
			result = w.cliLTC.SendToAddress(addr, float64(10))
		}
	case COIN_BCH:
		{
			if w.cliBCH == nil {
				return
			}
			result = w.cliBCH.SendToAddress(addr, float64(10))
		}
	case COIN_BSV:
		{
			if w.cliBSV == nil {
				return
			}
			result = w.cliBSV.SendToAddress(addr, float64(10))
		}
	case COIN_ETH:
		{
			result = w.SendETH("47de15108b35169c4aff4826d5c413fe117e361a900325f6d3df1f0e04cbd706",
				addr, 10, "")
		}
	case COIN_XLM:
		{
			result = xlm.FriendBot(addr)
		}
	case COIN_EOS:
		{
			url := "http://faucet.cryptokylin.io/get_token?" + addr
			res, err := http.Get(url)
			if err != nil {
				fmt.Println(err)
				return
			}
			defer res.Body.Close()

			body, err := io.ReadAll(res.Body)
			if err != nil {
				fmt.Println(err)
				return
			}

			result = string(body)
		}
	}
	return
}
