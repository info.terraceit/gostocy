package gostocy

import (
	"encoding/hex"
	"encoding/json"
	"fmt"
	"strconv"
	"testing"

	"gitlab.com/info.terraceit/gostocy/pkg/cryptocy"
	"gitlab.com/info.terraceit/gostocy/pkg/eth"
	"gitlab.com/info.terraceit/gostocy/pkg/trx"
)

func TestCreateSDK(t *testing.T) {
	pass := ""
	CreateSDK(pass)
	fmt.Println("..... mnemoric : ", Mnemonic)
	fmt.Println("..... seed :", hex.EncodeToString(Seed))
	fmt.Println("..... master Priv : ", GenMasterPrivKey(TEST_NET, Seed))
	fmt.Println("..... master Pub : ", GenMasterPubKey(TEST_NET, Seed))

	for i := 0; i < 10; i++ {
		GenSeed(Mnemonic, pass)
		fmt.Println("..... mnemoric : ", Mnemonic)
		fmt.Println("..... seed :", hex.EncodeToString(Seed))
		fmt.Println("..... master Priv : ", GenMasterPrivKey(TEST_NET, Seed))
		fmt.Println("..... master Pub : ", GenMasterPubKey(TEST_NET, Seed))
	}
}

func TestGenSeedFromMnemonic(t *testing.T) {
	// ..... mnemoric :  view rebuild metal someone isolate remind burger resemble core multiply twelve clarify
	// ..... seed : ec16df7cb8d8d0fa26fc031ef06057e3f4a68f3d337fd68f5b624a57e84f186020e6ba10ed318deb4d32e8552cb7307691599f72484b09f277eda555ef77e7eb
	// ..... master Priv :  L5YC55ooDE7CxE8M9nJ2w8VMy63rDXjw5A1ySBXPw7VXTq4rBMJU
	// ..... master Pub :  03f5e1da6d5276c5755963afea0119f5ecee6040c423fae93b40299bd5111ed6b4
	pass := "123456"
	mnemonic := "view rebuild metal someone isolate remind burger resemble core multiply twelve clarify"
	GenSeed(mnemonic, pass)
	fmt.Println("..... mnemoric : ", Mnemonic)
	fmt.Println("..... seed :", hex.EncodeToString(Seed))
	fmt.Println("..... master Priv : ", GenMasterPrivKey(TEST_NET, Seed))
	fmt.Println("..... master Pub : ", GenMasterPubKey(TEST_NET, Seed))
}

func TestMultiple(t *testing.T) {
	CreateSDK("12345")
	fmt.Println("..... mnemoric : ", Mnemonic)
	fmt.Println("..... seed :", hex.EncodeToString(Seed))
	fmt.Println("..... master Priv : ", GenMasterPrivKey(TEST_NET, Seed))
	fmt.Println("..... master Pub : ", GenMasterPubKey(TEST_NET, Seed))
	for i := 0; i < 1; i++ {
		w, _ := AddWallet(SIM_NET, "wallet"+strconv.Itoa(i))
		fmt.Println("......Addcoin...BTC.....", w.AddCoin(COIN_BTC, strconv.Itoa(i)))
		fmt.Println("......Addcoin...LTC.....", w.AddCoin(COIN_LTC, strconv.Itoa(i)))
		fmt.Println("......Addcoin...ETH.....", w.AddCoin(COIN_ETH, strconv.Itoa(i)))
		fmt.Println("......Addcoin...XLM.....", w.AddCoin(COIN_XLM, strconv.Itoa(i)))
		//b, _ := json.MarshalIndent(w, "", " ")
		//fmt.Println("...... wallet : ", string(b))
		//
		//		cxlm := w.GetAccountXLM(0)
		//		w.Faucet(COIN_XLM, cxlm.GetAddress())
		//		bxlm := w.GetBalance(COIN_XLM, cxlm.GetAddress())
		//		fmt.Println(".......", cxlm.GetAddress(), ":", bxlm)
		//		txlm := w.Send_XLM(cxlm.GetPrivKey(), "GAYMTHMZCJYQCFLVRYZ4DMI4Q5B6VFFBJFQVGE2WJHIOPDC4UAG4TD3K", 3)
		//		fmt.Println("..........txlm :", txlm)
		//
		//		if w.Connect_ETH("http://localhost:8545") {
		//			ceth := w.GetAccountETH(0)
		//			teth := w.Send_ETH("47de15108b35169c4aff4826d5c413fe117e361a900325f6d3df1f0e04cbd706", ceth.GetAddress(), float64(1000), "")
		//			fmt.Println("..........teth :", teth)
		//		}
		if w.ConnectBTC("localhost:18443", "123", "123") {
			//cbtc := w.GetAccountBTC(0)
			blockcount := w.GetBlockCount(COIN_BTC)
			w.SyncBlock(COIN_BTC, 100, blockcount)
		}
	}
	//	encrypted := Backup()
	//	fmt.Println(".....encrypted : ", encrypted)
	//	b := Restore(Seed, encrypted)
	//	fmt.Println("..... walletManager : ", b)
}

func TestGenEth(t *testing.T) {
	// GenMnemonic()
	// fmt.Println("..... mnemonic : ", Mnemonic)
	mnemonic := "tag volcano eight thank tide danger coast health above argue embrace heavy"
	GenSeed(mnemonic, "")
	fmt.Println("..... seed :", hex.EncodeToString(Seed))
	fmt.Println("..... master Priv : ", GenMasterPrivKey(TEST_NET, Seed))
	fmt.Println("..... master Pub : ", GenMasterPubKey(TEST_NET, Seed))

	for i := 0; i < 2; i++ {
		acc, _ := eth.NewAccount(TEST_NET, strconv.Itoa(i), 60, Seed)
		b, _ := json.Marshal(acc)
		fmt.Println(string(b))
	}
}

func TestPinForKeySet(t *testing.T) {
	/* ------------ 1. ONE TIME -- INSTALLATION ------------ */
	fmt.Println("/* ------------ 1. ONE TIME -- INSTALLATION ------------ */")

	// 1.1 : pneumonic.txt --> Local service / k8s secret / S3
	mnemonic := "tag volcano eight thank tide danger coast health above argue embrace heavy"

	// 1.2 : Root Input Pin
	PIN := "1234567890"

	// 1.3 : Master Seed
	masterSeed, err := GenSeedOnly(mnemonic)
	if err != nil {
		fmt.Println(err)
		return
	}

	// 1.4 : Encrypted Master Seed --> ms.txt --> Local service/ k8s secret / S3
	cryptocy.LOOP_PIN = 10
	encrypted := cryptocy.EncryptMasterKeyPIN(PIN, hex.EncodeToString(masterSeed))
	fmt.Println("...... encrypted :", string(encrypted))
	// ...... encrypted : 2f9c30d4bc888e3c458d62f3e30eb2480eca97576b43f5cafef2f1cff6dd7e5e834a3b207e1c7545548b051d51b8d2cbb2e27f21c92bfee51d7b8eeb

	// GO-4 : Pure after use
	mnemonic = ""
	PIN = ""
	for i := 0; i < len(masterSeed); i++ {
		masterSeed[i] = 0
	}

	/* ------------ 2. EVERYTIME -- BOOT UP ------------ */
	fmt.Println("/* ------------ 2. EVERYTIME -- BOOT UP ------------ */")

	// 2.0 : Root Input Pin
	PIN = "1234567890"

	// 2.1 : Descrypt
	decrypted, err := cryptocy.DecryptMasterKeyPIN(PIN, string(encrypted))
	if err != nil {
		fmt.Println("error DecryptMasterKeyPIN :", err)
		return
	}
	fmt.Println("...... decrypted :", hex.EncodeToString(decrypted))
	// ...... decrypted : 35d33aa807dce5a9febb45a3aa71143408a1fe665d38969477d6ee97e6524a7b

	// 2.2 : Retrieve Master Seed
	CreateSdkFromKey(hex.EncodeToString(decrypted))
	fmt.Println("..... mnemonic : ", Mnemonic)
	fmt.Println("..... seed :", hex.EncodeToString(Seed))
	fmt.Println("..... master Priv : ", GenMasterPrivKey(TEST_NET, Seed))
	fmt.Println("..... master Pub : ", GenMasterPubKey(TEST_NET, Seed))
	// ..... mnemonic :  cupboard oil present autumn soft stay wife hamster photo fatal eagle gym meat you offer poem notable photo volcano tag lazy circle clarify horse
	// ..... seed : 198293d36262ad9d86127726ff8458faa9de0d7fdd7bb82f122f5177ff2b00611d8d642abffccb5f2520db1c72c752f75ec8d155ac87f0e5d4c540a1c2eff57f
	// ..... master Priv :  cUymZjMDEbVYXoiXJyj8n9Q9n3H1X52ToWZF1JSSozdWRdeyMSxT
	// ..... master Pub :  03a4139d7ec91c6b8a8d63afd6e49e355f11a5034479132313ff6eb5a13e124e29

	// 2.3 : BitwiseXOR(Master Seed, Mask)
	mask := GenSeedRandom64()
	seedMasked := cryptocy.XOR(Seed, mask)
	fmt.Println("..... seed masked :", hex.EncodeToString(seedMasked))
	// ..... seed masked : c28e9b824c020ef87d7211d76acc61a6e72549cabb53f0a603c8762bd0088cd279fcbee2b381eea6f846395d417c418b44991d321dae5a8937ce72a5c39683b1

	// 2.4 : Memguard_Storage(Masked Master Seed)
	err = cryptocy.MemguardStorage(seedMasked)
	if err != nil {
		fmt.Println(err)
		return
	}

	// GO-4 : Pure after use
	RemoveAll()

	/* ------------ 3. GENERATE MERCHANT-USER ACCOUNT ON THE FLY ------------ */
	fmt.Println("/* ------------ 3. GENERATE MERCHANT-USER ACCOUNT ON THE FLY ------------ */")

	// 3.1 : Memguard_Retrieve(Masked Master Seed)
	seedRetrieve, err := cryptocy.MemguardRetrieve()
	if err != nil {
		fmt.Println(err)
		return
	}
	defer seedRetrieve.Destroy()

	// 3.2 : BitwiseXOR(Master Seed, Mask)
	seedMaster := cryptocy.XOR(*seedRetrieve, mask)
	fmt.Println("..... seedMaster :", hex.EncodeToString(seedMaster))
	// ..... seedMaster : 198293d36262ad9d86127726ff8458faa9de0d7fdd7bb82f122f5177ff2b00611d8d642abffccb5f2520db1c72c752f75ec8d155ac87f0e5d4c540a1c2eff57f

	// 3.3 : User Seed
	// Implement on fly

	// 3.4 : Derivation Path
	index := "10" //uint32(10)

	keySet, err := AddWalletSeed(TEST_NET, "wallet", seedMaster)
	if err != nil {
		fmt.Println(err)
		return
	}
	addrBTC := keySet.AddCoin(COIN_BTC, index)
	privBTC := keySet.GetPrivKey(COIN_BTC, index)

	addrBCH := keySet.AddCoin(COIN_BCH, index)
	privBCH := keySet.GetPrivKey(COIN_BCH, index)

	addrBSV := keySet.AddCoin(COIN_BSV, index)
	privBSV := keySet.GetPrivKey(COIN_BSV, index)

	addrLTC := keySet.AddCoin(COIN_LTC, index)
	privLTC := keySet.GetPrivKey(COIN_LTC, index)

	addrETH := keySet.AddCoin(COIN_ETH, index)
	privETH := keySet.GetPrivKey(COIN_ETH, index)

	addrXLM := keySet.AddCoin(COIN_XLM, index)
	privXLM := keySet.GetPrivKey(COIN_XLM, index)

	addrXRP := keySet.AddCoin(COIN_XRP, index)
	privXRP := keySet.GetPrivKey(COIN_XRP, index)

	keySet.AddCoin(COIN_EOS, index)
	privEOS := keySet.GetPrivKey(COIN_EOS, index)
	pubEOS := keySet.GetPubKey(COIN_EOS, index)
	addrEOS := keySet.GetPublicAddress(COIN_EOS, index)

	addrTRX := keySet.AddCoin(COIN_TRX, index)
	privTRX := keySet.GetPrivKey(COIN_TRX, index)

	addrAVAX := keySet.AddCoin(COIN_AVAX, index)
	privAVAX := keySet.GetPrivKey(COIN_AVAX, index)

	fmt.Println("......Addcoin...BTC.....", addrBTC, privBTC)
	fmt.Println("......Addcoin...BCH.....", addrBCH, privBCH)
	fmt.Println("......Addcoin...BSV.....", addrBSV, privBSV)
	fmt.Println("......Addcoin...LTC.....", addrLTC, privLTC)
	fmt.Println("......Addcoin...ETH.....", addrETH, privETH)
	fmt.Println("......Addcoin...EOS.....", addrEOS, pubEOS, privEOS)
	fmt.Println("......Addcoin...XLM.....", addrXLM, privXLM)
	fmt.Println("......Addcoin...XRP.....", addrXRP, privXRP)
	fmt.Println("......Addcoin...TRX.....", addrTRX, privTRX)
	fmt.Println("......Addcoin...AVAX.....", addrAVAX, privAVAX)

	// ......Addcoin...BTC..... mjEHk8k9tgojmZh5hXnoP1PQh4GEkeETLr cN5Nqz44sJibvPfWnBb5u2DiRQAYJEq9LWJENQPvhfUdpoiBCfGM
	// ......Addcoin...BCH..... bchtest:qq04r90d56avz8wcws39cxj6mgfv5r8s0cw0dhy833 cQnT7M3reht5WqAbeFXC2zFap6KxHkudV7Mbaub8ZaE7RywK3MFW
	// ......Addcoin...BSV..... bchtest:qzqlnum9n7ququ0nyh7qqwk3tfqprdceec7lufcpl5 cNVkaStCZSACczM2QVCfAtJo4rtruon2bByxkTct8A1E6BEejfFD
	// ......Addcoin...LTC..... mpeiQqedPNTeaG4KtYhsLK3xeJPpyryYC2 cSQnNwRKXbFatcmX7Ef5BGCpGCvTtS1k11RsLgqMcEHBhyPTjaXL
	// ......Addcoin...ETH..... 0x1B74d41eB957DA62eC835BEEDd8C63216C32B49D 8668462d35011b5305b73beb3b0921e8890213e56ff3bd42fd8dea6d78beea87
	// ......Addcoin...EOS.....  EOS6eRBJH713f1n4owAXbkmF9d78htLrSMEYbz28yZs43uQEcMwLP 5Htrt3G2hkNJyc1gmWB7qNi4xub4H9rJpLKD2J7RD1f5buyVhv1
	// ......Addcoin...XLM..... GBP6I6ORZ22FQRCLTQPP5N6UHWEYDXRNMKQAH4NWWQCJM5BHSLXDOO7G SCC3Z27WYRWY745UQV2LDTI7O4TJBW32WOI6D5OBIGZRPOK5ZUAEYITA
	// ......Addcoin...XRP..... rEPELqsRRiDkwGhdC51yjQ2b9VDcDL4Wiz ss5WQY7Y8Qe4eFtyDvBuThcUKs6pD
	// ......Addcoin...TRX..... TSDg5Lwkswm5vKz48ewQuDfBJq8meKebgj 14c551009172ecfb44c087d7b67550bd98932e7d585e74018b9dc83ced3e218e
	// ......Addcoin...AVAX..... 0xfAE625E5419d7Ee15F8424c7E82Db9f591026667 1a20ce9756d3afe083a91eb0ce7feaad8ee7b2a5b1026b89b8b5cbba8c1199ac

	// GO-4 : Pure after use
	PIN = ""
	for i := 0; i < len(Seed); i++ {
		Seed[i] = 0
	}
	for i := 0; i < len(seedMaster); i++ {
		seedMaster[i] = 0
	}
	for i := 0; i < len(seedMasked); i++ {
		seedMasked[i] = 0
	}
}

func TestPinForSign(t *testing.T) {
	/* ------------ 1. ONE TIME -- INSTALLATION ------------ */
	fmt.Println("/* ------------ 1. ONE TIME -- INSTALLATION ------------ */")

	// 1.1 : pneumonic.txt --> Local service / k8s secret / S3
	mnemonic := "eye devote kind axis real nurse inflict chimney lock cube during trouble"

	//1.2 : Root Input Pin
	PIN := "1234567890"

	// 1.3 : Master Seed
	masterSeed := GenSeed(mnemonic, "")

	// 1.4 : Derivation Path
	index := "0" //uint32(0)

	// 1.5 : Keyset
	admin, err := trx.NewAccount(TEST_NET, index, masterSeed)
	if err != nil {
		fmt.Println(err)
		return
	}
	keySet, err := hex.DecodeString(admin.GetPrivKey())
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("...... keySet :", hex.EncodeToString(keySet))

	// 1.6 : Encrypted Master Seed --> ak.txt --> Local service/ k8s secret / S3
	cryptocy.LOOP_PIN = 10
	encrypted := cryptocy.EncryptMasterKeySIGN(PIN, string(keySet))
	fmt.Println("...... encrypted :", string(encrypted))
	// ...... encrypted : 5ce46c23feafaeac40d2e48f4f776c29b911aa54c3809cab1e7502a9987ecafe6f97dcd387da9a9d8df587c3615ec2a4b5adebed784835e3bb9578369cd524627ffa8f71c903fb3cb95dc5718216633cc15c488043974b733594e46c

	// GO-4 : Pure after use
	mnemonic = ""
	PIN = ""
	for i := 0; i < len(masterSeed); i++ {
		masterSeed[i] = 0
	}
	admin = nil

	/* ------------ 2. EVERYTIME -- BOOT UP ------------ */
	fmt.Println("/* ------------ 2. EVERYTIME -- BOOT UP ------------ */")

	// 2.0 : Root Input Pin
	PIN = "1234567890"

	// 2.1 : Descrypt
	keySet, err = cryptocy.DecryptMasterKeyPIN(PIN, string(encrypted))
	if err != nil {
		fmt.Println("error DecryptMasterKeyPIN :", err)
		return
	}
	fmt.Println("...... decrypted :", hex.EncodeToString(keySet))
	// ...... decrypted : b03c0c3e290b1eb56ad6d6b0f1f457447752d324c00120d34710f9ba33a644f8

	// 2.3 : BitwiseXOR(keySet, Mask)
	mask := GenSeedRandom32()
	keySetMasked := cryptocy.XOR(keySet, mask)
	fmt.Println("..... keySet masked :", hex.EncodeToString(keySetMasked))
	// ..... keySet masked : 4145fda558a57ff1cf0f0dd6ceebc770ba32b91fbd59d4ed5a717af0d8944d7b

	// 2.4 : Memguard_Storage(Masked keySet)
	err = cryptocy.MemguardStorage(keySetMasked)
	if err != nil {
		fmt.Println(err)
		return
	}

	/* ------------ 3. FOR SIGNING TRANSACTIONS ------------ */
	fmt.Println("/* ------------ 3. FOR SIGNING TRANSACTIONS ------------ */")

	// 3.1 : Memguard_Retrieve(Masked keySet)
	keySetRetrieve, err := cryptocy.MemguardRetrieve()
	if err != nil {
		fmt.Println(err)
		return
	}
	defer keySetRetrieve.Destroy()

	// 3.2 : BitwiseXOR(Master Seed, Mask)
	keySet = cryptocy.XOR(*keySetRetrieve, mask)
	fmt.Println("..... keySet :", hex.EncodeToString(keySet))
	// ..... keySet : 2f55ab13df1a505b92436fba2046b770257ed760bb7d0508d5e3153a63af16c1

	// 3.3 : Sign Message
	message := []byte("message to sign")
	signature, err := trx.Sign(hex.EncodeToString(keySet), message)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("...... signature :", signature)

	// 3.4 : Verify Signature : TRUE
	kp, _ := trx.NewKeyPairFromPrivKey(hex.EncodeToString(keySet))
	pubKey, _ := hex.DecodeString(kp.PubKey)
	verified := trx.VerifySignature(pubKey, message, signature)
	fmt.Println(".....verified......", verified)

	// 3.5 : Verify Signature : FALSE
	kp, _ = trx.NewKeyPairFromPrivKey("2f690e598db64b54bb433a52d9769e01ea98fd24c6bacbf685279200a7c908aa")
	pubKey, _ = hex.DecodeString(kp.PubKey)
	verified = trx.VerifySignature(pubKey, message, signature)
	fmt.Println(".....verified......", verified)

	// 3.6 : Verify Signature : TRUE  : ETH
	kpETH, _ := eth.NewKeyPairFromPrivKey(hex.EncodeToString(keySet))
	pubKeyETH, _ := hex.DecodeString(kpETH.PubKey)
	verified = eth.VerifySignature(pubKeyETH, message, signature)
	fmt.Println(".....verified......", verified)

	// GO-4 : Pure after use
	PIN = ""
	for i := 0; i < len(keySet); i++ {
		keySet[i] = 0
	}
	for i := 0; i < len(keySetMasked); i++ {
		keySetMasked[i] = 0
	}
}
